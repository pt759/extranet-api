truncate table app_reference;

insert into app_reference (id, code, value, domain, description)
values (0,'COVER_GRACE_DAYS','30','ACCESS_CONTROL','Days before and after the policy is valid');

--Claims online access
insert into app_reference (id, code, value, domain, description)
values (1,'cc7a89ef-1b3e-453c-9bc3-4001456ed29d','cpbhodin','CLAIMS_ONLINE_GROUP','CPB Hodings');

insert into app_reference (id, code, value, domain, description)
values (2,'025d648e-3e87-4a33-ac42-10bbcc559c30','fred1024','CLAIMS_ONLINE_GROUP','Fred. Olsen Insurance Services');

insert into app_reference (id, code, value, domain, description)
values (3,'ee5bd6d2-1227-4d84-928b-262f0a94012a','knutsenoa','CLAIMS_ONLINE_GROUP','Knutsen OAS Shipping AS');

insert into app_reference (id, code, value, domain, description)
values (4,'d2ff2aab-c21c-4ba2-b649-f88ad1fd4126','tormdamp','CLAIMS_ONLINE_GROUP','Torm A/S');


insert into app_reference (id, code, value, domain, description)
values (5,'0ff3e57d-2a5c-4234-b0ef-5d1a6917c9f2','zorabins','QUOTE_ONLINE_GROUP','Zorab Insurance Services Ltd');

insert into app_reference (id, code, value, domain, description)
values (6,'cc7a89ef-1b3e-453c-9bc3-4001456ed29d','cpbhodin','QUOTE_ONLINE_GROUP','CPB Hodings');

insert into app_reference (id, code, value, domain, description)
values (7,'7fda0dde-c420-4b58-98dc-68626d49ba73','skuldlon','QUOTE_ONLINE_GROUP','Skuld London P&I');

insert into app_reference (id, code, value, domain, description)
values (8,'e3726689-5c8e-41ca-8e5d-73b8ed557a91','fastnetm','QUOTE_ONLINE_GROUP','Fastnet Marine Insurance Services International');

