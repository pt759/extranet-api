create table app_reference (
  id          number        not null,
  code        varchar2(200)        not null,
  value       varchar2(2000)  null,
  domain      varchar2(200) null,
  description varchar2(200)
);

CREATE SEQUENCE EXTRANET.HIBERNATE_SEQUENCE
    START WITH 1000
    MAXVALUE 9999999999999999999999999999
    MINVALUE 1
    NOCYCLE
    CACHE 20
    NOORDER;
