create or replace package app_reference_helper is
    function get_value(v_code varchar2, v_domain varchar2) return varchar2;
end app_reference_helper;
/

create or replace package body app_reference_helper is

    function get_value(v_code varchar2, v_domain varchar2) return varchar2 is
        l_value varchar2(1000);
    begin
        select value
        into l_value
        from extranet.app_reference
        where code = v_code
          and domain =  v_domain;

        return l_value;
    end get_value;

end app_reference_helper;