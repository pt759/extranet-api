create or replace package financial as
  procedure get_deposit_account_status2(p_account_no    in     number, p_invoiced_amt in out number,
                                        p_paid_amt      in out number, p_used_amt in out number,
                                        p_currency_code in out varchar2);
end;