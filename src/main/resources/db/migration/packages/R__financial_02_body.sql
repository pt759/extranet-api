create or replace package body financial as

  procedure get_deposit_account_status2(p_account_no    in     number, p_invoiced_amt in out number, p_paid_amt in out number, p_used_amt in out number,
                                        p_currency_code in out varchar2) is begin --sk_uw.get_deposit_account_status2(p_account_no,p_paid_amt ,p_used_amt,p_currency_code);

    sk_uw.get_deposit_account_status2(p_account_no, p_invoiced_amt, p_paid_amt,p_used_amt,p_currency_code);

  end;

end;
/
