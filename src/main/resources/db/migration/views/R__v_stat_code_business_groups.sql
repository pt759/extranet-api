create or replace view v_stat_code_business_groups as
  select pty.name dept_name, collation_dept_no dept_no, pgg.login login_group
  from tia.sk_party_group_group pgg,
       tia.sk_party_v pty
  where pgg.id in
        (select mg.pgg_id
         from tia.sk_multi_group mg,
              tia.sk_party_group pg
         where pg.stat_code = mg.stat_code)
    and pgg.member_broker = 'S'
    and pgg.stop_date is null
    and pgg.collation_dept_no is not null
    and pgg.owner_id = pty.id_no (+)