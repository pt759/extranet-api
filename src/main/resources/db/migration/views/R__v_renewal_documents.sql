create or replace view v_renewal_documents as
  select
    pp.DESCRIPTION description,
    pr.request_id,
    pr.request_date,
    pr.request_pgm                 program_id,
    renewal.NEW_POLICY_line_no,
    pol.c11                        stat_Code,
    pli.cover_start_date,
    plc.class,
    'R' type
  from SK_RENEWAL_ENTRY renewal inner join print_Request pr on pr.AGR_LINE_SEQ_NO = renewal.POLICY_LINE_SEQ_NO
    inner join print_program pp on pp.PROGRAM_ID = pr.request_pgm
    inner join policy pol on pol.POLICY_NO = pr.POLICY_NO and pol.suc_seq_no is null
    inner join policy_line pli on pli.AGR_LINE_no = renewal.new_policy_line_no and pli.suc_seq_no is null
    inner join archive a on a.document_key = pr.request_id
    inner join sk_product_line_codes plc on plc.product_line_id = pli.product_line_id

  where pr.request_pgm in ('RS005', 'RS006', 'RS007', 'RS013', 'RS450', 'RS454', 'RS459', 'RS460', 'RS461', 'RS480', 'RS493', 'RS494')
        and args like 'RENEWAL%'
        and plc.cover_entry = 'E'
			 and plc.renewal_class not in ('USV',
													 'BOW',
													 'LOH',
													 'CER')
			 and plc.renew = 'Y'
  and nvl(dbms_lob.getlength(a.document), 0) > 13000
;


