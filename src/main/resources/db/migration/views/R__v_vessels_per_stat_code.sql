  CREATE OR REPLACE FORCE VIEW V_VESSELS_PER_STAT_CODE AS
  select distinct pol.c11 as Stat_code,
						 plie.agr_line_no,
						 plie.product_line_id,
						 pen.prod_id,
						 pol.policy_no,
						 plie.short_desc,
		         plie.cover_start_date,
	plc.class
	  from policy_line plie,
			 policy pol,
			 policy_entity pen,
			 sk_party_group pg,
			 xla_reference xla,
			 policy_line plic,
			 object cov,
			 object ent,
			 sk_product_line_codes plc
	 where
			pen.policy_no = pol.policy_no
			 and pol.cancel_code = 0
			 and pol.policy_status = 'P'
			 and pol.suc_seq_no is null
			 and pol.expiry_code = xla.code
			 and xla.table_name = 'POLICY_EXPIRY_CODE'
			 and xla.language = 'SK'
			 and pol.policy_no = plie.policy_no
			 and plie.suc_seq_no is null
			 and plc.product_line_id = plie.product_line_id
			 and plc.cover_entry = 'E'
			 and plc.renewal_class not in ('USV',
													 'BOW',
													 'LOH',
													 'CER')				-- TIA-1893 added CER
			 and plc.renew = 'Y'
			 and plie.cancel_code = 0
			 and plie.cover_end_date >= sysdate
			 and plie.agr_line_no = ent.agr_line_no
			 and plie.cover_start_date between ent.cover_start_date
													 and nvl (ent.cover_end_date,
																 to_date ('3000', 'yyyy'))
			 and ent.n01 = cov.seq_no
			 and plic.agr_line_no = cov.agr_line_no
			 and plic.cover_start_date between cov.cover_start_date
													 and nvl (cov.cover_end_date,
																 to_date ('3000', 'yyyy'))
			 and plic.cover_end_date >= plic.cover_start_date
			 and plic.cancel_code = 0
;

