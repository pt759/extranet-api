create or replace view v_statcode_premium_deposit_acc as
select pgp.stat_code,
       a.name_id name_id,
       a.account_no,
       a.payment_method_in,
       tu.c01    creditcontroller_pt_user_id,
       n.name    policy_holder
from sk_party_group_party pgp
         inner join sk_party_group pg on pg.stat_code = pgp.stat_code and pg.stop_date is null
         inner join acc_account a on a.name_id = pgp.party_id and a.payment_method_in = 'DEPOSIT' and a.account_type = 1
         inner join name n on n.id_no = a.name_id
         left outer JOIN top_user tu ON tu.user_id = a.handler
where (select min(trans_date) from acc_item x where x.account_no = a.account_no) > sysdate - 720;