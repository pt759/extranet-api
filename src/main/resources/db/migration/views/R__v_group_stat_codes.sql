create or replace force view V_GROUP_STAT_CODES
            (STAT_CODE,
             NAME,
             ONLINE_DECLARATION,
             PRODUCTS,
             GROUP_NAME,
             GROUP_MANAGER,
             SERVICE_AGENT_NO)
as
select pg.stat_code,
       pg.name,
       mg.online_declaration          declaration,
       LISTAGG(distinct prod_id, ',') products,
       pgg.login                      group_name,
       tu.user_name,
       pol.service_agent_no
from tia.sk_party_group_group pgg
         inner join tia.sk_multi_group mg on pgg.id = mg.pgg_id
         inner join tia.sk_party_group pg on mg.stat_code = pg.stat_code and pg.stop_date is null
         inner join tia.top_user tu on pg.acc_manager = tu.user_id
         inner join tia.policy pol on pol.c11 = pg.stat_code and pol.policy_status = 'P'
         inner join tia.policy_entity pe on pe.POLICY_NO = pol.POLICY_NO
         inner join tia.policy_line pli on pol.policy_no = pli.policy_no and pli.cancel_code = 0 and
                                           trunc(sysdate) between pli.cover_start_date and pli.cover_end_date
group by pg.stat_code,
         pg.name,
         mg.online_declaration,
         pgg.login,
         tu.user_name,
         pol.service_agent_no
order by pg.name;