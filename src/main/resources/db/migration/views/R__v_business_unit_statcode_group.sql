create or replace force view extranet.v_business_unit_statcode_group as
select distinct login,
                pty.name group_name,
                D.DEPT_NO,
                tu.user_name
from sk_party_group_group pgg
         inner join sk_multi_group mg on mg.PGG_ID = pgg.id and pgg.member_broker = 'M' and pgg.stop_date is null and
                                         pgg.login is not null
         inner join sk_party_group pg on pg.STAT_CODE = mg.STAT_CODE and pg.STOP_DATE is null
         inner join department d on d.DEPT_NO = pg.DEPT_NO
         inner join sk_party_v pty on pgg.OWNER_ID = pty.ID_NO
         inner join top_user tu on tu.USER_ID = pgg.GROUP_MANAGER
order by pty.name;
