create or replace view v_correspondent_numbers as
 select nt.name_id_no, xla.description con_type, nt.phone_no
     from tia.name_telephone nt
       inner join tia.xla_reference xla on xla.code = nt.TELEPHONE_TYPE
        inner join v_correspondent_parties cp on cp.party_id = nt.name_id_no
    where xla.table_name = 'TELEPHONE_TYPE'
       and xla.code = nt.telephone_type
      and xla.language = 'SK'
      and nvl(nt.phone_no,'$') <> '$'
