create or replace view v_business_units as
select distinct d.dept_name, d.dept_no
from sk_party_group pg
         inner join sk_multi_group mg on pg.STAT_CODE = mg.STAT_CODE
         inner join sk_party_group_group pgg on pgg.id = mg.PGG_ID and pgg.STOP_DATE is null
         inner join department d on pg.DEPT_NO = d.DEPT_NO and pg.STOP_DATE is null
order by d.dept_name;


