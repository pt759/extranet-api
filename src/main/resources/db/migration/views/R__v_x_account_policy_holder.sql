create or replace view v_account_policy_holder as
  select x.policy_holder, x.account_no
  from (select POLICY_HOLDER, account_no from v_statcode_premium_accounts
        union select POLICY_HOLDER, account_no from v_statcode_claim_accounts
        union select POLICY_HOLDER, account_no from V_STATCODE_PREMIUM_DEPOSIT_ACC) x