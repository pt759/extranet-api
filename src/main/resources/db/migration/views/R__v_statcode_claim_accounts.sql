create or replace force view extranet.v_statcode_claim_accounts as
  select distinct pgp.stat_code,
                  acc.account_no,
                  acc.payment_method_in,
                  acc.account_type,
                  tu.c01 creditcontroller_pt_user_id,
                  acc.name_id,
                  n.name policy_holder
  from SK_PARTY_GROUP_PARTY pgp
         inner join acc_account acc on acc.name_id = pgp.party_id and acc.account_type in (3, 6)
         inner join acc_item ai on ai.account_no = acc.account_no
         inner join cla_case cc on cc.cla_Case_no = ai.claim_case_no and cc.status in ('OP', 'RO', 'CL')
         inner join cla_event ce on ce.cla_event_no = cc.cla_event_no
         left outer join top_user tu on tu.user_id = acc.handler
         inner join policy_line pli on pli.agr_line_no = cc.policy_line_no and pli.suc_seq_no is null
         inner join policy pol on pol.policy_no = pli.policy_no and pol.SUC_SEQ_NO is null
         inner join name n on n.id_no = acc.name_id;