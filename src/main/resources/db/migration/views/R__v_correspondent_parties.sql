create or replace view v_correspondent_parties as
  select distinct party_id
  from (select party_id
        from sk_correspondents_v
        union all select contact_id
                  from sk_correspondent_contacts_v
        union all select party_id
                  from sk_office_v);