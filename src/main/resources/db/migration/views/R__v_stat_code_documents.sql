create or replace force view V_STAT_CODE_DOCUMENTS
as
  select
    pp.description description,
    pr.request_id,
    pr.request_date,
    pr.request_pgm program_id,
    pr.agr_line_no,
    vs.stat_code,
    vs.cover_start_date,
    vs.class,
    'P' type
	  from tia.print_request pr,
			 tia.case_item ci,
			 archive a,
			 tia.print_program pp,
			 v_vessels_per_stat_code vs
	 where	  pr.request_id = a.document_key
			 and pr.request_emode = 'XP'
			 and pr.request_pgm = pp.program_id
			 and pr.request_pgm in ('CEO' || substr(sk_uw.get_policy_year(pr.policy_no),3,4),
                                    'CIO' || substr(sk_uw.get_policy_year(pr.policy_no),3,4),
                                    'RS005',
                                    'RS006',
                                    'RS007',
                                    'RS013',
                                    'RS042',
                                    'RS416',
                                    'RS418',
                                    'RS427',
                                    'RS429',
                                    'RS434',
                                    'RS438',
                                    'RS447',
                                    'RS450',
                                    'RS454',
                                    'RS459',
                                    'RS460',
                                    'RS461',
                                    'RS466',
                                    'RS468',
                                    'RS476',
                                    'RS477',
                                    'RS478',
                                    'RS480',
                                    'RS493',
                                    'RS494',
                                    'RS521',
                                    'RS522',
                                    'CE_CN',
                                    'CI_CN',
                                    'CEP18')
			 and pr.request_id = ci.request_id
             and nvl(ci.c02, 'X') <> 'Y'
			 and a.document is not null
			 and pr.agr_line_no = vs.agr_line_no
			 and a.document is not null
			 and pr.request_id =
               (select max (pr2.request_id)
                  from print_request pr2
                 where     pr2.request_pgm = pr.request_pgm
                       and pr2.policy_no = pr.policy_no
                       and pr2.request_emode = 'XP'
                       and pr2.agr_line_no = pr.agr_line_no)
       and nvl(dbms_lob.getlength( a.document),0)>13000;