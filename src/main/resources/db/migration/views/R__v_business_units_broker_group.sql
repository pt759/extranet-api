create or replace view v_business_unit_broker_group as
  select distinct d.dept_name, d.dept_no
  from sk_party_group pg,
       department d,
       tia.sk_multi_group mg,
       sk_party_group_group pgg
  where pg.dept_no = d.dept_no
    and pg.stat_code = mg.stat_code
    and pg.stop_date is null
    and PG.STAT_CODE = MG.STAT_CODE
    and MG.PGG_ID = PGG.ID
    and PGG.STOP_DATE is null
  order by d.dept_name