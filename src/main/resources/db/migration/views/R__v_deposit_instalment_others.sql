create or replace view v_deposit_instalment_other as
  select a.account_no,
         a.acc_item_no,
         a.item_text,
         a.currency_code,
         a.currency_amt currency_amount,
         a.currency_balance,
         a.due_date     item_due_date,
         a.trans_date
  from acc_item a
  where a.item_class in (5, 6)
    and a.policy_no is null
    and a.source = 809
  order by a.item_text;
