create or replace view v_broker_statcode_groups
as
	  select PGG.LOGIN,
					 pty.name group_name
		from tia.sk_party_group_group pgg,
             tia.sk_party_v pty
		where 	 pgg.member_broker = 'B'
				and pgg.owner_id = pty.id_no(+)
				and PGG.STOP_DATE is null
				and PGG.LOGIN is not null
				and pty.name is not null
				and exists
						 (select '1'
							 from tia.sk_party_group pg, tia.sk_multi_group mg
							where 	 pg.stat_code = mg.stat_code
									and mg.pgg_id = pgg.id
									and PG.STOP_DATE is null
						  union
						  select '1'
							 from dual
							where 'A' = 'A')
	order by pty.name;