create or replace force view extranet.v_statcode_premium_accounts as
  SELECT pg.stat_code,
         a.name_id name_id,
         a.account_no,
         a.payment_method_in,
         tu.c01    creditcontroller_pt_user_id,
         n.name    policy_holder
  FROM sk_party_group pg
         INNER JOIN policy p ON p.c11 = pg.stat_code AND p.suc_seq_no IS NULL and pg.stop_date IS NULL and p.account_no IS NOT NULL and p.policy_status = 'P'
         INNER JOIN acc_item ai ON ai.policy_no = p.policy_no
         INNER JOIN acc_account a ON a.account_no = ai.account_no and a.account_type = 1 and nvl(a.payment_method_in,'X') != 'DEPOSIT'
         INNER JOIN name n ON n.id_no = a.name_id
         left outer JOIN top_user tu ON tu.user_id = a.handler
         INNER JOIN policy_line pli ON pli.policy_no = p.policy_no AND pli.cancel_code = 0 AND pli.suc_seq_no IS NULL
         INNER JOIN policy_entity pen ON pen.policy_no = p.policy_no
  HAVING COUNT(ai.acc_item_no) > 0
  GROUP BY a.account_no,
           a.account_type,
           a.payment_method_in,
           pg.stat_code,
           a.name_id,
           n.name,
           tu.c01;