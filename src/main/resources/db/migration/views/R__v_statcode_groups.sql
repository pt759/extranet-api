 create or replace view v_statcode_groups as
select pgg.id group_id,
       pgg.login name_id,
       pty.name  group_name,
       pty.id_no party_id,
       pgg.member_broker group_type
from tia.sk_party_group_group pgg
         inner join tia.sk_multi_group mg on pgg.id = mg.pgg_id and pgg.stop_date is null and pgg.login is not null
         inner join tia.sk_party_group pg on mg.stat_code = pg.stat_code and pg.stop_date is null
         inner join tia.sk_party_v pty    on pgg.owner_id = pty.id_no and pty.name is not null and pty.stop_date is null
group by pgg.id, pgg.login, pty.name, pty.id_no, pgg.member_broker;
