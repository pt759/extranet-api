create or replace view v_deposit_instalment as
  select
    trim(tia.sk_rs80.find_highest_payment_no(acc_item_no))  invoice_no
    ,tia.sk_rs80.find_invoice_due_date(acc_item_no)         invoice_due_date
    ,tia.sk_rs80.find_invoice_date(acc_item_no)             invoice_date
    ,a.account_no
    ,a.acc_item_no
    ,a.item_id
    ,a.item_Text
    ,a.currency_code
    ,a.amt amount
    ,a.currency_amt currency_amount
    ,a.currency_balance
    ,tia.sk_rs80.find_deposit_paid(a.acc_item_no)           deposit_paid
    ,a.payment_method
    ,a.fully_matched
    ,a.due_date item_due_date
    ,a.trans_date
  from acc_item a
  where a.item_class in (1, 2) 
    and a.policy_no is null 
    and a.payment_method != 'DEPOSIT' 
    and a.source = 807
  order by invoice_no
;
