create or replace view v_acc_account as
   SELECT a.account_no,
          a.account_type,
          a.title,
          a.currency_code,
          a.payment_method_in,
          u.c01 creditcontroller_pt_user_id,
          (select max(x.trans_date) from ACC_ITEM x where x.account_no = a.account_no ) last_trans_date
     FROM ACC_ACCOUNT a 
            left outer JOIN top_user u ON u.user_id = a.handler;