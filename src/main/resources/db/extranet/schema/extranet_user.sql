drop user extranet cascade;

create user extranet identified by password01
    DEFAULT TABLESPACE EXTRANET_data
  TEMPORARY TABLESPACE EXTRANET_temp
  ;

grant connect to extranet ;
grant create view to extranet ;
grant create table to extranet ;
grant create sequence to extranet ;
grant create trigger to extranet ;
grant create procedure to extranet;


ALTER USER extranet QUOTA 100M ON EXTRANET_DATA;


