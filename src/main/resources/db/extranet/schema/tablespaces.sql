-- TIAD
create tablespace EXTRANET_DATA datafile 'D:\ORACLE\ORADATA\TIAD\EXTRANET_DATA.DBF' size 100M autoextend on next 100M maxsize 500M;


create temporary tablespace EXTRANET_TEMP tempfile 'D:\ORACLE\ORADATA\TIAD\EXTRANET_TEMP.DBF' size 10M autoextend on next 50M maxsize 500M tablespace group '' extent management local uniform size 1M;



-- TIAT
create tablespace EXTRANET_DATA datafile 'D:\ORACLE\ORADATA\TIAT\EXTRANET_DATA.DBF' size 100M autoextend on next 100M maxsize 500M;

create temporary tablespace EXTRANET_TEMP tempfile 'D:\ORACLE\ORADATA\TIAT\EXTRANET_TEMP.DBF' size 10M autoextend on next 50M maxsize 500M tablespace group '' extent management local uniform size 1M;

--TIAP
create tablespace EXTRANET_DATA datafile 'D:\ORACLE\ORADATA\TIAP\EXTRANET_DATA.DBF' size 100M autoextend on next 100M maxsize 500M;

create temporary tablespace EXTRANET_TEMP tempfile 'D:\ORACLE\ORADATA\TIAP\EXTRANET_TEMP.DBF' size 10M autoextend on next 50M maxsize 500M tablespace group '' extent management local uniform size 1M;
