-- Run as system administrator / TIA user.

--User access to stat codes
grant select on SK_PARTY_GROUP_GROUP to extranet;
grant select on SK_MULTI_GROUP to extranet;
grant select on SK_PARTY_GROUP to extranet;

--Documents archive
grant select on print_request to extranet;
grant select on archive to extranet;
grant select on object to extranet;
grant select on print_program to extranet;
grant execute on sk_uw to extranet;
grant select on xla_reference to extranet ;
grant select on sk_product_line_codes to extranet;
grant select on print_request to extranet;
grant select on policy_line to extranet;
grant select on policy to extranet;
grant select on policy_entity to extranet;
grant select on product_line to extranet;
grant select on print_argument to extranet;
grant select on case_item to extranet;

-- for correspondence
grant select on SK_CORRESPONDENTS_v to extranet;
grant select on SK_CORRESPONDENT_CONTACTS_V to extranet;
grant select on SK_CORRESPONDENT_NUMBERS_V to extranet;
grant select on SK_CORRESPONDENT_NUMBERS_SORT to extranet;

-- Should be able to delete soon
grant select on top_user to extranet;


-- Renewal ?
grant select on SK_RENEWAL_ENTRY to extranet;

--Userad admin
grant select on department to extranet;
grant select on sk_party_v to extranet;

-- Phase 2
grant select on print_argument to extranet;
grant execute on sk_rs80 to extranet;
grant select on acc_item to extranet;
grant select on acc_account to extranet;
grant select on name to extranet;
grant select on acc_payment_channel to extranet;
grant select on acc_payment to extranet;
grant select on acc_payment_item to extranet;
grant select on acc_bank to extranet;
grant select on acc_item_f6076_view to extranet;
grant select on xla_pe_reference to extranet;
grant execute on skuld to extranet;

grant select on SK_PARTY_GROUP_PARTY to extranet;
grant select on cla_event to extranet;
grant select on cla_case  to extranet;
grant execute on sk_uw to extranet;
-- Correspondent Search
grant select on sk_office_v to extranet;
grant select on name_telephone  to extranet;

