package com.skuld.extranet;


import lombok.extern.log4j.Log4j2;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import static javax.management.timer.Timer.ONE_HOUR;

@Configuration
@EnableCaching
@EnableScheduling
@Log4j2
public class CachingConfig {

    public static final String LDAP_CACHE_USER = "LDAP_USERS";
    public static final String STAT_CODES_FOR_GROUP = "STAT_CODES_FOR_GROUP";
    public static final String APP_REFERENCE = "APP_REFERENCE";
    public static final String STAT_CODE_GROUPS = "STAT_CODE_GROUPS";
    public static final String STAT_CODE_BUSINESS_GROUPS = "STAT_CODE_BUSINESS_GROUPS";

    public static final String ACCOUNTS_FOR_STAT_CODE = "ACCOUNTS_FOR_STAT_CODE";

    public static final String CORRESPONDENT_SEARCH_NUMBERS = "CORRESPONDENT_SEARCH_NUMBERS";
    public static final String CORRESPONDENT_SEARCH_CONTACTS = "CORRESPONDENT_SEARCH_CONTACTS";

    @Bean
    public CacheManager cacheManager() {
        log.info("Caching enabled for : AD users, StatCode Groups, Correspondent Search Numbers & Contacts");
        return new ConcurrentMapCacheManager(LDAP_CACHE_USER,
                STAT_CODES_FOR_GROUP,
                STAT_CODE_GROUPS,
                STAT_CODE_BUSINESS_GROUPS,
                ACCOUNTS_FOR_STAT_CODE,
                CORRESPONDENT_SEARCH_NUMBERS,
                CORRESPONDENT_SEARCH_CONTACTS,
                APP_REFERENCE
        );
    }

    @Scheduled(cron = "${cache.flush.ldap.users}")
    @CacheEvict(allEntries = true, value = {LDAP_CACHE_USER})
    public void reportCacheEvictForLdapUsers() {
        log.info("Flush LdapUsers Cache ");
    }

    @Scheduled(cron = "${cache.flush.group.statcodes}")
    @CacheEvict(allEntries = true, value = {STAT_CODES_FOR_GROUP, STAT_CODE_GROUPS, ACCOUNTS_FOR_STAT_CODE})
    public void reportCacheEvictStatCodes() {
        log.info("Flush statcode group Cache ");
    }


    @Scheduled(fixedRate = ONE_HOUR)
    @CacheEvict(allEntries = true, value = {CORRESPONDENT_SEARCH_NUMBERS, CORRESPONDENT_SEARCH_CONTACTS})
    public void reportCacheEvictCorrespondentNumbers() {
        log.info("Flush Correspondents data");
    }

}