package com.skuld.extranet.memberPerformanceBonus;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.global.RESTendPoints;
import com.skuld.extranet.spa.pages.PageEntityView;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MemberPerformanceBonus {

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    @JsonView(PageEntityView.ClientView.class)
    private BigDecimal amount;

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    @JsonView(PageEntityView.ClientView.class)
    private BigDecimal balance;

    @JsonView(PageEntityView.ClientView.class)
    private int performanceYear;

    @JsonView(PageEntityView.ClientView.class)
    private List<String> statCodeNames;

    @JsonView(PageEntityView.ClientView.class)
    private String downloadUrl;

    private String statCode;
    private String statCodeName;

    public MemberPerformanceBonus() {
    }

    public MemberPerformanceBonus(ResultSet r) throws SQLException {
        this.statCodeName = r.getString("STAT_NAME");
        this.statCode = r.getString("STAT_CODE");
        this.amount = r.getBigDecimal("AMOUNT_USD");
        this.balance = r.getBigDecimal("BALANCE_USD");
        this.performanceYear = r.getInt("PERFORMANCE_BONUS_YEAR");
    }


    @JsonView(PageEntityView.ClientView.class)
    public boolean isPayable() {
        return this.balance != null && this.balance.compareTo(BigDecimal.ZERO) == 0;
    }

    public static class Mapper implements RowMapper<MemberPerformanceBonus> {
        public MemberPerformanceBonus map(ResultSet r, StatementContext ctx) throws SQLException {
            return new MemberPerformanceBonus(r);
        }
    }

    public MemberPerformanceBonus setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public MemberPerformanceBonus setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }


    public BigDecimal getBalance() {
        return balance;
    }


    public int getPerformanceYear() {
        return performanceYear;
    }

    public MemberPerformanceBonus setPerformanceYear(int performanceYear) {
        this.performanceYear = performanceYear;
        return this;
    }

    public String getStatCodeName() {
        return statCodeName;
    }

    public MemberPerformanceBonus setStatCodeName(String statCodeName) {
        this.statCodeName = statCodeName;
        return this;
    }

    public MemberPerformanceBonus setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public List<String> getStatCodeNames() {
        return statCodeNames;
    }

    public MemberPerformanceBonus setStatCodeNames(List<String> statCodeNames) {
        this.statCodeNames = statCodeNames;
        return this;
    }

    public String getDownloadUrl() {
        return isPayable() ? RESTendPoints.urlMemberPerformancePdf(statCode) : "";
    }

    public MemberPerformanceBonus setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
        return this;
    }

    public String getStatCode() {
        return statCode;
    }
}


