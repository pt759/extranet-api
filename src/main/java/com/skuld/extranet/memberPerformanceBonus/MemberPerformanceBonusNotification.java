package com.skuld.extranet.memberPerformanceBonus;

import com.skuld.extranet.core.user.dto.StatCodeDTO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class MemberPerformanceBonusNotification {

    private Long id;
    private String statCodeGroup;
    private String statCodeGroupDescripton;
    private List<StatCodeDTO> statCode;
    private List<String> email;

    public MemberPerformanceBonusNotification(ResultSet rs) {

    }

    public MemberPerformanceBonusNotification() {

    }

    public List<StatCodeDTO> getStatCode() {
        return statCode;
    }

    public MemberPerformanceBonusNotification setStatCode(List<StatCodeDTO> statCode) {
        this.statCode = statCode;
        return this;
    }

    public String getStatCodeGroup() {
        return statCodeGroup;
    }

    public MemberPerformanceBonusNotification setStatCodeGroup(String statCodeGroup) {
        this.statCodeGroup = statCodeGroup;
        return this;
    }

    public String getStatCodeGroupDescripton() {
        return statCodeGroupDescripton;
    }

    public MemberPerformanceBonusNotification setStatCodeGroupDescripton(String statCodeGroupDescripton) {
        this.statCodeGroupDescripton = statCodeGroupDescripton;
        return this;
    }

    public List<String> getEmail() {
        return email;
    }

    public MemberPerformanceBonusNotification setEmail(List<String> email) {
        this.email = email;
        return this;
    }

    public static class Mapper implements RowMapper<MemberPerformanceBonusNotification> {
        @Override
        public MemberPerformanceBonusNotification map(ResultSet rs, StatementContext ctx) throws SQLException {
            return new MemberPerformanceBonusNotification(rs);
        }
    }
}
