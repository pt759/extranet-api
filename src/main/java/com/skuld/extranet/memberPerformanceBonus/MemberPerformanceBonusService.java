package com.skuld.extranet.memberPerformanceBonus;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.memberPerformanceBonus.pdf.MemberPerformancePdf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
public class MemberPerformanceBonusService {

    private final MemberPerfomanceBonusRepository memberPerfomanceBonusRepository;

    @Value("${memberPerformance.visible}")
    private Boolean memberPerformanceVisible;

    @Autowired
    public MemberPerformanceBonusService(MemberPerfomanceBonusRepository memberPerfomanceBonusRepository) {
        this.memberPerfomanceBonusRepository = memberPerfomanceBonusRepository;
    }

    public MemberPerformanceBonusService setVisible(boolean visible) {
        this.memberPerformanceVisible = visible;
        return this;
    }


    public MemberPerformanceBonus getMemberPerformanceBonus(String statCode) {
        List<MemberPerformanceBonus> memberPerformanceBonusList = memberPerfomanceBonusRepository.getMemberPerformanceBonus(statCode);
        MemberPerformanceBonus memberPerformanceBonus = null;

        if (isNotEmpty(memberPerformanceBonusList)) {
            memberPerformanceBonus = memberPerformanceBonusList.get(0);

            List<String> statCodesNames = memberPerformanceBonusList.stream()
                    .filter(x -> x.getStatCodeName() != null)
                    .map(MemberPerformanceBonus::getStatCodeName)
                    .sorted()
                    .collect(Collectors.toList());

            memberPerformanceBonus
                    .setAmount(memberPerformanceBonus.getAmount())
                    .setBalance(memberPerformanceBonus.getBalance())
                    .setPerformanceYear(memberPerformanceBonus.getPerformanceYear())
                    .setStatCodeNames(statCodesNames);
        }
        return memberPerformanceVisible ? memberPerformanceBonus : null;
    }

    public MemberPerformancePdf memberPerformancePdf(String statCode) {
        MemberPerformanceBonus memberPerformanceBonus = getMemberPerformanceBonus(statCode);

        String displayFilename = "member_performance_" + memberPerformanceBonus.getStatCode() + "_" + memberPerformanceBonus.getPerformanceYear() + ".pdf";
        MemberPerformancePdf pdf = new MemberPerformancePdf(displayFilename).write(memberPerformanceBonus);
        return pdf;
    }

    public Base64Container memberPerformancePdfAsBase64(String statCode) {
        MemberPerformancePdf pdf = memberPerformancePdf(statCode);

        Base64Container container = Base64Container.wrapPdfFromStorageAsUtf8(pdf.getTmpFileName());
        container.setFilename(pdf.getDisplayFilename());
        pdf.removeFile();
        return container;
    }
}
