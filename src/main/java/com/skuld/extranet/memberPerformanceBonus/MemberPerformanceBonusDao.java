package com.skuld.extranet.memberPerformanceBonus;

import com.skuld.extranet.core.common.ClosableDao;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface MemberPerformanceBonusDao extends ClosableDao {

    @SqlUpdate("Insert into EXTRANET.MemberPerformanceNotification(STAT_CODE, STAT_CODE_NAME, STAT_CODE_GROUP, EMAIL) " +
            "Values  "+
            "(:statCode, :statCodeName, :statCodeGroup, :email) ")
    @RegisterRowMapper(MemberPerformanceBonusNotification.Mapper.class)
    Number insert(@Bind("statCode") String statCode,
                  @Bind("statCodeName") String statCodeName, @Bind("statCodeGroup") String statCodeGroup, @Bind("email") String email);

    @SqlQuery("with src as (select  mg.stat_code, mg.stat_code_group_id from dwh.hlp_dim_member_group mg " +
            "            join (select distinct stat_code from dwh.fact_mem_stat_code_status_v3 " +
            "                   where mem_status in ('New curr year', 'Active curr year') " +
            "                    and product_line = 'Owner' ) scs on mg.stat_code = scs.stat_code) " +
            "    select src.stat_code, dm.stat_name, pb.amount_usd, pb.balance_usd, pb.performance_bonus_year " +
            "    from dwh.hlp_dim_member_group mg " +
            "        inner join src on mg.stat_code_group_id = src.stat_code_group_id " +
            "        join dwh.adm_performance_bonus pb on mg.stat_code_group_id = pb.stat_code_group_id " +
            "        join (select distinct stat_code, stat_name from dwh.dim_member) dm on src.stat_code= dm.stat_code " +
            "    where  mg.stat_code = :stat_code" +
            "    order by src.stat_code")
    @RegisterRowMapper(MemberPerformanceBonus.Mapper.class)
    List<MemberPerformanceBonus> getMemberPerformanceBonus(@Bind("stat_code") String statCode);

    @SqlQuery("select stat_code from dwh.HLP_DIM_MEMBER_GROUP ")
    List<String> getMemberPerformanceBonusStatCodes();

}
