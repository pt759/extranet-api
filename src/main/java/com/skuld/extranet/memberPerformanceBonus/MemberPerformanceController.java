package com.skuld.extranet.memberPerformanceBonus;

import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.global.RESTendPoints;
import com.skuld.extranet.memberPerformanceBonus.pdf.MemberPerformancePdf;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static com.skuld.extranet.global.RESTendPoints.member_performance_pdf_file_statCode;
import static com.skuld.extranet.global.RESTendPoints.member_performance_pdf_statCode_pdf_base64;

@Tag(name = "Financials", description = "Member Performance ")
@RestController
@RequestMapping(RESTendPoints.memberPerformance)
public class MemberPerformanceController extends SkuldController {
    final static Logger logger = LoggerFactory.getLogger(MemberPerformanceController.class);

    @Autowired
    private MemberPerformanceBonusService service;

    @Operation(summary = "Download Member Performance PDF (File)")
    @RequestMapping(
            path = member_performance_pdf_file_statCode
            , method = RequestMethod.GET)
    public void downloadPDFFile(
            @PathVariable(value = "statcode") String statCode,
            HttpServletResponse response) {
        MemberPerformancePdf pdf = service.memberPerformancePdf(statCode);
        pdf.export(response);
    }


    @Operation(summary = "Download Member Performance PDF (Base64)")
    @RequestMapping(
            path = member_performance_pdf_statCode_pdf_base64
            , method = RequestMethod.GET)
    public ResponseEntity downloadPDFBase64(
            @PathVariable(value = "statcode") String statCode) {
        Base64Container container = service.memberPerformancePdfAsBase64(statCode);
        logger.info("Member Performance Download: " + container.getFilename());
        return responseEntityWrapper(container);
    }


}
