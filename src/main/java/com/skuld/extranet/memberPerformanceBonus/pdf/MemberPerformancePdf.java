package com.skuld.extranet.memberPerformanceBonus.pdf;

import com.skuld.extranet.core.common.SkuldFormatter;
import com.skuld.extranet.core.common.SkuldWebUtil;
import com.skuld.extranet.global.Global;
import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonus;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class MemberPerformancePdf extends SkuldPdf {

    final static Logger logger = LoggerFactory.getLogger(MemberPerformancePdf.class);


    public MemberPerformancePdf(String filename) {
        super(filename);
    }

    public MemberPerformancePdf write(MemberPerformanceBonus member) {

        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);

            try (PDPageContentStream content = new PDPageContentStream(doc, page)) {
                PDRectangle mediaBox = page.getMediaBox();
                float pdfTop = mediaBox.getHeight();
                float pdfWidth = mediaBox.getWidth();
                float margin = 35;
                float textMargin = 14;

                float logoWidth = 200;
                float logoX = margin;
                float logoY = pdfTop - margin;
                addLogo(logoX, logoY, logoWidth, Global.imgSkuldLogoRedTransparent, content, doc);

                float mainContentHeight = 400;
                float footerHeight = 200;
                float lineMargin = margin * 2;
                addPageLayout(mainContentHeight, footerHeight, lineMargin, pdfWidth, content);

                float mainContentX = lineMargin + textMargin;
                float mainContentY = footerHeight + mainContentHeight;
                addMainContent(mainContentX, mainContentY, content, member);

                float footerContentX = lineMargin;
                float footerContentY = footerHeight - lineMargin - textMargin;
                addFooterContent(footerContentX, footerContentY, margin, pdfWidth, content);

            } catch (Exception ex) {
                logger.error("Failed to generate PDF for " + member.getStatCodeName() + "\n" + ex.getMessage());
            }

            doc.save(getTempFile());
//            doc.close();
            return this;
        } catch (Exception ex) {
            logger.error("Failed to save PDF for " + member.getStatCodeName() + "\n" + ex.getMessage());
        }

        return this;
    }

    private File getTempFile() throws IOException {
        File tmp = File.createTempFile(getDisplayFilename(), ".pdf");
        setTmpFileName(tmp.getAbsolutePath());
        return tmp;
    }

    private void addPageLayout(float mainContentHeight, float footerHeight, float lineMargin, float pdfWidth, PDPageContentStream content) throws IOException {
        content.setNonStrokingColor(Color.decode("#e71324"));
        content.addRect(lineMargin, footerHeight, 0.5f, mainContentHeight);
        content.fill();

        content.addRect(0, 0, pdfWidth, footerHeight);
        content.fill();

        content.setNonStrokingColor(Color.decode("#ffffff"));
        content.addRect(lineMargin, footerHeight - lineMargin, 0.5f, lineMargin);
        content.fill();
    }

    private void addLogo(float x, float y, float width, String imgPath, PDPageContentStream content, PDDocument doc) throws IOException, URISyntaxException {
        PDImageXObject pdImage = PDImageXObject.createFromFile(SkuldWebUtil.getImageFilePath(imgPath, this), doc);

        float height = width * width / pdImage.getWidth();

        content.drawImage(pdImage, x, y - height, width, height);
    }

    private void addMainContent(float x, float y, PDPageContentStream content, MemberPerformanceBonus member) throws IOException {
        content.setNonStrokingColor(Color.decode("#000000"));
        content.beginText();
        content.newLineAtOffset(x, y - 16);

        content.setLeading(40);
        content.setFont(PDType1Font.HELVETICA_BOLD, 16);
        content.showText("MEMBERS PERFORMANCE BONUS " + member.getPerformanceYear());
        content.newLine();

        //font is missing nbsp, replace in case we get nbsp from number formatter
        String formattedAmount = SkuldFormatter.formatBigDecimalWithThousandsSeparator(member.getAmount());
        formattedAmount = formattedAmount.replaceAll("\u00A0", " ");

        content.setLeading(20);
        content.setFont(PDType1Font.HELVETICA, 36);
        content.showText("USD " + formattedAmount);
        content.newLine();

        content.setLeading(18);
        content.setFont(PDType1Font.HELVETICA, 14);
        content.showText("Payment will be effected as agreed.");
        content.newLine();

        content.newLine();
        content.newLine();
        content.newLine();
        content.newLine();

        content.setLeading(16);
        content.setFont(PDType1Font.HELVETICA_BOLD, 14);
        content.showText("Bonus applies to:");
        content.newLine();

        content.setFont(PDType1Font.HELVETICA, 14);
        for (String statCodeName : member.getStatCodeNames()) {
            content.showText(statCodeName);
            content.newLine();
        }
        content.endText();
    }

    private void addFooterContent(float x, float y, float margin, float pdfWidth, PDPageContentStream content) throws IOException {
        content.beginText();
        content.setNonStrokingColor(Color.decode("#ffffff"));
        content.setFont(PDType1Font.HELVETICA, 12);
        content.newLineAtOffset(x, y - 12);
        content.setLeading(14);

        content.showText("SKULD Mutual Protection and Indemnity Association (Bermuda) Ltd.");
        content.newLine();
        content.showText("46 Reid Street");
        content.newLine();
        content.showText("Innovation House");
        content.newLine();
        content.showText("HM12, Hamilton");
        content.newLine();
        content.showText("Bermuda");
        content.newLine();
        content.endText();

        content.beginText();
        content.setNonStrokingColor(Color.decode("#ffffff"));
        content.setFont(PDType1Font.HELVETICA, 20);
        content.newLineAtOffset(pdfWidth - 118 - margin, margin);
        content.showText("SKULD.COM");
        content.newLine();
        content.endText();
    }


}