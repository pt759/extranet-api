package com.skuld.extranet.memberPerformanceBonus.pdf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class SkuldPdf {

    final static Logger logger = LoggerFactory.getLogger(SkuldPdf.class);


    private String displayFilename = "";
    private String tmpFileName;

    public SkuldPdf(String filename) {
        this.displayFilename = filename;
    }

    public void export(HttpServletResponse response) {

        response.addHeader("Content-disposition", "attachment; filename=\"" + displayFilename + "\"");

        try {
            File file = new File(tmpFileName);
            ByteArrayOutputStream pdfAsByteArrayStream = convertPdfFileToOutputStream(file);

            ServletOutputStream outputStream = response.getOutputStream();
            pdfAsByteArrayStream.writeTo(outputStream);

            outputStream.close();
            response.flushBuffer();
            removeFile();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


    }

    private ByteArrayOutputStream convertPdfFileToOutputStream(File pdfFile) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream in = new FileInputStream(pdfFile);
        byte[] buff = new byte[8000];
        int bytesRead = 0;
        baos = new ByteArrayOutputStream();
        while ((bytesRead = in.read(buff)) != -1) {
            baos.write(buff, 0, bytesRead);
        }
        in.close();
        return baos;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public SkuldPdf setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
        return this;
    }

    public void removeFile() {
        try {
            File file = new File(tmpFileName);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception ex) {
            logger.error("Failed to delete file: " + ex.getMessage());
        }
    }

    public String getTmpFileName() {
        return tmpFileName;
    }

    public SkuldPdf setTmpFileName(String tmpFileName) {
        this.tmpFileName = tmpFileName;
        return this;
    }
}
