package com.skuld.extranet.memberPerformanceBonus;

import com.skuld.extranet.core.common.repo.DBDwhRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberPerfomanceBonusRepository extends DBDwhRepo {
    public long writeNotifiactions(String statCode, String statCodeName, String statCodeGroup, String statCodeGroupDescripton, String email) {
        Number messageId = getJdbi().withExtension(MemberPerformanceBonusDao.class, dao -> dao.insert(statCode, statCodeName, statCodeGroup, email));
        return messageId.longValue();
    }

    public List<String> getMemberPerformanceBonusStatCodes() {
        return getJdbi().withExtension(MemberPerformanceBonusDao.class, dao -> dao.getMemberPerformanceBonusStatCodes());    }

    public List<MemberPerformanceBonus> getMemberPerformanceBonus(String statCode){
        return getJdbi().withExtension(MemberPerformanceBonusDao.class, dao -> dao.getMemberPerformanceBonus(statCode));
    }

}
