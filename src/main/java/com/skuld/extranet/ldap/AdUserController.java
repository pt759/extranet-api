package com.skuld.extranet.ldap;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.common.SimpleResponeBuilder;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.ldap.domain.LdapUserViews;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.NamingException;
import java.util.List;

@RestController
@RequestMapping("/users")
@Tag(name = "Users", description = "Retrieve user information from AD")
public class AdUserController {

    @Autowired
    LDAPService service;

    @Operation(summary = "Retrieve AD user information")
    @RequestMapping(path = "/{email}/",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity getUserByMail(@PathVariable(value = "email") String email) throws NamingException {
        LDAPUser user;
        if (email.startsWith("pt") && (email.length() == 5 || email.length() == 4)) {
            user = service.findFirstUserBySamaAccount(email);
        } else {
            user = service.findFirstUserByMail(email);
        }
        return SimpleResponeBuilder.buildResponseEntity(user);
    }


    @JsonView({LdapUserViews.ContactSync.class})
    @Operation(summary = "Retrieve all AD users")
    @RequestMapping(path = "/all",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity all() throws NamingException {
        List<LDAPUser> user = service.allLdapUsersFromAD();
        return SimpleResponeBuilder.buildResponseEntity(user);
    }


}
