package com.skuld.extranet.ldap;

import com.skuld.extranet.ldap.domain.LDAPUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.CachingConfig.LDAP_CACHE_USER;

@Log4j2
@Component
public class ActiveDirectory {

    private final String[] returnAttributes = LDAPAttributes.attributes;
    private final String baseFilter = "(&((&(objectCategory=Person)(objectClass=User)  " +
            "(|" +
            "(sAMAccountName=pt0*)(sAMAccountName=pt5*) " +
            "(sAMAccountName=pt1*)(sAMAccountName=pt6*) " +
            "(sAMAccountName=pt2*)(sAMAccountName=pt7*) " +
            "(sAMAccountName=pt3*)(sAMAccountName=pt8*) " +
            "(sAMAccountName=pt4*)(sAMAccountName=pt9*) " +
            ")      " +
            "))";
    @Value("${ldap.user}")
    private String ldapUserName;

    @Value("${ldap.user.password}")
    private String ldapPassword;

    @Value("${ldap.domaincontroller}")
    private String ldapDomainController;

    @Value("${ldap.baseDN}")
    private String baseDN;

    private DirContext dirContext;
    private SearchControls searchCtls;


    public ActiveDirectory() {
    }

    public void openConnection() {
        Hashtable properties = new Hashtable();
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        properties.put(Context.PROVIDER_URL, "LDAP://" + ldapDomainController);
        properties.put(Context.SECURITY_PRINCIPAL, ldapUserName + "@" + ldapDomainController);
        properties.put(Context.SECURITY_CREDENTIALS, ldapPassword);
//        properties.put(DirContext.SECURITY_AUTHENTICATION, "simple");
//        properties.put(Context.SECURITY_PROTOCOL, "ssl");

        try {
            dirContext = new InitialDirContext(properties);

        } catch (NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }

        searchCtls = new SearchControls();
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchCtls.setReturningAttributes(returnAttributes);
    }

    public NamingEnumeration<SearchResult> searchUser(String searchValue, String searchBy) throws NamingException {
        String filter = getFilter(searchValue, searchBy);

        return this.dirContext.search(baseDN, filter, this.searchCtls);
    }

    private NamingEnumeration<SearchResult> all() {
        NamingEnumeration<SearchResult> search = null;
        try {
            search = this.dirContext.search(this.baseDN, this.baseFilter + ")", this.searchCtls);
        } catch (NamingException e) {
            log.error("Failed to retrieve AD users: " + e.getMessage());
        }
        return search;
    }

    @Cacheable(LDAP_CACHE_USER)
    public List<LDAPUser> allUsersFromAD() {
        log.info("allUsersFromAD() - (cached when running)");
        openConnection();
        NamingEnumeration<SearchResult> result = all();
        List<LDAPUser> ldapUsers = LDAPUser.listOfUsers(result);
        closeLdapConnection();

        return ldapUsers.stream()
                .filter(user -> user.isValidSkuldUser())
                .collect(Collectors.toList());
    }

    public void closeLdapConnection() {
        try {
            if (dirContext != null)
                dirContext.close();
        } catch (NamingException e) {
            log.error(e.getMessage());
        }
    }

    private String getFilter(String searchValue, String searchBy) {
        String filter = this.baseFilter;
        if (searchBy.equals(LDAPAttributes.mail)) {
            filter += "(mail=" + searchValue + "))";
        } else if (searchBy.equals(LDAPAttributes.sAMAccountName)) {
            filter += "(samaccountname=" + searchValue + "))";
        }
        return filter;
    }

    public SearchControls getSearchCtls() {
        return searchCtls;
    }
}