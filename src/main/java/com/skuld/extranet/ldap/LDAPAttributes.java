package com.skuld.extranet.ldap;

public interface LDAPAttributes {
    String sAMAccountName = "sAMAccountName";
    String userprincipalname = "userPrincipalName";
    String mailnickname = "mailnickname";
    String title = "title";
    String mail = "mail";
    String department = "department";
    String cn = "cn";
    String displayName = "displayName";
    String mobile = "mobile";
    String telephonenumber = "telephoneNumber";
    String msrtcsipLine = "msrtcsip-line";
    String description = "description";
    String initials = "initials";
    String userAccountControl = "userAccountControl";
    String givenName = "givenName";
    String surName = "sn";

    String[] attributes = {sAMAccountName, userprincipalname, mailnickname, title, mail, department, cn, displayName, mobile, telephonenumber, msrtcsipLine, description, initials,userAccountControl,givenName,surName};
}
