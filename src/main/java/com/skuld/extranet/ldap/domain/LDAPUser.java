package com.skuld.extranet.ldap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.skuld.extranet.core.claim.domain.Views;
import com.skuld.extranet.ldap.LDAPAttributes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LDAPUser {
    final static Logger logger = LoggerFactory.getLogger(LDAPUser.class);

    private final String colon_blank = ": ";
    private String sAMAccountName;

    @JsonView({LdapUserViews.LdapBasic.class, Views.ClaimDetail.class})
    private String name;

    @JsonView({LdapUserViews.LdapBasic.class, Views.ClaimDetail.class})
    private String email;


    @JsonView({LdapUserViews.LdapBasic.class, Views.ClaimDetail.class})

    private String title;

    @JsonView({LdapUserViews.LdapBasic.class, Views.ClaimDetail.class})
    private String directPhone;

    @JsonView({LdapUserViews.LdapBasic.class, Views.ClaimDetail.class})
    private String mobilePhone;

    @JsonView({LdapUserViews.LdapBasic.class, Views.ClaimDetail.class})
    private String initials;

    @JsonView({LdapUserViews.ContactSync.class})
    private String emailWithUserID;
    @JsonView({LdapUserViews.ContactSync.class})
    private String department;


    private String cn;
    private String description;
    private String userAccountControl;
    private String surName;
    private String givenName;


    private final static String STATUS_DISABLED = "546,514";

    public LDAPUser(SearchResult searchResult) {

        Attributes r = searchResult.getAttributes();
        this.sAMAccountName = getAttributeValue(r, LDAPAttributes.sAMAccountName);
        this.name = getAttributeValue(r, LDAPAttributes.displayName);
        this.cn = getAttributeValue(r, LDAPAttributes.cn);
        this.emailWithUserID = getAttributeValue(r, LDAPAttributes.userprincipalname);
        this.email = getAttributeValue(r, LDAPAttributes.mail);
        this.title = getAttributeValue(r, LDAPAttributes.title);
        this.directPhone = getAttributeValue(r, LDAPAttributes.telephonenumber);
        this.mobilePhone = getAttributeValue(r, LDAPAttributes.mobile);
        this.description = getAttributeValue(r, LDAPAttributes.description);
        this.department = getAttributeValue(r, LDAPAttributes.department);
        this.initials = getAttributeValue(r, LDAPAttributes.initials);
        this.userAccountControl = getAttributeValue(r, LDAPAttributes.userAccountControl);
        this.surName = getAttributeValue(r, LDAPAttributes.surName);
        this.givenName = getAttributeValue(r, LDAPAttributes.givenName);
    }

    private String getAttributeValue(Attributes r, String attribute) {
        String value = nvl(r.get(attribute));
        return value.replace(attribute + colon_blank, "");

    }

    private String nvl(Attribute attribute) {
        return attribute != null ? attribute.toString() : "";
    }

    public static List<LDAPUser> listOfUsers(NamingEnumeration<SearchResult> list) {
        List<LDAPUser> users = new ArrayList<>();
        if (list != null) {
            while (list.hasMoreElements()) {
                users.add(new LDAPUser(list.nextElement()));
            }
        }
        return users;
    }

    @JsonView({LdapUserViews.ContactSync.class})
    public boolean isDisabled() {
        return (isNotEmpty(getUserAccountControl()) && STATUS_DISABLED.contains(getUserAccountControl()));
    }

    @JsonIgnore
    public boolean isValidSkuldUser() {
        return isNotEmpty(name) && isNotEmpty(emailWithUserID) && emailWithUserID.contains("skuld.com");
    }

    @JsonIgnore
    public String getDescription() {
        return description;
    }

    public static PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

    public static String getPhoneFormatted(String phoneStr) {

        if (StringUtils.isEmpty(phoneStr)) {
            return "";
        }

        Phonenumber.PhoneNumber phoneNumber;
        try {
            phoneNumber = phoneUtil.parse(phoneStr, "NO");
        } catch (NumberParseException e) {
            logger.info("Phone is not a valid phone number: " + phoneStr);
            return "";
        }
        return phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
    }

    public String getDirectPhone() {
        return getPhoneFormatted(directPhone);
    }

    public String getMobilePhone() {
        return getPhoneFormatted(mobilePhone);
    }
}
