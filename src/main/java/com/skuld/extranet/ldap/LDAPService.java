package com.skuld.extranet.ldap;

import com.skuld.extranet.ldap.domain.LDAPUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchResult;
import java.util.List;

@Log4j2
@Component
@Service
public class LDAPService {




    @Autowired
    private ActiveDirectory ad;

    public List<LDAPUser> findUser(String searchValue, String searchBy) throws NamingException {

        ad.openConnection();
        NamingEnumeration<SearchResult> result = ad.searchUser(searchValue, searchBy);
        List<LDAPUser> ldapUsers = LDAPUser.listOfUsers(result);
        ad.closeLdapConnection();
        return ldapUsers;
    }

    public NamingEnumeration<SearchResult> findUserWithAllInfo(String searchValue, String searchby) throws NamingException {
        ad.openConnection();
        ad.getSearchCtls().setReturningAttributes(null);

        NamingEnumeration<SearchResult> result = ad.searchUser(searchValue, searchby);

        ad.closeLdapConnection();
        return result;

    }

    public List<LDAPUser> allLdapUsersFromAD() {
        return ad.allUsersFromAD();
    }

    public LDAPUser findFirstUserByMail(String email) throws NamingException {
        List<LDAPUser> users = findUser(email.trim(), LDAPAttributes.mail);
        LDAPUser user = null;
        if (!users.isEmpty()) {
            user = users.get(0);
        }
        return user;
    }

    public LDAPUser findFirstUserBySamaAccountSafe(String sAMAccountName) {
        LDAPUser user = new LDAPUser();
        try {
            user = findFirstUserBySamaAccount(sAMAccountName);
        } catch (NamingException e) {
            log.error("Unable to find user information for : " + sAMAccountName);
        }
        return user;
    }

    public LDAPUser findFirstUserBySamaAccount(String sAMAccountName) throws NamingException {
        List<LDAPUser> users = findUser(sAMAccountName, LDAPAttributes.sAMAccountName);
        LDAPUser user = null;
        if (!users.isEmpty()) {
            user = users.get(0);
        }
        return user;
    }

    public LDAPUser matchUserOnSaMAccountName(String sAMAccountName) {
        LDAPUser user = allLdapUsersFromAD().stream()
                .filter(line -> line.getSAMAccountName().equalsIgnoreCase(sAMAccountName))
                .findFirst()
                .orElse(null);

        return user;
    }

    public LDAPService setAd(ActiveDirectory ad) {
        this.ad = ad;
        return this;
    }


}
