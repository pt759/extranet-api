package com.skuld.extranet.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="azureapi")
public class AzureApiServiceConfig {

	@Value("${azureapi.url}")
	private static String url;
	
	@Value("${azureapi.username}")
	private static String userName;
	
	@Value("${azureapi.password}")
	private static String password;
	
	
    
}
