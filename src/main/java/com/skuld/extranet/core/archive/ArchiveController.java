package com.skuld.extranet.core.archive;

import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.archive.service.ArchiveService;
import com.skuld.extranet.core.archive.service.CoreArchiveClient;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.skuld.extranet.global.RESTendPoints.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Deprecated
@Tag(name = "Extranet - Deprecated", description = "Download document from Archive in TIA")
@RestController
@RequestMapping(archive)
@CrossOrigin
public class ArchiveController extends SkuldController {

    @Autowired
    ArchiveService service;

    @Autowired
    CoreArchiveClient coreArchiveClient;

    @GetMapping("/{requestid}/")
    public ResponseEntity getDocumentFeign(@PathVariable("requestid") String requestId) {
        Base64Container archive = coreArchiveClient.getArchive(requestId);
        return responseEntityWrapper(archive);
    }

    @GetMapping(archive_StatCode_DocumentKey)
    public ResponseEntity getDocumentFeign(@PathVariable("statcode") String statcode,@PathVariable("documentKey") String documentKey) {
        Base64Container archive = coreArchiveClient.getArchive(statcode, documentKey);
        return responseEntityWrapper(archive);
    }

    @Operation(summary = "Download Invoice by Invoice No (finds request id) (Base64)")
    @RequestMapping(
            path = archive_invoice_with_InvoiceNo,
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity getInvoiceBase64(
            @PathVariable(value = "invoiceno") String invoiceNo) {
        Base64Container data = service.invoiceAsBase64(invoiceNo);
        return buildResponseEntity(data);
    }

    @Deprecated
    //TODO: How do we fix this, and is there a need to fix this?
    @Operation(summary = "Download Invoice by Document Key (File)")
    @RequestMapping(
            path = archive_invoice_file_with_InvoiceNo,
            method = RequestMethod.GET
    )
    public void getInvoiceFile(
            @PathVariable(value = "invoiceno") String invoiceNo,
            HttpServletResponse response
    ) {
        service.invoiceAsFile(invoiceNo, response);
    }

    @Operation(summary = "Get DocumentRef list for stat code or agr_line_no")
    @RequestMapping(
            path = archive_StatCode_AgrLineNo,
            method = RequestMethod.GET)
    public ResponseEntity documentList(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "agrLineNo", required = false) String agrLineNo
    ) {
        List<DocumentRef> documentList = null;
        if (agrLineNo != null) {
            documentList = service.getDocumentsReferenceArgNo(statCode, agrLineNo);
        } else if (statCode != null) {
            documentList = service.getDocumentsReferenceStatCode(statCode);
        }
        return buildResponseEntity((documentList));
    }

}
