package com.skuld.extranet.core.archive.service;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.claim.domain.EventStatus;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "skuld.core.archive", url = "${skuld.api.core.url}")
public interface CoreArchiveClient {

    @GetMapping("/archive/{statcode}/{requestid}/")
    Base64Container getArchive(@PathVariable("statcode") String statcode, @PathVariable("requestid") String requestid);

    @GetMapping("/archive/{requestid}/")
    Base64Container getArchive(@PathVariable("requestid") String requestid);

    @GetMapping("/ms/{statCode}/{version}/{product}/")
    Base64Container getMemberStatisticsPdf(
            @PathVariable(value = "statCode") String statCode,
            @PathVariable(value = "version") String version,
            @PathVariable(value = "product") String product,
            @RequestParam(value = "currency", required = false) String currency,
            @RequestParam(value = "statName", required = false) String statName,
            @RequestParam(value = "fileName") String fileName);

    @GetMapping("/claim/{statCode}/{product}/{status}/")
    Base64Container getClaimsToPdfBase64(@PathVariable("statCode") String statCode,
                                         @PathVariable("product") String product,
                                         @PathVariable("status") EventStatus status);


}

