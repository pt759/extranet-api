package com.skuld.extranet.core.archive.repo;


import com.skuld.extranet.core.archive.domain.ArchiveEntity;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.common.repo.DBTiaRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArchiveRepository extends DBTiaRepo {
    final static Logger logger = LoggerFactory.getLogger(ArchiveRepository.class);

    @Deprecated
    public ArchiveEntity getDocument(String statCode, String documentKey) {
        ArchiveEntity item = getJdbi().withExtension(ArchiveDao.class, dao -> dao.document(statCode, documentKey));
        return item;
    }

    @Deprecated
    public ArchiveEntity archiveEntity(String documentKey) {
        return getJdbi().withExtension(ArchiveDao.class, dao -> dao.archiveEntity(documentKey));
    }

    public String getRequestIdForInvoice(String invoiceNo) {
        return getJdbi().withExtension(ArchiveDao.class, dao -> dao.getRequestIdForInvoice(invoiceNo));
    }

    public List<DocumentRef> getAgrDocuments(String statCode, String agrLineNO) {
		List<DocumentRef> documentsList = getJdbi().withExtension(ArchiveDao.class, dao -> dao.getDocumentRefAgrNo(statCode,agrLineNO));
		return documentsList;
	}

	public List<DocumentRef> getRenewalDocuments(String statCode, String agrLineNO) {
		List<DocumentRef> documentsList = getJdbi().withExtension(ArchiveDao.class, dao -> dao.getRenewalDocuments(statCode,agrLineNO));
		return documentsList;
	}

	public List<DocumentRef> getStaCodeDocuments(String statCode) {
		List<DocumentRef> documentsList = getJdbi().withExtension(ArchiveDao.class, dao -> dao.getAllDocumentRefStatCode(statCode));
		return documentsList;
	}
}
