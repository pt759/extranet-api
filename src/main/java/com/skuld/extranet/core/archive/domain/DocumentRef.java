package com.skuld.extranet.core.archive.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.vessel.domain.VesselView;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.skuld.extranet.core.common.SkuldFormatter.formatLocaldateFor;
import static com.skuld.extranet.core.common.SkuldSqlHelper.fromSqlDate;


public class DocumentRef {

    @JsonView(VesselView.Documents.class)
    private String fileName;
    @JsonView(VesselView.Documents.class)
    private String requestId;
    @JsonView(VesselView.Documents.class)
    private String vesselName;
    @JsonView(VesselView.Documents.class)
    private String url;

    @JsonView(VesselView.Documents.class)
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate requestDate;

    @JsonView(VesselView.Documents.class)
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate startDate;

    @JsonView(VesselView.Documents.class)
    private String programId;

    @JsonView(VesselView.Documents.class)
    private List<DocumentRef> versions;

    @JsonView(VesselView.Documents.class)
    private String productClass;

    private String type; //P(policy), R(renewal)
    private String productClassForFilename;

    @Override
    public String toString() {
        return "fileName='" + fileName + '\'' +
                ", requestId='" + requestId + '\'' +
                ", vesselName='" + vesselName + '\'' +
                ", programid='" + programId + '\'' +
                ", url='" + url + '\'' +
                ", requestDate='" + requestDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", class='" + getProductClass() + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public DocumentRef() {
    }

    public DocumentRef(ResultSet r) throws SQLException {
        this.fileName = r.getString("description");
        this.requestId = r.getString("request_id");
        this.programId = r.getString("program_id");
        this.requestDate = fromSqlDate(r.getDate("request_date"));
        this.startDate = fromSqlDate(r.getDate("cover_start_date"));
        this.productClass = r.getString("class");
        this.type = r.getString("type");
    }


    public String getVesselName() {
        return vesselName;
    }

    public DocumentRef setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public boolean programNotInList(List<String> listOfProgramIds) {
        return !programIdInList(listOfProgramIds);
    }

    public boolean programIdInList(List<String> listOfProgramIds) {
        return listOfProgramIds.stream().anyMatch(d -> d.equalsIgnoreCase(this.programId));
    }

    public boolean maxRequestIdForStartDate(List<DocumentRef> val) {
        return val.stream().anyMatch(x -> (x.getStartDateAsString().equals(this.getStartDateAsString()) && Integer.valueOf(x.getRequestId()) >= Integer.valueOf(this.requestId)));
    }

    public String getProductClassForFilename() {
        if (StringUtils.isNotEmpty(productClass)) {
            return " (" + productClass + ")";
        }
        return "";
    }


    public static class Mapper implements RowMapper<DocumentRef> {
        public DocumentRef map(ResultSet r, StatementContext ctx) throws SQLException {
            return new DocumentRef(r);
        }
    }

    public String getUrl() {
        return url;
    }

    public DocumentRef setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getFileName() {
        return fileName + getProductClassForFilename();
    }

    public DocumentRef setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getRequestId() {
        return requestId;
    }

    public Integer getRequestIdAsInt() {
        return Integer.valueOf(requestId);
    }

    public DocumentRef setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public String getType() {
        return type;
    }

    public DocumentRef setType(String type) {
        this.type = type;
        return this;
    }

    public String getProgramId() {
        return programId;
    }

    public String getProgramIdAndCladd() {
        return programId + " " + productClass;
    }

    public DocumentRef setProgramId(String programId) {
        this.programId = programId;
        return this;
    }

    public DocumentRef setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public String getProgramIdDate() {
        return programId + "-" + getRequestDateAsString();
    }

    public String getRequestDateAsString() {
        return formatLocaldateFor(requestDate);
    }

    public String getStartDateAsString() {
        return formatLocaldateFor(startDate);
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getStartDateAsLocalDate() {
        return startDate;
    }

    public DocumentRef setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public List<DocumentRef> getVersions() {
        if (versions == null) {
            versions = new ArrayList<>();
        }
        return versions;
    }

    public DocumentRef setVersions(List<DocumentRef> versions) {
        this.versions = versions;
        return this;
    }

    public String getProductClass() {
        return productClass;
    }

    public DocumentRef setProductClass(String productClass) {
        this.productClass = productClass;
        return this;
    }
}
