package com.skuld.extranet.core.archive.repo;


import com.skuld.extranet.core.archive.domain.ArchiveEntity;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.common.ClosableDao;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface ArchiveDao extends ClosableDao {

    @Deprecated
    @SqlQuery(" select a.document document, " +
            " dbms_lob.getlength( a.document) blob_length, " +
            " upper(a.document_format) document_format," +
            " a.document_key " +
            " from archive a " +
            " inner join PRINT_REQUEST pr on pr.REQUEST_ID = a.DOCUMENT_KEY " +
            " inner join policy p on p.POLICY_NO = pr.POLICY_NO " +
            "where a.document_key =  :documentKey " +
            " and p.c11 = :statCode"
    )
    @RegisterRowMapper(ArchiveEntity.Mapper.class)
    ArchiveEntity document(@Bind("statCode") String statCode, @Bind("documentKey") String documentKey);

    @Deprecated
    @SqlQuery(" select a.document document, " +
            " dbms_lob.getlength( a.document) blob_length, " +
            " upper(a.document_format) document_format," +
            " a.document_key " +
            " from archive a " +
            "where a.document_key =  :documentKey "
    )
    @RegisterRowMapper(ArchiveEntity.Mapper.class)
    ArchiveEntity archiveEntity(@Bind("documentKey") String documentKey);

    @SqlQuery("select max(request_id) " +
            "from PRINT_ARGUMENT pa " +
            "       inner join archive a on a.document_key = pa.request_id " +
            "where keyword = 'INVOICE_NO' " +
            "  and value = :invoiceNo " +
            "  and dbms_lob.getlength(a.document) > 13000 ")
    String getRequestIdForInvoice(@Bind("invoiceNo") String invoiceNo);

    @SqlQuery("select * from v_stat_code_documents where agr_line_no = :agrLineNo and stat_code = :statCode" )
    @RegisterRowMapper(DocumentRef.Mapper.class)
    List<DocumentRef> getDocumentRefAgrNo(@Bind("statCode") String statCode, @Bind("agrLineNo") String agrLineNo);
	
	@SqlQuery("select * from v_renewal_documents where new_policy_line_no = :agrLineNo and stat_code = :statCode" )
	@RegisterRowMapper(DocumentRef.Mapper.class)
	List<DocumentRef> getRenewalDocuments(@Bind("statCode") String statCode, @Bind("agrLineNo") String agrLineNo);

	@SqlQuery("select * from v_stat_code_documents where stat_code = :statCode")
	@RegisterRowMapper(DocumentRef.Mapper.class)
	List<DocumentRef> getAllDocumentRefStatCode(@Bind("statCode") String statCode);

}
