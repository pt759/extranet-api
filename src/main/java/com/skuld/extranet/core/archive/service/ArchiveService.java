package com.skuld.extranet.core.archive.service;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.archive.domain.ArchiveEntity;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.archive.repo.ArchiveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArchiveService {

    final static Logger logger = LoggerFactory.getLogger(ArchiveService.class);

    @Autowired
    private CoreArchiveClient coreArchiveClient;

    @Autowired
    private ArchiveRepository repo;

    public void setRepo(ArchiveRepository repo) {
        this.repo = repo;
    }

    /*
    Very very slow
     */
    public List<DocumentRef> getDocumentsReferenceStatCode(String statCode) {
        List<DocumentRef> documents = null;
        if (statCode != null) {
            documents = repo.getStaCodeDocuments(statCode);
        }

        logger.debug("(deprecated) Returned " + documents == null ? "null" : documents.size() + " documents");
        return documents;
    }

    public List<DocumentRef> getDocumentsReferenceArgNo(String statCode, String agrLineNo) {
        List<DocumentRef> allDocuments = new ArrayList<>();
        if (agrLineNo != null) {
            List<DocumentRef> policyDocuments = repo.getAgrDocuments(statCode, agrLineNo);

            List<String> programIdList = policyDocuments.stream().map(DocumentRef::getProgramId).collect(Collectors.toList());

            List<DocumentRef> docsRenewal = repo.getRenewalDocuments(statCode, agrLineNo);
            docsRenewal = docsRenewal.stream().filter(p -> p.programNotInList(programIdList)).collect(Collectors.toList());

            allDocuments.addAll(policyDocuments);
            allDocuments.addAll(docsRenewal);
        }
        logger.debug("Returned " + allDocuments == null ? "null" : allDocuments.size() + " documents");
        return allDocuments;
    }

    public void invoiceAsFile(String invoiceNo, HttpServletResponse response) {
        String documentKey = repo.getRequestIdForInvoice(invoiceNo);
        ArchiveEntity document = repo.archiveEntity(documentKey);
        downloadOrMessage(documentKey, response, document);
        document.releaseBlob();
    }

    public Base64Container invoiceAsBase64(String invoiceNo) {
        String requestIdForInvoice = repo.getRequestIdForInvoice(invoiceNo);
        Base64Container archive = coreArchiveClient.getArchive(requestIdForInvoice);
        return archive;
    }

    /*
    Download Code
    */

    private void downloadOrMessage(String documentId, HttpServletResponse response, ArchiveEntity document) {
        try {
            if (document != null) {
                download(document, response);
            } else {
                String txt = "Unable to find the document id " + documentId + "";
                htmlMessage(txt + ".", response);
                logger.info(txt);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void htmlMessage(String message, HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.TEXT_HTML_VALUE);
        PrintWriter out = response.getWriter();
        out.println("<h3>" + message + "</h3>");
    }

    private void download(ArchiveEntity document, HttpServletResponse response) throws SQLException, IOException {
        String downloadedFilename = document.getFilename();

        response.setContentType(document.getMimeType());
        response.addHeader("Content-Disposition", "attachment; filename=" + downloadedFilename);
        response.setContentLength(document.getLength());

        OutputStream responseOutputStream = response.getOutputStream();
        responseOutputStream.write(document.getBlobAsBytes());


        logger.info("Downloaded file: " + downloadedFilename);
    }

    public void writePDFToDisc(String filename, ArchiveEntity entity) {
        try {
            entity.getBlobAsBytes();
            FileOutputStream fos = new FileOutputStream(filename);
            fos.write(entity.getBlobAsBytes());
        } catch (IOException e) {
            logger.error("IO Exception: ", e);
        }
    }

    /*
    end of download code
    */
}

	

