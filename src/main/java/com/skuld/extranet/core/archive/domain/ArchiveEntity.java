package com.skuld.extranet.core.archive.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.skuld.extranet.core.common.SkCommon.*;

@Deprecated
public class ArchiveEntity {


    private Blob blob;
    private int length;
    private String documentKey;
    private String mimeType;
    private String documentFormat;
    private String extension;

    private int blobLength;
    private byte[] blobAsBytes;

    public static class Mapper implements RowMapper<ArchiveEntity> {
        public ArchiveEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new ArchiveEntity(r);
        }
    }

    public ArchiveEntity(ResultSet rs) throws SQLException {
        this.blob = rs.getBlob("document");
        this.length = rs.getInt("blob_length");
        this.documentKey = "" + rs.getString("document_key");

        setDocumentFormat(rs.getString("document_format"));

        if (blob != null) {
            blobLength = (int) blob.length();
            blobAsBytes = blob.getBytes(1, blobLength);
        }
    }

    public void setDocumentFormat(String documentFormat) {
        this.documentFormat = documentFormat;
        switch (documentFormat) {
            case "PDF":
                this.extension = formatPDF;
                this.mimeType = mimeTypePdf;
                break;
            case "WORD":
                this.extension = formatWord;
                this.mimeType = mimeTypeWord;
                break;
            default:
                this.extension = ".unknown";
                this.mimeType = mimeTypeFile;
                break;
        }
    }

    public Blob getBlob() {
        return blob;
    }

    public void setBlob(Blob blob) {
        this.blob = blob;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setDocumentKey(String documentKey) {
        this.documentKey = documentKey;
    }

    public String getFilename() {
        return documentKey + extension;
    }


    public int getBlobLength() {
        return blobLength;
    }

    public void setBlobLength(int blobLength) {
        this.blobLength = blobLength;
    }

    public byte[] getBlobAsBytes() {
        return blobAsBytes;
    }

    public void setBlobAsBytes(byte[] blobAsBytes) {
        this.blobAsBytes = blobAsBytes;
    }

    public void releaseBlob() {
        blob = null;
        blobAsBytes = null;
    }



}
