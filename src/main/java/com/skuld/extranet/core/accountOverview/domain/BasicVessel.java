package com.skuld.extranet.core.accountOverview.domain;

public class BasicVessel {
    public String vesselName;
    public String imo;
    public String grossTonnage;
    public String vesselType;

    public BasicVessel(String vesselName, String imo, String grossTonnage, String vesselType) {
        this.vesselName = vesselName;
        this.imo = imo;
        this.grossTonnage = grossTonnage;
        this.vesselType = vesselType;
    }


}
