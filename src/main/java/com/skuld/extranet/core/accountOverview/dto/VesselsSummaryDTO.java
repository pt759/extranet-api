package com.skuld.extranet.core.accountOverview.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VesselsSummaryDTO {
    private long totalVessels;
    private String mainCovers;
    private String totalNetPremium;
}