package com.skuld.extranet.core.accountOverview;

import com.skuld.extranet.core.accountOverview.domain.AccountCurrencySummary;
import com.skuld.extranet.core.accountOverview.domain.AccountManager;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.accountOverview.dto.SummaryHeaderDTO;
import com.skuld.extranet.core.accountOverview.dto.VesselsSummaryDTO;
import com.skuld.extranet.core.accountOverview.repo.AccountOverviewRepository;
import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.api.vessel.service.VesselClientService;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.claim.service.ClaimsClientService;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import com.skuld.extranet.global.GlobalText;
import com.skuld.extranet.ldap.LDAPService;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.spa.pages.domain.Product;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.skuld.common.SkuldFormatter.formatBigDecimalWithThousandsSeparator;
import static java.util.Objects.isNull;

@Log4j2
@Service
public class AccountOverviewService {

    private final AccountOverviewRepository accountOverviewRepository;
    private final LDAPService ldapService;
    private final AzureAdUserService azureAdUserService;
    private final ClaimService claimService;
    private final VesselClientService vesselClientService;
    private final ClaimsClientService claimsClientService;


    @Autowired
    public AccountOverviewService(AccountOverviewRepository accountOverviewRepository,
                                  LDAPService ldapService,
                                  AzureAdUserService azureAdUserService,
                                  ClaimService claimService, VesselClientService vesselClientService, ClaimsClientService claimsClientService) {
        this.accountOverviewRepository = accountOverviewRepository;
        this.ldapService = ldapService;
        this.azureAdUserService = azureAdUserService;
        this.claimService = claimService;
        this.vesselClientService = vesselClientService;
        this.claimsClientService = claimsClientService;
    }

    public List<AccountOverview> getAllClientsForBroker(String statCodeGroupLogin) {
        List<StatCodeDTO> statCodes = azureAdUserService.getStatCodesForGroup(statCodeGroupLogin);

        return convertToAccountOverview(statCodes);
    }

    public SummaryHeaderDTO getBrokerSummaryHeader(String statCodeGroupLogin) {
        List<AccountOverview> allClientsForBroker = getAllClientsForBroker(statCodeGroupLogin);
        log.debug("Broker " + statCodeGroupLogin + " has " + allClientsForBroker.size() + " clients");

        Set<String> statCodes = allClientsForBroker
                .stream()
                .filter(x -> !isNull(x.getStatCode()))
                .map(AccountOverview::getStatCode)
                .collect(Collectors.toSet());

        return getSummaryHeader(statCodes);
    }

    public SummaryHeaderDTO getClientSummaryHeader(String statCode) {
        return getSummaryHeader(Set.of(statCode));
    }

    public SummaryHeaderDTO getSummaryHeader(Set<String> statCodes) {
        return SummaryHeaderDTO.builder()
                .vesselsHeader(getVesselsSummary(statCodes))
                .claimsHeader(getClaimsWithVessels(statCodes))
                .build();

    }

    private VesselsSummaryDTO getVesselsSummary(Set<String> statCodes) {
        return VesselsSummaryDTO.builder()
                .totalVessels(countNumberOfVessels(statCodes))
                .totalNetPremium(getTotalNetPremium(statCodes))
                .mainCovers(getMainCovers(statCodes))
                .build();
    }

    public String getTotalNetPremium(Set<String> statCodes) {
        List<AccountOverview> accountOverviewList = new ArrayList<>();

        statCodes
                .parallelStream()
                .forEach(statCode -> accountOverviewList.add(accountOverviewRepository.getBrokerClientOverview(statCode)));

        Set<String> currencies = accountOverviewList
                .stream()
                .filter(x -> !isNull(x.getCurrency()))
                .map(AccountOverview::getCurrency)
                .collect(Collectors.toSet());

        List<AccountCurrencySummary> accountCurrencySummaries = new ArrayList<>();

        for (String currency : currencies) {
            accountCurrencySummaries.add(new AccountCurrencySummary(currency, currencyGrossPremium(currency, accountOverviewList)));
        }

        StringBuilder netPremium = new StringBuilder(GlobalText.netPremium);
        accountCurrencySummaries.forEach(
                accountCurrencySummary -> netPremium.append(accountCurrencySummary.getCurrency() + ": " + accountCurrencySummary.getGrossPremium() + ". ")
        );

        return netPremium.toString();

    }

    private String currencyGrossPremium(String currency, List<AccountOverview> accountOverviewList) {
        BigDecimal reduce = accountOverviewList
                .stream()
                .filter(x -> !isNull(x.getGrossPremium()))
                .filter(x -> currency.equals(x.getCurrency()))
                .map(AccountOverview::getGrossPremium)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return formatBigDecimalWithThousandsSeparator(reduce);
    }

    protected ClientClaimsSummaryDTO getClaimsWithVessels(Set<String> statCodes) {
        long totalVesselsWithClaim = 0;
        long totalClaims = 0;
        List<ClientClaimsSummaryDTO> claimsOverviewSummary = new ArrayList<>();

        statCodes.parallelStream()
                .forEach(statCode -> claimsOverviewSummary.add(claimsClientService.getClaimOverviewSummary(statCode)));

        for (ClientClaimsSummaryDTO client : claimsOverviewSummary) {
            totalVesselsWithClaim += client.getVesselsWithClaim();
            totalClaims += client.getOpenClaims();
        }

        return ClientClaimsSummaryDTO
                .builder()
                .openClaims(totalClaims).
                vesselsWithClaim(totalVesselsWithClaim)
                .build();
    }


    protected long countNumberOfVessels(Set<String> statCodes) {
        List<VesselListDTO> vessels = new ArrayList<>();

        statCodes
                .parallelStream()
                .forEach(statCode -> vessels.addAll(vesselClientService.vesselsFor(statCode)));

        return vessels.stream()
                .map(VesselListDTO::getImo)
                .distinct()
                .count();
    }

    public List<AccountOverview> convertToAccountOverview(List<StatCodeDTO> statCodes) {
        List<AccountOverview> clients = new ArrayList<>();

        statCodes.forEach(
                statCode -> {
                    AccountOverview client = getAccountOverview(statCode.getStatCode());
                    client.setClientName(statCode.getStatCodeName());
                    clients.add(client);
                });

        return clients;
    }

    public AccountOverview getAccountOverview(String statCode) {
        AccountOverview client = getBrokerClientOverview(statCode);
        List<Product> productsForAccount = getProductsForAccount(statCode);
        client.setStatCode(statCode);
        client.setProducts(productsForAccount);
        return client;
    }

    private AccountOverview getBrokerClientOverview(String statCode) {
        return accountOverviewRepository.getBrokerClientOverview(statCode);
    }

    private List<Product> getProductsForAccount(String statCode) {
        return accountOverviewRepository.getProductsForAccount(statCode);
    }

    public AccountManager getAccountManagerPtUserName(String statCode) {
        return accountOverviewRepository.getAccountManager(statCode);
    }

    public LDAPUser getAccountManager(String statcode) {
        AccountManager manager = getAccountManagerPtUserName(statcode);
        LDAPUser accountManager = null;
        try {
            accountManager = ldapService.findFirstUserBySamaAccount(manager.sAMAccountName);
        } catch (Exception e) {
            log.error("Unable to find account manager for statcode " + statcode);
        }

        return accountManager;
    }

    public List<Claim> recentOpenClaims(String statCodeGroupLogin, int currentLimit) {
        List<String> statCodes = getStatCodesFromRepo(statCodeGroupLogin);

        List<Claim> claims = new ArrayList<>();
        for (String statCode : statCodes) {
            claims.addAll(claimService.claimListOpenWithoutEventHandler(statCode));
        }
        claims = claimService.limitAndSortClaimList(currentLimit, claims);
        return claims;
    }

    protected List<String> getStatCodesFromRepo(String statCodeGroupLogin) {
        List<StatCodeDTO> clients = azureAdUserService.getStatCodesForGroup(statCodeGroupLogin);

        return clients.stream().map(StatCodeDTO::getStatCode).collect(Collectors.toList());
    }

    private String getMainCovers(Set<String> statCodes) {
        List<Product> productList = new ArrayList<>();

        statCodes.parallelStream().forEach(
                statCode -> productList.addAll(getProductsForAccount(statCode))
        );


        String products = productList
                .stream()
                .distinct()
                .sorted()
                .map(Product::description)
                .collect(Collectors.joining(","));
        if (productList.size() == 1)
            return "Product " + products;

        if (productList.size() > 1)
            return "Products " + products;

        return "";
    }
}
