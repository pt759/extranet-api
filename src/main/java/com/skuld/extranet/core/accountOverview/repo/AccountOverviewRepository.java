package com.skuld.extranet.core.accountOverview.repo;


import com.skuld.extranet.core.accountOverview.domain.AccountManager;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.spa.pages.domain.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountOverviewRepository extends DBDwhRepo {

    public AccountManager getAccountManager(String statcode) {
        return getJdbi().withExtension(AccountOverviewDao.class, dao -> dao.accountManager(statcode));
    }

    public AccountOverview getBrokerClientOverview(String statCode) {
        AccountOverview client = getJdbi().withExtension(AccountOverviewDao.class, dao -> dao.brokerClientOverview(statCode));

        if (client == null) {
            client = new AccountOverview();
        }
        return client;
    }

    public List<Product> getProductsForAccount(String statCode) {
        return getJdbi().withExtension(AccountOverviewDao.class, dao -> dao.getAccountProducts(statCode));
    }
}
