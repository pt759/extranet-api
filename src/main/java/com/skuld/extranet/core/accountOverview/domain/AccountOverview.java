package com.skuld.extranet.core.accountOverview.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverViewSummary;
import com.skuld.extranet.spa.pages.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountOverview {

    @JsonView(AccountView.Basic.class)
    private String clientName;

    @JsonView(AccountView.Basic.class)
    private String statCode;

    @JsonView(AccountView.Basic.class)
    private String openClaims;

    @JsonView(AccountView.Basic.class)
    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    private BigDecimal grossPremium;

    @JsonView(AccountView.Basic.class)
    private String currency;

    @JsonIgnore
    private List<Product> products;

    ClientOverViewSummary clientOverViewSummary;


    public AccountOverview(ResultSet r) throws SQLException {
        this.statCode = r.getString("stat_code");
        this.openClaims = r.getString("claims");
        this.grossPremium = r.getBigDecimal("paid_premium");
        this.currency = r.getString("currency");
    }

    @JsonProperty("products")
    @JsonView(AccountView.Basic.class)
    public String getProductsAsString() {
        return StringUtils.join(products, ", ");
    }

    public static class Mapper implements RowMapper<AccountOverview> {
        public AccountOverview map(ResultSet r, StatementContext ctx) throws SQLException {
            return new AccountOverview(r);
        }
    }
}
