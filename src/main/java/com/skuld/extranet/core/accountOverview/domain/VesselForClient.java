package com.skuld.extranet.core.accountOverview.domain;

public class VesselForClient extends BasicVessel {


    public String grossPremium;

    public VesselForClient(String vesselName, String imo, String grossTonnage, String vesselType, String grossPremium) {
        super(vesselName, imo, grossTonnage, vesselType);
        this.grossPremium = grossPremium;
    }
}
