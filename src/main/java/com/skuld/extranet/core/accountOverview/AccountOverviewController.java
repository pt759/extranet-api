package com.skuld.extranet.core.accountOverview;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.accountOverview.domain.AccountView;
import com.skuld.extranet.core.accountOverview.dto.SummaryHeaderDTO;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.Views;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.ldap.domain.LdapUserViews;
import com.skuld.extranet.security.service.ContentFilterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Account overview", description = "Account Overview for Extranet")
@CrossOrigin
@RestController
@RequestMapping("/accountoverview")

public class AccountOverviewController extends SkuldController {

    private final AccountOverviewService accountOverviewService;
    private final ContentFilterService contentFilterService;

    @Autowired
    public AccountOverviewController(AccountOverviewService accountOverviewService,
                                     ContentFilterService contentFilterService) {
        this.accountOverviewService = accountOverviewService;
        this.contentFilterService = contentFilterService;
    }

    @Operation(summary = "Summary of claims and vessels"
            , description = "List number of total vessels, all claims and main covers")
    @GetMapping(path = "/summary/{statcodegrouplogin}/"
            , produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity brokerSummary(@PathVariable(value = "statcodegrouplogin") String statCodeGroupLogin) {
        SummaryHeaderDTO summaryHeaderDTO = accountOverviewService.getBrokerSummaryHeader(statCodeGroupLogin);
        return buildResponseEntity(summaryHeaderDTO);
        //TODO merge with getAccountsForBroker
    }

    @Operation(summary = "Account Manager for statcode")
    @JsonView(LdapUserViews.LdapBasic.class)
    @GetMapping(path = "/accountmanager/{statcode}/",
            produces = {"application/json"})
    public ResponseEntity accountManager(@PathVariable(value = "statcode") String statcode) {
        LDAPUser accountManager = accountOverviewService.getAccountManager(statcode);
        return buildResponseEntity(accountManager);
    }


    @Operation(summary = "Client list for Broker")
    @GetMapping(path = "/clients/{statcodegrouplogin}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getAccountsForBroker(
            @PathVariable(value = "statcodegrouplogin") String statCodeGroupLogin) {
        List<AccountOverview> membersForLogin = accountOverviewService.getAllClientsForBroker(statCodeGroupLogin);

        return buildResponseEntity(contentFilterService.filterAccountOverview(membersForLogin));
    }


    @JsonView(AccountView.Basic.class)
    @Operation(summary = "Single client Overview")
    @GetMapping(
            path = "/client/{statcode}/",
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity getAccountOverview(@PathVariable(value = "statcode") String statCode) {
        AccountOverview account = accountOverviewService.getAccountOverview(statCode);

        return buildResponseEntity(account);
    }

    @Operation(summary = "Single client summary header")
    @GetMapping(
            path = "/client/summary/{statcode}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getAccountSummaryHeader(@PathVariable(value = "statcode") String statCode) {
        SummaryHeaderDTO summaryHeaderDTO = accountOverviewService.getClientSummaryHeader(statCode);

        return buildResponseEntity(summaryHeaderDTO);
    }

    @Operation(summary = "Recent Open claims list for Broker")
    @JsonView(Views.ClaimLatest.class)
    @GetMapping(
            path = "/claim/recent/{statcodegrouplogin}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity recentClaims(@PathVariable(value = "statcodegrouplogin") String statCodeGroupLogin,
                                       @RequestParam(value = "limit", required = false, defaultValue = "5") int limit
    ) {
        List<Claim> result = accountOverviewService.recentOpenClaims(statCodeGroupLogin, limit);
        return buildResponseEntity(result);
    }
}
