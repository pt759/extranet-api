package com.skuld.extranet.core.accountOverview.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class AccountCurrencySummary {
    private String currency;
    private String grossPremium;
}
