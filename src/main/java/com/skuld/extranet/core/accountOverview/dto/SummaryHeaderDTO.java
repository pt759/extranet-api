package com.skuld.extranet.core.accountOverview.dto;

import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SummaryHeaderDTO {
    private VesselsSummaryDTO vesselsHeader;
    private ClientClaimsSummaryDTO claimsHeader;


}
