package com.skuld.extranet.core.accountOverview.domain;


import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;


public class AccountManager {

    public String statCode;
    public String sAMAccountName;
    public String managerName;

    public AccountManager() {
    }

    public AccountManager(ResultSet r) throws SQLException {
        this.statCode = r.getString("stat_code");
        this.sAMAccountName = r.getString("windows_id");
        this.managerName = r.getString("manager");
    }

    public static class Mapper implements RowMapper<AccountManager> {
        public AccountManager map(ResultSet r, StatementContext ctx) throws SQLException {
            return new AccountManager(r);
        }
    }

}
