package com.skuld.extranet.core.accountOverview.repo;


import com.skuld.extranet.core.accountOverview.domain.AccountManager;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.spa.pages.domain.Product;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface AccountOverviewDao extends ClosableDao {

    @SqlQuery("select * from V_CLIENT_UW_MANAGER where stat_code = :stat_code")
    @RegisterRowMapper(AccountManager.Mapper.class)
    AccountManager accountManager(@Bind("stat_code") String statCode);

    @SqlQuery("select * from mv_broker_client_overview where stat_code = :statcode")
    @RegisterRowMapper(AccountOverview.Mapper.class)
    AccountOverview brokerClientOverview(@Bind("statcode") String statCode);

    @SqlQuery("select product from MV_CLIENT_PRODUCT where stat_code = :statcode")
    @RegisterRowMapper(Product.Mapper.class)
    List<Product> getAccountProducts(@Bind("statcode") String statCode);
}
