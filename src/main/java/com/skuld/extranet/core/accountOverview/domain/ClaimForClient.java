package com.skuld.extranet.core.accountOverview.domain;

public class ClaimForClient {
    public String vesselName;
    public String caseType;
    public String incidentDate;
    public String caseHandler;


    public ClaimForClient(String vesselName, String caseType, String incidentDate, String caseHandler) {
        this.vesselName = vesselName;
        this.caseType = caseType;
        this.incidentDate = incidentDate;
        this.caseHandler = caseHandler;
    }
}
