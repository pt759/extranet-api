package com.skuld.extranet.core.vesselSearch.repo;

import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.vesselSearch.domain.VesselCertificate;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface VesselSearchDao extends ClosableDao {

    @SqlQuery("select * from mv_listofvessels")
    @RegisterRowMapper(VesselSearch.Mapper.class)
    List<VesselSearch> allVesselsOnCover();

    @SqlQuery("select * from mv_listofvessels where skipsid = :vesselId")
    @RegisterRowMapper(VesselSearch.Mapper.class)
    List<VesselSearch> filterById(@Bind("vesselId") String vesselId);
    
    @SqlQuery("select * from mv_listofvessels where imo = :imo")
    @RegisterRowMapper(VesselSearch.Mapper.class)
    List<VesselSearch> filterByImo(@Bind("imo") String imo);

    @SqlQuery("select * from mv_listofvessels where skipsnavn like :name order by skipsnavn")
    @RegisterRowMapper(VesselSearch.Mapper.class)
    List<VesselSearch> filterByNameContains(@Bind("name") String name);

    @SqlQuery("select skipsid, certificate, cert_type from mv_listofvessels_cert where skipsid = :vesselId")
    @RegisterRowMapper(VesselCertificate.Mapper.class)
    List<VesselCertificate> certificatesForVessel(@Bind("vesselId") String vesselId);

    @SqlQuery("select skipsid, certificate, cert_type from mv_listofvessels_cert")
    @RegisterRowMapper(VesselCertificate.Mapper.class)
    List<VesselCertificate> certificates();

}
