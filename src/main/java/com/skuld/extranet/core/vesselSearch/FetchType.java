package com.skuld.extranet.core.vesselSearch;

public enum FetchType {

    IMO, NAME, VESSELID
}
