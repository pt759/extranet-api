package com.skuld.extranet.core.vesselSearch.domain;

import java.sql.ResultSet;
import java.util.List;

public class VesselSearchBuilder {
    private String vesselID;
    private String vesselName;
    private String note;
    private String member;
    private String imo;
    private String builtYear;
    private String grossTonnage;
    private String vesselType;
    private String flag;
    private String flagDescription;
    private String coverType;
    private String businessUnit;
    private String currentRegOwnerId;
    private String currentRegOwner;
    private List<VesselCertificate> certificates;
    private ResultSet r;

    public VesselSearchBuilder setVesselID(String vesselID) {
        this.vesselID = vesselID;
        return this;
    }

    public VesselSearchBuilder setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public VesselSearchBuilder setNote(String note) {
        this.note = note;
        return this;
    }

    public VesselSearchBuilder setMember(String member) {
        this.member = member;
        return this;
    }

    public VesselSearchBuilder setImo(String imo) {
        this.imo = imo;
        return this;
    }

    public VesselSearchBuilder setBuiltYear(String builtYear) {
        this.builtYear = builtYear;
        return this;
    }

    public VesselSearchBuilder setGrossTonnage(String grossTonnage) {
        this.grossTonnage = grossTonnage;
        return this;
    }

    public VesselSearchBuilder setVesselType(String vesselType) {
        this.vesselType = vesselType;
        return this;
    }

    public VesselSearchBuilder setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public VesselSearchBuilder setFlagDescription(String flagDescription) {
        this.flagDescription = flagDescription;
        return this;
    }

    public VesselSearchBuilder setCoverType(String coverType) {
        this.coverType = coverType;
        return this;
    }

    public VesselSearchBuilder setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
        return this;
    }

    public VesselSearchBuilder setCurrentRegOwnerId(String currentRegOwnerId) {
        this.currentRegOwnerId = currentRegOwnerId;
        return this;
    }

    public VesselSearchBuilder setCurrentRegOwner(String currentRegOwner) {
        this.currentRegOwner = currentRegOwner;
        return this;
    }

    public VesselSearchBuilder setCertificates(List<VesselCertificate> certificates) {
        this.certificates = certificates;
        return this;
    }

    public VesselSearchBuilder setR(ResultSet r) {
        this.r = r;
        return this;
    }

    public VesselSearch createVessel() {
        return new VesselSearch(vesselID, vesselName, note, member, imo, builtYear, grossTonnage, vesselType, flag, flagDescription, coverType, businessUnit, currentRegOwnerId, currentRegOwner, certificates);
    }
}