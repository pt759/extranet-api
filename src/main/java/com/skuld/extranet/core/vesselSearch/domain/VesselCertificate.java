package com.skuld.extranet.core.vesselSearch.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skuld.extranet.core.DatabaseObject;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VesselCertificate implements DatabaseObject {

    private String vesselID;
    private String certificate;
    private String certificateType;

    public VesselCertificate() {
    }

    public VesselCertificate(VesselCertificate vesselCertificate) {
        this.vesselID = vesselCertificate.getVesselID();
        this.certificate = vesselCertificate.getCertificate();
        this.certificateType = vesselCertificate.getCertificateType();
    }

    public VesselCertificate(String vesselID, String certificate, String certificateType) {
        this.vesselID = vesselID;
        this.certificate = certificate;
        this.certificateType = certificateType;
    }

    public VesselCertificate(ResultSet r) throws SQLException {
        this.vesselID = r.getString("SKIPSID");
        this.certificate = r.getString("CERTIFICATE");
        this.certificateType = r.getString("CERT_TYPE");
    }

    public static class Mapper implements RowMapper<VesselCertificate> {
        public VesselCertificate map(ResultSet r, StatementContext ctx) throws SQLException {
            return new VesselCertificate(r);
        }
    }

    @JsonIgnore
    public String getVesselID() {
        return vesselID;
    }

    public void setVesselID(String vesselID) {
        this.vesselID = vesselID;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }


    @Override
    public String toString() {
        return vesselID + ";" + certificate + ";" + certificateType;
    }
}
