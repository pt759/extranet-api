package com.skuld.extranet.core.vesselSearch.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skuld.extranet.core.DatabaseObject;
import lombok.Data;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties({"vesselId", "coverType"})
public class VesselSearch implements DatabaseObject {


    String vesselID;
    String vesselName;
    String note;
    String member;
    String imo;
    String builtYear;
    String grossTonnage;
    String vesselType;
    String flag;
    String flagDescription;
    String coverType;
    String businessUnit;
    String currentRegOwnerId;
    String currentRegOwner;
    List<VesselCertificate> certificates;

    public VesselSearch(String vesselID) {
        this.vesselID = vesselID;
    }

    public VesselSearch(ResultSet r) throws SQLException {
        this.vesselID = r.getString("skipsid");
        this.vesselName = r.getString("skipsnavn");
        this.note = r.getString("note");
        this.member = r.getString("navn");
        this.imo = r.getString("imo");
        this.builtYear = r.getString("yearbuilt");
        this.grossTonnage = r.getString("gt");
        this.vesselType = r.getString("vesselgroup");
        this.flagDescription = r.getString("flag");
        this.businessUnit = r.getString("syndicate");

        // Do we need these fields?
//        this.flag = r.getString("flag");
        this.currentRegOwnerId = r.getString("current_reg_owner_id");
        this.currentRegOwner = r.getString("registered_owner");
        this.coverType = "cover_type";
        this.certificates = new ArrayList<>();

    }

    public static class Mapper implements RowMapper<VesselSearch> {
        public VesselSearch map(ResultSet r, StatementContext ctx) throws SQLException {
            return new VesselSearch(r);
        }
    }


    @JsonIgnore
    public String getVesselID() {
        return vesselID;
    }

    @JsonIgnore
    public String getFlag() {
        return flag;
    }


    @JsonIgnore
    public String getCurrentRegOwnerId() {
        return currentRegOwnerId;
    }



    public VesselSearch() {

    }

    public VesselSearch(String vesselID, String vesselName, String imo) {
        this.vesselID = vesselID;
        this.vesselName = vesselName;
        this.imo = imo;
    }

    public VesselSearch(String vesselID, String vesselName, String note, String member, String imo, String builtYear, String grossTonnage, String vesselType, String flag, String flagDescription, String coverType, String businessUnit, String currentRegOwnerId, String currentRegOwner, List<VesselCertificate> certificates) {
        this.vesselID = vesselID;
        this.vesselName = vesselName;
        this.note = note;
        this.member = member;
        this.imo = imo;
        this.builtYear = builtYear;
        this.grossTonnage = grossTonnage;
        this.vesselType = vesselType;
        this.flag = flag;
        this.flagDescription = flagDescription;
        this.coverType = coverType;
        this.businessUnit = businessUnit;
        this.currentRegOwnerId = currentRegOwnerId;
        this.currentRegOwner = currentRegOwner;
        this.certificates = certificates;
    }

    @Override
    public String toString() {
        return vesselID + ";" + vesselName + ";" + imo;
    }
}
