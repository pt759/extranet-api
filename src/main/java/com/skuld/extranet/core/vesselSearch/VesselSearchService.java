package com.skuld.extranet.core.vesselSearch;

import com.skuld.extranet.core.vesselSearch.domain.VesselCertificate;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import com.skuld.extranet.core.vesselSearch.repo.VesselSearchRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VesselSearchService {
    final static Logger logger = LoggerFactory.getLogger(VesselSearchService.class);

    private List<VesselCertificate> masterCertList = null;

    @Autowired
    VesselSearchRepository vesselRepo;
    private final int limitOfVesselsForFetchingCertFromDB = 25;


    public VesselSearchService(VesselSearchRepository repo) {
        this.vesselRepo = repo;
    }

    public VesselSearchService() {
    }

    protected VesselSearchRepository getVesselRepo() {
        return vesselRepo;
    }

    public void setVesselRepo(VesselSearchRepository vesselRepo) {
        this.vesselRepo = vesselRepo;
    }

    public List<VesselSearch> allVessels() {
        List<VesselSearch> vessels = vesselRepo.getVesselsFromRepo();
        return vessels;
    }


    public List<VesselSearch> filterVesselsBy(String query, FetchType type) {
        long l = System.currentTimeMillis();
        logger.info("filterVesselsBy " + type + " with " + query);

        List<VesselSearch> vessels = new ArrayList<>();
        switch (type) {
            case IMO:
                vessels = vesselRepo.getVesselByImoFromRepo(query);
                break;
            case NAME:
                if (StringUtils.isNotEmpty(query)) {
                    vessels = vesselRepo.getVesselsByNameFromRepo(query);
                    vessels = equalToQueryFirstInList(query, vessels);
                }
                break;
            case VESSELID:
                vessels = vesselRepo.getVesselsByIdFromRepo(query);
                break;
            default:
        }
        if (vessels.size() > 0) {
            setCertificatesFor(vessels);
        }

        long l2 = System.currentTimeMillis();
        logger.info("Found number of vessels with certificates: " + vessels.size() + " in " + (l2 - l) + " ms");
        return vessels;
    }

    protected List<VesselSearch> equalToQueryFirstInList(String query, List<VesselSearch> vessels) {
        if (vessels.size() > 0) {

            return vessels.stream().sorted(hitsFirst(query)).collect(Collectors.toList());
        } else {
            return vessels;
        }
    }

    private Comparator<? super VesselSearch> hitsFirst(String query) {
        return (v1, v2) -> query.equalsIgnoreCase(v1.getVesselName()) ? -1 : query.equalsIgnoreCase(v2.getVesselName()) ? 1 : 0;
    }


    protected void setCertificatesFor(List<VesselSearch> vessels) {
        List<VesselCertificate> certificates;
        masterCertList = null;// Only Cache this for every hit
        for (VesselSearch vessel : vessels) {
            if (vessels.size() < limitOfVesselsForFetchingCertFromDB) {
                certificates = vesselRepo.getCertificatesFromRepoFor(vessel);
            } else {
                certificates = findCertificatesForVesselFromCache(vessel);
            }
            splitMLCCertificates(certificates);
            vessel.setCertificates(certificates);
        }
    }

    protected List<VesselCertificate> findCertificatesForVesselFromCache(VesselSearch vessel) {
        if (masterCertList == null) {
            masterCertList = vesselRepo.getCertificatesFromRepo();
        }

        List<VesselCertificate> certificatesForVessel = new ArrayList<>();
        for (VesselCertificate vesselCertificate : masterCertList) {
            if (vessel.getVesselID().equals(vesselCertificate.getVesselID())) {
                VesselCertificate currentCert = new VesselCertificate(vesselCertificate);
                certificatesForVessel.add(currentCert);
            }
        }
        return certificatesForVessel;
    }


    // TODO Remove BR from the view code.
    // We're sending <BR> from the database View.
    public void splitMLCCertificates(List<VesselCertificate> list) {
        List<VesselCertificate> mlcList = new ArrayList<>();
        VesselCertificate mlc = null;
        String txtBR = "<BR>";
        String certTypeMLC = "MLC";

        for (VesselCertificate certificate : list) {
            if (certificate.getCertificateType().equals(certTypeMLC) && certificate.getCertificate().contains(txtBR)) {

                mlc = new VesselCertificate();
                mlc.setVesselID(certificate.getVesselID());
                mlc.setCertificateType(certTypeMLC);
                String mlcPartA = certificate.getCertificate();
                String mlcPartTwo = mlcPartA.substring(mlcPartA.indexOf(txtBR)).replace(txtBR, "");
                mlc.setCertificate(mlcPartTwo);
                certificate.setCertificate(mlcPartA.substring(0, mlcPartA.indexOf(txtBR)));
                mlcList.add(mlc);
            }
        }

        list.addAll(mlcList);
    }

    public List<VesselCertificate> getMasterCertList() {
        return masterCertList;
    }

    public void setMasterCertList(List<VesselCertificate> masterCertList) {
        this.masterCertList = masterCertList;
    }
}
