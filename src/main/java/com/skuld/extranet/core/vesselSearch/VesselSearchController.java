package com.skuld.extranet.core.vesselSearch;

import com.skuld.common.SkuldController;
import com.skuld.extranet.core.VesselSearchFromTIA.service.VesselSearchTiaService;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.skuld.extranet.global.RESTendPoints.vesselSearch;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Log4j2
@RestController
@RequestMapping(vesselSearch)
@CrossOrigin
public class VesselSearchController extends SkuldController {

    @Autowired
    VesselSearchService vesselService;

    @Autowired
    VesselSearchTiaService vesselSearchTiaService;


    @RequestMapping(
            path = "/list",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity vessels() {
        log.debug("VesselResource (all):");
        List<VesselSearch> vessels = vesselService.allVessels();
        log.debug("size:" + vessels.size());
        return buildResponseEntity(vessels);
    }

    @RequestMapping(
            path = "/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity vesselWithCertificate(@RequestParam Map<String, String> requestParams) {
        String query = requestParams.get("query");
        return buildResponseEntity(vesselSearchTiaService.filterVesselBy(query));
    }

}
