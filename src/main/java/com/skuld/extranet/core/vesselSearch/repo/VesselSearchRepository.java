package com.skuld.extranet.core.vesselSearch.repo;

import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.core.vesselSearch.domain.VesselCertificate;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public class VesselSearchRepository extends DBDwhRepo {
    final static Logger logger = LoggerFactory.getLogger(VesselSearchRepository.class);


    public List<VesselSearch> getVesselsFromRepo() {
        List<VesselSearch> vessels  = getJdbi().withExtension(VesselSearchDao.class, dao -> dao.allVesselsOnCover());
        return vessels;
    }

    public List<VesselSearch> getVesselByImoFromRepo(String vesselImo) {
        List<VesselSearch> vessels = getJdbi().withExtension(VesselSearchDao.class, dao -> dao.filterByImo(vesselImo));
        return vessels;
    }

    public List<VesselSearch> getVesselsByNameFromRepo(String name) {
        List<VesselSearch> vessels = getJdbi().withExtension(VesselSearchDao.class, dao -> dao.filterByNameContains("%" + name.toUpperCase() + "%"));
        return vessels;
    }

    public List<VesselSearch> getVesselsByIdFromRepo(String vesselId) {
        List<VesselSearch> vessels = getJdbi().withExtension(VesselSearchDao.class, dao -> dao.filterById(vesselId));
        return vessels;
    }

    public List<VesselCertificate> getCertificatesFromRepo() {
        logger.info("Loading all certificates - Start");
        List<VesselCertificate> certificates = getJdbi().withExtension(VesselSearchDao.class, dao -> dao.certificates());
        logger.info("Load all certificates - Done");
        return certificates;
    }

    public List<VesselCertificate> getCertificatesFromRepoFor(VesselSearch vessel) {
        logger.info("Load certificates for " + vessel.getVesselID() + ":" + vessel.getVesselName());
        List<VesselCertificate> certificates = getJdbi().withExtension(VesselSearchDao.class, dao -> dao.certificatesForVessel(vessel.getVesselID()));
        return certificates;
    }

}
