package com.skuld.extranet.core.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SkuldAmounts {

    public static final int amountDecimal = 2;

    // https://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static BigDecimal round(BigDecimal value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = value;
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd;
    }

    public static Double roundSafe(Double amount, int places) {
        return amount != null ? round(amount, places) : new Double(0);
    }

    public static BigDecimal roundSafe(BigDecimal amount, int places) {
        return amount != null ? round(amount, places) : new BigDecimal(0);
    }

    public static BigDecimal nvl(BigDecimal bigDecimal) {

        if (bigDecimal==null){
            return BigDecimal.ZERO;
        }

        return bigDecimal;
    }
}
