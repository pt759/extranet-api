package com.skuld.extranet.core.common.domain;


import java.util.List;

public class CodeValueWithList extends CodeValue {

    private List list;

    public CodeValueWithList(CodeValue codeValue) {
        super(codeValue.getCode(), codeValue.getValue(), codeValue.getDescription());
    }

    public CodeValueWithList(CodeValue codeValue, List list) {
        this(codeValue);
        this.list = list;
    }


    public CodeValueWithList(String code, String description, String value) {
        super(code, description, value);
    }

    public CodeValueWithList(String code, String value) {
        super(code, value);
    }

    public List getList() {
        return list;
    }

    public CodeValueWithList setList(List list) {
        this.list = list;
        return this;
    }
}
