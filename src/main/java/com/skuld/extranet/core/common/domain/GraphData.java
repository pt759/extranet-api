package com.skuld.extranet.core.common.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GraphData {

    String label;
    Integer value;

    public GraphData() {
    }

    public GraphData(String label, Integer value) {
        this.label = label;
        this.value = value;
    }

    public GraphData(ResultSet rs) throws SQLException {
        this.label = rs.getString("label");
        this.value = rs.getInt("value");
    }

    public String getLabel() {
        return label;
    }

    public GraphData setLabel(String label) {
        this.label = label;
        return this;
    }

    public Integer getValue() {
        return value;
    }

    public GraphData setValue(Integer value) {
        this.value = value;
        return this;
    }

    public static class Mapper implements RowMapper<GraphData> {
        @Override
        public GraphData map(ResultSet rs, StatementContext ctx) throws SQLException {
            return new GraphData(rs);
        }
    }
}
