package com.skuld.extranet.core.common;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class SkuldWebUtil {

    final static Logger logger = LoggerFactory.getLogger(SkuldWebUtil.class);

    public static String getImageFilePath(String path, Object thisClass) throws URISyntaxException, MalformedURLException {
        return urlDecode(thisClass.getClass().getResource(path).toURI().toURL().getFile());
    }

    /*
    Assuming endpoint is a downloadable resource which will be wrapped
    */
    public String urlContentAsBase64String(String uri) {
        logger.info("Url top open:" + uri);
        URL url;
        try {
            url = new URL(uri.replace(' ', '+'));
            URLConnection urlConnection = url.openConnection();
            InputStream inputStream = urlConnection.getInputStream();
            byte[] bytes = IOUtils.toByteArray(inputStream);
            String text = Base64.encodeBase64String(bytes);
            return text;
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException: " + e.getMessage());
        } catch (IOException e) {
            logger.error("IOException: " + e.getMessage());
        }
        return "";

    }

    public static int converterStringToIntWithDefaultValue(String value, String defaultValue) {
        int listLimit;
        try {
            if (StringUtils.isNumeric(value)) {
                listLimit = Integer.valueOf(value);
            } else {
                listLimit = Integer.valueOf(defaultValue);
            }

        } catch (Exception e) {
            listLimit = Integer.valueOf(defaultValue);
        }
        return listLimit;
    }

    public static String urlEncode(String text) {
        String result;
        result = URLEncoder.encode(text, StandardCharsets.UTF_8);
        return result;
    }

    public static String urlDecode(String text) {
        String result;
        result = URLDecoder.decode(text, StandardCharsets.UTF_8);
        return result;
    }
}
