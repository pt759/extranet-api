package com.skuld.extranet.core.common;

import java.time.LocalDate;

public class SkuldLocalDate {

    LocalDate date;

    public SkuldLocalDate(LocalDate date) {
        this.date = date;
    }

    public String getDateFormatted() {
        return date != null ? SkuldFormatter.formatLocaldateFor(date) : "";
    }

    public LocalDate getDate() {
        return date != null ? date : null;
    }

}
