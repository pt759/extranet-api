package com.skuld.extranet.core.common;

/*
* All DAO Interfaces should extend this class
*/
public interface ClosableDao {

    /**
     * close with no args is used to close the connection
     */
//    void close();
}
