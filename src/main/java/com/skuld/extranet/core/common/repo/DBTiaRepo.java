package com.skuld.extranet.core.common.repo;


import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;

public class DBTiaRepo {

    Jdbi jdbi;

    @Autowired
    public void jdbi(DataSource datasource) {
        this.jdbi = Jdbi.create(datasource);
        this.jdbi.installPlugin(new SqlObjectPlugin());
    }

    public Jdbi getJdbi() {
        return jdbi;
    }

    public void setJdbi(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

}
