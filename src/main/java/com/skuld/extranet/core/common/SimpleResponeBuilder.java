package com.skuld.extranet.core.common;

import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.TEXT_PLAIN;

public class SimpleResponeBuilder {

    public static ResponseEntity buildResponseEntity(Object obj) {
        if (obj != null) {
            return ResponseEntity
                    .status(OK)
                    .body(obj);
        } else {
            return ResponseEntity
                    .status(OK)
                    .contentType(TEXT_PLAIN)
                    .build();
        }
    }

}
