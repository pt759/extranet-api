package com.skuld.extranet.core.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class SkuldSqlHelper {

    final static Logger logger = LoggerFactory.getLogger(SkuldSqlHelper.class);

    public static LocalDate fromSqlDate(Date someDate) {

        if (someDate != null) {
            return someDate.toLocalDate();
        } else {
            return null;
        }
    }

    public static String nvl(String value, String replace) {
        return value != null ? value : replace;
    }

    public static String formatSqlDate(String sqlDate){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = null;
        try {
            java.util.Date date = sf.parse(sqlDate);
            formattedDate = new SimpleDateFormat("dd.MM.YYYY").format(date);
        } catch (ParseException e) {
            formattedDate = "";
            logger.error("Policy first start date has invalid format: " + sqlDate);
        }

        return formattedDate;


    }
}
