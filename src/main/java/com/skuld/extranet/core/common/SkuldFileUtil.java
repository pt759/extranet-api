package com.skuld.extranet.core.common;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class SkuldFileUtil {

    final static Logger logger = LoggerFactory.getLogger(SkuldFileUtil.class);

    public File writeFile(String filename, ByteArrayOutputStream byteArrayOutputStream) {
        File file = new File(filename);
        try (FileOutputStream fop = new FileOutputStream(file)) {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            fop.write(byteArrayOutputStream.toByteArray());
            fop.flush();
            fop.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Error:" + e.getMessage());
        }
        return file;
    }

    public void writeFile(String filename, InputStream input) {
        try {
            File targetFile = new File(filename);
            FileUtils.copyInputStreamToFile(input, targetFile);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public static File writeBase64ToFile(String base64EncodedString, String filename) {
        try {
            byte[] data = Base64.decodeBase64(base64EncodedString);
            File file = new File(filename);
            FileUtils.writeByteArrayToFile(file, data);
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
