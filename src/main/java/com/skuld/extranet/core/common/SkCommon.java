package com.skuld.extranet.core.common;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;

public interface SkCommon {

    String todo = "<todo>";
    String unknown = "<Unknown>";

    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    DecimalFormat decimalTwo = new DecimalFormat("##.00");

    String group_others_code = "OTHERS";
    String group_others_desc = "Others";

     String mimeTypePdf = "application/pdf";
     String mimeTypeFile = "application/octet-stream";
     String mimeTypeWord = "application/msword'";

    String formatPDF = ".pdf";
    String formatWord = ".doc";

}
