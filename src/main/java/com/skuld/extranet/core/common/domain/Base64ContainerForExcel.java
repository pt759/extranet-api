package com.skuld.extranet.core.common.domain;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.excel.ExcelListConverter;
import com.skuld.extranet.global.Global;

public class Base64ContainerForExcel extends Base64Container {

    public static Base64Container getBase64ContainerWithExcel(ExcelListConverter excel) {
        return getBase64ContainerWithExcel(excel.getExcelAsBase64Text(), excel.getFilename());
    }

    public static Base64Container getBase64ContainerWithExcel(String base64Text, String filename) {
        if (base64Text != null) {
            Base64Container result = new Base64ContainerForExcel();
            return Base64Container.builder()
                    .data(base64Text)
                    .filename(filename)
                    .contentType(Global.APPLICATION_EXCEL_VALUE)
                    .charSet(Global.CHARSET_UTF8)
                    .build();
        }
        return null;
    }
}
