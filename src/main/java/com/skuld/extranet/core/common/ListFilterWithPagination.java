package com.skuld.extranet.core.common;

public class ListFilterWithPagination {

    protected int page;
    protected int pageSize;

    public ListFilterWithPagination() {
        page = 0;
        pageSize = 0;
    }

    public boolean paginationActive() {
        return 0 < pageSize;
    }

    public long skipRows() {
        return pageSize * page;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }


    public ListFilterWithPagination setPage(int page) {
        this.page = page;
        return this;
    }

    public ListFilterWithPagination setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }
}
