package com.skuld.extranet.core.common;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static com.skuld.extranet.core.common.SkCommon.dateFormatter;

public class SkuldFormatter {

    public static String formatTimeStamp(LocalDateTime time) {
        return formatTimeStamp(time, dateFormatter);
    }

    public static String formatTimeStamp(LocalDateTime timeStamp, DateTimeFormatter formatter) {
        return timeStamp == null ? "" : timeStamp.format(formatter);
    }

    public static String formatLocaldateFor(LocalDate localDate) {
        return localDate != null ? localDate.format(dateFormatter) : "";
    }

    public static String formatBigDecimalWithThousandsSeparator(BigDecimal number){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');

        DecimalFormat formatter = new DecimalFormat("#,##0", symbols);

        return formatter.format(number);
    }
}
