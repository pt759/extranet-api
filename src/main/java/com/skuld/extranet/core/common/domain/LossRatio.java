package com.skuld.extranet.core.common.domain;

import java.util.List;

public class LossRatio {

    List<CodeValue> currentYear;
    List<CodeValue> fiveYear;


    public List<CodeValue> getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(List<CodeValue> currentYear) {
        this.currentYear = currentYear;
    }

    public List<CodeValue> getFiveYear() {
        return fiveYear;
    }

    public void setFiveYear(List<CodeValue> fiveYear) {
        this.fiveYear = fiveYear;
    }
}
