package com.skuld.extranet.core.common;

import java.time.LocalDate;

public class PolicyService {

    private Integer currentPolicyYear;

    public Integer currentPolicyYear() {
        if (currentPolicyYear == null) {
            currentPolicyYear = policyYear(LocalDate.now());
        }
        return currentPolicyYear;
    }

    public String currentPolicyYearAsString() {
        return currentPolicyYear().toString();
    }

    public String policyYearAsString(LocalDate date) {
        LocalDate currentPolicyYear = date.minusDays(51);
        return String.valueOf(currentPolicyYear.getYear());
    }

    public int policyYear(LocalDate date) {
        LocalDate currentPolicyYear = date.minusDays(51);
        return currentPolicyYear.getYear();
    }

    public boolean isValidFiveYearPolicyYear(String year) {
        return (currentPolicyYear() - 5) <= Integer.valueOf(year) && Integer.valueOf(year) < currentPolicyYear();

    }

    public PolicyService setCurrentPolicyYear(Integer currentPolicyYear) {
        this.currentPolicyYear = currentPolicyYear;
        return this;
    }

}
