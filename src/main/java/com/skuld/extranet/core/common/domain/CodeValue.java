package com.skuld.extranet.core.common.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.skuld.extranet.core.common.SkCommon;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class CodeValue {

    public static final String keyValue = "label";
    public static final String keyDescription = "hint";

    String code;

    @JsonProperty(keyValue)
    String value;

    @JsonProperty(keyDescription)
    String description;

    public CodeValue() {
    }

    public CodeValue(String code, String value, String description) {
        this(code, value);
        this.description = description;
    }

    public CodeValue(String code, String value) {
        this.code = code;
        this.value = value;
        this.description = "";
    }

    public CodeValue(ResultSet rs) throws SQLException {
        this.code = rs.getString("code");
        this.value = rs.getString("value");
        this.description = rs.getString("description");


    }

    public static class Mapper implements RowMapper<CodeValue> {
        @Override
        public CodeValue map(ResultSet rs, StatementContext ctx) throws SQLException {
            return new CodeValue(rs);
        }
    }


    @Override
    public String toString() {
        return "CodeValue{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    /*
    Limits (limit) a list to @param(limit) items, and adds the value of the
     remaining items into the last item in the list.
     */
    public static List<CodeValue> groupListWithLimitOf(int limit, List<CodeValue> list) {
        List<CodeValue> mainGroupList = list;
        if (limit < list.size()) {

            // all items
            mainGroupList = list.subList(0, limit);
            CodeValue lastCodeValue = mainGroupList.get(mainGroupList.size() - 1);

            List<CodeValue> otherInList = list.subList(limit, list.size());

            Integer value = Integer.valueOf(lastCodeValue.getValue());
            for (CodeValue item : otherInList) {
                value += Integer.valueOf(item.getValue());
            }

            lastCodeValue.setValue(value.toString());
            lastCodeValue.setCode(SkCommon.group_others_code);
            lastCodeValue.setDescription(SkCommon.group_others_desc);
        }
        return mainGroupList;

    }

    public String getCode() {
        return code;
    }

    public CodeValue setCode(String code) {
        this.code = code;
        return this;
    }

    public String getValue() {
        return value;
    }

    public CodeValue setValue(String value) {
        this.value = value;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CodeValue setDescription(String description) {
        this.description = description;
        return this;
    }
}