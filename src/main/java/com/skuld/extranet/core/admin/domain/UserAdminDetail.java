package com.skuld.extranet.core.admin.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.domain.UserView;
import com.skuld.extranet.core.user.dto.StatCodeDTO;

import java.util.ArrayList;
import java.util.List;

public class UserAdminDetail {

    @JsonView({UserView.AzureGroupAdmin.class})
    private AzureAdGroup azureGroup;

    @JsonView({UserView.AzureGroupAdmin.class})
    private List<AzureAdUser> users;

    @JsonView({UserView.AzureGroupAdmin.class})
    private List<StatCodeDTO> statCodes;

    public AzureAdGroup getAzureGroup() {
        return azureGroup;
    }

    public UserAdminDetail setAzureGroup(AzureAdGroup azureGroup) {
        this.azureGroup = azureGroup;
        return this;
    }

    public UserAdminDetail() {
        this.users = new ArrayList<>();
    }

    public List<AzureAdUser> getUsers() {
        return users;
    }

    public UserAdminDetail setUsers(List<AzureAdUser> users) {
        this.users = users;
        return this;
    }

    public List<StatCodeDTO> getStatCodes() {
        return statCodes;
    }

    public UserAdminDetail setStatCodes(List<StatCodeDTO> statCodes) {
        this.statCodes = statCodes;
        return this;
    }
}
