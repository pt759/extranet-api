package com.skuld.extranet.core.admin;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.extranet.core.admin.domain.UserAdminDetail;
import com.skuld.extranet.core.admin.service.UserAdminService;
import com.skuld.extranet.core.user.domain.BusinessUnit;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import com.skuld.extranet.core.user.domain.UserView;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "User Admin")
@RestController
@RequestMapping("/admin/")
public class UserAdminController extends SkuldController {
    final static Logger logger = LoggerFactory.getLogger((AzureAdUserService.class));

    @Autowired
    private UserAdminService service;


    @JsonView({UserView.AzureGroupAdmin.class})
    @Operation(summary = "Get Business Units and Broker Group")
    @RequestMapping(
            path = "/bu/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity businessUnitsAndBrokerGroup() {
        List<BusinessUnit> list = service.getBusinessUnits();
        return buildResponseEntity(list);
    }

    @JsonView({UserView.AzureGroupAdmin.class})
    @Operation(summary = "Get BU's and Brokers stat code groups")
    @RequestMapping(
            path = "/department/{deptNo}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity businessUnitsGroups(
            @PathVariable(value = "deptNo") String deptNo) {
        List<StatCodeGroup> list = service.getBusinessUnitStataCodeGroups(deptNo);
        return buildResponseEntity(list);
    }
    
    @JsonView({UserView.AzureGroupAdmin.class})
    @Operation(summary = "Members and stat codes for StatCodeGroupLogin")
    @RequestMapping(
            path = "/group/{groupName}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity businessUnitsAndBrokerGroup(
            @PathVariable(value = "groupName") String groupName) {
        UserAdminDetail entry = service.getUserAdminDetail(groupName);
        return buildResponseEntity(entry);
    }

    @Deprecated
    @JsonView({UserView.AzureGroupAdmin.class})
    @Operation(summary = "Members for StatCodeGroupLogin")
    @RequestMapping(
            path = "/group/{groupName}/members",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity members(@PathVariable(value = "groupName") String groupName) {
        UserAdminDetail entry = service.getUserAdminDetail(groupName);
        return buildResponseEntity(entry);
    }


}
