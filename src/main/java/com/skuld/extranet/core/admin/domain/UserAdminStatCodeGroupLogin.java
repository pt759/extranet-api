package com.skuld.extranet.core.admin.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.accountOverview.domain.AccountManager;
import com.skuld.extranet.core.user.domain.UserView;
import com.skuld.extranet.core.user.dto.StatCodeDTO;

public class UserAdminStatCodeGroupLogin {

    private StatCodeDTO statCode;
    private AccountManager accountManager;

    @JsonView({UserView.AzureGroupAdmin.class})
    public String getAccountManager(){
        return accountManager != null ? accountManager.managerName : "";
    }

    @JsonView({UserView.AzureGroupAdmin.class})
    public String getStatCode(){
        return statCode != null ? statCode.getStatCode() : "";
    }

    @JsonView({UserView.AzureGroupAdmin.class})
    public String getStatCodeName(){
        return statCode != null ? statCode.getStatCodeName() : "";
    }


    public UserAdminStatCodeGroupLogin setStatCode(StatCodeDTO statCode) {
        this.statCode = statCode;
        return this;
    }


    public UserAdminStatCodeGroupLogin setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
        return this;
    }
}
