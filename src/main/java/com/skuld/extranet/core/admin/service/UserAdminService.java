package com.skuld.extranet.core.admin.service;

import com.skuld.extranet.core.accountOverview.AccountOverviewService;
import com.skuld.extranet.core.accountOverview.domain.AccountManager;
import com.skuld.extranet.core.admin.domain.UserAdminDetail;
import com.skuld.extranet.core.admin.domain.UserAdminStatCodeGroupLogin;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.domain.BusinessUnit;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.service.AzureAdGroupService;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import com.skuld.extranet.core.user.service.StatCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserAdminService {

    final static Logger logger = LoggerFactory.getLogger(UserAdminService.class);


    private AzureAdGroupService azureGroupService;
    private AzureAdUserService azureUserService;
    private AccountOverviewService accountOverviewService;
    private StatCodeService statCodeService;

    @Autowired
    public UserAdminService(AzureAdGroupService azureGroupService,
                            AzureAdUserService azureUserService,
                            AccountOverviewService accountOverviewService,
                            StatCodeService statCodeService) {
        this.azureGroupService = azureGroupService;
        this.azureUserService = azureUserService;
        this.accountOverviewService = accountOverviewService;
        this.statCodeService = statCodeService;
    }

    public List<BusinessUnit> getBusinessUnits() {
        return statCodeService.getBusinessUnits();
    }

    public List<UserAdminStatCodeGroupLogin> getUserAdminStatCodeGroupLogin(String statCodeGroupLogin) {
        List<UserAdminStatCodeGroupLogin> members = new ArrayList<>();

        List<StatCodeDTO> statCodesForGroup = azureUserService.getStatCodesForGroup(statCodeGroupLogin);
        for (StatCodeDTO statCode : statCodesForGroup) {
            UserAdminStatCodeGroupLogin member = new UserAdminStatCodeGroupLogin();
            AccountManager accountManager = accountOverviewService.getAccountManagerPtUserName(statCode.getStatCode());
            member.setAccountManager(accountManager).setStatCode(statCode);
            members.add(member);
        }
        return members;
    }

    public UserAdminDetail getUserAdminDetail(String statcodegrouplogin) {
        AzureAdGroup groupByName = azureGroupService.getGroupByName(statcodegrouplogin);
        UserAdminDetail adminDetail = new UserAdminDetail();

        if (groupByName != null) {
            List<AzureAdUser> users = azureGroupService.getGroupMemberWithSkuldsById(groupByName.getGroupId());
            adminDetail.setAzureGroup(groupByName).setUsers(users);
        } else {
            logger.debug("Group " + statcodegrouplogin + "does not exist. Creating in Azure");
            StatCodeGroup statCodeGroup = statCodeService.getStatCodeGroup(statcodegrouplogin);
            groupByName = azureGroupService.createGroup(statcodegrouplogin, statCodeGroup.getGroupName());
            adminDetail.setAzureGroup(groupByName);
        }

        List<StatCodeDTO> statCodesForGroupCached = statCodeService.getStatCodesForGroupCached(statcodegrouplogin);
        if (statCodesForGroupCached != null && statCodesForGroupCached.size() > 0) {
            groupByName.setBroker(true);
            adminDetail.setStatCodes(statCodesForGroupCached);
        }
        return adminDetail;
    }

    public List<StatCodeGroup> getBusinessUnitStataCodeGroups(String deptNo) {
        List<StatCodeGroup> statCodeGroups = null;

        logger.info("Getting stat code groups for dept: " + deptNo);
        try {

            if (deptNo != null && Integer.parseInt(deptNo) >= 0) {
                switch (deptNo) {
                    case "0":
                        statCodeGroups = statCodeService.getBrokersStatCodeGroups();
                        break;
                    default:
                        statCodeGroups = statCodeService.getBusinessUnitStatCodeGroups(deptNo);
                        break;
                }
            }
        } catch (NumberFormatException e) {
            logger.error(e.getMessage());
            statCodeGroups = null;
        }
        return statCodeGroups;
    }

    public List<StatCodeGroup> getBrokerStataCodeGroups() {
        List<StatCodeGroup> statCodeGroups = statCodeService.getBrokersStatCodeGroups();
        return statCodeGroups;
    }

    public UserAdminService setAzureGroupService(AzureAdGroupService azureGroupService) {
        this.azureGroupService = azureGroupService;
        return this;
    }

    public UserAdminService setAzureUserService(AzureAdUserService azureUserService) {
        this.azureUserService = azureUserService;
        return this;
    }

    public UserAdminService setAccountOverviewService(AccountOverviewService accountOverviewService) {
        this.accountOverviewService = accountOverviewService;
        return this;
    }

    public UserAdminService setStatCodeService(StatCodeService statCodeService) {
        this.statCodeService = statCodeService;
        return this;
    }
}
