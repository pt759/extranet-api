package com.skuld.extranet.core.vessel.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.common.SkuldSqlHelper;
import com.skuld.extranet.spa.pages.PageEntityView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@AllArgsConstructor
@Builder
public class Vessel {


    @JsonView(VesselView.Basic.class)
    private String vesselId;

    @JsonProperty("imo")
    @JsonView(VesselView.Basic.class)
    private String imo_number;

    @JsonView(VesselView.Basic.class)
    private String vesselName;

    @JsonView(VesselView.Basic.class)
    private String vesselType;

    @JsonView(VesselView.Basic.class)
    private String premiumPaid;

    @JsonView({VesselView.List.class, VesselView.Entered.class})
    private String status;

    @JsonView(VesselView.List.class)
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate fromDate;

    @JsonView(VesselView.List.class)
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate toDate;

    @JsonView({VesselView.List.class, VesselView.Entered.class})
    private String products;

    @JsonView(VesselView.Detail.class)
    private String yearBuilt;
    @JsonView(VesselView.Detail.class)
    private String gt;

    @JsonView(VesselView.Detail.class)
    private String classification;

    @JsonView(VesselView.Detail.class)
    @JsonProperty("portOfRegistryDescription")
    public String getPortOfRegistryDescription() {
        if (StringUtils.isNotBlank(this.portOfRegistry)) {
            return this.portOfRegistry + ", " + this.getFlagState();
        } else {
            return this.getFlagState();
        }
    }


    @JsonView(PageEntityView.SearchListBroker.class)
    private String statCode;
    @JsonView(PageEntityView.SearchListBroker.class)
    private String statCodeName;

    @JsonIgnore
    private String vesselTypeCode;

    @JsonIgnore
    private String portOfRegistryCode;

    @JsonIgnore
    private String portOfRegistry;

    @JsonIgnore
    private String flagState;

    @JsonView({VesselView.List.class, VesselView.Entered.class})
    private String statCurrency;

    @JsonView({VesselView.List.class, VesselView.Entered.class})
    private String statCurrencyType;


    public static class Mapper implements RowMapper<Vessel> {
        public Vessel map(ResultSet r, StatementContext ctx) throws SQLException {
            return new Vessel(r);
        }
    }

    public Vessel() {
    }

    public Vessel(ResultSet r) throws SQLException {

        this.vesselId = r.getString("vessel_id");
        this.statCode = r.getString("stat_code");
        this.products = r.getString("product");
        this.imo_number = r.getString("imo_number");
        this.vesselName = r.getString("vessel_name");
        this.vesselTypeCode = r.getString("vessel_type_code");
        this.vesselType = r.getString("vessel_type");
        this.yearBuilt = r.getString("year_built");
        this.gt = r.getString("gt");
        this.portOfRegistryCode = r.getString("port_of_registry_code");
        this.portOfRegistry = r.getString("port_of_registry");
        this.flagState = r.getString("flag_state");
        this.classification = r.getString("classification");
        this.premiumPaid = r.getString("premium_paid");
        this.status = r.getString("status");
        this.fromDate = SkuldSqlHelper.fromSqlDate(r.getDate("policy_effective_start"));
        this.toDate = SkuldSqlHelper.fromSqlDate(r.getDate("policy_effective_end"));
        this.statCurrency = r.getString("currency");

    }


    public String getVesselId() {
        return vesselId;
    }

    public String getStatCode() {
        return statCode;
    }

    public String getProducts() {
        return products;
    }

    public String getImo_number() {
        return imo_number;
    }

    public String getVesselName() {
        return vesselName;
    }

    public String getVesselTypeCode() {
        return vesselTypeCode;
    }

    public String getVesselType() {
        return vesselType;
    }

    public String getYearBuilt() {
        return yearBuilt;
    }


    public String getGt() {
        return gt;
    }


    public String getPortOfRegistryCode() {
        return portOfRegistryCode;
    }

    public String getPortOfRegistry() {
        return portOfRegistry;
    }

    public String getClassification() {
        return classification;
    }

    public String getPremiumPaid() {
        return premiumPaid;
    }

    public String getStatus() {
        return status;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }


    public String getFlagState() {
        return flagState;
    }


    public Vessel setVesselId(String vesselId) {
        this.vesselId = vesselId;
        return this;
    }

    public Vessel setImo_number(String imo_number) {
        this.imo_number = imo_number;
        return this;
    }

    public Vessel setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public Vessel setVesselType(String vesselType) {
        this.vesselType = vesselType;
        return this;
    }

    public Vessel setPremiumPaid(String premiumPaid) {
        this.premiumPaid = premiumPaid;
        return this;
    }

    public Vessel setStatus(String status) {
        this.status = status;
        return this;
    }

    public Vessel setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public Vessel setToDate(LocalDate toDate) {
        this.toDate = toDate;
        return this;
    }

    public Vessel setProducts(String products) {
        this.products = products;
        return this;
    }

    public Vessel setYearBuilt(String yearBuilt) {
        this.yearBuilt = yearBuilt;
        return this;
    }

    public Vessel setGt(String gt) {
        this.gt = gt;
        return this;
    }

    public Vessel setClassification(String classification) {
        this.classification = classification;
        return this;
    }

    public Vessel setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public Vessel setVesselTypeCode(String vesselTypeCode) {
        this.vesselTypeCode = vesselTypeCode;
        return this;
    }

    public Vessel setPortOfRegistryCode(String portOfRegistryCode) {
        this.portOfRegistryCode = portOfRegistryCode;
        return this;
    }

    public Vessel setPortOfRegistry(String portOfRegistry) {
        this.portOfRegistry = portOfRegistry;
        return this;
    }

    public Vessel setFlagState(String flagState) {
        this.flagState = flagState;
        return this;
    }

    public String getStatCodeName() {
        return statCodeName;
    }

    public Vessel setStatCodeName(String statCodeName) {
        this.statCodeName = statCodeName;
        return this;
    }

    public String getStatCurrency() {
        return statCurrency;
    }

    public Vessel setStatCurrency(String statCurrency) {
        this.statCurrency = statCurrency;
        return this;
    }

    public String getStatCurrencyType() {
        return statCurrencyType;
    }

    public Vessel setStatCurrencyType(String statCurrencyType) {
        this.statCurrencyType = statCurrencyType;
        return this;
    }

    public static Vessel from(VesselListDTO vesselListDTO) {
        return Vessel.builder()
                .vesselId(vesselListDTO.getVesselId())
                .imo_number(vesselListDTO.getImo())
                .statCode(vesselListDTO.getStatCode())
                .statCodeName(vesselListDTO.getStatCodeName())
                .vesselName(vesselListDTO.getVesselName())
                .vesselType(vesselListDTO.getVesselType())
                .premiumPaid(vesselListDTO.getPremiumPaid())
                .status(vesselListDTO.getStatus())
                .fromDate(vesselListDTO.getFromDate())
                .toDate(vesselListDTO.getToDate())
                .products(vesselListDTO.getProducts())
                .build();
    }

}
