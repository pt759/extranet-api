package com.skuld.extranet.core.vessel.repo;

import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.vessel.domain.GrossPremium;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselPolicyLine;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface VesselDao extends ClosableDao {

    @RegisterRowMapper(Vessel.Mapper.class)
    @SqlQuery("select * from mv_vessel where vessel_id = :vessel_id and stat_code = :stat_code")
    Vessel find(@Bind("vessel_id") String vesselId,@Bind("stat_code") String statCode);

    @SqlQuery("select * from mv_vessel where stat_code = :stat_code order by vessel_name ")
    @RegisterRowMapper(Vessel.Mapper.class)
    List<Vessel> vesselsFor(@Bind("stat_code") String statCode);

    @SqlQuery("select distinct * from V_VESSEL_DETAILS_DOCUMENTS where vessel_id = :vid and stat_code = :stat_code")
    @RegisterRowMapper(VesselPolicyLine.Mapper.class)
    List<VesselPolicyLine> findVesselPolicy(@Bind("vid") String vid,@Bind("stat_code") String statCode);

    @SqlQuery("select * from mv_vessel_details_product_prem where vessel_id= :vessel_id and stat_code = :stat_code order by class desc")
    @RegisterRowMapper(GrossPremium.Mapper.class)
    List<GrossPremium> grossPremium(@Bind("vessel_id") String vesselId, @Bind("stat_code") String statCode);
}
