package com.skuld.extranet.core.vessel.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VesselPolicyLine {
    private String stat_code;
    private String policy_line_id;
    private String vesselId;

    public VesselPolicyLine(ResultSet r) throws SQLException {
        this.stat_code = r.getString("stat_code");
        this.policy_line_id = r.getString("policy_line_id");
        this.vesselId = r.getString("vessel_id");
    }

    public String getStat_code() {
        return stat_code;
    }

    public void setStat_code(String stat_code) {
        this.stat_code = stat_code;
    }

    public String getPolicy_line_id() {
        return policy_line_id;
    }

    public void setPolicy_line_id(String policy_line_id) {
        this.policy_line_id = policy_line_id;
    }

    public String getVesselId() {
        return vesselId;
    }

    public void setVesselId(String vesselId) {
        this.vesselId = vesselId;
    }

    public static class Mapper implements RowMapper<VesselPolicyLine> {
        public VesselPolicyLine map(ResultSet r, StatementContext ctx) throws SQLException {
            return new VesselPolicyLine(r);
        }
    }
}
