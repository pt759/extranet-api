package com.skuld.extranet.core.vessel.domain;

public class VesselInformationView {

    public interface VesselInformation {
    }

    public interface VesselList extends VesselInformation {
    }

    public interface VesselBasic extends VesselList {
    }

    public interface VesselDetails extends VesselBasic {
    }

}
