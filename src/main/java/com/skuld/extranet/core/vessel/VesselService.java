
package com.skuld.extranet.core.vessel;


import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.api.vessel.service.VesselClientService;
import com.skuld.extranet.core.common.domain.Base64ContainerForExcel;
import com.skuld.extranet.core.vessel.domain.GrossPremium;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import com.skuld.extranet.core.vessel.domain.VesselPolicyLine;
import com.skuld.extranet.core.vessel.repo.VesselRepository;
import com.skuld.extranet.excel.ExcelListConverter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class VesselService {

    final VesselRepository vesselRepo;
    final VesselClientService vesselClientService;

    public VesselService(VesselRepository vesselRepo, VesselClientService vesselClientService) {
        this.vesselRepo = vesselRepo;
        this.vesselClientService = vesselClientService;
    }

    public List<Vessel> vesselsFor(String statCode) {
        return vesselRepo.vesselsFor(statCode);
    }

    public Vessel find(String vesselId, String statCode) {
        return vesselRepo.findVessel(vesselId, statCode);
    }

    //TODO: move this code into the search filter or, perhaps move it into the VesselAPI
    public List<Vessel> searchForVessel(VesselFilter filter) {
        List<Vessel> vessels = vesselClientService.searchForVessel(filter);
//        List<Vessel> vessels = vesselsFor(filter.getStatCode());
//        vessels = filterVessels(filter, vessels);
//        vessels = applyPagination(filter, vessels);
//        log.info("searchforvessel:" + vessels.size());
        return vessels;
    }

    @Deprecated
    private List<Vessel> applyPagination(VesselFilter filter, List<Vessel> vessels) {
        if (!filter.paginationActive()) {
            return vessels;
        }

        return vessels.stream()
                .skip((filter.skipRows()))
                .limit(filter.getPageSize())
                .collect(Collectors.toList());

    }

    @Deprecated
    private List<Vessel> filterVessels(VesselFilter filter, List<Vessel> vessels) {
        List<Vessel> list = new ArrayList<>();

        if (StringUtils.isNotEmpty(filter.getVessel())) {
            list = vessels.stream()
                    .filter(line -> (line.getVesselName().contains(filter.getVessel()) || line.getImo_number().contains(filter.getVessel())))
                    .collect(Collectors.toList());
        } else {
            list = vessels.stream()
                    .filter(line -> (filter.getStatus().contains(VesselFilter.all) || filter.getStatus().contains(line.getStatus())))
                    .filter(line -> (filter.getProduct().contains(VesselFilter.all) || CollectionUtils.containsAny(filter.getProduct(), Arrays.asList(line.getProducts().split(",")))))
                    .collect(Collectors.toList());
        }

        return list;

    }


    public Base64Container getVesselExcelConverterBase64(String statCode) {
        ExcelListConverter excel = getVesselExcelConverter(statCode);
        Base64Container result = Base64ContainerForExcel.getBase64ContainerWithExcel(excel);
        return result;
    }

    /*Moved to VesselAPI*/
    @Deprecated
    public ExcelListConverter getVesselExcelConverter(String statCode) {
        List<Vessel> list = vesselsFor(statCode);
        return new VesselExcelConverter(list, VesselExcelConverter.getStaticFilename());
    }


    public List<VesselPolicyLine> findVesselPolicyLine(Vessel vessel) {
        return vesselRepo.findVesselPolicy(vessel.getVesselId(), vessel.getStatCode());
    }

    public List<GrossPremium> grossPremiumsFor(String vesselId, String statCode) {
        return vesselRepo.grossPremium(vesselId, statCode);
    }
}
