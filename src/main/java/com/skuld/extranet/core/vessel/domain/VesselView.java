package com.skuld.extranet.core.vessel.domain;

public class VesselView {

    public interface Basic {
    }

    public interface Entered extends Basic {
    }

    public interface List extends Basic {
    }

    public interface Detail extends List{
    }

    public interface Documents{}

    public interface GrossPremium {}

}
