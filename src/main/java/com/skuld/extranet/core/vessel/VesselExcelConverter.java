package com.skuld.extranet.core.vessel;

import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.excel.ExcelListConverter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class VesselExcelConverter extends ExcelListConverter {

    private static final String SheetName = "Vessels";

    public static String getStaticFilename() {
        return "Exported list of vessels.xlsx";
    }

    final static Logger logger = LoggerFactory.getLogger(VesselExcelConverter.class);

    public VesselExcelConverter(List<? extends Object> list, String filename) {
        super(list, filename, SheetName);
    }


    protected Row populateRowHeader(Sheet sheet) {
        Row rowHeader = sheet.createRow(0);
        CellStyle headerStyle = styleHelper.headerStyleWithSolidRedBackground();
        Integer column = 0;
        styleHelper.createCellWithStyle(column++, "Vessel Name", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "IMO", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Status", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Year Built", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "GT", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Vessel Type", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Currency", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Premium", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Products", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Port of Registry", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Classification", headerStyle, rowHeader);
        return rowHeader;
    }

    protected void populateDataRows(Sheet sheet) {
        CellStyle style = styleHelper.styleGeneric();
        int startRow = 1;
        int column;
        for (Vessel item : (List<Vessel>) list) {
            Row row = sheet.createRow(startRow++);
            column = 0;
            styleHelper.createCellWithStyle(column++, item.getVesselName(), style, row);
            styleHelper.createNumericCellWithStyle(column++, item.getImo_number(), style, row);
            styleHelper.createCellWithStyle(column++, item.getStatus(), style, row);
            styleHelper.createNumericCellWithStyle(column++, item.getYearBuilt(), style, row);
            styleHelper.createNumericCellWithStyle(column++, item.getGt(), style, row);
            styleHelper.createCellWithStyle(column++, item.getVesselType(), style, row);
            styleHelper.createCellWithStyle(column++, item.getStatCurrency(), style, row);
            styleHelper.createNumericCellWithStyle(column++, item.getPremiumPaid(), style, row);
            styleHelper.createCellWithStyle(column++, item.getProducts(), style, row);
            styleHelper.createCellWithStyle(column++, item.getPortOfRegistryDescription(), style, row);
            styleHelper.createCellWithStyle(column++, item.getClassification(), style, row);
        }
    }

    protected void populateFooterRow(Sheet sheet) {

    }

}
