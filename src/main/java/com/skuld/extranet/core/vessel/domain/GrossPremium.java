package com.skuld.extranet.core.vessel.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GrossPremium {

    private String statCode;
    private String vesselId;

    @JsonView(VesselView.GrossPremium.class)
    private String product;

    @JsonView(VesselView.GrossPremium.class)
    private String productClass;

    @JsonView(VesselView.GrossPremium.class)
    private BigDecimal grossPremium;

    @JsonView(VesselView.GrossPremium.class)
    private String currency;

    public GrossPremium(ResultSet r) throws SQLException {
        this.statCode = r.getString("stat_code");
        this.vesselId = r.getString("vessel_id");
        this.product = r.getString("product");
        this.productClass = r.getString("class");
        this.grossPremium = r.getBigDecimal("paid_premium");
        this.currency= r.getString("currency");
    }


    public static class Mapper implements RowMapper<GrossPremium> {
        public GrossPremium map(ResultSet r, StatementContext ctx) throws SQLException {
            return new GrossPremium(r);
        }
    }

    public String getStatCode() {
        return statCode;
    }

    public GrossPremium setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public String getVesselId() {
        return vesselId;
    }

    public GrossPremium setVesselId(String vesselId) {
        this.vesselId = vesselId;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public GrossPremium setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getProductClass() {
        return productClass;
    }

    public GrossPremium setProductClass(String productClass) {
        this.productClass = productClass;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getGrossPremium() {
        return grossPremium;
    }

    public GrossPremium setGrossPremium(BigDecimal grossPremium) {
        this.grossPremium = grossPremium;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public GrossPremium setCurrency(String currency) {
        this.currency = currency;
        return this;
    }
}
