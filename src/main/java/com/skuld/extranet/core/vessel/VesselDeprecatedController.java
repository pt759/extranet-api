package com.skuld.extranet.core.vessel;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselView;
import com.skuld.extranet.excel.ExcelListConverter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Deprecated
@Log4j2
@RestController
@RequestMapping("/vessels")
@CrossOrigin
@Tag(name = "Vessel", description = "Vessels for the Extranet - Moved to Vessel-API")
public class VesselDeprecatedController extends SkuldController {

    final VesselService service;

    public VesselDeprecatedController(VesselService service) {
        this.service = service;
    }

    /*Replaced by Vessl-API*/
    @Deprecated
    @Operation(summary = "Vessel Detail Page: Single Vessel")
    @JsonView(VesselView.Detail.class)
    @RequestMapping(
            path = "/{statcode}/{vesselId}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity findVessel(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "vesselId") String vesselId
    ) {
        Vessel vessel = service.find(vesselId, statCode);
        return buildResponseEntity(vessel);
    }

    /*Moved to Vessl-API*/
    @Deprecated
    @Operation(summary = "Vessel list: Export list to Excel ")
    @RequestMapping(
            path = "list/excel/{statcode}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity downloadListOfVesselToExcel(@PathVariable(value = "statcode") String statCode) {
        log.info("Download Excel list of vessel (base64) for statcode: " + statCode);
        Base64Container container = service.getVesselExcelConverterBase64(statCode);
        return responseEntityWrapper(container);
    }


    /*Moved to Vessl-API*/
    @Deprecated
    @Operation(summary = "Vessel list: Export list to Excel (File)")
    @RequestMapping(
            path = "/list/excel/file/{statcode}/",
            method = RequestMethod.GET
    )
    public void excelListOfVesselFile(@PathVariable(value = "statcode") String statCode, HttpServletResponse response) {
        log.info("Download Excel list of vessel (file) for statcode: " + statCode);
        ExcelListConverter excel = service.getVesselExcelConverter(statCode);
        excel.export(response);
    }
}
