package com.skuld.extranet.core.vessel.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.skuld.extranet.core.common.ListFilterWithPagination;

import java.util.ArrayList;
import java.util.List;


@JsonPropertyOrder({"statCode", "vessel", "status", "product", "page", "pageSize"})
public class VesselFilter extends ListFilterWithPagination {

    public static String all = "*";
    private String statCode; // Mandatory
    private String vessel; //vesselname or imo number

    private List<String> status;
    public List<String> product;

    public VesselFilter(String statCode, String vessel) {
        this.statCode = statCode;
        this.vessel = vessel;
    }

    public VesselFilter(String statCode, String vessel, List<String> status, List<String> product, int page, int pageSize) {
        super();
        this.statCode = statCode;
        this.vessel = vessel;
        this.status = status;
        this.product = product;
        this.page = page;
        this.pageSize = pageSize;
    }

    public VesselFilter() {
        this.statCode = "";
        this.vessel = "";
        this.status = new ArrayList<>();
        this.product = new ArrayList<>();

        this.page = 0;
        this.pageSize = 10;
    }


    public String getStatCode() {
        return statCode;
    }

    public VesselFilter setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public String getVessel() {
        return vessel;
    }

    public VesselFilter setVessel(String vessel) {
        this.vessel = vessel;
        return this;
    }

    public List<String> getStatus() {
        return status;
    }

    public VesselFilter setStatus(List<String> status) {
        this.status = status;
        return this;
    }

    public List<String> getProduct() {
        return product;
    }

    public VesselFilter setProduct(List<String> product) {
        this.product = product;
        return this;
    }

}
