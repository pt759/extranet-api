package com.skuld.extranet.core.vessel.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import java.beans.ConstructorProperties;
import java.time.LocalDate;

public class DimVessel {

    @JsonProperty("v_id")
    @JsonView(VesselInformationView.VesselBasic.class)
    private String v_Id;

    @JsonProperty("vesselId")
    @JsonView(VesselInformationView.VesselBasic.class)
    private String vessel_Id;

    @JsonProperty("vesselName")
    @JsonView(VesselInformationView.VesselList.class)
    private String current_Ship_Name;

    @JsonProperty("imo")
    @JsonView(VesselInformationView.VesselList.class)
    private String imo_Number;

    @JsonProperty("yearBuilt")
    @JsonView(VesselInformationView.VesselBasic.class)
    private String year_Built;

    @JsonProperty("gt")
    @JsonView(VesselInformationView.VesselBasic.class)
    private String current_Gt;

    @JsonProperty("classification")
    @JsonView(VesselInformationView.VesselBasic.class)
    private String class_Society_Name;


    @JsonProperty("vesselType")
    @JsonView(VesselInformationView.VesselList.class)
    private String vessel_Type_Name;

    @JsonProperty("portOfRegistry")
    @JsonView(VesselInformationView.VesselBasic.class)
    public String getPortOfRegistry(){
        return this.port_Of_Registry_Desc + ", " + flag_State;
    }


    private LocalDate vessel_Effective_Start;
    private LocalDate vessel_Effective_End;
    private String vessel_Type_Id;
    private String vessel_Group;

    private String port_Of_Registry_Desc;

    private String flag_Code;

    private String historic_Gt;

    private String previous_Ship_Name;
    private String version_Ship_Name;

    private String class_Society;
    private String currency_Code;
    private String former_Owner;

    private String flag_State;
    private String historic_Vessel_Type_Name;

    private String vessel_Group_Id;
    private String vessel_Age;
    private String historic_Vessel_Type_Id;
    private String call_Sign;
    private String port_Of_Registry;
    private String build_Place;
    private String parcel_Tanker;
    private String double_Hull;
    private String sbt;
    private String newest;
    private String double_Bottom;
    private String current_Reg_Owner_Id;
    private String eni_No;
    private String load_Tons;
    private String age_Band;
    private String gt_Band;
    private String dwt;
    private String passenger_Capacity;
    private String gt_Band_Alt;

    @ConstructorProperties({"v_Id", "vessel_Id", "vessel_Effective_Start", "vessel_Effective_End", "vessel_Type_Id", "vessel_Group", "year_Built", "flag_Code", "imo_Number", "current_Gt", "historic_Gt", "current_Ship_Name", "previous_Ship_Name", "version_Ship_Name", "class_Society", "currency_Code", "former_Owner", "class_Society_Name", "flag_State", "historic_Vessel_Type_Name", "vessel_Type_Name", "vessel_Group_Id", "vessel_Age", "historic_Vessel_Type_Id", "call_Sign", "port_Of_Registry", "build_Place", "parcel_Tanker", "double_Hull", "sbt", "newest", "double_Bottom", "current_Reg_Owner_Id", "eni_No", "load_Tons", "age_Band", "gt_Band", "dwt", "passenger_Capacity", "gt_Band_Alt", "port_Of_Registry_Desc"})
    public DimVessel(String v_Id, String vessel_Id, LocalDate vessel_Effective_Start, LocalDate vessel_Effective_End, String vessel_Type_Id, String vessel_Group, String year_Built, String flag_Code, String imo_Number, String current_Gt, String historic_Gt, String current_Ship_Name, String previous_Ship_Name, String version_Ship_Name, String class_Society, String currency_Code, String former_Owner, String class_Society_Name, String flag_State, String historic_Vessel_Type_Name, String vessel_Type_Name, String vessel_Group_Id, String vessel_Age, String historic_Vessel_Type_Id, String call_Sign, String port_Of_Registry, String build_Place, String parcel_Tanker, String double_Hull, String sbt, String newest, String double_Bottom, String current_Reg_Owner_Id, String eni_No, String load_Tons, String age_Band, String gt_Band, String dwt, String passenger_Capacity, String gt_Band_Alt, String port_Of_Registry_Desc) {
        this.v_Id = v_Id;
        this.vessel_Id = vessel_Id;
        this.vessel_Effective_Start = vessel_Effective_Start;
        this.vessel_Effective_End = vessel_Effective_End;
        this.vessel_Type_Id = vessel_Type_Id;
        this.vessel_Group = vessel_Group;
        this.year_Built = year_Built;
        this.flag_Code = flag_Code;
        this.imo_Number = imo_Number;
        this.current_Gt = current_Gt;
        this.historic_Gt = historic_Gt;
        this.current_Ship_Name = current_Ship_Name;
        this.previous_Ship_Name = previous_Ship_Name;
        this.version_Ship_Name = version_Ship_Name;
        this.class_Society = class_Society;
        this.currency_Code = currency_Code;
        this.former_Owner = former_Owner;
        this.class_Society_Name = class_Society_Name;
        this.flag_State = flag_State;
        this.historic_Vessel_Type_Name = historic_Vessel_Type_Name;
        this.vessel_Type_Name = vessel_Type_Name;
        this.vessel_Group_Id = vessel_Group_Id;
        this.vessel_Age = vessel_Age;
        this.historic_Vessel_Type_Id = historic_Vessel_Type_Id;
        this.call_Sign = call_Sign;
        this.port_Of_Registry = port_Of_Registry;
        this.build_Place = build_Place;
        this.parcel_Tanker = parcel_Tanker;
        this.double_Hull = double_Hull;
        this.sbt = sbt;
        this.newest = newest;
        this.double_Bottom = double_Bottom;
        this.current_Reg_Owner_Id = current_Reg_Owner_Id;
        this.eni_No = eni_No;
        this.load_Tons = load_Tons;
        this.age_Band = age_Band;
        this.gt_Band = gt_Band;
        this.dwt = dwt;
        this.passenger_Capacity = passenger_Capacity;
        this.gt_Band_Alt = gt_Band_Alt;
        this.port_Of_Registry_Desc = port_Of_Registry_Desc;
    }

    public void setV_Id(String v_Id) {
        this.v_Id = v_Id;
    }

    public void setVessel_Id(String vessel_Id) {
        this.vessel_Id = vessel_Id;
    }

    public void setVessel_Effective_Start(LocalDate vessel_Effective_Start) {
        this.vessel_Effective_Start = vessel_Effective_Start;
    }

    public void setVessel_Effective_End(LocalDate vessel_Effective_End) {
        this.vessel_Effective_End = vessel_Effective_End;
    }

    public void setVessel_Type_Id(String vessel_Type_Id) {
        this.vessel_Type_Id = vessel_Type_Id;
    }

    public void setVessel_Group(String vessel_Group) {
        this.vessel_Group = vessel_Group;
    }

    public void setYear_Built(String year_Built) {
        this.year_Built = year_Built;
    }

    public void setFlag_Code(String flag_Code) {
        this.flag_Code = flag_Code;
    }

    public void setImo_Number(String imo_Number) {
        this.imo_Number = imo_Number;
    }

    public void setCurrent_Gt(String current_Gt) {
        this.current_Gt = current_Gt;
    }

    public void setHistoric_Gt(String historic_Gt) {
        this.historic_Gt = historic_Gt;
    }

    public void setCurrent_Ship_Name(String current_Ship_Name) {
        this.current_Ship_Name = current_Ship_Name;
    }

    public void setPrevious_Ship_Name(String previous_Ship_Name) {
        this.previous_Ship_Name = previous_Ship_Name;
    }

    public void setVersion_Ship_Name(String version_Ship_Name) {
        this.version_Ship_Name = version_Ship_Name;
    }

    public void setClass_Society(String class_Society) {
        this.class_Society = class_Society;
    }

    public void setCurrency_Code(String currency_Code) {
        this.currency_Code = currency_Code;
    }

    public void setFormer_Owner(String former_Owner) {
        this.former_Owner = former_Owner;
    }

    public void setClass_Society_Name(String class_Society_Name) {
        this.class_Society_Name = class_Society_Name;
    }

    public void setFlag_State(String flag_State) {
        this.flag_State = flag_State;
    }

    public void setHistoric_Vessel_Type_Name(String historic_Vessel_Type_Name) {
        this.historic_Vessel_Type_Name = historic_Vessel_Type_Name;
    }

    public void setVessel_Type_Name(String vessel_Type_Name) {
        this.vessel_Type_Name = vessel_Type_Name;
    }

    public void setVessel_Group_Id(String vessel_Group_Id) {
        this.vessel_Group_Id = vessel_Group_Id;
    }

    public void setVessel_Age(String vessel_Age) {
        this.vessel_Age = vessel_Age;
    }

    public void setHistoric_Vessel_Type_Id(String historic_Vessel_Type_Id) {
        this.historic_Vessel_Type_Id = historic_Vessel_Type_Id;
    }

    public void setCall_Sign(String call_Sign) {
        this.call_Sign = call_Sign;
    }

    public void setPort_Of_Registry(String port_Of_Registry) {
        this.port_Of_Registry = port_Of_Registry;
    }

    public void setBuild_Place(String build_Place) {
        this.build_Place = build_Place;
    }

    public void setParcel_Tanker(String parcel_Tanker) {
        this.parcel_Tanker = parcel_Tanker;
    }

    public void setDouble_Hull(String double_Hull) {
        this.double_Hull = double_Hull;
    }

    public void setSbt(String sbt) {
        this.sbt = sbt;
    }

    public void setNewest(String newest) {
        this.newest = newest;
    }

    public void setDouble_Bottom(String double_Bottom) {
        this.double_Bottom = double_Bottom;
    }

    public void setCurrent_Reg_Owner_Id(String current_Reg_Owner_Id) {
        this.current_Reg_Owner_Id = current_Reg_Owner_Id;
    }

    public void setEni_No(String eni_No) {
        this.eni_No = eni_No;
    }

    public void setLoad_Tons(String load_Tons) {
        this.load_Tons = load_Tons;
    }

    public void setAge_Band(String age_Band) {
        this.age_Band = age_Band;
    }

    public void setGt_Band(String gt_Band) {
        this.gt_Band = gt_Band;
    }

    public void setDwt(String dwt) {
        this.dwt = dwt;
    }

    public void setPassenger_Capacity(String passenger_Capacity) {
        this.passenger_Capacity = passenger_Capacity;
    }

    public void setGt_Band_Alt(String gt_Band_Alt) {
        this.gt_Band_Alt = gt_Band_Alt;
    }

    public void setPort_Of_Registry_Desc(String port_Of_Registry_Desc) {
        this.port_Of_Registry_Desc = port_Of_Registry_Desc;
    }

    public String getV_Id() {
        return v_Id;
    }

    public String getVessel_Id() {
        return vessel_Id;
    }

    public LocalDate getVessel_Effective_Start() {
        return vessel_Effective_Start;
    }

    public LocalDate getVessel_Effective_End() {
        return vessel_Effective_End;
    }

    public String getVessel_Type_Id() {
        return vessel_Type_Id;
    }

    public String getVessel_Group() {
        return vessel_Group;
    }

    public String getYear_Built() {
        return year_Built;
    }

    public String getFlag_Code() {
        return flag_Code;
    }

    public String getImo_Number() {
        return imo_Number;
    }

    public String getCurrent_Gt() {
        return current_Gt;
    }

    public String getHistoric_Gt() {
        return historic_Gt;
    }

    public String getCurrent_Ship_Name() {
        return current_Ship_Name;
    }

    public String getPrevious_Ship_Name() {
        return previous_Ship_Name;
    }

    public String getVersion_Ship_Name() {
        return version_Ship_Name;
    }

    public String getClass_Society() {
        return class_Society;
    }

    public String getCurrency_Code() {
        return currency_Code;
    }

    public String getFormer_Owner() {
        return former_Owner;
    }

    public String getClass_Society_Name() {
        return class_Society_Name;
    }

    public String getFlag_State() {
        return flag_State;
    }

    public String getHistoric_Vessel_Type_Name() {
        return historic_Vessel_Type_Name;
    }

    public String getVessel_Type_Name() {
        return vessel_Type_Name;
    }

    public String getVessel_Group_Id() {
        return vessel_Group_Id;
    }

    public String getVessel_Age() {
        return vessel_Age;
    }

    public String getHistoric_Vessel_Type_Id() {
        return historic_Vessel_Type_Id;
    }

    public String getCall_Sign() {
        return call_Sign;
    }

    public String getPort_Of_Registry() {
        return port_Of_Registry;
    }

    public String getBuild_Place() {
        return build_Place;
    }

    public String getParcel_Tanker() {
        return parcel_Tanker;
    }

    public String getDouble_Hull() {
        return double_Hull;
    }

    public String getSbt() {
        return sbt;
    }

    public String getNewest() {
        return newest;
    }

    public String getDouble_Bottom() {
        return double_Bottom;
    }

    public String getCurrent_Reg_Owner_Id() {
        return current_Reg_Owner_Id;
    }

    public String getEni_No() {
        return eni_No;
    }

    public String getLoad_Tons() {
        return load_Tons;
    }

    public String getAge_Band() {
        return age_Band;
    }

    public String getGt_Band() {
        return gt_Band;
    }

    public String getDwt() {
        return dwt;
    }

    public String getPassenger_Capacity() {
        return passenger_Capacity;
    }

    public String getGt_Band_Alt() {
        return gt_Band_Alt;
    }

    public String getPort_Of_Registry_Desc() {
        return port_Of_Registry_Desc;
    }
}
