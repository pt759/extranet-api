package com.skuld.extranet.core.vessel.domain;

public class VesselForExtranetBuilder {
    private String classSociety;
    private String portOfRegistry;
    private String premium;
    private String product;
    private String status;
    private String accountManager;


    public VesselForExtranetBuilder setAccountManager(String accountManager) {
        this.accountManager = accountManager;
        return this;
    }

    public VesselForExtranetBuilder setClassSociety(String classSociety) {
        this.classSociety = classSociety;
        return this;
    }

    public VesselForExtranetBuilder setPortOfRegistry(String portOfRegistry) {
        this.portOfRegistry = portOfRegistry;
        return this;
    }

    public VesselForExtranetBuilder setPremium(String premium) {
        this.premium = premium;
        return this;
    }

    public VesselForExtranetBuilder setProduct(String product) {
        this.product = product;
        return this;
    }

    public VesselForExtranet createVesselForExtranet() {
        return new VesselForExtranet(classSociety, portOfRegistry, premium, product, status,accountManager);
    }


    public VesselForExtranetBuilder setStatus(String status) {
        this.status = status;
        return this;
    }
}