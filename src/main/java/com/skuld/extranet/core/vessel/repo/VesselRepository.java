package com.skuld.extranet.core.vessel.repo;

import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.core.vessel.domain.GrossPremium;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselPolicyLine;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VesselRepository extends DBDwhRepo {

    public List<Vessel> vesselsFor(String statCode) {
        List<Vessel> vessels = getJdbi().withExtension(VesselDao.class, dao -> dao.vesselsFor(statCode));
        return vessels;

    }

    public Vessel findVessel(String vesselId, String statCode) {
        Vessel vessel = getJdbi().withExtension(VesselDao.class,
                dao -> dao.find(vesselId, statCode));
        return vessel;
    }

    public List<VesselPolicyLine> findVesselPolicy(String vId, String statCode) {
        return getJdbi().withExtension(VesselDao.class, dao -> dao.findVesselPolicy(vId, statCode));
    }

    public List<GrossPremium> grossPremium(String vesselId, String statCode) {
        return getJdbi().withExtension(VesselDao.class, dao -> dao.grossPremium(vesselId, statCode));
    }

}
