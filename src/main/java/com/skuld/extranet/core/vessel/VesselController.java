package com.skuld.extranet.core.vessel;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import com.skuld.extranet.core.vessel.domain.VesselView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/vessels")
@CrossOrigin
@Tag(name = "Vessel", description = "Vessels for the Extranet")
@Log4j2
public class VesselController extends SkuldController {

    final VesselService service;

    public VesselController(VesselService service) {
        this.service = service;
    }

    @Operation(summary = "Vessel List Page: List of Vessels")
    @JsonView(VesselView.List.class)
    @RequestMapping(
            path = "/list/{statcode}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity listOfVessel(
            @PathVariable(value = "statcode") String statCode) {
        List<Vessel> vessel = service.vesselsFor(statCode);
        return buildResponseEntity(vessel);
    }

    @Operation(summary = "Vessel List Page: List of Vessels")
    @JsonView(VesselView.List.class)
    @RequestMapping(
            path = "/search/{statcode}/",
            method = RequestMethod.POST,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity search(
            @PathVariable(value = "statcode") String statCode,
            @RequestBody VesselFilter filter) {

        if (filter.getStatCode() != null || StringUtils.isEmpty(statCode)) {
            return noContent();
        }
        List<Vessel> vessel = service.searchForVessel(filter);
        return buildResponseEntity(vessel);
    }

    @Operation(summary = "Overview Page: Entered vessels")
    @JsonView(VesselView.Entered.class)
    @RequestMapping(
            path = "/entered/{statcode}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity listOfVesselEntered(
            @PathVariable(value = "statcode") String statCode) {
        List<Vessel> vessel = service.vesselsFor(statCode);
        return buildResponseEntity(vessel);
    }

}
