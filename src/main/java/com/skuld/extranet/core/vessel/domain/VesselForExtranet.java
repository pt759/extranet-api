package com.skuld.extranet.core.vessel.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;


@JsonIgnoreProperties({"certificates", "note","coverType","currentRegOwner","businessUnit","member"})
public class VesselForExtranet extends VesselSearch {

    private String classSociety;
    private String portOfRegistry;
    private String premium;
    private String product;
    private String status;



    private String accountManager;

    public VesselForExtranet(String classSociety, String portOfRegistry, String premium, String product, String status, String accountManager) {
        this.classSociety = classSociety;
        this.portOfRegistry = portOfRegistry;
        this.premium = premium;
        this.product = product;
        this.status = status;
        this.accountManager = accountManager;
    }

    public String getClassSociety() {
        return classSociety;
    }

    public void setClassSociety(String classSociety) {
        this.classSociety = classSociety;
    }

    public String getPortOfRegistry() {
        return portOfRegistry;
    }

    public void setPortOfRegistry(String portOfRegistry) {
        this.portOfRegistry = portOfRegistry;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }
}
