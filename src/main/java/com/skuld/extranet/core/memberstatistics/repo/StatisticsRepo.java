package com.skuld.extranet.core.memberstatistics.repo;

import com.skuld.extranet.core.common.domain.GraphData;
import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticsRepo extends DBDwhRepo {

    final static Logger logger = LoggerFactory.getLogger(StatisticsRepo.class);


    public List<StatisticsEntity> statistics(String statCode) {
        List<StatisticsEntity> list = getJdbi().withExtension(StatisticsDao.class, dao -> dao.statistics(statCode));
        return list;
    }

    public List<ClaimStatisticsEntity> statisticsClaimsSixYear(String statCode) {
        return getJdbi().withExtension(StatisticsDao.class, dao -> dao.statisticsClaimsSixYear(statCode));
    }

    public List<ClaimStatisticsEntity> statisticsClaimsFiveYear(String statCode) {
        return getJdbi().withExtension(StatisticsDao.class, dao -> dao.statisticsClaimsFiveYear(statCode));
    }

    public List<GraphData> lossRatio5Year(String statcode, String productCode) {
        return getJdbi().withExtension(StatisticsDao.class, dao -> dao.lossRatio5Year(statcode, productCode));
    }
}