package com.skuld.extranet.core.memberstatistics.service;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.archive.service.CoreArchiveClient;
import com.skuld.extranet.core.common.SkuldWebUtil;
import com.skuld.extranet.core.common.domain.GraphData;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntityVersion;
import com.skuld.extranet.core.memberstatistics.repo.StatisticsRepo;
import com.skuld.extranet.core.user.service.StatCodeService;
import com.skuld.extranet.global.RESTendPoints;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.StatYearType;
import com.skuld.extranet.spa.pages.domain.UrlLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.global.RESTendPoints.*;

@Service
public class MemberStatisticsService {
    final static Logger logger = LoggerFactory.getLogger(MemberStatisticsService.class);


    private final StatisticsRepo statisticsRepo;
    private final StatCodeService statCodeService;
    private final CoreArchiveClient coreArchiveClient;

    @Autowired
    public MemberStatisticsService(StatisticsRepo statisticsRepo,
                                   StatCodeService statCodeService,
                                   CoreArchiveClient coreArchiveClient) {
        this.statisticsRepo = statisticsRepo;
        this.statCodeService = statCodeService;
        this.coreArchiveClient = coreArchiveClient;
    }

    public List<ClaimStatisticsEntity> statisticsClaimSixYear(String statCode) {
        return statisticsRepo.statisticsClaimsSixYear(statCode);
    }

    public List<ClaimStatisticsEntity> statisticsClaimFiveYear(String statCode) {
        List<ClaimStatisticsEntity> list = statisticsRepo.statisticsClaimsFiveYear(statCode);
        return list;
    }

    public List<StatisticsEntity> getStatistics(String statCode) {
        List<StatisticsEntity> list = statisticsRepo.statistics(statCode);
        return list;
    }

    public List<ClaimStatisticsEntity> filteredClaimStatisticsEntityOnProduct(List<ClaimStatisticsEntity> claimStatisticsFiveYear, String productCode) {
        return claimStatisticsFiveYear.stream().filter(line -> productCode.equalsIgnoreCase(line.getProduct())).collect(Collectors.toList());
    }

    public List<UrlLink> getExcelUrlForStatistics(String statCode, Product product) {
        List<UrlLink> list = new ArrayList<>();
        list.add(convertExcelEndpointToUrl(statCode, product, StatisticsEntityVersion.distributed));
        list.add(convertExcelEndpointToUrl(statCode, product, StatisticsEntityVersion.fiveYear));
        list.add(convertExcelEndpointToUrl(statCode, product, StatisticsEntityVersion.actual));
        return list;
    }

    public UrlLink convertExcelEndpointToUrl(String statCode, Product product, StatisticsEntityVersion version) {
        String url = extranet + statistics_excel_StatCode_Product_Version
                .replace(pathVariableStatCode, statCode)
                .replace(pathVariableProduct, SkuldWebUtil.urlEncode(product.value()))
                .replace(pathVariableVersion, version.toString());
        logger.info(url);
        UrlLink link = new UrlLink();
        link.setUrl(url)
                .setCode(version.toString().toUpperCase())
                .setTitle(version.getDescription());
        return link;
    }

    public List<UrlLink> getMemberStatisticsUrls(String statCode, Product product) {
        List<UrlLink> list = new ArrayList<>();
        list.add(convertPDFEndpointToUrl(statCode, StatYearType.distributed, product));
        list.add(convertPDFEndpointToUrl(statCode, StatYearType.fiveYears, product));
        list.add(convertPDFEndpointToUrl(statCode, StatYearType.sixYears, product));
        return list;
    }

    public UrlLink convertPDFEndpointToUrl(String statCode, StatYearType version, Product product) {
        String url = RESTendPoints.statisticsPage + RESTendPoints.pdf_StatCode_Version_Product
                .replace(pathVariableStatCode, statCode)
                .replace(pathVariableVersion, version.getCode())
                .replace(pathVariableProduct, product.value());

        UrlLink link = new UrlLink();
        link.setUrl(url).setCode(version.getCode()).setTitle(version.toString());

        return link;
    }


    public List<GraphData> getLossRatio5Years(String statcode, String productCode) {
        return statisticsRepo.lossRatio5Year(statcode, productCode);
    }

    public Base64Container getMemberStatisticsPdf(String statCode, String version, String product, String filename) {
        return coreArchiveClient.getMemberStatisticsPdf(statCode, version, product, null, "", filename);
    }
}
