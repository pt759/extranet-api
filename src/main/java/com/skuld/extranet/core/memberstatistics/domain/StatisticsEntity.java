package com.skuld.extranet.core.memberstatistics.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.Math.round;

public class StatisticsEntity {


    private String statCode;
    private String policyYear;
    private Double weightedGt;
    private Double weightedNoVessels;
    private Double netPremium;
    private Double netRetainedPremium;
    private Double excessReinsurance;
    private Double payments;
    private Double reserves;
    private Double poolClaims;
    private Double grossPremium;
    private Integer claimsTotal;
    private Integer claims;
    private Double lossRatio;
    private String currency;


    @JsonIgnore
    private String productClass;
    @JsonIgnore
    private String product;

    @JsonIgnore
    private String version;

    public StatisticsEntity(ResultSet r) throws SQLException {
        this.statCode = r.getString("stat_code");
//        this.policyYear = r.getString("policy_year");
        this.policyYear = r.getString("stat_year");
        this.productClass = r.getString("class");
        this.currency = r.getString("currency");
        this.product = r.getString("product");
        this.version = r.getString("version");

        this.weightedGt = r.getDouble("weighted_gt");
        this.weightedNoVessels = r.getDouble("weighted_no_vessels");
        this.netPremium = r.getDouble("net_premium");
        this.netRetainedPremium = r.getDouble("net_retained_premium");
        this.excessReinsurance = r.getDouble("excess_reinsurance");
        this.payments = r.getDouble("payments");
        this.reserves = r.getDouble("reserves");
        this.poolClaims = r.getDouble("pool_claims");
        this.grossPremium = r.getDouble("gross_premium");
        this.claimsTotal = r.getInt("claims_total");
        this.claims = r.getInt("claims");

        this.lossRatio = r.getDouble("loss_ratio");

    }

    public StatisticsEntity() {
    }

    public static class Mapper implements RowMapper<StatisticsEntity> {
        public StatisticsEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new StatisticsEntity(r);
        }
    }


    public String getStatCode() {
        return statCode;
    }

    public void setStatCode(String statCode) {
        this.statCode = statCode;
    }

    public String getPolicyYear() {
        return policyYear;
    }

    public StatisticsEntity setPolicyYear(String policy_year) {
        this.policyYear = policy_year;
        return this;
    }


    public String getProduct() {
        return product;
    }

    public StatisticsEntity setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public StatisticsEntity setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public StatisticsEntity setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public long getWeightedGt() {
        return round(weightedGt);
    }

    public StatisticsEntity setWeightedGt(Double weightedGt) {
        this.weightedGt = weightedGt;
        return this;
    }

    public Double getWeightedNoVessels() {
        return Math.round(weightedNoVessels * 100) / 100.0;
    }

    public StatisticsEntity setWeightedNoVessels(Double weightedNoVessels) {
        this.weightedNoVessels = weightedNoVessels;
        return this;
    }

    public long getNetPremium() {
        return round(netPremium);
    }

    public StatisticsEntity setNetPremium(Double netPremium) {
        this.netPremium = netPremium;
        return this;
    }

    public long getNetRetainedPremium() {
        return round(netRetainedPremium);
    }

    public StatisticsEntity setNetRetainedPremium(Double netRetainedPremium) {
        this.netRetainedPremium = netRetainedPremium;
        return this;
    }

    public long getExcessReinsurance() {
        return round(excessReinsurance);
    }

    public StatisticsEntity setExcessReinsurance(Double excessReinsurance) {
        this.excessReinsurance = excessReinsurance;
        return this;
    }

    public long getPayments() {
        return round(payments);
    }

    public StatisticsEntity setPayments(Double payments) {
        this.payments = payments;
        return this;
    }

    public long getReserves() {
        return round(reserves);
    }

    public StatisticsEntity setReserves(Double reserves) {
        this.reserves = reserves;
        return this;
    }

    public long getPoolClaims() {
        return round(poolClaims);
    }

    public StatisticsEntity setPoolClaims(Double poolClaims) {
        this.poolClaims = poolClaims;
        return this;
    }

    public long getGrossPremium() {
        return round(grossPremium);
    }

    public StatisticsEntity setGrossPremium(Double grossPremium) {
        this.grossPremium = grossPremium;
        return this;
    }

    public Integer getClaimsTotal() {
        return claimsTotal;
    }

    public StatisticsEntity setClaimsTotal(Integer claimsTotal) {
        this.claimsTotal = claimsTotal;
        return this;
    }

    public Integer getClaims() {
        return claims;
    }

    public StatisticsEntity setClaims(Integer claims) {
        this.claims = claims;
        return this;
    }

    public Double getLossRatio() {
        return lossRatio;
    }

    public StatisticsEntity setLossRatio(Double lossRatio) {
        this.lossRatio = lossRatio;
        return this;
    }

    public String getProductClass() {
        return productClass;
    }

    public StatisticsEntity setProductClass(String productClass) {
        this.productClass = productClass;
        return this;
    }
}
