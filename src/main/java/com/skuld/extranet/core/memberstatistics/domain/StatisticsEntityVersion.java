package com.skuld.extranet.core.memberstatistics.domain;

import com.skuld.extranet.spa.pages.domain.StatYearType;

/*
 * Used in the Database
 */
public enum StatisticsEntityVersion {
    distributed("D", "Distributed"),
    actual("A", "Stat years"),
    fiveYear("5YEAR", "Five years") // Not applicable in database.
    ;

    private final String value;
    private final String description;

    StatisticsEntityVersion(String value, String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public String toString() {
        return value;
    }

    public String dbFilterValue() {
        if (this.equals(fiveYear)) {
            return actual.value;
        }
        return value;
    }


    public String value() {
        return value;
    }

    public static StatisticsEntityVersion fromString(String code) {
        for (StatisticsEntityVersion b : StatisticsEntityVersion.values()) {
            if (b.value().equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }

    public static StatYearType convert(StatisticsEntityVersion version) {

        if (version.equals(StatisticsEntityVersion.distributed)) {
            return StatYearType.distributed;
        } else if (version.equals(StatisticsEntityVersion.actual)) {
            return StatYearType.statYears;
        } else if (version.equals(StatisticsEntityVersion.fiveYear)) {
            return StatYearType.fiveYears;
        }

        return null;
    }

    public String getDescription() {
        return description;
    }
}
