package com.skuld.extranet.core.memberstatistics.domain;

import com.google.common.collect.Iterables;
import com.skuld.extranet.core.common.SkCommon;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.skuld.extranet.core.claim.service.ClaimService.key_total_claims;

public class ClaimStatisticsEntity {
    private String statCode;
    private String policyYear;
    private String product;
    private String claimsType;
    private String claimsTypeDesc;
    private Integer claims;

    public ClaimStatisticsEntity() {
    }


    public ClaimStatisticsEntity(ResultSet r) throws SQLException {
        statCode = r.getString("stat_code");
        policyYear = r.getString("policy_year");
        product = r.getString("product");
        claimsType = r.getString("claims_type");
        claimsTypeDesc = r.getString("claims_type_desc");
        claims = r.getInt("claims");
    }


    public static class Mapper implements RowMapper<ClaimStatisticsEntity> {
        public ClaimStatisticsEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new ClaimStatisticsEntity(r);
        }
    }


    public static ClaimStatisticsEntity createTotalClaimEntry(List<ClaimStatisticsEntity> list) {
        Integer totalClaim = list.stream().mapToInt(codeValue -> codeValue.getClaims()).sum();

        return new ClaimStatisticsEntity()
                .setClaims(totalClaim)
                .setClaimsType(key_total_claims)
                .setClaimsTypeDesc(key_total_claims);

    }

    /*
    Assume list is in Order of Descending Number of Claims
    Will Limit list to @param(limit) items,
    and count the remaining claims
    and create a other group
    and add the remaining claim in the o ther group
    */
    public static List<ClaimStatisticsEntity> groupListWithLimitOf(int limit, List<ClaimStatisticsEntity> list) {
        List<ClaimStatisticsEntity> mainGroupList = list;
        if (limit < list.size()) {

            mainGroupList = list.subList(0, limit);
            ClaimStatisticsEntity lastCodeValue = Iterables.getLast(mainGroupList);

            List<ClaimStatisticsEntity> otherInList = list.subList(limit, list.size());

            Integer value = Integer.valueOf(lastCodeValue.getClaims());
            for (ClaimStatisticsEntity item : otherInList) {
                value += Integer.valueOf(item.getClaims());
            }

            lastCodeValue.setClaims(value);
            lastCodeValue.setClaimsType(SkCommon.group_others_code);
            lastCodeValue.setClaimsTypeDesc(SkCommon.group_others_desc);
        }
        return mainGroupList;

    }

    public String getStatCode() {
        return statCode;
    }

    public ClaimStatisticsEntity setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public String getPolicyYear() {
        return policyYear;
    }

    public ClaimStatisticsEntity setPolicyYear(String policyYear) {
        this.policyYear = policyYear;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public ClaimStatisticsEntity setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getClaimsType() {
        return claimsType;
    }

    public ClaimStatisticsEntity setClaimsType(String claimsType) {
        this.claimsType = claimsType;
        return this;
    }

    public String getClaimsTypeDesc() {
        return claimsTypeDesc;
    }

    public ClaimStatisticsEntity setClaimsTypeDesc(String claimsTypeDesc) {
        this.claimsTypeDesc = claimsTypeDesc;
        return this;
    }

    public Integer getClaims() {
        return claims;
    }

    public ClaimStatisticsEntity setClaims(Integer claims) {
        this.claims = claims;
        return this;
    }
}
