package com.skuld.extranet.core.memberstatistics.controller;

import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import com.skuld.extranet.core.memberstatistics.service.MemberStatisticsService;
import com.skuld.extranet.global.RESTendPoints;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.StatYearType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Statics", description = "Statistics for Extranet")
@RestController
@RequestMapping(RESTendPoints.statisticsPage)
@CrossOrigin
public class MemberStatisticsController extends SkuldController {

    @Autowired
    private MemberStatisticsService memberStatisticsService;

    @Operation(summary = "Statistics Page: Premiums and Claims Per Year")
    @GetMapping(path = "/{statcode}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity statistics(@PathVariable(value = "statcode") String statCode) {
        List<StatisticsEntity> rows = memberStatisticsService.getStatistics(statCode);
        return buildResponseEntity(rows);
    }


    @Operation(summary = "Statistics Page: Claim Statistics six year")
    @GetMapping(path = "claims/sixyear/{statcode}/",
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity statisticsClaimsCurrentYear(@PathVariable(value = "statcode") String statCode) {
        List<ClaimStatisticsEntity> rows = memberStatisticsService.statisticsClaimSixYear(statCode);
        return buildResponseEntity(rows);
    }

    @Operation(summary = "Statistics Page: Claim Statistics five year")
    @GetMapping(path = "claims/fiveyear/{statcode}/",
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity statisticsClaimsFiveYear(@PathVariable(value = "statcode") String statCode) {
        List<ClaimStatisticsEntity> rows = memberStatisticsService.statisticsClaimFiveYear(statCode);
        return buildResponseEntity(rows);
    }

    @Operation(summary = "Statistics Page: Download statistics as pdf")
    @GetMapping(path = RESTendPoints.pdf_StatCode_Version_Product,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity downloadOpenClaimsToPdfBase64(@PathVariable(value = "statcode") String statCode,
                                                        @PathVariable(value = "version") String version,
                                                        @PathVariable(value = "product") String product
    ) {
        StatYearType statVersion = StatYearType.fromString(version);
        Product thisProduct = Product.fromString(product);
        if (statVersion != null && thisProduct != null) {
            String filename = "MemberStatistics-" + thisProduct.description() + ".pdf";
            Base64Container container = memberStatisticsService.getMemberStatisticsPdf(statCode, statVersion.getCode(), thisProduct.value(), filename);
            return responseEntityWrapper(container);
        } else {
            return buildResponseEntity(null);
        }
    }

}