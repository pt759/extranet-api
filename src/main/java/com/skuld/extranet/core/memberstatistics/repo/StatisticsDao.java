package com.skuld.extranet.core.memberstatistics.repo;

import com.skuld.extranet.core.common.domain.GraphData;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface StatisticsDao {

    @SqlQuery("select * from MV_STATISTICS where stat_code  = :stat_code order by class, stat_year ")
    @RegisterRowMapper(StatisticsEntity.Mapper.class)
    List<StatisticsEntity> statistics(@Bind("stat_code") String statCode);


    @SqlQuery("select * from V_STATISTICS_CLAIMS_6_YEAR  where stat_code  = :stat_code order by claims desc")
    @RegisterRowMapper(ClaimStatisticsEntity.Mapper.class)
    List<ClaimStatisticsEntity> statisticsClaimsSixYear(@Bind("stat_code") String statCode);

    @SqlQuery("select * from V_STATISTICS_CLAIMS_5_YEAR where stat_code  = :stat_code order by claims desc")
    @RegisterRowMapper(ClaimStatisticsEntity.Mapper.class)
    List<ClaimStatisticsEntity> statisticsClaimsFiveYear(@Bind("stat_code") String statCode);

    // TODO: Move out from this DAO
    @SqlQuery("select stat_year label, (loss_ratio *100) value " +
            "from MV_LOSS_RATIO_FIVE_YEAR " +
            "where stat_code = :statCode and product = :productCode " +
            "order by label")
    @RegisterRowMapper(GraphData.Mapper.class)
    List<GraphData> lossRatio5Year(@Bind("statCode") String statCode, @Bind("productCode") String productCode);
}
