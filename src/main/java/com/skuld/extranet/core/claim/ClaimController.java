package com.skuld.extranet.core.claim;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.core.claim.domain.ClaimListFilter;
import com.skuld.extranet.core.claim.domain.Views;
import com.skuld.extranet.core.claim.excel.ClaimExcelConverter;
import com.skuld.extranet.core.claim.excel.ClaimListExcelConverter;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.excel.ExcelListConverter;
import com.skuld.extranet.global.RESTendPoints;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.skuld.extranet.core.claim.domain.EventStatus.all;
import static com.skuld.extranet.core.claim.domain.EventStatus.open;
import static com.skuld.extranet.global.RESTendPoints.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Claim", description = "Claims Operations for extranet")
@RestController
@RequestMapping(claims)
@CrossOrigin
@Deprecated
public class ClaimController extends SkuldController {

    @Autowired
    private ClaimService claimService;

    final static Logger logger = LoggerFactory.getLogger(ClaimController.class);

    @Operation(summary = "Claim Estimates")
    @RequestMapping(path = "estimates/{statcode}/{eventno}/", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity stats(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "eventno") String claimEventNo

    ) {
        List<ClaimEstimates> list = claimService.estimates(statCode, claimEventNo);
        return buildResponseEntity(list);
    }

    /*
     * Claim list without Estimates or any other sums.
     */
    @Operation(summary = "Claim List Page: Claim List")
    @JsonView(Views.ClaimList.class)
    @RequestMapping(path = "list/{statcode}/", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity claimList(@PathVariable(value = "statcode") String statCode) {
        List<Claim> statistics = claimService.claimListAll(statCode);
        return buildResponseEntity((statistics));
    }

    /*
     * Claim list without Estimates or any other sums.
     */
    @Operation(summary = "Claim List Page: Claim List with Pagination")
    @JsonView(Views.ClaimList.class)
    @RequestMapping(path = "list/{statcode}/{page}", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity claimListWithPage(@PathVariable(value = "statcode") String statCode,
                                            @PathVariable(value = "page") int page, @RequestParam(value = "rows", defaultValue = "10") Integer rows) {
        int offsetNextRows = rows;
        int offsetRows = page * offsetNextRows;
        List<Claim> statistics = claimService.claimListAllWithPage(statCode, offsetRows, offsetNextRows);
        return buildResponseEntity((statistics));
    }

    /*
     * Claim list without Estimates or any other sums.
     */
    @Operation(summary = "Claim Search")
    @JsonView(Views.ClaimList.class)
    @RequestMapping(path = "search/{statcode}/", method = RequestMethod.POST, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity claimSearch(
            @PathVariable(value = "statcode") String statCode,
            @RequestBody ClaimListFilter filter) {
        if (filter.getStatCode() != null || StringUtils.isEmpty(statCode)) {
            return noContent();
        }
        List<Claim> list = claimService.claimListAllWithPage(filter);
        return buildResponseEntity((list));
    }

    @Operation(summary = "Claim List (Open)")
    @JsonView(Views.ClaimSimpleListOpen.class)
    @RequestMapping(path = "list/{statCcde}/open", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity listOfClaimsOpen(@PathVariable(value = "statcode") String statCode) {
        logger.info("Retrieve all Open Claims");
        List<Claim> list = claimService.claimListOpen(statCode);
        return buildResponseEntity(list);
    }

    @Operation(summary = "Claim (Single)")
    @JsonView({Views.ClaimDetail.class})
    @RequestMapping(path = "event/{statcode}/{eventno}/", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity claimEvent(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "eventno") String claimEventNo
    ) {
        Claim claim = claimService.claimEvent(statCode, claimEventNo);
        return buildResponseEntity(claim);
    }

    @Operation(summary = "Claim List Page: Export Claim list to Excel")
    @RequestMapping(path = claims_open_excel_StatCode, method = RequestMethod.GET, produces = {
            APPLICATION_JSON_VALUE})
    public ResponseEntity downloadListOfClaimsToExcel(@PathVariable(value = "statcode") String statCode) {
        Base64Container container = claimService.claimListAllAsBase64(statCode);
        return responseEntityWrapper(container);
    }

    @Operation(summary = "Claim List Page: Export Claim list to Excel (File)")
    @RequestMapping(path = claims_open_excel_file_StatCode, method = RequestMethod.GET)
    public void downloadListOfClaimsToExcelDirect(@PathVariable(value = "statcode") String statCode, HttpServletResponse response) {
        List<Claim> list = claimService.claimListAll(statCode);
        if (list.size() > 0) {
            ExcelListConverter excel = new ClaimListExcelConverter(list, ClaimListExcelConverter.getStaticFilename(list.get(0).getStatName()));
            excel.export(response);
        }
    }

    @Operation(summary = "Claim Detail Page: Export Single Claim to Excel")
    @RequestMapping(path = "event/excel/{statcode}/{eventno}/", method = RequestMethod.GET, produces = {
            APPLICATION_JSON_VALUE})
    public ResponseEntity downloadSingleClaimsToExcelBase64(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "eventno") String claimEventNo
    ) {
        logger.info("Download Claim Event as File");
        Base64Container container = claimService.claimAsBase64(statCode, claimEventNo);
        return responseEntityWrapper(container);
    }

    @Operation(summary = "Claim Detail Page: Export Single Claim to Excel")
    @RequestMapping(path = "event/excel/file/{statcode}/{eventno}/", method = RequestMethod.GET, produces = {
            APPLICATION_JSON_VALUE})
    public void downloadSingleClaimsToExcelFile(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "eventno") String claimEventNo, HttpServletResponse response) throws IOException {
        logger.info("Download Claim Event as base64.");
        Claim claim = claimService.claimEvent(statCode, claimEventNo);
        if (claim != null) {
            List<ClaimEstimates> estimates = claimService.estimates(statCode, claimEventNo);
            ClaimExcelConverter claimExcel = new ClaimExcelConverter(
                    "Claim Statistics for " + claim.getStatName() + " .xlsx", "Claim");
            claimExcel.export(claim, estimates, response);
        }
    }

    @Operation(summary = "Claim list Page: Download open claims as pdf")
    @RequestMapping(path = claims_open_pdf_StatCode_Product, method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity downloadOpenClaimsToPdfBase64(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "product") String product
    ) {
        Base64Container container = claimService.getClaimsToPdfBase64(statCode, product, open);
        return responseEntityWrapper(container);

    }

    @Operation(summary = "Claim list Page: Download claims as pdf")
    @RequestMapping(path = RESTendPoints.claims_all_pdf_StatCode_Product, method = RequestMethod.GET, produces = {
            APPLICATION_JSON_VALUE})
    public ResponseEntity downloadAllClaimsToPdfBase64(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "product") String product
    ) {
        Base64Container container = claimService.getClaimsToPdfBase64(statCode, product, all);
        return responseEntityWrapper(container);
    }

}
