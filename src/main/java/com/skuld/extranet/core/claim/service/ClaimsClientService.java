package com.skuld.extranet.core.claim.service;

import com.skuld.extranet.core.claim.client.ClaimsClient;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ClaimsClientService {

    private final ClaimsClient claimsClient;

    public ClaimsClientService(ClaimsClient claimsClient) {
        this.claimsClient = claimsClient;
    }

    public ClientClaimsSummaryDTO getClaimOverviewSummary(String statCode) {
        StopWatch stopWatch = StopWatch.createStarted();
        ClientClaimsSummaryDTO claimOverviewSummary = claimsClient.getClaimOverviewSummary(statCode);
        stopWatch.stop();

        if (stopWatch.getTime() > 400)
            log.warn("Slow Claim overview for stat code " + statCode + " uses " + stopWatch.getTime() + "ms");

        return claimOverviewSummary;
    }
}
