package com.skuld.extranet.core.claim.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ClientClaimsSummaryDTO {
    private long openClaims;
    private long vesselsWithClaim;
}
