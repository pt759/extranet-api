package com.skuld.extranet.core.claim.client;

import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "skuld.api.claims.client", url = "${skuld.api.claims.url}", configuration = ClaimsClientConfiguration.class)
public interface ClaimsClient {

    @GetMapping("/claims/list/open/{statcode}/")
    ClientClaimsSummaryDTO getClaimOverviewSummary(@PathVariable(value = "statcode") String statCode);

}
