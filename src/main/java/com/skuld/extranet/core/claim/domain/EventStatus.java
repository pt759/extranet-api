package com.skuld.extranet.core.claim.domain;

/*
Claim Status
*/
public enum EventStatus {
    open("Open"),
    closed("Closed"),
    all("all");

    private final String value;

    EventStatus(String value) {
        this.value = value;
    }

    public boolean equalsName(String value) {
        return value.equals(value);
    }

    public String toString() {
        return this.value;
    }
}
