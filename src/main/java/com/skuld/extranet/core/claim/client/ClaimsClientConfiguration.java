package com.skuld.extranet.core.claim.client;

import com.skuld.extranet.error.SkuldClientErrorDecoder;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import static com.skuld.common.security.authentication.SecurityValues.apiKeyHeader;

public class ClaimsClientConfiguration {

    @Value("${skuld.api.claims.api-key}")
    String apiKey;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header(apiKeyHeader, apiKey);
        };
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new SkuldClientErrorDecoder();
    }
}
