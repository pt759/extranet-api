package com.skuld.extranet.core.claim.excel;

import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.excel.ExcelListConverter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ClaimListExcelConverter extends ExcelListConverter {

    private static final String SheetName = "Claims";

    final static Logger logger = LoggerFactory.getLogger(ClaimListExcelConverter.class);

    public ClaimListExcelConverter(List<? extends Object> list, String filename) {
        super(list,filename, SheetName);
    }

    protected Row populateRowHeader(Sheet sheet) {
        Row rowHeader = sheet.createRow(0);
        CellStyle headerStyle = styleHelper.headerStyleWithSolidRedBackground();
        Integer column = 0;
        styleHelper.createCellWithStyle(column++, "Vessel Name", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Case Number", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Case Type", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Case Description", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Status", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Policy  Year", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Incident Date", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Incident Place", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Product", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "IMO", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Vessel Type", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Guarantees", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Claim Handler", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Payments", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Deductibles", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Reserves", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Total USD", headerStyle, rowHeader);
        return rowHeader;
    }

    protected void populateDataRows(Sheet sheet) {
        CellStyle rowStyle = styleHelper.styleGeneric();
        int startRow = 1;
        for (Claim item : (List<Claim>) list) {
            Row row = sheet.createRow(startRow++);
            styleHelper.createCellWithStyle(0, item.getVesselName(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(1, item.getClaEventNo(), rowStyle, row);
            styleHelper.createCellWithStyle(2, item.getClaimsTypeDesc(), rowStyle, row);
            styleHelper.createCellWithStyle(3, item.getIncidentDesc(), rowStyle, row);
            styleHelper.createCellWithStyle(4, item.getStatus(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(5, item.getStatYear(), rowStyle, row);
            styleHelper.createCellWithStyle(6, item.getIncidentDateAsString(), rowStyle, row);
            styleHelper.createCellWithStyle(7, item.getIncidentPlace(), rowStyle, row);
            styleHelper.createCellWithStyle(8, item.getProductAsString(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(9, item.getImoNumber(), rowStyle, row);
            styleHelper.createCellWithStyle(10, item.getVesselType(), rowStyle, row);
            styleHelper.createCellWithStyle(11, item.getGuarantee(), rowStyle, row);
            styleHelper.createCellWithStyle(12, item.getEventHandlerUserName(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(13, item.getNetPayment(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(14, item.getDeductibles(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(15, item.getNetReserves(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(16, item.getTotalUsd(), rowStyle, row);
        }
    }


    protected void populateFooterRow(Sheet sheet) {

    }



    public static String getStaticFilename(String name) {
        return "Claim Statistics for " + name + " .xlsx";
    }
}
