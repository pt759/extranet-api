package com.skuld.extranet.core.claim.domain;

public class Views {

    public interface ClaimBasic {
    }

    public interface ClaimLatest extends ClaimBasic {
    }

    public interface ClaimList extends ClaimBasic {
    }

    public interface ClaimSimpleListOpen extends ClaimBasic {
    }

    public interface ClaimDetail extends ClaimList {
    }

}
