package com.skuld.extranet.core.claim.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

@AllArgsConstructor
@Builder
@Data
public class ClaimEstimates {

    @JsonProperty("itemTypeCode")
    private String itemTypeCode;

    @JsonProperty("itemType")
    private String itemType;

    @JsonProperty("itemGroup")
    private String itemGroup;

    @JsonProperty("caseNumber")
    private String claEventNo;


    private Long reserves;
    private Long payments;
    private Long total;

    public ClaimEstimates() {
        this.reserves = 0L;
        this.payments = 0L;
        this.total = 0L;


    }

    public ClaimEstimates(ResultSet r) throws SQLException {
        this.claEventNo = r.getString("cla_event_no");
        this.itemTypeCode = r.getString("item_type_code");
        this.itemType = r.getString("item_type");
        this.itemGroup = r.getString("item_group");

        this.reserves = r.getLong("reserves");
        this.payments = r.getLong("payments");
        this.total = r.getLong("total");

    }

    public static class Mapper implements RowMapper<ClaimEstimates> {
        public ClaimEstimates map(ResultSet r, StatementContext ctx) throws SQLException {
            return new ClaimEstimates(r);
        }
    }

    public String getItemTypeCode() {
        return itemTypeCode;
    }

    public ClaimEstimates setItemTypeCode(String itemTypeCode) {
        this.itemTypeCode = itemTypeCode;
        return this;
    }

    public String getItemType() {
        return itemType;
    }

    public ClaimEstimates setItemType(String itemType) {
        this.itemType = itemType;
        return this;
    }

    public String getItemGroup() {
        return itemGroup;
    }

    public ClaimEstimates setItemGroup(String itemGroup) {
        this.itemGroup = itemGroup;
        return this;
    }

    public String getClaEventNo() {
        return claEventNo;
    }

    public ClaimEstimates setClaEventNo(String claEventNo) {
        this.claEventNo = claEventNo;
        return this;
    }

    public Long getReserves() {
        return reserves;
    }

    public ClaimEstimates setReserves(Long reserves) {
        this.reserves = reserves;
        return this;
    }

    public Long getPayments() {
        return payments;
    }

    public ClaimEstimates setPayments(Long payments) {
        this.payments = payments;
        return this;
    }

    public Long getTotal() {
        return total;
    }

    public ClaimEstimates setTotal(Long total) {
        this.total = total;
        return this;
    }
}
