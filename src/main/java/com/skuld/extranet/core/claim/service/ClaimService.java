package com.skuld.extranet.core.claim.service;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.archive.service.CoreArchiveClient;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.core.claim.domain.ClaimListFilter;
import com.skuld.extranet.core.claim.domain.EventStatus;
import com.skuld.extranet.core.claim.excel.ClaimExcelConverter;
import com.skuld.extranet.core.claim.excel.ClaimListExcelConverter;
import com.skuld.extranet.core.claim.repo.ClaimDetailRepository;
import com.skuld.extranet.core.common.domain.Base64ContainerForExcel;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.common.domain.LossRatio;
import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.core.user.domain.TiaUser;
import com.skuld.extranet.core.user.service.TiaUserService;
import com.skuld.extranet.ldap.LDAPService;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.UrlLink;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.skuld.extranet.global.RESTendPoints.*;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service
@Deprecated
public class ClaimService extends DBDwhRepo {

    final static Logger logger = LoggerFactory.getLogger(ClaimService.class);

    public static final String key_total_claims = "totalClaims";
    public static final String key_year = "year";

    private ClaimDetailRepository repo;
    private LDAPService ldapService;
    private TiaUserService tiaUserService;
    private CoreArchiveClient coreArchiveClient;

    @Autowired
    public ClaimService(ClaimDetailRepository claimDetailRepository,
                        LDAPService ldapService,
                        TiaUserService tiaUserService,
                        CoreArchiveClient coreArchiveClient) {
        this.repo = claimDetailRepository;
        this.ldapService = ldapService;
        this.tiaUserService =tiaUserService;
        this.coreArchiveClient = coreArchiveClient;

    }

    public ClaimService(ClaimDetailRepository repo) {
        this.repo = repo;
    }


    public List<Claim> claimListOpen(String statCode) {
        return claimListWithEvenHandler(statCode, null, EventStatus.open.toString());
    }

    public List<Claim> claimListAll(String statCode) {
        return claimListWithEvenHandler(statCode, null, null);
    }


    public List<Claim> claimListOpenWithoutEventHandler(String statCode) {
        List<Claim> claims = repo.listOfClaims(statCode, null, EventStatus.open.toString());
        return claims;
    }

    private List<Claim> claimListWithEvenHandler(String statCode, String products, String status) {
        List<Claim> claims = repo.listOfClaims(statCode, products, status);
        addEventHandlerToClaims(claims);
        return claims;
    }

    public List<Claim> claimListAllWithPage(String statCode, int offsetRows, int offsetNextRows) {
        return claimListWithPage(statCode, null, null, offsetRows, offsetNextRows);
    }

    public List<Claim> claimListAllWithPage(ClaimListFilter filter) {
        List<Claim> claims = claimListAll(filter.getStatCode());
        claims = filterClaims(claims, filter);
        addEventHandlerToClaims(claims);
        return claims;

    }

    public List<Claim> filterClaims(List<Claim> claims, ClaimListFilter filter) {
        List<Claim> list = claims;

        if (isNotEmpty(filter.getVesselName())) {
            list = filterOnVesselName(claims, filter);

        } else if (isNotEmpty(filter.getCaseNumber())) {
            list = claims.stream()
                    .filter(line -> (line.getClaEventNo().startsWith(filter.getCaseNumber())))
                    .collect(Collectors.toList());
        } else {

            list = claims.stream()
                    .filter(line -> (filter.getYears().contains(ClaimListFilter.all) || filter.getYears().contains(line.getStatYear())))
                    .filter(line -> (filter.getCaseType().contains(ClaimListFilter.all) || filter.getCaseType().contains(line.getClaimsType())))
                    .filter(line -> (filter.getStatCode().contains(ClaimListFilter.all) || filter.getStatCode().equalsIgnoreCase(line.getStatCode())))
                    .filter(line -> (filter.getProduct().contains(ClaimListFilter.all) || filter.getProduct().contains(line.getProductAsString())))
                    .filter(line -> (filter.getStatus().contains(ClaimListFilter.all) || filter.getStatus().contains(line.getStatus())))
                    .collect(Collectors.toList());
        }

        if (filter.getPageSize() > 0) {
            list = list.stream()
                    .skip((filter.skipRows()))
                    .limit(filter.getPageSize())
                    .collect(Collectors.toList());
        }

        return list;
    }

    private List<Claim> filterOnVesselName(List<Claim> claims, ClaimListFilter filter) {
        List<Claim> list;
        list = claims.stream()
                .filter(line -> (filter.getVesselName().equalsIgnoreCase(line.getVesselName())))
                .collect(Collectors.toList());
        return list;
    }

    private List<Claim> claimListWithPage(String statCode, String products, String status, int offsetRows, int offsetNextRows) {
        List<Claim> claimDetailVS = repo.listOfClaimsWithPage(statCode, products, status, offsetRows, offsetNextRows);
        addEventHandlerToClaims(claimDetailVS);
        return claimDetailVS;
    }

    private void addEventHandlerToClaims(List<Claim> claims) {
        claims.forEach(item -> {
                    Optional<TiaUser> sAMAccountName = Optional.ofNullable(tiaUserService.getUser(item.getEventHandler()));

                    if (sAMAccountName.isPresent()) {
                        item.setEventHandlerLdapUser(ldapService.matchUserOnSaMAccountName(sAMAccountName.get().getPtUserId()));
                    }
                }
        );
    }


    @Deprecated
    public List<CodeValue> claimCountForCurrentYear(String statCode) {
        List<CodeValue> codeValues = repo.claimCountForCurrentYear(statCode);
        codeValues.add(createTotalCodeValueForClaimCount(codeValues));
        return codeValues;
    }


    @Deprecated
    public List<CodeValue> claimCountForCurrentYearWithGroupOthers(String statCode, Integer limit) {
        List<CodeValue> list = repo.claimCountForCurrentYear(statCode);

        if (list.size() > limit) {
            list = CodeValue.groupListWithLimitOf(limit, list);
        }

        list.add(createTotalCodeValueForClaimCount(list));

        return list;
    }


    private CodeValue createTotalCodeValueForClaimCount(List<CodeValue> codeValues) {
        Integer totalClaim = codeValues.stream().mapToInt(codeValue -> Integer.valueOf(codeValue.getValue())).sum();
        return new CodeValue(key_total_claims, totalClaim.toString(), key_total_claims);
    }

    public List<Claim> recentClaimsForStatCode(String statCode, String eventStatus, int limit) {
        List<Claim> list = repo.listOfClaims(statCode, null, eventStatus);
        List<Claim> result = limitAndSortClaimList(limit, list);
        return result;
    }

    public List<Claim> limitAndSortClaimList(int limit, List<Claim> list) {
        return list.stream()
                .sorted(Comparator.comparing(Claim::getIncidentDate)
                        .reversed())
                .limit(limit)
                .collect(Collectors.toList());
    }

    public List<ClaimEstimates> estimates(String statCode, String claimEventNo) {
        List<ClaimEstimates> estimates = repo.estimates(statCode, claimEventNo);
        estimates = estimates.stream().distinct().collect(Collectors.toList());
        estimates.add(calculateTotalEstimateEntry(estimates));
        return estimates;
    }

    private ClaimEstimates calculateTotalEstimateEntry(List<ClaimEstimates> estimates) {
        ClaimEstimates totalEstimates = new ClaimEstimates();
        totalEstimates.setItemTypeCode("TOTAL");
        totalEstimates.setItemType("Total");

        estimates.stream().forEach(item -> {
            totalEstimates.setPayments(totalEstimates.getPayments() + item.getPayments());
            totalEstimates.setReserves(totalEstimates.getReserves() + item.getReserves());
            totalEstimates.setTotal(totalEstimates.getTotal() + item.getTotal());
        });
        return totalEstimates;
    }

    public Claim claimEvent(String statCode, String claimEventNo) {
        if (!StringUtils.isNumeric(claimEventNo)) {
            return null;
        }
        Claim claim = repo.claim(statCode, claimEventNo);
        setClaimHandler(claim);
        return claim;
    }

    private void setClaimHandler(Claim claim) {
        if (claim != null) {
            try {
                TiaUser tiaUser = tiaUserService.getUser(claim.getEventHandler());
                claim.setEventHandlerLdapUser(ldapService.findFirstUserBySamaAccount(tiaUser.getPtUserId()));
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

    public LossRatio lossRatioForClientMock(String statCode) {

        LossRatio lossRatio = new LossRatio();
        List<CodeValue> list = new ArrayList<>();
        CodeValue value2 = new CodeValue("LR", "Loss Ratio", "5");
        list.add(value2);
        lossRatio.setFiveYear(list);
        return lossRatio;
    }

    public ClaimService setRepo(ClaimDetailRepository repo) {
        this.repo = repo;
        return this;
    }

    public Base64Container claimAsBase64(String statCode, String claimEventNo) {
        Base64Container container = new Base64Container();
        Claim claim = claimEvent(statCode, claimEventNo);
        if (claim != null) {
            List<ClaimEstimates> estimates = estimates(statCode, claimEventNo);
            ClaimExcelConverter claimExcel = new ClaimExcelConverter("Claim Statistics for " + claim.getStatName() + " .xlsx", "Claim");
            String base64Text = claimExcel.getExcelAsBase64Text(claim, estimates);
            String filename = "CaseNumber_" + claimEventNo + ".xlsx";
            container = Base64ContainerForExcel.getBase64ContainerWithExcel(base64Text, filename);
        }
        return container;
    }

    public Base64Container claimListAllAsBase64(String statCode) {
        List<Claim> list = claimListAll(statCode);
        if (list.size() > 0) {
            ClaimListExcelConverter excel = new ClaimListExcelConverter(list, ClaimListExcelConverter.getStaticFilename(list.get(0).getStatName()));
            return Base64ContainerForExcel.getBase64ContainerWithExcel(excel);
        }
        return null;
    }

    public List<Claim> filterClaimsOn(String query, List<Claim> claims) {
        List<Claim> list = claims.stream()
                .filter(doesVesselnameOrImoOrEventNumberContain(query))
                .collect(Collectors.toList());
        return list;
    }

    private Predicate<Claim> doesVesselnameOrImoOrEventNumberContain(String query) {
        return c -> (c.getVesselName().contains(query)) || (c.getImoNumber().contains((query))) || (c.getClaEventNo().contains(query));
    }


    public UrlLink getListOfClaimExcelUrls(String statCode) {
        return new UrlLink("All Claims", urlForAllClaimsAsExcel(statCode), UrlLink.CODE_EXCEL);
    }

    public List<UrlLink> getListOfClaimUrls(String statCode, Product product) {
        List<UrlLink> list = new ArrayList<>();
        list.add(new UrlLink("Open Claims (" + product.description() + ")", urlForOpenClaimAsPDF(statCode, product), UrlLink.CODE_PDF));
        list.add(new UrlLink("All Claims (" + product.description() + ")", urlForAllClaimAsPDF(statCode, product), UrlLink.CODE_PDF));
        return list;
    }

    public boolean claimEventExists(String statCode, String eventNo) {
        return repo.claimEventExists(statCode, eventNo);
    }

    public void setupLdapService(LDAPService ldapService) {
        this.ldapService = ldapService;
    }

    public void setupTiaUserService(TiaUserService tiaUserService) {
        this.tiaUserService = tiaUserService;
    }

    public Base64Container getClaimsToPdfBase64(String statCode, String product, EventStatus all) {
        return coreArchiveClient.getClaimsToPdfBase64(statCode, product, all);
    }
}

