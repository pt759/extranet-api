package com.skuld.extranet.core.claim.repo;


import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.common.repo.DBDwhRepo;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClaimDetailRepository extends DBDwhRepo {
    final static Logger logger = LoggerFactory.getLogger(ClaimDetailRepository.class);


    public ClaimDetailRepository() {
    }

    public List<ClaimEstimates> estimates(String statCode, String claimEventNo) {
        List<ClaimEstimates> list = getJdbi().withExtension(ClaimDao.class, dao -> dao.claimEstimatesFor(statCode, claimEventNo));
        return list;
    }

    public Boolean claimEventExists(String statCode, String claimEventNo) {
        Integer result = getJdbi().withExtension(ClaimDao.class, dao -> dao.claimEventExists(statCode, claimEventNo));
        return result != null && result.equals(1);
    }

    public Claim claim(String statCode, String claimEventNo) {
        Claim claimDetail = getJdbi()
                .withExtension(ClaimDao.class, dao -> {
                    // TODO: Fix this shit - "Should only return one row, or do we return many potential rows and combine the products?
                    List<Claim> list = dao.claimEvent(statCode, claimEventNo);
                    return list.size() > 0 ? list.get(0) : null;
                });

        return claimDetail;

    }

    public List<Claim> listOfClaims(String statCode, String productLine, String eventStatus) {
        List<Claim> list = getJdbi().withExtension(ClaimDao.class, dao -> dao.list(statCode, productLine, eventStatus));
        return list;
    }

    public List<Claim> listOfClaimsWithPage(String statCode, String productLine, String eventStatus, int offsetRows, int offsetNextRows) {
        List<Claim> list = getJdbi().withExtension(ClaimDao.class, dao -> dao.listWithPage(statCode, productLine, eventStatus, offsetRows, offsetNextRows));
        return list;
    }

    @Deprecated
    public List<CodeValue> claimCountForCurrentYear(String statCode) {
        Jdbi jdbi = getJdbi().registerRowMapper(new CodeValue.Mapper());
        List<CodeValue> list = jdbi.withExtension(ClaimDao.class, dao -> dao.claimCountForCurrentYear(statCode));
        return list;
    }


}
