package com.skuld.extranet.core.claim.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.skuld.extranet.core.common.ListFilterWithPagination;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({"statCode", "caseNumber", "VesselName", "years", "status", "caseType", "product", "page", "pageSize"})
public class ClaimListFilter extends ListFilterWithPagination {

    public static String all = "*";
    private String statCode; // Mandatory

    private String caseNumber;
    private String vesselName;
    private String imo;

    private List<String> years;
    private List<String> status;

    public List<String> caseType;
    public List<String> product;

    private String queryString ;

    public ClaimListFilter() {
        this.vesselName = "";
        this.imo = "";
        this.caseNumber = "";

        this.statCode = "";
        this.years = new ArrayList<>();
        this.caseType = new ArrayList<>();
        this.product = new ArrayList<>();
        this.status = new ArrayList<>();


    }

    public ClaimListFilter status(List<String> status) {
        this.status = status;
        return this;
    }

    public String getStatCode() {
        return statCode;
    }

    public List<String> getYears() {
        return years;
    }

    public List<String> getCaseType() {
        return caseType;
    }

    public List<String> getProduct() {
        return product;
    }



    public ClaimListFilter years(List<String> years) {
        this.years = years;
        return this;
    }

    public ClaimListFilter caseType(List<String> caseType) {
        this.caseType = caseType;
        return this;
    }

    public ClaimListFilter setProduct(List<String> product) {
        this.product = product;
        return this;
    }



    public ClaimListFilter statCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public List<String> getStatus() {
        return status;
    }

    public ClaimListFilter setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public ClaimListFilter setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
        return this;
    }

    public String getVesselName() {
        return vesselName;
    }

    public String getCaseNumber() {
        return caseNumber;
    }


    public void setImo(String imo) {
        this.imo = imo;
    }

    public String getImo() {
        return imo;
    }

    public String getQueryString() {
        return queryString;
    }

    public ClaimListFilter setQueryString(String queryString) {
        this.queryString = queryString;
        return this;
    }
}
