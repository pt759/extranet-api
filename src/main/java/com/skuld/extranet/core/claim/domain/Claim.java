package com.skuld.extranet.core.claim.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.common.SkuldSqlHelper;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.spa.pages.PageEntityView;
import com.skuld.extranet.spa.pages.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static com.skuld.extranet.core.common.SkuldFormatter.formatLocaldateFor;
import static com.skuld.extranet.spa.pages.domain.Product.fromString;

@AllArgsConstructor
@Builder
@Data
public class Claim {

    @JsonView({Views.ClaimList.class})
    @JsonProperty("policyYear")
    private String statYear;

    @JsonView({Views.ClaimLatest.class, PageEntityView.SearchListBroker.class})
    @JsonProperty("clientName")
    private String statName;

    @JsonView({Views.ClaimLatest.class, PageEntityView.SearchListBroker.class})
    @JsonProperty("statCode")
    private String statCode;

    @JsonView({Views.ClaimBasic.class})
    @JsonProperty("caseNumber")
    private String claEventNo;

    @JsonView({Views.ClaimBasic.class})
    @JsonProperty("vesselName")
    private String vesselName;

    @JsonIgnore
    @JsonProperty("caseTypeCode")
    private String claimsType;

    @JsonView({Views.ClaimBasic.class})
    @JsonProperty("caseType")
    private String claimsTypeDesc;

    private LocalDate incidentDate;

    @JsonProperty("handler")
    private String eventHandler;//initials

    @JsonView(Views.ClaimList.class)
    @JsonProperty("status")
    private String status;

    @JsonView(Views.ClaimList.class)
    @JsonProperty("description")
    private String incidentDesc;

    @JsonView(Views.ClaimDetail.class)
    @JsonProperty("incidentPlace")
    private String incidentPlace;

    @JsonView(Views.ClaimList.class)
    private String product;

    @JsonView(Views.ClaimDetail.class)
    @JsonProperty("imo")
    private String imoNumber;

    @JsonView(Views.ClaimDetail.class)
    private String vesselType;

    @JsonView(Views.ClaimDetail.class)
    private String guarantee;

    private String membersRef;

    private String conflictEvent;
    private String netReserves;
    private String netPayment;
    private String deductibles;
    private String totalUsd;
    private String claims;
    private String statusCode;
    private String vesselTypeCode;


    private LDAPUser eventHandlerUser;
    private String statCurrencyType;
    private String statCurrency;

    @JsonView(Views.ClaimDetail.class)
    private LDAPUser eventHandlerLdapUser;

    public Claim() {}

    public Claim(ResultSet rs) throws SQLException {
        this.statName = rs.getString("stat_name");
        this.statYear = rs.getString("stat_year");
        this.statCode = rs.getString("stat_code");
        this.statCurrency = rs.getString("stat_currency");
        this.statCurrencyType = rs.getString("stat_curr_type");
        this.claEventNo = rs.getString("cla_event_no");
        this.vesselName = rs.getString("vessel_name");
        this.claimsType = rs.getString("claims_type");
        this.claimsTypeDesc = rs.getString("claims_type_desc");

        this.incidentDate = SkuldSqlHelper.fromSqlDate(rs.getDate("incident_date"));
        this.eventHandler = rs.getString("event_handler");

        this.statusCode = rs.getString("status_code");
        this.status = rs.getString("status");

        this.incidentDesc = rs.getString("incident_desc");
        this.incidentPlace = rs.getString("incident_place");
        this.product = rs.getString("product");
        this.imoNumber = rs.getString("imo_number");
        this.vesselTypeCode = rs.getString("vessel_type_code");
        this.vesselType = rs.getString("vessel_type");
        this.membersRef = rs.getString("members_ref");

        this.guarantee = rs.getString("guarantee");
        this.netReserves = rs.getString("net_reserves");
        this.netPayment = rs.getString("net_payment");
        this.deductibles = rs.getString("deductibles");
        this.totalUsd = rs.getString("total_usd");

        this.conflictEvent = rs.getString("conflict_event");

    }

    @JsonView({Views.ClaimBasic.class})
    @JsonProperty("incidentDate")
    public String getIncidentDateAsString() {
        return formatLocaldateFor(incidentDate);
    }

    public static class Mapper implements RowMapper<Claim> {
        public Claim map(ResultSet r, StatementContext ctx) throws SQLException {
            return new Claim(r);
        }
    }

    @JsonView(Views.ClaimSimpleListOpen.class)
    public String getEventHandlerName() {
        return eventHandlerUser != null ? eventHandlerUser.getName() : "";
    }

    @JsonView({Views.ClaimSimpleListOpen.class, Views.ClaimDetail.class})
    public String getEventHandlerMail() {
        return eventHandlerUser != null ? eventHandlerUser.getEmail() : "";
    }

    @JsonView({Views.ClaimSimpleListOpen.class})
    public String getEventHandlerUserName() {
        return eventHandlerUser != null ? eventHandlerUser.getName() : "";
    }

    public LDAPUser getEventHandlerUser() {
        return eventHandlerUser;
    }

    public void setEventHandlerUser(LDAPUser eventHandlerUser) {
        this.eventHandlerUser = eventHandlerUser;
    }

    public LDAPUser getEventHandlerLdapUser() {
        return eventHandlerLdapUser;
    }

    public Claim setEventHandlerLdapUser(LDAPUser user) {
        this.eventHandlerLdapUser = user;
        //TODO: dont need both fields, but for now.
        this.eventHandlerUser = user;
        return this;
    }

    @JsonIgnore
    public String getProductAsString() {
        return product;
    }

    @JsonIgnore
    public Product getProduct() {
        return fromString(product);
    }
}
