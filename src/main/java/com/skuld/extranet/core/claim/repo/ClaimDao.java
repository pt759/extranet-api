package com.skuld.extranet.core.claim.repo;


import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.common.domain.CodeValue;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface ClaimDao extends ClosableDao {


    @SqlQuery("select * from mv_claims " +
            " where stat_code = :stat_code " +
            " and ( :product is null or product = :product) " +
            " and ( :status is null or status = :status )" +
            " order by incident_date desc")
    @RegisterRowMapper(Claim.Mapper.class)
    List<Claim> list(@Bind("stat_code") String statCode,
                     @Bind("product") String product,
                     @Bind("status") String eventStatus);

    @SqlQuery("select * from mv_claims " +
            " where stat_code = :stat_code " +
            " and ( :product is null or product = :product ) " +
            " and ( :status is null or status = :status )" +
            " order by stat_year desc, vessel_name " +
            " OFFSET :page ROWS FETCH NEXT :pageSize ROWS ONLY ")
    @RegisterRowMapper(Claim.Mapper.class)
    List<Claim> listWithPage(@Bind("stat_code") String statCode,
                             @Bind("product") String product,
                             @Bind("status") String eventStatus,
                             @Bind("page") int offsetRows,
                             @Bind("pageSize") int offsetNextRows
    );


    @SqlQuery("select * from mv_claims where cla_event_no = :cla_event_no and stat_code =:statCode")
    @RegisterRowMapper(Claim.Mapper.class)
    List<Claim> claimEvent(@Bind("statCode") String statCode,
                           @Bind("cla_event_no") String claimEventNo);


    @SqlQuery("select 1 from mv_claims where cla_event_no = :cla_event_no and stat_code =:statCode")
    @RegisterRowMapper(Claim.Mapper.class)
    Integer claimEventExists(@Bind("statCode") String statCode,
                           @Bind("cla_event_no") String claimEventNo);

    @Deprecated
    @SqlQuery("SELECT claims_type code, claims_type_desc description, claims value " +
            " FROM V_STATISTICS_CLAIMS_CURR_YEAR " +
            " where stat_code = :stat_code " +
            " order by value desc "
    )
    List<CodeValue> claimCountForCurrentYear(@Bind("stat_code") String statCode);

    @SqlQuery("select * from MV_CLAIMS_DETAILS_ESTIMATE v inner join mv_claims c on c.cla_event_no = v.cla_event_no " +
            " where v.cla_event_no = :cla_event_no and c.stat_code = :statCode " +
            " order by item_group ")
    @RegisterRowMapper(ClaimEstimates.Mapper.class)
    List<ClaimEstimates> claimEstimatesFor(@Bind("statCode") String statCode, @Bind("cla_event_no") String claimEventNo);

}
