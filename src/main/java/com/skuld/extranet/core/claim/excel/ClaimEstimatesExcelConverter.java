package com.skuld.extranet.core.claim.excel;

import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.excel.ExcelListConverter;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ClaimEstimatesExcelConverter extends ExcelListConverter {

    final static Logger logger = LoggerFactory.getLogger(ClaimEstimatesExcelConverter.class);

    public ClaimEstimatesExcelConverter(List<? extends Object> list, String filename, String sheetName) {
        super(list, filename, sheetName);
    }

    protected Row populateRowHeader(Sheet sheet) {
        Row rowHeader = sheet.createRow(0);
        CellStyle headerStyle = styleHelper.headerStyleWithSolidRedBackground();
        Integer column = 0;
        styleHelper.createCellWithStyle(column++, "Item description", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Payments", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Reserves", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Total", headerStyle, rowHeader);
        return rowHeader;
    }

    protected void populateDataRows(Sheet sheet) {
        CellStyle rowStyle = styleHelper.styleGeneric();
        int startRow = 1;
        for (ClaimEstimates item : (List<ClaimEstimates>) list) {

            if (startRow == list.size()) {
                rowStyle = styleHelper.styleGeneric();
                rowStyle.setBorderTop(BorderStyle.THIN);
                rowStyle.setBorderBottom(BorderStyle.DOUBLE);
            }

            Row row = sheet.createRow(startRow++);
            styleHelper.createCellWithStyle(0, item.getItemType(), rowStyle, row);
            styleHelper.createNumericCellWithStyle(1, String.valueOf(item.getPayments()), rowStyle, row);
            styleHelper.createNumericCellWithStyle(2, String.valueOf(item.getReserves()), rowStyle, row);
            styleHelper.createNumericCellWithStyle(3, String.valueOf(item.getTotal()), rowStyle, row);


        }
    }

    @Override
    protected void populateFooterRow(Sheet sheet) {

    }

}
