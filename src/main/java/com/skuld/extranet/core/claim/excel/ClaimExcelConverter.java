package com.skuld.extranet.core.claim.excel;

import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.excel.ExcelFormat;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

public class ClaimExcelConverter extends ExcelFormat {

    final static Logger logger = LoggerFactory.getLogger(ClaimExcelConverter.class);

    private int columnForEstimates = 0;

    public ClaimExcelConverter(String filename, String sheetName) {
        super(filename, sheetName);
    }

    public void export(Claim claim, List<ClaimEstimates> estimates, HttpServletResponse response) {


        response.addHeader("Content-Transfer-Encoding", "binary");
        response.addHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");

        ServletOutputStream outputStream;
        try {
            outputStream = response.getOutputStream();
            Workbook workBook = createSheet(getSheetName(), claim, estimates);
            workBook.write(outputStream);
            workBook.close();
            outputStream.close();
            response.flushBuffer();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public String getExcelAsBase64Text(Claim claim, List<ClaimEstimates> estimates) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Workbook workBook = createSheet(getSheetName(), claim, estimates);
            workBook.write(out);
            workBook.close();
            out.close();

            String base64Text = Base64.encodeBase64String(out.toByteArray());
//            logger.info("Base64Text: " + base64Text);
            return base64Text;
        } catch (Exception e) {
            logger.error("error: " + e.getMessage());
        }

        return "";
    }


    public File getFile(Claim claim, List<ClaimEstimates> estimates, String filename) {

        try {
            File file = new File(filename);
            FileOutputStream out = new FileOutputStream(file);

            Workbook workBook = createSheet(getSheetName(), claim, estimates);
            workBook.write(out);
            workBook.close();
            out.close();
            out.flush();
            return file;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public Workbook createSheet(String sheetName, Claim claim, List<ClaimEstimates> estimates) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row rowHeader = sheet.createRow(0);
        populateRowHeader(rowHeader);
        populateDataRows(claim, estimates, sheet);

        //Auto size all the columns
        for (int x = 0; x < sheet.getRow(0).getPhysicalNumberOfCells(); x++) {
            sheet.autoSizeColumn(x);
        }


        return workbook;
    }


    protected void populateRowHeader(Row rowHeader) {
        CellStyle headerStyle = styleHelper.headerStyleWithSolidRedBackground();
        Integer column = 0;
        styleHelper.createCellWithStyle(column++, "Status", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Policy year", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Vessel Name", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Case Number", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Members Ref", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Product", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Claim Handler", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Case Type", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Case Description", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Incident Date", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Incident Place", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Guarantees", headerStyle, rowHeader);

        columnForEstimates = column;
        styleHelper.createCellWithStyle(column++, "Item description", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Payments", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Reserves", headerStyle, rowHeader);
        styleHelper.createCellWithStyle(column++, "Total USD", headerStyle, rowHeader);
    }

    protected void populateDataRows(Claim claim, List<ClaimEstimates> estimates, Sheet sheet) {
        CellStyle rowStyle = styleHelper.styleGeneric();
        int startRow = 1;

        Row row = sheet.createRow(startRow);
        Integer column = 0;
        styleHelper.createCellWithStyle(column++, claim.getStatus(), rowStyle, row);
        styleHelper.createNumericCellWithStyle(column++, claim.getStatYear(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getVesselName(), rowStyle, row);
        styleHelper.createNumericCellWithStyle(column++, claim.getClaEventNo(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getMembersRef(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getProductAsString(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getEventHandlerUserName(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getClaimsTypeDesc(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getIncidentDesc(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getIncidentDateAsString(), rowStyle, row);
        styleHelper.createCellWithStyle(column++, claim.getIncidentPlace(), rowStyle, row);

        styleHelper.createCellWithStyle(column++, claim.getGuarantee(), rowStyle, row);

        styleHelper.createCellWithStyle(column++, "", rowStyle, row);
        styleHelper.createCellWithStyle(column++, "", rowStyle, row);
        styleHelper.createCellWithStyle(column++, "", rowStyle, row);
        styleHelper.createCellWithStyle(column++, "", rowStyle, row);
        styleHelper.createCellWithStyle(column++, "", rowStyle, row);

        if (estimates != null && estimates.size() > 0) {
            populateEstimates(estimates, sheet);
        }
    }

    private void populateEstimates(List<ClaimEstimates> estimates, Sheet sheet) {
        CellStyle rowStyle = styleHelper.styleGeneric();
        int startRow = 2;
        for (ClaimEstimates item : estimates) {

            if ((startRow - 1) == estimates.size()) {
                rowStyle = styleHelper.styleGenericNew();
                rowStyle.setBorderTop(BorderStyle.THIN);
                rowStyle.setBorderBottom(BorderStyle.DOUBLE);
            }

            Integer column = columnForEstimates;//12

            Row row = sheet.createRow(startRow++);
            styleHelper.createCellWithStyle(column++, item.getItemType(), rowStyle, row);
            styleHelper.createCellWithStyle(column++, "", rowStyle, row);
            styleHelper.createNumericCellWithStyle(column++, String.valueOf(item.getPayments()), rowStyle, row);
            styleHelper.createNumericCellWithStyle(column++, String.valueOf(item.getReserves()), rowStyle, row);
            styleHelper.createNumericCellWithStyle(column++, String.valueOf(item.getTotal()), rowStyle, row);
        }
    }


    protected void populateFooterRow(List<?> list, Sheet sheet) {

    }
}
