package com.skuld.extranet.core.tia.client.dto;

import com.skuld.extranet.core.tia.client.TiaApiMessageLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TiaStatusDTO {
    public String status_code;
    public String error_message;

    public TiaApiMessageLevel getStatus() {
        return TiaApiMessageLevel.from(status_code);
    }


}
