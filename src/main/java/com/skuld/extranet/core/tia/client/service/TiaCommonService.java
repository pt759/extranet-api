package com.skuld.extranet.core.tia.client.service;

import com.skuld.extranet.core.tia.client.TiaCommonClient;
import com.skuld.extranet.core.tia.client.dto.TiaBusinessUnitStatCodeGroupDTO;
import com.skuld.extranet.core.tia.client.dto.TiaUserDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TiaCommonService {

    private final TiaCommonClient tiaCommonClient;

    public TiaCommonService(TiaCommonClient tiaCommonClient) {
        this.tiaCommonClient = tiaCommonClient;
    }

    public Optional<List<TiaBusinessUnitStatCodeGroupDTO>> getBusinessUnitStatCodeGroups() {
        List<TiaBusinessUnitStatCodeGroupDTO> items = tiaCommonClient
                .getBusinessUnitStatCodeGroups()
                .getContent()
                .getItems();

        return Optional.of(items);
    }

    public Optional<List<TiaUserDTO>> getTiaUsers() {
        List<TiaUserDTO> items = tiaCommonClient
                .getTiaUsers()
                .getContent()
                .getItems();

        return Optional.of(items);
    }


}

