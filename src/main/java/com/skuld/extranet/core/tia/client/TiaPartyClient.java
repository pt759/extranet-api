package com.skuld.extranet.core.tia.client;

import com.skuld.extranet.core.tia.client.dto.TiaResultDTO;
import com.skuld.extranet.core.tia.client.dto.TiaStatCodeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "tia.party.client", url = "${tia.ords.api.url}", configuration = TiaClientConfiguration.class)
public interface TiaPartyClient {

    @GetMapping("/skStatCode/v1/groups/statCodes?group_name={statCodeGroupName}")
    EntityModel<TiaResultDTO<TiaStatCodeDTO>> getStatCodesForGroup(@PathVariable String statCodeGroupName);

    @GetMapping("/skStatCode/v1/groups/statCodes?stat_code={statCode}")
    EntityModel<TiaResultDTO<TiaStatCodeDTO>> getStatCode(@PathVariable String statCode);
}