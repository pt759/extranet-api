package com.skuld.extranet.core.tia.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TiaOperationDTO {
    private String error_type;
    private String error_message;
    private String logId;

    public boolean hasValue() {
        return !isBlank(error_message);
    }
}
