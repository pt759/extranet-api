package com.skuld.extranet.core.tia.client.dto;

import lombok.Data;

@Data
public class TiaBusinessUnitStatCodeGroupDTO {
    private String dept_name;
    private String dept_no;
    private String login_group;
}
