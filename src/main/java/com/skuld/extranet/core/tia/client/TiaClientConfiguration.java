package com.skuld.extranet.core.tia.client;

import com.skuld.common.tia.SkuldTiaClientErrorDecoder;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

import java.util.Arrays;

@Profile("!dev")
public class TiaClientConfiguration {

    @Value("${tia.ords.api.token}")
    String urlToken;

    @Value("${tia.ords.api.clientid}")
    String clientId;

    @Value("${tia.ords.api.secret}")
    String secret;

    @Bean
    public ErrorDecoder errorDecoder() {
        return new SkuldTiaClientErrorDecoder();
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return new OAuth2FeignRequestInterceptor(new DefaultOAuth2ClientContext(), resource());
    }

    private OAuth2ProtectedResourceDetails resource() {
        final ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
        details.setAccessTokenUri(urlToken);
        details.setClientId(clientId);
        details.setClientSecret(secret);
        details.setScope(Arrays.asList("read", "write"));
        return details;
    }

}
