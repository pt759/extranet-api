package com.skuld.extranet.core.tia.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TiaResultDTO<T> extends RepresentationModel {
    List<TiaStatusDTO> status;
    List<TiaOperationDTO> operation;
    List<T> items;

    public boolean containStatus() {
        return !isEmpty(status);
    }

    public boolean containItems() {
        return !isEmpty(items);
    }


}
