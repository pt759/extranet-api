package com.skuld.extranet.core.tia.client.dto;

import lombok.Data;

@Data
public class TiaStatCodeDTO {
    private String stat_code;
    private String name;
    private String declaration;
    private String products;
    private String group_name;
    private String user_name;
    private Long service_agent_no;
}
