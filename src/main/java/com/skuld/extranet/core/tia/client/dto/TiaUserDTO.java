package com.skuld.extranet.core.tia.client.dto;

import lombok.Data;

@Data
public class TiaUserDTO {
    private String user_id;
    private String user_name;
    private String pt_user_id;
    private String email_with_userid;
    private String email;
    private String description;
    private String phone_extent;
}
