package com.skuld.extranet.core.tia.client;

import com.skuld.extranet.core.tia.client.dto.TiaBusinessUnitStatCodeGroupDTO;
import com.skuld.extranet.core.tia.client.dto.TiaResultDTO;
import com.skuld.extranet.core.tia.client.dto.TiaUserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "tia.common.client", url = "${tia.ords.api.url}", configuration = TiaClientConfiguration.class)
public interface TiaCommonClient {

    @GetMapping("/skStatCode/v1/businessUnitsGroups")
    EntityModel<TiaResultDTO<TiaBusinessUnitStatCodeGroupDTO>> getBusinessUnitStatCodeGroups();

    @GetMapping("/skCommon/v1/tiaUsers")
    EntityModel<TiaResultDTO<TiaUserDTO>> getTiaUsers();
}


