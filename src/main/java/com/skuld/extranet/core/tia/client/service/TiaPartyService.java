package com.skuld.extranet.core.tia.client.service;

import com.skuld.extranet.core.tia.client.TiaPartyClient;
import com.skuld.extranet.core.tia.client.dto.TiaStatCodeDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TiaPartyService {

    private final TiaPartyClient tiaPartyClient;

    public TiaPartyService(TiaPartyClient tiaPartyClient) {

        this.tiaPartyClient = tiaPartyClient;
    }

    public Optional<List<TiaStatCodeDTO>> getStatCode(String statCode) {
        List<TiaStatCodeDTO> items = tiaPartyClient
                .getStatCode(statCode)
                .getContent()
                .getItems();

        return Optional.of(items);
    }

    public Optional<List<TiaStatCodeDTO>> getStatCodesForGroup(String statCodeGroupId) {
        List<TiaStatCodeDTO> items = tiaPartyClient
                .getStatCodesForGroup(statCodeGroupId)
                .getContent()
                .getItems();

        return Optional.of(items);

    }
}
