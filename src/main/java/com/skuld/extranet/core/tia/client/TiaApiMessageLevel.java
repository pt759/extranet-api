package com.skuld.extranet.core.tia.client;

import java.util.Arrays;

public enum TiaApiMessageLevel {

    ERROR("ERR"),
    WARN("WARN"),
    INFO("INFO"),
    OK("OK"),
    DEBUG("DEBU");

    private final String tiaCode;

    TiaApiMessageLevel(String tiaCode) {
        this.tiaCode = tiaCode;
    }

    public static TiaApiMessageLevel from(String code) {
        for (TiaApiMessageLevel x : TiaApiMessageLevel.values()) {
            if (x.tiaCode.equalsIgnoreCase(code)) {
                return x;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + code + ", Allowed values are " + Arrays.toString(values()));
    }

    public String tiaCode() {
        return tiaCode;
    }
}

