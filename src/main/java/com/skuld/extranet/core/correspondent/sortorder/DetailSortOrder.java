package com.skuld.extranet.core.correspondent.sortorder;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailSortOrder {

    private String contactType;
    private String sortOrder;


    public DetailSortOrder(String contactType, String sortOrder) {
        this.contactType = contactType;
        this.sortOrder = sortOrder;
    }

    private DetailSortOrder(ResultSet rs) throws SQLException {
        this.contactType = rs.getString("con_type");
        this.sortOrder = rs.getString("sort_no");
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }


    public static class Mapper implements RowMapper<DetailSortOrder> {
        public DetailSortOrder map(ResultSet r, StatementContext ctx) throws SQLException {
            return new DetailSortOrder(r);
        }
    }
}
