package com.skuld.extranet.core.correspondent;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.correspondent.domain.Correspondent;
import com.skuld.extranet.core.correspondent.domain.CorrespondentCountry;
import com.skuld.extranet.core.correspondent.domain.CorrespondentPort;
import com.skuld.extranet.core.correspondent.service.CorrespondentService;
import com.skuld.extranet.core.correspondent.sortorder.GroupCorrespondent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.skuld.extranet.core.common.SimpleResponeBuilder.buildResponseEntity;
import static com.skuld.extranet.global.RESTendPoints.correspondentSearch;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(correspondentSearch)
@CrossOrigin
public class CorrespondentController {


    @Autowired
    CorrespondentService service;

    final static Logger logger = LoggerFactory.getLogger(CorrespondentController.class);


    @JsonView(CorrespondentView.Listing.class)
    @RequestMapping(
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity correspondents(
            @RequestParam(value = "country", required = false, defaultValue = "") String country,
            @RequestParam(value = "port", required = false, defaultValue = "") String port
    ) {

        logger.info("correspondents for country= '" + country + "' and port= '" + port + "'");

        List<Correspondent> correspondents = service.filterBy(port, country);
        logger.info("correspondents.size= " + correspondents.size());

        return buildResponseEntity(correspondents);
    }

    @JsonView(CorrespondentView.Single.class)
    @RequestMapping(
            path = "full/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity correspondentsFullView(
            @RequestParam(value = "country", required = false, defaultValue = "") String country,
            @RequestParam(value = "port", required = false, defaultValue = "") String port
    ) {
        logger.info("correspondents for country= '" + country + "' and port= '" + port + "'");
        List<Correspondent> correspondents = service.filterByWithDetails(port, country);
        logger.info("correspondents.size= " + correspondents.size());
        return buildResponseEntity(correspondents);
    }

    // Brukes fra Episerver
    @JsonView(CorrespondentView.Single.class)
    @RequestMapping(
            path = "full/group/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity correspondentsFullViewGrouped(
            @RequestParam(value = "country", required = false, defaultValue = "") String country,
            @RequestParam(value = "port", required = false, defaultValue = "") String port
    ) {
        logger.info("correspondents for country= '" + country + "' and port= '" + port + "'");
        List<GroupCorrespondent> group = service.filterAndGroup(port,country);
        logger.info("correspondents.size= " + group.size());
        return buildResponseEntity(group);
    }


    /*
    Find an actual correspondent, based in a given Port.
    * */
    @JsonView(CorrespondentView.Single.class)
    @RequestMapping(path = "{id}",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity correspondentBy(
            @PathVariable(value = "id") String id,
            @RequestParam(value = "port") String port) {
        logger.info("get correspondent id= " + id + " port=" + port);
        Correspondent correspondent = service.correspondentBy(id, port);
        return buildResponseEntity(correspondent);
    }

    @RequestMapping(path = "list",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity correspondents() {
        List<Correspondent> correspondents = service.correspondentList();
        return buildResponseEntity(correspondents);
    }

    @RequestMapping(path = "countries", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity countries() {
        List<CorrespondentCountry> correspondents = service.countries();

        return buildResponseEntity(correspondents);
    }

    @RequestMapping(path = "ports", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity ports() {
        List<CorrespondentPort> correspondents = service.ports();
        return buildResponseEntity(correspondents);
    }



}
