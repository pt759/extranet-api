package com.skuld.extranet.core.correspondent.service;


import com.skuld.extranet.core.correspondent.contact.Contact;
import com.skuld.extranet.core.correspondent.domain.Correspondent;
import com.skuld.extranet.core.correspondent.domain.CorrespondentCountry;
import com.skuld.extranet.core.correspondent.domain.CorrespondentPort;
import com.skuld.extranet.core.correspondent.number.CorrespondentNumber;
import com.skuld.extranet.core.correspondent.repo.CorrespondentRepository;
import com.skuld.extranet.core.correspondent.sortorder.GroupCorrespondent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service
public class CorrespondentService {

    final static Logger logger = LoggerFactory.getLogger(CorrespondentService.class);

    @Autowired
    CorrespondentRepository repo;


    public CorrespondentService() {
        repo = new CorrespondentRepository();
    }

    public CorrespondentService(CorrespondentRepository repo) {
        this.repo = repo;
    }


    public List<Correspondent> correspondentList() {
        return repo.getCorrespondentListFromRepo();
    }

    public List<Correspondent> correspondentByIdForPI(String partyId) {
        List<Correspondent> correspondents = repo.correspondentByIdForPI(partyId);
        return correspondents;
    }

    public List<Correspondent> correspondentByIdForHM(String partyId) {
        return repo.correspondentByIdForHM(partyId);
    }

    public Correspondent correspondentBy(String partyId, String port) {
        List<Correspondent> correspondents = repo.correspondentBy(partyId, port);
        populateCorrespondents(correspondents);
        return correspondents.get(0);
    }


    public List<Correspondent> filterBy(String port, String country) {
        List<Correspondent> correspondents = findCorrespondentsBy(port, country);
        return correspondents;
    }

    public List<Correspondent> filterByWithDetails(String port, String country) {
        List<Correspondent> correspondents = findCorrespondentsBy(port, country);
        populateCorrespondents(correspondents);
        return correspondents;
    }

    public List<Correspondent> filterByWithDetailsGroup(String port, String country) {
        List<Correspondent> correspondents = findCorrespondentsBy(port, country);
        populateCorrespondents(correspondents);
        return correspondents;
    }

    private List<Correspondent> findCorrespondentsBy(String port, String country) {
        List<Correspondent> correspondents = new ArrayList<>();
        if (isNotEmpty(port)) {
            logger.info("get by port " + port);
            correspondents = repo.filterPortBy(port);
        } else if (isNotEmpty(country)) {
            logger.info("get by country " + country);
            correspondents = repo.filterCountryBy(country);
        }
        return correspondents;
    }

    public List<GroupCorrespondent> filterAndGroup(String port, String country) {
        List<Correspondent> correspondents = filterByWithDetails(port, country);

        correspondents.sort(Comparator.comparing(Correspondent::getTownId));
        List<GroupCorrespondent> list = new ArrayList<>();

        GroupCorrespondent group = new GroupCorrespondent("Unknown");
        String oldGroupId = group.getGroupName();

        for (Correspondent correspondent : correspondents) {
            if (!oldGroupId.equalsIgnoreCase((correspondent.getTownId()))) {
                group = new GroupCorrespondent(correspondent.getTownId());
                oldGroupId = group.getGroupId();
                group.setGroupName(correspondent.getGroupName());
                list.add(group);
            }
            group.addCorrespondents(correspondent);
        }
        list.sort((Comparator.comparing(GroupCorrespondent::getGroupName)));
        return list;
    }

    private void populateCorrespondents(List<Correspondent> correspondents) {
        List<CorrespondentNumber> allNumbers = repo.getAllNumbers();
        List<Contact> allContacts = repo.getAllContacts();
        for (Correspondent correspondent : correspondents) {
            List<CorrespondentNumber> numbers = matchNumberFor(correspondent.getPartyId(), allNumbers);
            correspondent.setCorrespondentContactNumbers(numbers);
            List<Contact> contacts = matchContactFor(correspondent.getPartyId(),allContacts);
            correspondent.setContacts(contacts);
            for (Contact contact : contacts) {
                List<CorrespondentNumber> numberForContact = matchNumberFor(contact.getContactId(), allNumbers);
                contact.setContactNumbers(numberForContact);
            }
        }
    }

    private List<Contact> matchContactFor(String partyId, List<Contact> list) {
        return list.stream().filter(item -> item.getPartyId().equalsIgnoreCase(partyId)).collect(Collectors.toList());
    }

    private List<CorrespondentNumber> matchNumberFor(String contactId, List<CorrespondentNumber> list){
        return list.stream().filter(item -> item.getPartyContactId().equalsIgnoreCase(contactId)).collect(Collectors.toList());
    }

    public void setRepo(CorrespondentRepository repo) {
        this.repo = repo;
    }

    public List<Correspondent> correspondentById(String id, String pi, String hm) {
        List<Correspondent> correspondent = new ArrayList<>();
        if (isNotEmpty(id)) {
            if (isNotEmpty(pi) && pi.equalsIgnoreCase("Y")) {
                correspondent = correspondentByIdForPI(id);
            } else if (isNotEmpty(pi) && hm.equalsIgnoreCase("Y")) {
                correspondent = correspondentByIdForHM(id);

            }
        }
        populateCorrespondents(correspondent);
        return correspondent;
    }

    public List<CorrespondentCountry> countries() {
        return repo.allCountries();
    }

    public List<CorrespondentPort> ports() {
        return repo.allPorts();
    }
}
