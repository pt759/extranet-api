package com.skuld.extranet.core.correspondent.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CorrespondentPort {

    private String port;

    private String portId;
    private String country;

    public CorrespondentPort(ResultSet r) throws SQLException {
        this.port = r.getString("town");
        this.portId = r.getString("town_id");
        this.country= r.getString("country");
    }

    public static class Mapper implements RowMapper<CorrespondentPort> {
        public CorrespondentPort map(ResultSet r, StatementContext ctx) throws SQLException {
            return new CorrespondentPort(r);
        }
    }

    @JsonProperty("description")
    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @JsonProperty("code")
    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getCountry() {
        return country;
    }

    public CorrespondentPort setCountry(String country) {
        this.country = country;
        return this;
    }
}
