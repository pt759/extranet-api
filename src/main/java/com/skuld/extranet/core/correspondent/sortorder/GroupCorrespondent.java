package com.skuld.extranet.core.correspondent.sortorder;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.correspondent.CorrespondentView;
import com.skuld.extranet.core.correspondent.domain.Correspondent;

import java.util.ArrayList;
import java.util.List;


public class GroupCorrespondent {

    @JsonView({CorrespondentView.Listing.class})
    private String groupName;

    private String groupId;



    @JsonView({CorrespondentView.Listing.class})
    private List<Correspondent> correspondents;

    public GroupCorrespondent(String groupId) {
        this.groupId= groupId;
        this.groupName = "";
        correspondents = new ArrayList<>();
    }

    public GroupCorrespondent() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Correspondent> getCorrespondents() {
        return correspondents;
    }

    public void setCorrespondents(List<Correspondent> correspondents) {
        this.correspondents = correspondents;
    }

    public void addCorrespondents(Correspondent correspondent) {
        correspondents.add(correspondent);
    }

    public String getGroupId() {
        return groupId;
    }

}
