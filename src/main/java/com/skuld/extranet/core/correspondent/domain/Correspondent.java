package com.skuld.extranet.core.correspondent.domain;


import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.correspondent.CorrespondentView;
import com.skuld.extranet.core.correspondent.contact.Contact;
import com.skuld.extranet.core.correspondent.number.CorrespondentNumber;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Correspondent {


    @JsonView({CorrespondentView.Listing.class})
    private String name;

    @JsonView({CorrespondentView.Listing.class})
    private String partyId;

    @JsonView({CorrespondentView.Single.class})
    private String address;

    @JsonView({CorrespondentView.Listing.class})
    private String country;

    @JsonView({CorrespondentView.Listing.class})
    private String townId;

    @JsonView({CorrespondentView.Listing.class})
    private String town;

    @JsonView({CorrespondentView.Single.class})
    private String notes;

    @JsonView({CorrespondentView.Single.class})
    private String seqNo;

    @JsonView({CorrespondentView.Single.class})
    private String typeRef;

    @JsonView({CorrespondentView.Single.class})
    private String majorArea;

    @JsonView({CorrespondentView.Single.class})
    private String subArea;

    @JsonView({CorrespondentView.Single.class})
    private boolean showState;

    @JsonView({CorrespondentView.Listing.class})
    private String townRefId;

    @JsonView({CorrespondentView.Listing.class})
    private String townRef;

    @JsonView({CorrespondentView.Listing.class})
    private boolean isTownReference() {
        return StringUtils.isNotEmpty(townRefId);
    }


    @JsonView({CorrespondentView.Single.class})
    private String corrRole;

    //    @JsonView({CorrespondentView.Single.class})
    //Ignore field
    private String appName;

    @JsonView({CorrespondentView.Single.class})
    private String correspondent;

    @JsonView({CorrespondentView.Listing.class})
    private String pi;

    @JsonView({CorrespondentView.Listing.class})
    private String hull;

    @JsonView({CorrespondentView.Listing.class})
    private String townRefUrl;

    @JsonView({CorrespondentView.Single.class})
    private boolean autoShow;

    @JsonView({CorrespondentView.Single.class})
    List<Contact> contacts;

    @JsonView({CorrespondentView.Single.class})
    List<CorrespondentNumber> correspondentContactNumbers;


    public Correspondent() {
    }

    public Correspondent(ResultSet r) throws SQLException {
        this.partyId = r.getString("PARTY_ID");
        this.name = r.getString("NAME");
        this.address = r.getString("POSTAL_ADDRESS");
        this.country = r.getString("COUNTRY");

        this.townId = r.getString("TOWN_ID");
        this.town = r.getString("TOWN");
        this.townRefId = r.getString("TOWN_REF_ID");
        this.townRef = r.getString("TOWN_REF");

        this.notes = r.getString("NOTES");
        this.seqNo = r.getString("SEQ_NO");
        this.majorArea = r.getString("MAJOR_AREA");
        this.subArea = r.getString("SUB_AREA");

        this.corrRole = r.getString("CORR_ROLE");
        this.appName = r.getString("APP_NAME");
        this.correspondent = r.getString("CORRESPONDENT");
        this.pi = r.getString("PI");
        this.hull = r.getString("HULL");
        this.autoShow = r.getString("AUTOSHOW").equalsIgnoreCase("1");
    }

    public String getGroupName() {
        if (getShowState()) {
            return town + ", " + subArea + ", " + country;
        } else {
            return town + ", " + country;
        }
    }

    public static class Mapper implements RowMapper<Correspondent> {
        public Correspondent map(ResultSet r, StatementContext ctx) throws SQLException {
            return new Correspondent(r);
        }
    }

    @Override
    public String toString() {
        return getName();
    }


    public String getName() {
        return name;
    }

    public Correspondent setName(String name) {
        this.name = name;
        return this;
    }

    public String getPartyId() {
        return partyId;
    }

    public Correspondent setPartyId(String partyId) {
        this.partyId = partyId;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Correspondent setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Correspondent setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getTownId() {
        return townId;
    }

    public Correspondent setTownId(String townId) {
        this.townId = townId;
        return this;
    }

    public String getTown() {
        return town;
    }

    public Correspondent setTown(String town) {
        this.town = town;
        return this;
    }

    public String getNotes() {
        return notes;
    }

    public Correspondent setNotes(String notes) {
        this.notes = notes;
        return this;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public Correspondent setSeqNo(String seqNo) {
        this.seqNo = seqNo;
        return this;
    }

    public String getTypeRef() {
        return typeRef;
    }

    public Correspondent setTypeRef(String typeRef) {
        this.typeRef = typeRef;
        return this;
    }

    public String getMajorArea() {
        return majorArea;
    }

    public Correspondent setMajorArea(String majorArea) {
        this.majorArea = majorArea;
        return this;
    }

    public boolean isShowState() {
        return getShowState();
    }

    public String getTownRefId() {
        return townRefId;
    }

    public Correspondent setTownRefId(String townRefId) {
        this.townRefId = townRefId;
        return this;
    }

    public String getTownRef() {
        return townRef;
    }

    public Correspondent setTownRef(String townRef) {
        this.townRef = townRef;
        return this;
    }

    public String getCorrRole() {
        return corrRole;
    }

    public Correspondent setCorrRole(String corrRole) {
        this.corrRole = corrRole;
        return this;
    }

    public String getAppName() {
        return appName;
    }

    public Correspondent setAppName(String appName) {
        this.appName = appName;
        return this;
    }

    public String getCorrespondent() {
        return correspondent;
    }

    public Correspondent setCorrespondent(String correspondent) {
        this.correspondent = correspondent;
        return this;
    }

    public String getPi() {
        return pi;
    }

    public Correspondent setPi(String pi) {
        this.pi = pi;
        return this;
    }

    public String getHull() {
        return hull;
    }

    public Correspondent setHull(String hull) {
        this.hull = hull;
        return this;
    }

    public boolean isAutoShow() {
        return autoShow;
    }

    public Correspondent setAutoShow(boolean autoShow) {
        this.autoShow = autoShow;
        return this;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public Correspondent setContacts(List<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public List<CorrespondentNumber> getCorrespondentContactNumbers() {
        return correspondentContactNumbers;
    }

    public Correspondent setCorrespondentContactNumbers(List<CorrespondentNumber> correspondentContactNumbers) {
        this.correspondentContactNumbers = correspondentContactNumbers;
        return this;
    }

    public String getTownRefUrl() {
        if (townRefId != null) {
            //TODO: Fix this url stuff here ...
            return "http://localhost:8080/correspondents/?port=" + this.townRefId;
        } else {
            return null;
        }
    }

    public void setTownRefUrl(String townRefUrl) {
        this.townRefUrl = townRefUrl;
    }

    public String getSubArea() {
        return subArea;
    }

    public Correspondent setSubArea(String subArea) {
        this.subArea = subArea;
        return this;
    }

    public boolean getShowState() {
        return "USA".equalsIgnoreCase(getCountry());
    }

    public Correspondent setShowState(boolean hasState) {
        this.showState = hasState;
        return this;
    }
}
