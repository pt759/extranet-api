package com.skuld.extranet.core.correspondent.repo;

import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.correspondent.contact.Contact;
import com.skuld.extranet.core.correspondent.domain.Correspondent;
import com.skuld.extranet.core.correspondent.domain.CorrespondentCountry;
import com.skuld.extranet.core.correspondent.domain.CorrespondentPort;
import com.skuld.extranet.core.correspondent.number.CorrespondentNumber;
import com.skuld.extranet.core.correspondent.sortorder.DetailSortOrder;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface CorrespondentDao extends ClosableDao {


    @SqlQuery("select * from tia.sk_correspondents_v where party_id = :partyId")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> filterById(@Bind("partyId") String partyId);

    @SqlQuery("select * from tia.sk_correspondents_v where party_id = :partyId and pi ='Y'")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> filterByIdForPI(@Bind("partyId") String partyId);

    @SqlQuery("select * from tia.sk_correspondents_v where party_id = :partyId and hull ='Y'")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> filterByIdForHM(@Bind("partyId") String partyId);

    @SqlQuery("select * from tia.sk_correspondents_v where party_id = :partyId and town = :town")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> correspondentBy(@Bind("partyId") String partyId, @Bind("town") String port);

    @SqlQuery("select * from tia.sk_correspondents_v where name like :name")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> filterByName(@Bind("name") String name);

    @SqlQuery("select * from tia.sk_correspondents_v where upper(town) = upper(:town) ")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> filterByPort(@Bind("town") String town);

    @SqlQuery("select * from tia.sk_correspondents_v where upper(country) = upper(:country) ")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> filterByCountry(@Bind("country") String country);

    @SqlQuery("select * from tia.sk_correspondents_v")
    @RegisterRowMapper(Correspondent.Mapper.class)
    List<Correspondent> all();

    @SqlQuery("select con_type, sort_no from tia.sk_correspondent_numbers_sort order by sort_no")
    @RegisterRowMapper(DetailSortOrder.Mapper.class)
    List<DetailSortOrder> sortOrder();

    @SqlQuery("select * from tia.sk_correspondent_numbers_v where name_id_no = :name_id_no")
    @RegisterRowMapper(CorrespondentNumber.Mapper.class)
    List<CorrespondentNumber> filterNumberBy(@Bind("name_id_no") String nameId);



    @SqlQuery("select * from tia.sk_correspondent_contacts_v  where owner  = :party_id order by sort_no")
    @RegisterRowMapper(Contact.Mapper.class)
    List<Contact> filterContactsBy(@Bind("party_id") String party_id);

    @SqlQuery("select distinct country from tia.sk_correspondents_v  order by 1")
    @RegisterRowMapper(CorrespondentCountry.Mapper.class)
    List<CorrespondentCountry> allCountries();

    @SqlQuery("select distinct country, town,town_id from tia.sk_correspondents_v  order by 1 ")
    @RegisterRowMapper(CorrespondentPort.Mapper.class)
    List<CorrespondentPort> allPort();

    @SqlQuery("select * from v_correspondent_numbers ")
    @RegisterRowMapper(CorrespondentNumber.Mapper.class)
    List<CorrespondentNumber> allNumbers();

    @SqlQuery("select * from tia.sk_correspondent_contacts_v")
    @RegisterRowMapper(Contact.Mapper.class)
    List<Contact> allContacts();
}
