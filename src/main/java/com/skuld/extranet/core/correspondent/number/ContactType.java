package com.skuld.extranet.core.correspondent.number;

public enum ContactType {

    MobilePhone, HomePhone, OfficePhone,
}
