package com.skuld.extranet.core.correspondent.repo;

import com.skuld.extranet.core.common.repo.DBTiaRepo;
import com.skuld.extranet.core.correspondent.contact.Contact;
import com.skuld.extranet.core.correspondent.domain.Correspondent;
import com.skuld.extranet.core.correspondent.domain.CorrespondentCountry;
import com.skuld.extranet.core.correspondent.domain.CorrespondentPort;
import com.skuld.extranet.core.correspondent.number.CorrespondentNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.skuld.extranet.CachingConfig.CORRESPONDENT_SEARCH_CONTACTS;
import static com.skuld.extranet.CachingConfig.CORRESPONDENT_SEARCH_NUMBERS;

@Component
@Service
public class CorrespondentRepository extends DBTiaRepo {
    final static Logger logger = LoggerFactory.getLogger(CorrespondentRepository.class);


    public List<Correspondent> getCorrespondentListFromRepo() {
        List<Correspondent> correspondents = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.all());
        return correspondents;
    }

    public List<Correspondent> correspondentById(String partyId) {
        List<Correspondent> correspondents = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterById(partyId));
        return correspondents;
    }

    public List<Correspondent> correspondentByIdForPI(String partyId) {
        List<Correspondent> correspondents = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterByIdForPI(partyId));
        return correspondents;
    }

    public List<Correspondent> correspondentBy(String partyId, String port) {
        List<Correspondent> correspondent = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.correspondentBy(partyId, port));
        return correspondent;
    }

    public List<Correspondent> correspondentByIdForHM(String partyId) {
        return getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterByIdForHM(partyId));
    }

    public List<Correspondent> filterBy(String name) {
        List<Correspondent> correspondents = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterByName(name));
        return correspondents;
    }

    public List<Correspondent> filterPortBy(String port) {
        logger.info("filter by port " + port);
        List<Correspondent> correspondents = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterByPort(port));
        logger.info("size:" + correspondents.size());
        return correspondents;
    }

    public List<Correspondent> filterCountryBy(String country) {
        List<Correspondent> correspondents = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterByCountry(country));
        return correspondents;
    }

    public List<CorrespondentNumber> getNumberFor(String party_id) {
        List<CorrespondentNumber> numbers = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterNumberBy(party_id));
        return numbers;
    }


    public List<Contact> getContacts(String party_id) {
        List<Contact> contacts = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.filterContactsBy(party_id));
        return contacts;
    }


    public List<CorrespondentCountry> allCountries() {
        List<CorrespondentCountry> countries = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.allCountries());
        return countries;
    }

    public List<CorrespondentPort> allPorts() {
        List<CorrespondentPort> countries = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.allPort());
        return countries;
    }

    @Cacheable(CORRESPONDENT_SEARCH_NUMBERS)
    public List<CorrespondentNumber> getAllNumbers() {
        List<CorrespondentNumber> numbers = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.allNumbers());
        return numbers;
    }

    @Cacheable(CORRESPONDENT_SEARCH_CONTACTS)
    public List<Contact> getAllContacts() {
        List<Contact> contacts = getJdbi().withExtension(CorrespondentDao.class, dao -> dao.allContacts());
        return contacts;
    }
}
