package com.skuld.extranet.core.correspondent.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CorrespondentCountry {


    private String code;
    private String country;

    public CorrespondentCountry(ResultSet r) throws SQLException {
        this.country = r.getString("country");
    }

    @JsonProperty("description")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public static class Mapper implements RowMapper<CorrespondentCountry> {
        public CorrespondentCountry map(ResultSet r, StatementContext ctx) throws SQLException {
            return new CorrespondentCountry(r);
        }
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
