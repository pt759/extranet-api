package com.skuld.extranet.core.correspondent.number;


import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.correspondent.CorrespondentView;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CorrespondentNumber {

    final static Logger logger = LoggerFactory.getLogger(CorrespondentNumber.class);

    private String partyContactId;
    @JsonView({CorrespondentView.Single.class})
    private String conType;
    @JsonView({CorrespondentView.Single.class})
    private String description;

    public CorrespondentNumber() {
    }

    public CorrespondentNumber(ResultSet r) throws SQLException {
        this.partyContactId = r.getString("NAME_ID_NO");
        this.conType = r.getString("CON_TYPE");
        this.description = r.getString("PHONE_NO");
    }

    @Override
    public String toString() {
        return conType + ":" + description;
    }

    public static class Mapper implements RowMapper<CorrespondentNumber> {
        public CorrespondentNumber map(ResultSet r, StatementContext ctx) throws SQLException {
            return new CorrespondentNumber(r);
        }
    }

    public String getPartyContactId() {
        return partyContactId;
    }

    public void setPartyContactId(String partyContactId) {
        this.partyContactId = partyContactId;
    }

    public String getConType() {
        return conType;
    }

    public void setConType(String conType) {
        this.conType = conType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
