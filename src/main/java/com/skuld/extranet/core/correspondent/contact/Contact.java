package com.skuld.extranet.core.correspondent.contact;


import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.correspondent.CorrespondentView;
import com.skuld.extranet.core.correspondent.number.CorrespondentNumber;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class Contact {
    final static Logger logger = LoggerFactory.getLogger(Contact.class);

    private String partyId;//Owner

    @JsonView({CorrespondentView.Single.class})
    private String contactId;

    @JsonView({CorrespondentView.Single.class})
    private String name;

    @JsonView({CorrespondentView.Single.class})
    private int sortNo;//TODO: number?

    @JsonView({CorrespondentView.Single.class})
    List<CorrespondentNumber> contactNumbers;


    //@Transient in this object. Stored seperately in CorrNumber Table.
    private List<CorrespondentNumber> contactDetails;//=> CorrNumbers.csv

    public Contact(ResultSet r) throws SQLException {
        this.contactId = r.getString("CONTACT_ID");
        this.partyId = r.getString("OWNER");//
        this.name = r.getString("CONTACT_NAME");
        this.sortNo = r.getInt("SORT_NO");
    }


    public static class Mapper implements RowMapper<Contact> {
        public Contact map( ResultSet r, StatementContext ctx) throws SQLException {
            return new Contact(r);
        }
    }


    public String toString() {
        return "Contact:" + name;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CorrespondentNumber> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<CorrespondentNumber> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public List<CorrespondentNumber> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(List<CorrespondentNumber> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public int getSortNo() {
        return sortNo;
    }

    public void setSortNo(int sortNo) {
        this.sortNo = sortNo;
    }


}
