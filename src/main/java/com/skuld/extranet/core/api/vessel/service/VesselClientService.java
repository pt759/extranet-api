package com.skuld.extranet.core.api.vessel.service;

import com.skuld.extranet.core.api.vessel.client.VesselClient;
import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class VesselClientService {
    final VesselClient vesselClient;

    public VesselClientService(VesselClient vesselClient) {
        this.vesselClient = vesselClient;
    }

    public List<VesselListDTO> vesselsFor(String statCode) {
        return vesselClient.vesselsFor(statCode);
    }

    public List<Vessel> searchForVessel(VesselFilter vesselFilter) {
        List<VesselListDTO> vesselListDTOS = this.searchForVesselList(vesselFilter);
        return vesselListDTOS
                .stream()
                .map(Vessel::from)
                .collect(Collectors.toList());
    }

    public List<VesselListDTO> searchForVesselList(VesselFilter vesselFilter) {
        return vesselClient.searchForVessel(vesselFilter);
    }
}
