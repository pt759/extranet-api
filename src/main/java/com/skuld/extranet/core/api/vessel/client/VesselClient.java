package com.skuld.extranet.core.api.vessel.client;

import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "skuld.vessel.api.client", url = "${skuld.api.vessels.url}", configuration = VesselClientConfiguration.class)
public interface VesselClient {

    @GetMapping("/api/vessels/{statcode}/")
    List<VesselListDTO> vesselsFor(@PathVariable(value = "statcode") String statCode);

    @PostMapping("/api/vessels/search/")
    List<VesselListDTO> searchForVessel(@RequestBody VesselFilter vesselFilter);
}
