package com.skuld.extranet.core.api.vessel.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/*
From Vessel-API
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VesselListDTO {
    private String vesselId;
    private String imo;
    private String vesselName;
    private String vesselType;
    private String premiumPaid;
    private String status;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String statCode;
    private String products;
    private String classification;
    private String grossTonnage;
    private String statCodeName;
    private String statCurrency;
    private String statCurrencyType;
    private String flagState;
    private GeoData geoData;
    private int builtYear;
    private int age;

}
