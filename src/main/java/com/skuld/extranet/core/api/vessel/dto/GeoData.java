package com.skuld.extranet.core.api.vessel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeoData {
    private String status;
    private double heading;
    private LocalDate lastLocationUpdatedAt;
    private GeoDataPosition geoDataPosition;
    private String lastLocationUpdatedAtAsString;
    private boolean hasGeoDataPosition;
}
