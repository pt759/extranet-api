package com.skuld.extranet.core.api.vessel.client;

import com.skuld.extranet.error.SkuldClientErrorDecoder;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import static com.skuld.common.security.authentication.SecurityValues.apiKeyHeader;

@Log4j2
public class VesselClientConfiguration {

    @Value("${skuld.api.vessels.api-key}")
    String apiKey;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header(apiKeyHeader, apiKey);
        };
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new SkuldClientErrorDecoder();
    }
}
