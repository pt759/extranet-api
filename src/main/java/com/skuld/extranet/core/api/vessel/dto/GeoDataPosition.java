package com.skuld.extranet.core.api.vessel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeoDataPosition {
    private double lng;
    private double lat;
}
