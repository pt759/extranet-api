package com.skuld.extranet.core.VesselSearchFromTIA.dto;

import com.skuld.extranet.core.VesselSearchFromTIA.dto.tia.VesselFromTiaDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class VesselDTO {

    String vesselName;
    String member;
    String imo;
    String builtYear;
    String grossTonnage;
    String vesselType;
    String flagDescription;
    String businessUnit;
    String currentRegOwner;
    List<VesselCertificateDTO> certificates;


    public static VesselDTO from(VesselFromTiaDTO tiaDTO) {

        VesselDTO vesselDTO = VesselDTO.builder()
                .vesselName(tiaDTO.getVessel_name())
                .member(tiaDTO.getPolicy_holder())
                .imo(tiaDTO.getImo())
                .builtYear(tiaDTO.getYear_built())
                .grossTonnage(tiaDTO.getGt())
                .vesselType(tiaDTO.getVessel_type_name())
                .flagDescription(tiaDTO.getFlag())
                .businessUnit(tiaDTO.getBusiness_unit())
                .currentRegOwner(tiaDTO.getRegistered_owner())
                .build();

        if (CollectionUtils.isNotEmpty(tiaDTO.getCertificates())) {
            vesselDTO.setCertificates(VesselCertificateDTO.from(tiaDTO.getCertificates()));
        } else {
            vesselDTO.setCertificates(new ArrayList<>());
        }

        return vesselDTO;
    }



}
