package com.skuld.extranet.core.VesselSearchFromTIA.dto.tia;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class VesselFromTiaDTO {

    String vessel_id;
    String vessel_name;
    String note;
    String policy_holder;
    String imo;
    String year_built;
    String gt;
    String vessel_group;
    String vessel_type_name;
    String flag;
    String business_unit;
    String registered_owner;
    List<VesselCertificateFromTiaDTO> certificates;

}
