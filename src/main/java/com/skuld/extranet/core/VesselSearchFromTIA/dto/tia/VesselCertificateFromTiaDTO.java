package com.skuld.extranet.core.VesselSearchFromTIA.dto.tia;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class VesselCertificateFromTiaDTO {

    private String cert_type;
    private String cert_desc;

    @Override
    public String toString() {
        return cert_type + ";" + cert_desc;
    }
}
