package com.skuld.extranet.core.VesselSearchFromTIA.client;

import com.skuld.extranet.core.VesselSearchFromTIA.dto.tia.VesselFromTiaDTO;
import com.skuld.extranet.core.tia.client.TiaClientConfiguration;
import com.skuld.extranet.core.tia.client.dto.TiaResultDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "skuld.tia.ords.vessel.client", url = "${tia.ords.api.url}", configuration = TiaClientConfiguration.class)
public interface TiaVesselClient {

    @GetMapping("/skVessel/v1/ownerPandiVessels")
    EntityModel<TiaResultDTO<VesselFromTiaDTO>> getVesselOnCover(@RequestParam(value = "vessel_name_imo") String query);
}
