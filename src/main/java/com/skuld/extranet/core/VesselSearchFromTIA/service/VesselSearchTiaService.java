package com.skuld.extranet.core.VesselSearchFromTIA.service;

import com.skuld.extranet.core.VesselSearchFromTIA.client.TiaVesselClient;
import com.skuld.extranet.core.VesselSearchFromTIA.dto.VesselDTO;
import com.skuld.extranet.core.VesselSearchFromTIA.dto.tia.VesselFromTiaDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Log4j2
@Service
public class VesselSearchTiaService {

    TiaVesselClient tiaVesselClient;

    public VesselSearchTiaService(TiaVesselClient tiaVesselClient) {
        this.tiaVesselClient = tiaVesselClient;
    }

    public List<VesselDTO> filterVesselBy(String query) {
        List<VesselDTO> vesselsWithCertificates = new ArrayList<>();
        if (isNotBlank(query) && query.matches("^[a-zA-Z0-9 _]*$")) {
            vesselsWithCertificates = getVesselOnCoverFromTIA(query.trim()).stream().map(VesselDTO::from).collect(Collectors.toList());
        }
        return vesselsWithCertificates;
    }

    List<VesselFromTiaDTO> getVesselOnCoverFromTIA(String query) {
        List<VesselFromTiaDTO> vesselFromTia = new ArrayList<>();
        try {
            long timeStart = System.currentTimeMillis();
            log.info("filterVesselsBy " + query);
            vesselFromTia = tiaVesselClient.getVesselOnCover(query).getContent().getItems();
            long timeComplete = System.currentTimeMillis();
            log.info("Found number of vessels with certificates: " + vesselFromTia.size() + " in " + (timeComplete - timeStart) + " ms");
        } catch (Exception exception) {
            log.error("Unable to search for " + query + ".", exception);
        }
        return vesselFromTia;
    }

}
