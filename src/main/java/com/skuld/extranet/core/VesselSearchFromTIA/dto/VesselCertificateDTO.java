package com.skuld.extranet.core.VesselSearchFromTIA.dto;

import com.skuld.extranet.core.VesselSearchFromTIA.dto.tia.VesselCertificateFromTiaDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class VesselCertificateDTO {

    String certificateType;
    String certificate;

    public static VesselCertificateDTO from(VesselCertificateFromTiaDTO source) {
        return VesselCertificateDTO.builder()
                .certificate(source.getCert_desc())
                .certificateType(source.getCert_type())
                .build();
    }

    public static List<VesselCertificateDTO> from(List<VesselCertificateFromTiaDTO> certificatesFromTIA) {
        List<VesselCertificateDTO> collect = certificatesFromTIA.stream().map(VesselCertificateDTO::from).collect(Collectors.toList());
        return fixForLineBreaks(collect);
    }

    public static List<VesselCertificateDTO> fixForLineBreaks(List<VesselCertificateDTO> vesselCertificates) {
        String deliminatorLineBreak = "\n";

        List<VesselCertificateDTO> list = new ArrayList<>();
        vesselCertificates.forEach(certificate -> {
            if (certificate.getCertificate().contains(deliminatorLineBreak)) {
                List<String> descriptionForCertificates = Arrays.asList(certificate.getCertificate().split(deliminatorLineBreak));
                descriptionForCertificates.forEach(x -> {
                    list.add(new VesselCertificateDTO(certificate.getCertificateType(), x));
                });
            } else {
                list.add(certificate);
            }
        });
        return list;
    }
}
