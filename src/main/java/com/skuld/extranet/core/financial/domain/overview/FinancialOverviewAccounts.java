package com.skuld.extranet.core.financial.domain.overview;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skuld.extranet.core.financial.domain.AccountGroup;

import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.core.financial.domain.AccountGroup.*;

public class FinancialOverviewAccounts {

    @JsonIgnore
    private List<FinancialOverviewAccountData> allAccounts;

    public FinancialOverviewAccounts() {
    }

    @JsonProperty("premiumAccounts")
    public List<FinancialOverviewAccountData> getPremiumAccounts() {
        return allAccounts.stream()
                .filter(i -> i.getAccountType().equals(PREMIUM) ||
                        i.getAccountType().equals(PREMIUM_DEPOSIT))
                .collect(Collectors.toList());
    }

    @JsonProperty("claimAccounts")
    public List<FinancialOverviewAccountData> getClaimAccounts() {
        return filterAccount(CLAIM);
    }

    @JsonProperty("otherAccounts")
    public List<FinancialOverviewAccountData> getOtherAccounts() {
        return filterAccount(OTHER);
    }

    private List<FinancialOverviewAccountData> filterAccount(AccountGroup accountGroup) {
        return allAccounts.stream().filter(i -> i.getAccountType().equals(accountGroup)).collect(Collectors.toList());
    }

    public List<FinancialOverviewAccountData> getAllAccounts() {
        return allAccounts;
    }

    public FinancialOverviewAccounts setAllAccounts(List<FinancialOverviewAccountData> allAccounts) {
        this.allAccounts = allAccounts;
        return this;
    }

    @Override
    public String toString() {
        return "FinancialOverviewAccounts{" +
                "allAccounts=" + allAccounts +
                '}';
    }
}
