package com.skuld.extranet.core.financial.service;

import com.skuld.common.domain.Base64Container;
import com.skuld.common.domain.YesNo;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.common.SkuldSqlHelper;
import com.skuld.extranet.core.common.SkuldWebUtil;
import com.skuld.extranet.core.common.domain.Base64ContainerForExcel;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.BankDetail;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalment;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositStatus;
import com.skuld.extranet.core.financial.domain.deposit.entity.DepositStatusEntity;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import com.skuld.extranet.core.financial.domain.invoice.Invoice;
import com.skuld.extranet.core.financial.domain.invoice.InvoiceType;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccountData;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoice;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountTotal;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountVessel;
import com.skuld.extranet.core.financial.excel.StatementOfAccountExcel;
import com.skuld.extranet.core.financial.repo.FinancialDwhRepository;
import com.skuld.extranet.core.financial.repo.FinancialRepository;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.excel.ExcelListConverter;
import com.skuld.extranet.global.RESTendPoints;
import com.skuld.extranet.ldap.LDAPService;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.financial.Report;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.skuld.common.domain.YesNo.No;
import static com.skuld.common.domain.YesNo.Yes;
import static com.skuld.extranet.CachingConfig.ACCOUNTS_FOR_STAT_CODE;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType.Unpaid;
import static com.skuld.extranet.global.SkuldPredicate.distinctByKey;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Log4j2
@Service
public class FinancialService {

    @Value("${financial.balance.limit}")
    private BigDecimal balanceLimit;

    @Value("${tia.report.finance.rs80}")
    private String urlBaseStatementOfAccount;

    @Value("${tia.report.finance.rs84}")
    private String urlBaseInstalmentReport;

    @Value("${tia.url.oracle.reports}")
    private String urlOracleReports;

    @Value("${creditcontrol.email}")
    private String creditControlEmail;

    @Value("${creditcontrol.name}")
    private String creditControlName;

    private FinancialRepository financialRepository;
    private final FinancialDwhRepository repoDwh;
    private final LDAPService ldapService;
    private final ClaimService claimService;

    @Autowired
    public FinancialService(FinancialRepository financialRepository,
                            FinancialDwhRepository repoDwh,
                            LDAPService ldapService,
                            ClaimService claimService) {
        this.financialRepository = financialRepository;
        this.repoDwh = repoDwh;
        this.ldapService = ldapService;
        this.claimService = claimService;
    }

    private final SkuldWebUtil web = new SkuldWebUtil();

    public List<AccountItemEntity> getAccItems(String accountNo, AccountItemEntity.ItemType itemType, YesNo deposit) {
        List<AccountItemEntity> accountItemEntities = getAccountItemsFromRepo(accountNo, itemType, deposit);

        return accountItemEntities.stream()
                .filter(item -> (item.getBalance().abs().compareTo(balanceLimit) > 0) && (isEmpty(item.getInvoiceNoSafe()) || item.getInvoiceNoSafe().startsWith("9")))
                //Todo : do we need this line?
                .filter(item -> (isEmpty(item.getInvoiceNoSafe()) || item.validInvoiceNumber()))
                .collect(Collectors.toList());
    }

    private List<AccountItemEntity> getAccountItemsFromRepo(String accountNo, AccountItemEntity.ItemType itemType, YesNo deposit) {
        return financialRepository.accountItems(accountNo, itemType, deposit, onHoldItems(accountNo));
    }

    public Base64Container statementOfAccountAsPDF(String statCode, String accountNumberOrPolicyHolder, String itemType, String filename) {

        String url = getStatementOfAccountUrl(statCode, accountNumberOrPolicyHolder, itemType);

        String data = web.urlContentAsBase64String(url);
        Base64Container container = Base64Container.wrapPdfAsUtf8(data, filename);

        return container;
    }

    public String getStatementOfAccountUrl(String statCode, String accountNo, String itemType) {
        String url = urlBaseStatementOfAccount;

        if (!(itemType.equalsIgnoreCase("u") || itemType.equalsIgnoreCase("d")) || itemType == null) {
            log.debug("Invalid value for item type. Setting default U");
            itemType = "U";
        }

        return url.replace("#ACCOUNT_NO#", accountNo)
                .replace("#ITEMTYPE", itemType)
                .replace("#ONHOLD", onHoldItems(accountNo).value());
    }

    public String getInstalmentReportUrl(String statCode, String accountNumber) {
        String date = getPolicyFistStartDate(statCode, accountNumber);
        String url = urlBaseInstalmentReport.replace("#STAT_CODE", statCode)
                .replace("#ACCOUNT_NO", accountNumber)
                .replace("#FROM_DATE", date);
        return url;
    }

    public List<LDAPUser> getCreditControllersForStatCode(List<StatCodeAccountEntity> statCodeAccounts) {
        List<LDAPUser> creditControllers = new ArrayList<>();

        statCodeAccounts.stream()
                .filter(distinctByKey(StatCodeAccountEntity::getCreditController))
                .forEach(account -> add(creditControllers, account.getCreditController())
                );

        if (creditControllers.size() == 0) {
            creditControllers.add(defaultCreditController());
            log.info("Setting a default credit controller: " + creditControlEmail);
        }

        return creditControllers;
    }

    private LDAPUser defaultCreditController() {
        return LDAPUser.builder()
                .name(creditControlName)
                .email(creditControlEmail)
                .emailWithUserID(creditControlEmail).build();
    }

    private void add(List<LDAPUser> creditControllers, String userName) {
        LDAPUser creditController = findCreditController(userName);
        if (creditController == null || StringUtils.isEmpty(userName)) {
            log.info("Unable to find credit controller for intials: " + userName);
        } else if (creditController.isDisabled()) {
            log.info("Credit controller is not active anymore: " + userName);
        } else {
            creditControllers.add(creditController);
        }
    }

    public List<StatCodeAccountEntity> getStatCodePremiumAccountsFromRepo(String statCode) {
        return financialRepository.getStatCodePremiumAccounts(statCode);
    }

    public List<StatCodeAccountEntity> getStatCodeDepositAccountsFromRepo(String statCode) {
        return financialRepository.getStatCodeDepositAccounts(statCode);
    }

    public List<StatCodeAccountEntity> getStatCodeClaimAccountsFromRepo(String statCode) {
        return financialRepository.getStatCodeClaimAccounts(statCode);
    }

    public LDAPUser findCreditController(String username) {
        return ldapService.findFirstUserBySamaAccountSafe(username);
    }

    public AccountEntity getAccount(String account) {
        return financialRepository.getAccount(account);
    }

    public AccountEntity matchAccount(String accountNo, List<AccountEntity> allAccounts) {
        return allAccounts.stream().filter(i -> i.getAccountNo().equalsIgnoreCase(accountNo)).findFirst().orElse(null);
    }

    public List<AccountSum> getAccountSumSortedByCurrencyForDepositBalance(String accountNo) {
        List<AccountSum> accountSum = financialRepository.getAccountSum(accountNo, Unpaid, onHoldItems(accountNo), Yes);
        return sortBalanceLimit(accountSum);
    }

    public List<AccountSum> getAccountSumSortedByCurrency(String accountNo) {
        List<AccountSum> accountSum = financialRepository.getAccountSum(accountNo, Unpaid, onHoldItems(accountNo), No);
        return sortBalanceLimit(accountSum);
    }

    private List<AccountSum> sortBalanceLimit(List<AccountSum> accountSum) {
        return accountSum.stream()
                .sorted(Comparator.comparing(i -> i.getCurrency()))
                .collect(Collectors.toList());
    }

    public FinancialOverviewAccountData getOverviewAccount(String accountNo) {
        FinancialOverviewAccountData overviewAccount = FinancialOverviewAccountData.from(getAccount(accountNo));
        List<AccountSum> accountSum = getAccountSumSortedByCurrency(accountNo);
        overviewAccount.setAccountSum(accountSum);
        overviewAccount.setPolicyHolder(financialRepository.getPolicyHolder(accountNo));
        return overviewAccount;
    }

    public List<BankDetail> getBankDetailFor(String accountNo, AccountItemEntity.ItemType itemType, YesNo deposit, YesNo onHold) {
        List<BankDetail> bankDetails;
        bankDetails = financialRepository.getBankDetails(accountNo, itemType.value(), deposit.value(), onHold.value());
        bankDetails.stream().forEach(i -> i.setBankAddress(financialRepository.getBankAddress(i.getBankIdNumber())));
        return bankDetails;
    }

    public List<BankDetail> getBankDetailsByCurrencies(String accountNumber, AccountItemEntity.ItemType itemType, YesNo deposit, YesNo onHold, List<String> currencies) {
        List<BankDetail> bankDetails = getBankDetailFor(accountNumber, itemType, deposit, onHold);
        return bankDetails.stream()
                .filter(p -> currencies.contains(p.getBankCurrencyCode()))
                .collect(Collectors.toList());
    }

    public List<String> currenciesForInvoices(List<AccountItemEntity> accItems) {
        return accItems.stream()
                .filter(paymentType -> InvoiceType.Invoice.value().equalsIgnoreCase(paymentType.getPaymentMethod()))
                .filter(distinctByKey(AccountItemEntity::getCurrency))
                .map(AccountItemEntity::getCurrency)
                .collect(Collectors.toList());
    }

    public List<AccountItemEntity> getAccountItemsFor(String accountNo, AccountItemEntity.ItemType itemType, YesNo deposit) {
        List<AccountItemEntity> accItems = getAccItems(accountNo, itemType, deposit);
        return accItems;
    }

    public List<StatementOfAccountVessel> convertAccItemsToAccountVessel(List<AccountItemEntity> accItems) {
        Map<String, List<AccountItemEntity>> grouped = groupByInvoiceNumberAccountNumberCurrencyVesselName(accItems);
        List<StatementOfAccountVessel> rows = convertToRows(grouped);

        return sortRows(rows);
    }

    private List<StatementOfAccountVessel> sortRows(List<StatementOfAccountVessel> rows) {
        rows.sort(Comparator.comparing(StatementOfAccountVessel::getVesselName));
        StatementOfAccountVessel statementOfAccountRows = rows.stream().filter(item -> item.getVesselName().equalsIgnoreCase(AccountItemEntity.GroupOther)).findAny().orElse(null);
        if (statementOfAccountRows != null) {
            rows.remove(statementOfAccountRows);
            rows.add(statementOfAccountRows);
        }
        return rows;
    }

    public Map<String, List<AccountItemEntity>> groupByInvoiceNumberAccountNumberCurrencyVesselName(List<AccountItemEntity> accItems) {
        Map<String, List<AccountItemEntity>> collect = accItems.stream().collect(Collectors.groupingBy(i -> i.groupByAccountNumberCurrencyVesselName()));
        return collect;
    }

    public List<StatementOfAccountVessel> convertToRows(Map<String, List<AccountItemEntity>> accountItemMap) {
        List<StatementOfAccountVessel> thisRows = new ArrayList<>();
        for (Map.Entry<String, List<AccountItemEntity>> entry : accountItemMap.entrySet()) {
            thisRows.add(getStatementOfAccountRows(entry.getValue()));
        }
        return thisRows;
    }

    private StatementOfAccountVessel getStatementOfAccountRows(List<AccountItemEntity> list) {
        List<StatementOfAccountTotal> totals = new ArrayList<>();
        list.stream().filter(distinctByKey(AccountItemEntity::getCurrency)).forEach(item -> {
            StatementOfAccountTotal total = new StatementOfAccountTotal();
            total.setCurrency(item.getCurrency());
            totals.add(total);
        });

        StatementOfAccountVessel row;
        if (CollectionUtils.isEmpty(list)) {
            row = new StatementOfAccountVessel();
        } else {
            row = StatementOfAccountVessel.fromAccItem(list.get(0));
        }

        row.setInvoiceTotals(totals);
        row.setInvoices(StatementOfAccountInvoice.fromAccItems(list));
        row.calculateInvoiceTotals();
        return row;
    }

    public List<CharterDeliveryReportEntity> getAllCharterDeclarations(String accountNo) {
        return repoDwh.getCharterDeclarations(accountNo);
    }

    public Base64Container getStatementOfAccountBase64(String statCode, String accountNo) {
        ExcelListConverter excel = getStatementOfAccountExcel(statCode, accountNo);
        Base64Container result = Base64ContainerForExcel.getBase64ContainerWithExcel(excel.getExcelAsBase64Text(), excel.getFilename());
        return result;
    }

    public ExcelListConverter getStatementOfAccountExcel(String statCode, String accountNo) {
        log.info("For now, hard coding (U)npaid and (N)o for account No: " + accountNo);
        AccountEntity account = getAccount(accountNo);
        List<AccountItemEntity> accItems = getAccountItemsFor(accountNo, Unpaid, No);
        List<StatementOfAccountVessel> list = convertAccItemsToAccountVessel(accItems);
        String filename = StatementOfAccountExcel.getStaticFilename(accountNo);
        return new StatementOfAccountExcel(list, filename, account);
    }


    @Deprecated
    public List<Report> getReports() {
        List<Report> reports = new ArrayList<>();
        reports.add(new Report("Statement of account", RESTendPoints.urlStatementOfAccount()));
        reports.add(new Report("Installment Report", RESTendPoints.urlInstallmentReport()));

        return reports;
    }

    public DepositStatus getDepositAccountStatus(String accountNo) {
        DepositStatusEntity depositAccountStatus = financialRepository.getDepositAccountStatus(accountNo);
        return DepositStatus.convertFrom(depositAccountStatus);
    }

    public List<Invoice> getOverdueInvoicesFor(String statCode) {
        List<StatCodeAccountEntity> statCodeAccounts = getStatCodeAccountsCached(statCode);
        List<AccountEntity> allAccounts = getAllAccountsFor(statCodeAccounts);
        return getOverdueInvoicesFor(allAccounts);
    }

    public List<AccountEntity> getAllAccountsFor(List<StatCodeAccountEntity> statCodeAccounts) {
        List<AccountEntity> accounts = new ArrayList<>();
        statCodeAccounts.stream().forEach(i -> accounts.add(getAccount(i.getAccountNo())));
        return accounts;
    }

    public List<Invoice> getOverdueInvoicesFor(List<AccountEntity> accounts) {
        List<String> distinctAccountFromAccounts = getDistinctAccountFromAccounts(accounts);


        List<Invoice> invoices = getOpenInvoicesFor(distinctAccountFromAccounts);
        invoices = getInvoicesWithOverDueAndThatHasAmount(invoices);

        Map<String, String> policyHolderMap = policyHoldersByAccountNumber(distinctAccountFromAccounts);
        updateInvoicesWithPolicyHolderAndInvoiceType(accounts, invoices, policyHolderMap);

        invoices = sortInvoicesByDueDate(invoices);
        return invoices;
    }

    private Map<String, String> policyHoldersByAccountNumber(List<String> distinctAccountFromAccounts) {
        Map<String, String> policyHolderMap = new HashMap<>();
        distinctAccountFromAccounts.stream().forEach(a -> policyHolderMap.put(a, financialRepository.getPolicyHolder(a)));
        return policyHolderMap;
    }

    private void updateInvoicesWithPolicyHolderAndInvoiceType(List<AccountEntity> accounts, List<Invoice> invoices, Map<String, String> policyHolderMap) {
        invoices.stream().forEach(invoice -> updateInvoiceTypeAndPolicyHolder(invoice, accounts, policyHolderMap));
    }

    private List<Invoice> sortInvoicesByDueDate(List<Invoice> invoices) {
        return invoices.stream().sorted(Comparator.comparing(Invoice::getDueDate)).collect(Collectors.toList());
    }

    private void updateInvoiceTypeAndPolicyHolder(Invoice i, List<AccountEntity> accounts, Map<String, String> policyHolderMap) {
        AccountEntity account = filterAccount(i.getAccountNo(), accounts);
        i.setInvoiceType(account.getAccountGroup());
        i.setPolicyHolder(policyHolderMap.get(account.getAccountNo()));
    }

    private AccountEntity filterAccount(String accountNo, List<AccountEntity> allAccounts) {
        AccountEntity accountEntity = allAccounts.stream().filter(i -> accountNo.equalsIgnoreCase(i.getAccountNo())).findFirst().get();
        return accountEntity;
    }

    protected List<Invoice> getInvoicesWithOverDueAndThatHasAmount(List<Invoice> invoices) {
        return invoices.stream()
                .filter(invoice -> invoice.getOriginalAmount().compareTo(BigDecimal.ZERO) != 0 && invoice.hasOverdueItem())
                .collect(Collectors.toList());
    }

    public List<Invoice> getInvoicesFor(String statCode) {
        List<StatCodeAccountEntity> accountsForStatCode = getStatCodeAccountsCached(statCode);
        List<String> distinctAccountFromStatCode = getDistinctAccountFromStatCode(accountsForStatCode);
        List<Invoice> invoices = getOpenInvoicesFor(distinctAccountFromStatCode);
        return invoices;
    }

    @Cacheable(ACCOUNTS_FOR_STAT_CODE)
    public List<StatCodeAccountEntity> getStatCodeAccountsCached(String statCode) {
        List<StatCodeAccountEntity> statCodeAccounts = getStatCodePremiumAccountsFromRepo(statCode);
        statCodeAccounts.addAll(getStatCodeClaimAccountsFromRepo(statCode));
        statCodeAccounts.addAll(getStatCodeDepositAccountsFromRepo(statCode));
        return statCodeAccounts;
    }

    public boolean checkStatCodeAccountsAccess(List<StatCodeDTO> statCodeDTOS, String accountNo) {
        List<StatCodeAccountEntity> statCodeAccountEntities = new ArrayList<>();
        statCodeAccountEntities.addAll(financialRepository.getClaimAccount(accountNo));
        statCodeAccountEntities.addAll(financialRepository.getDepositAccount(accountNo));
        statCodeAccountEntities.addAll(financialRepository.getPremiumAccount(accountNo));

        List<String> accountStatCodes = statCodeAccountEntities.stream().map(StatCodeAccountEntity::getStatCode).collect(Collectors.toList());
        boolean hasAccess = statCodeDTOS.stream().map(StatCodeDTO::getStatCode).anyMatch(userStatCode -> accountStatCodes.contains(userStatCode));

        return hasAccess;
    }

    protected List<Invoice> getOpenInvoicesFor(List<String> accounts) {
        List<Invoice> openInvoicesForAllAccounts = new ArrayList<>();
        if (isNotEmpty(accounts)) {
            accounts.stream().forEach(accountNo -> openInvoicesForAllAccounts.addAll(getInvoicesForAccount(accountNo)));
        }
        return openInvoicesForAllAccounts;
    }

    protected List<Invoice> getInvoicesForAccount(String account) {
        List<AccountItemEntity> accItemsForAccount = getAccItems(account, Unpaid, No);
        return convertAccItemsToInvoices(accItemsForAccount);
    }

    protected List<Invoice> convertAccItemsToInvoices(List<AccountItemEntity> accItemsForAccount) {
        List<Invoice> invoices = new ArrayList<>();

        if (CollectionUtils.isEmpty(accItemsForAccount)) {
            return invoices;
        }

        Map<String, List<AccountItemEntity>> groupAccountItemsByInvoice = accItemsForAccount.stream()
                .filter(i -> i.validInvoiceNumber())
                .collect(Collectors.groupingBy(AccountItemEntity::getInvoiceNoSafe));

        for (Map.Entry<String, List<AccountItemEntity>> accItemsByInvoiceNo : groupAccountItemsByInvoice.entrySet()) {
            List<AccountItemEntity> list = accItemsByInvoiceNo.getValue();
            AccountItemEntity baseItem = list.get(0);
            invoices.add(Invoice.fromAccItem(baseItem, list));

        }
        return invoices;
    }

    private List<String> getDistinctAccountFromAccounts(List<AccountEntity> entities) {
        return entities.stream().filter(distinctByKey(AccountEntity::getAccountNo)).map(AccountEntity::getAccountNo).collect(Collectors.toList());
    }

    private List<String> getDistinctAccountFromStatCode(List<StatCodeAccountEntity> entities) {
        return entities.stream().filter(distinctByKey(StatCodeAccountEntity::getAccountNo)).map(StatCodeAccountEntity::getAccountNo).collect(Collectors.toList());
    }

    public List<InstallmentsEntity> getAllInstalments(String accountNo) {
        List<InstallmentsEntity> instalments = financialRepository.getInstalments(accountNo);
        instalments.addAll(getOtherInstalments(accountNo));
        return instalments;
    }

    public List<InstallmentsEntity> getOtherInstalments(String accountNo) {
        List<InstallmentsEntity> instalmentOthers = financialRepository.getInstalmentOthers(accountNo);
        return instalmentOthers.stream().filter(x -> x.getCurrencyBalance().abs().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
    }

    public List<DepositInstalment> getDepositInstalmentOverview(String accountNo) {
        List<InstallmentsEntity> instalments = getAllInstalments(accountNo);
        List<DepositInstalment> overviewList = mapInstalmentsToDTO(instalments);
        return overviewList;
    }

    protected List<DepositInstalment> mapInstalmentsToDTO(List<InstallmentsEntity> instalments) {

        List<DepositInstalment> list = instalments.stream()
                .filter(x -> (x.getCurrencyBalance().abs().compareTo(BigDecimal.ZERO) > 0 || x.getDepositPaid().abs().compareTo(BigDecimal.ZERO) > 0))
                .filter(distinctByKey(InstallmentsEntity::getInvoiceNo))
                .map(instalment -> DepositInstalment.buildFrom(instalment)
                        .setItemsFilteredBy(instalment, instalments))
                .collect(Collectors.toList());

        return list;
    }

    public Base64Container instalmentReportAsPDF(String statCode, String accountNumber, String fileName) {
        String url = getInstalmentReportUrl(statCode, accountNumber);
        String data = web.urlContentAsBase64String(url);
        Base64Container container = Base64Container.wrapPdfAsUtf8(data, fileName);

        return container;
    }


    public Base64Container InstalmentReportPDF(String statCode, String accountNo, String filename) {
        String url = getInstalmentReportUrl(statCode, accountNo);
        String data = web.urlContentAsBase64String(url);
        Base64Container container = Base64Container.wrapPdfAsUtf8(data, filename);

        return container;
    }

    public YesNo onHoldItems(String accountNo) {
        YesNo onHold;
        AccountEntity account = financialRepository.getAccount(accountNo);
        AccountGroup accountGroup = (account == null ? AccountGroup.OTHER : account.getAccountGroup());

        switch (accountGroup) {
            case CLAIM:
                onHold = No;
                break;
            case PREMIUM:
            case PREMIUM_DEPOSIT:
                onHold = Yes;
                break;
            default:
                onHold = No;
                break;
        }
        return onHold;
    }

    public String getPolicyFistStartDate(String statCode, String accountNo) {
        String firstDate = financialRepository.getPolicyFistStartDate(statCode, accountNo);
        return SkuldSqlHelper.formatSqlDate(firstDate);
    }

    public boolean doStatCodeHaveAccessToEvent(String statCode, String eventNo) {
        return claimService.claimEventExists(statCode, eventNo);
    }

    public List<Product> getAccountProducts(String accountNo) {
        List<String> accountProducts = financialRepository.getAccountProducts(accountNo);
        List<Product> products = new ArrayList<>();

        accountProducts.forEach(x -> products.add(Product.fromString(x)));
        return products;
    }

    public FinancialService setFinancialRepository(FinancialRepository financialRepository) {
        this.financialRepository = financialRepository;
        return this;
    }
}
