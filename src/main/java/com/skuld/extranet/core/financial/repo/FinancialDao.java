package com.skuld.extranet.core.financial.repo;


import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.BankDetail;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import org.jdbi.v3.core.statement.OutParameters;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.OutParameter;
import org.jdbi.v3.sqlobject.statement.SqlCall;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.sql.Types;
import java.util.List;

public interface FinancialDao extends ClosableDao {

    @SqlQuery("select  vessel_name , " +
            "  tia.sk_rs80.find_description (i_item_text, i_agr_line_no, i_item_id, i_acc_item_no, i_money_class, i_collection_group, i_source, i_workpad_no) " +
            "                                                       description, " +
            "  trim(tia.sk_rs80.find_highest_payment_no(i_acc_item_no)) payment_no, " +
            "  tia.sk_rs80.find_invoice_date(i_acc_item_no)             invoice_date," +
            "  tia.sk_rs80.find_invoice_due_date(i_acc_item_no)         invoice_due_date, " +
            "  due_date, " +
            "  tia.sk_rs80.find_event_no(i_item_id)                     event_no, " +
            "  tia.sk_rs80.find_member_ref(i_item_id)                   member_ref," +
            "  i_trans_date, " +
            "  i_currency_code, " +
            "  i_payment_method, " +
            "  i_start_date, " +
            "  i_end_date, " +
            "  i_currency_amt, " +
            "  nvl(i_on_hold,'N') i_on_hold, " +
            "  i_currency_balance, " +
            "  tia.sk_rs80.due_balance_item(i_currency_balance, :itemType, due_date, i_item_class, i_payment_status ) i_due_balance, " +
            "  i_acc_item_no ," +
            "  i_account_no, " +
            "  0 i_vessel_name_order " +
            "  from table(tia.sk_rs80.acc_item_i_account(" +
            "                                        p_accont_no =>  :accountNo, " +
            "                                        p_from_date=>    null, " +
            "                                        p_to_date=>      null, " +
            "                                        p_item_types=>   :itemType, " +
            "                                        p_deposit=>      :deposit, " +
            "                                        p_hold=>         :onHold" +
            "                                        ) " +
            "  )")
    @RegisterRowMapper(AccountItemEntity.Mapper.class)
    List<AccountItemEntity> accountItems(@Bind("accountNo") String accountNo,
                                         @Bind("itemType") String itemType,
                                         @Bind("deposit") String deposit,
                                         @Bind("onHold") String onHold
    );

    @SqlQuery("select * from v_acc_account where account_no = :accountNo")
    @RegisterRowMapper(AccountEntity.Mapper.class)
    AccountEntity getAccount(@Bind("accountNo") String accountNo);

    @SqlQuery("select * from v_statcode_premium_accounts where stat_code = :statCode ")
    @RegisterRowMapper(StatCodeAccountEntity.Mapper.class)
    List<StatCodeAccountEntity> getStatCodePremiumAccounts(@Bind("statCode") String statCode);

    @SqlQuery("select * from v_statcode_premium_deposit_acc where stat_code = :statCode ")
    @RegisterRowMapper(StatCodeAccountEntity.Mapper.class)
    List<StatCodeAccountEntity> getStatCodeDepositAccounts(@Bind("statCode") String statCode);

    @SqlQuery("select * from v_statcode_claim_accounts where stat_code = :statCode ")
    @RegisterRowMapper(StatCodeAccountEntity.Mapper.class)
    List<StatCodeAccountEntity> getStatCodeClaimAccounts(@Bind("statCode") String statCode);


    @SqlQuery("select * from v_statcode_premium_accounts where account_no = :accountNo ")
    @RegisterRowMapper(StatCodeAccountEntity.Mapper.class)
    List<StatCodeAccountEntity> getPremiumAccount(@Bind("accountNo") String accountNo);

    @SqlQuery("select * from v_statcode_premium_deposit_acc where account_no = :accountNo ")
    @RegisterRowMapper(StatCodeAccountEntity.Mapper.class)
    List<StatCodeAccountEntity> getDepositAccount(@Bind("accountNo") String accountNo);

    @SqlQuery("select * from v_statcode_claim_accounts where account_no = :accountNo ")
    @RegisterRowMapper(StatCodeAccountEntity.Mapper.class)
    List<StatCodeAccountEntity> getClaimAccount(@Bind("accountNo") String accountNo);



    @SqlQuery("select " +
            " at.t_currency_code        currency, " +
            " at.t_sum_currency_balance balance, " +
            " at.t_item_count item_count, " +
            " db.d_sum_currency_balance due_balance " +
            "from " +
            "table(tia.sk_rs80.account_total(:accountNumber, null, null, :onHold , 'A', :includeDeposit)) at " +
            " left outer join " +
            "      table(tia.sk_rs80.due_balance(  :accountNumber, null, null, :onHold , :dueBalanceItemType , :includeDeposit)) db " +
            "      on at.t_account_no  = db.d_account_no and db.d_currency_code = at.t_currency_code " +
            "where at.t_account_no    = :accountNumber")
    @RegisterRowMapper(AccountSum.Mapper.class)
    List<AccountSum> getAccountSum(@Bind("accountNumber") String accountNumber,
                                   @Bind("dueBalanceItemType") String dueBalanceItemType,
                                   @Bind("onHold") String onHold,
                                   @Bind("includeDeposit") String includeDeposit
                                   );

    @SqlQuery("select * " +
            "from table(tia.sk_rs80.bank_details(:accountNumber,:itemType, :deposit, :onHold))")
    @RegisterRowMapper(BankDetail.Mapper.class)
    List<BankDetail> getBankDetails(@Bind("accountNumber") String accountNumber,
                                    @Bind("itemType") String itemType,
                                    @Bind("deposit") String deposit,
                                    @Bind("onHold") String onHold);

    @SqlQuery("select tia.skuld.get_party_address(:bankIdNumber) address from dual")
    String getBankAddress(@Bind("bankIdNumber") String bankIdNumber);

    @SqlQuery("select distinct * from v_account_policy_holder where account_no = :accountNumber ")
    String getPolicyHolder(@Bind("accountNumber") String accountNumber);

    @SqlQuery("select " +
            "   current_ship_name " +
            "  ,p_line_id product " +
            "  ,eff_Start " +
            "  ,eff_end " +
            "  ,currency " +
            "  ,account_no " +
            "  ,rebate " +
            "  ,round( premium,2) premium " +
            "  ,round( tax_amount,2) tax_amount " +
            "  ,tax_domestic_pct + tax_all_pct tax_percent " +
            " from mv_charter_delivery_report " +
            " where account_no = :account_no "
    )
    @RegisterRowMapper(CharterDeliveryReportEntity.Mapper.class)
    List<CharterDeliveryReportEntity> getCharterDeclarations(@Bind("account_no") String accountNumber);

    @SqlCall("{ CALL financial.get_deposit_account_status2(:accountNo, :invoicedAmt, :paidAmt, :usedAmt, :currency) }")
    @OutParameter(name = "invoicedAmt", sqlType = Types.NUMERIC)
    @OutParameter(name = "paidAmt", sqlType = Types.NUMERIC)
    @OutParameter(name = "usedAmt", sqlType = Types.NUMERIC)
    @OutParameter(name = "currency", sqlType = Types.VARCHAR)
    OutParameters getDepositAccountStatus(@Bind("accountNo") String accountNo);

    @SqlQuery("select * from v_deposit_instalment where account_no = :account_no")
    @RegisterRowMapper(InstallmentsEntity.Mapper.class)
    List<InstallmentsEntity> getInstalments(@Bind("account_no") String accountNo);

    @SqlQuery("select * from v_deposit_instalment_other where account_no = :account_no")
    @RegisterRowMapper(InstallmentsEntity.MapperForOtherInstalment.class)
    List<InstallmentsEntity> getInstalmentOthers(@Bind("account_no") String accountNo);

    //TODO RESTAPI-686
    @SqlQuery("SELECT min(p.first_start_date) " +
            "  FROM sk_party_group pg " +
            "         INNER JOIN policy p ON p.c11 = pg.stat_code " +
            "                                  AND SYSDATE BETWEEN p.cover_start_date AND p.cover_end_date " +
            "                                  AND p.suc_seq_no IS NULL " +
            "         INNER JOIN acc_account a ON a.account_no = p.account_no " +
            "         INNER JOIN policy_line pli ON pli.policy_no = p.policy_no " +
            "                                  AND pli.cancel_code = 0 " +
            "                                  AND SYSDATE BETWEEN pli.cover_start_date " +
            "                                  AND pli.cover_end_date " +
            "                                  AND pli.suc_seq_no IS NULL " +
            "  WHERE pg.stop_date IS NULL " +
            "    AND p.account_no IS NOT NULL " +
            "    AND p.policy_status = 'P' " +
            "    AND a.account_type = 1 " +
            "    and a.account_no = :accountNo " +
            "    and pg.stat_code = :statCode")
    String getStatCodePolicyFistStartDate(@Bind("statCode") String statCode,
                                          @Bind("accountNo") String accountNo);

    @SqlQuery("select description " +
            "from xla_pe_reference xla " +
            "      inner join policy_entity pe on pe.prod_id = xla.id " +
            "      inner join policy p on p.policy_no = pe.policy_no " +
            "      inner join acc_account a on a.account_no = p.account_no " +
            "      inner join policy_line pli on pli.policy_no = p.policy_no " +
            "                                            and pli.cancel_code = 0 " +
            "                                            and sysdate between pli.cover_start_date " +
            "                                            and pli.cover_end_date " +
            "                                            and pli.suc_seq_no is null " +
            "where p.account_no is not null " +
            "  and p.policy_status = 'P' " +
            "  and a.account_type = 1 " +
            "  and a.account_no = :accountNo " +
            "group by description")
    List<String> getAccountProducts(@Bind("accountNo") String accountNo);
}
