package com.skuld.extranet.core.financial.domain.entity;


import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static com.skuld.extranet.core.common.SkuldSqlHelper.fromSqlDate;


public class CharterDeliveryReportEntity {


    private String currentShipName;
    private String product;
    private String rebate;
    private LocalDate effStart;
    private LocalDate effEnd;
    private String currency;
    private String accountNo;
    private BigDecimal premium;
    private BigDecimal taxAmount;
    private Integer taxPercent;


    public CharterDeliveryReportEntity() {
    }

    public CharterDeliveryReportEntity(ResultSet r) throws SQLException {
        this.accountNo = r.getString("account_no");
        this.currentShipName = r.getString("current_ship_name");
        this.effStart = fromSqlDate(r.getDate("eff_Start"));
        this.effEnd = fromSqlDate(r.getDate("eff_End"));
        this.product = r.getString("product");
        this.rebate = r.getString("rebate");
        this.currency = r.getString("currency");
        this.premium = r.getBigDecimal("premium");
        this.taxAmount = r.getBigDecimal("tax_amount");
        this.taxPercent = r.getInt("tax_percent");
    }

    public BigDecimal getPremiumInclTax() {
        if (premium!=null){
            if (taxAmount!=null){
                return premium.add(taxAmount);
            }
            else{
                return premium;
            }
        }
        return BigDecimal.ZERO;
    }


    public static class Mapper implements RowMapper<CharterDeliveryReportEntity> {
        public CharterDeliveryReportEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new CharterDeliveryReportEntity(r);
        }
    }

    public String getCurrentShipName() {
        return currentShipName;
    }

    public CharterDeliveryReportEntity setCurrentShipName(String currentShipName) {
        this.currentShipName = currentShipName;
        return this;
    }

    public LocalDate getEffStart() {
        return effStart;
    }

    public CharterDeliveryReportEntity setEffStart(LocalDate effStart) {
        this.effStart = effStart;
        return this;
    }

    public LocalDate getEffEnd() {
        return effEnd;
    }

    public CharterDeliveryReportEntity setEffEnd(LocalDate effEnd) {
        this.effEnd = effEnd;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public CharterDeliveryReportEntity setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public CharterDeliveryReportEntity setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public CharterDeliveryReportEntity setPremium(BigDecimal premium) {
        this.premium = premium;
        return this;
    }

    public CharterDeliveryReportEntity setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public String getRebate() {
        return rebate;
    }

    public CharterDeliveryReportEntity setRebate(String rebate) {
        this.rebate = rebate;
        return this;
    }

    public boolean isRebateItem() {
        return "Y".equals(this.rebate);
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public CharterDeliveryReportEntity setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
        return this;
    }

    public Integer getTaxPercent() {
        return taxPercent;
    }

    public CharterDeliveryReportEntity setTaxPercent(Integer taxPercent) {
        this.taxPercent = taxPercent;
        return this;
    }
}
