package com.skuld.extranet.core.financial.domain.deposit;

public enum InstalmentType {
    other("Other"), normal("Normal");

    private final String value;

    InstalmentType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static InstalmentType fromString(String code) {
        for (InstalmentType b : InstalmentType.values()) {
            if (b.value.equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }
}
