package com.skuld.extranet.core.financial.domain.entity;

import com.skuld.extranet.core.common.SkuldSqlHelper;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.AccountSum;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static com.skuld.extranet.core.financial.domain.AccountGroup.*;

public class AccountEntity {
    private String accountNo;
    private String accountType;
    private String name;
    private String currency;
    private String creditControllerPtUserId;
    private String paymentMethod;
    private AccountGroup accountGroup;
    private AccountSum accountSum;

    private LocalDate lastTransDate;

    public AccountEntity() {
    }

    public AccountEntity(ResultSet r) throws SQLException {
        this.accountNo = r.getString("account_no");
        this.accountType = r.getString("account_type");
        this.name = r.getString("title");
        this.currency = r.getString("currency_code");
        this.creditControllerPtUserId = r.getString("creditcontroller_pt_user_id");
        this.paymentMethod = r.getString("payment_method_in");
        this.lastTransDate = SkuldSqlHelper.fromSqlDate(r.getDate("last_trans_date"));
    }

    public AccountGroup getAccountGroup() {
        switch (accountType) {
            case "1":
                if ("DEPOSIT".equalsIgnoreCase(paymentMethod)) {
                    return PREMIUM_DEPOSIT;
                } else
                    return PREMIUM;
            case "3":
            case "5":
            case "6":
                return CLAIM;
            default:
                return OTHER;
        }
    }

    public String getPolicyHolderId() {
        return "0";
    }

    public boolean isPremiumGroup() {
        return AccountGroup.PREMIUM.equals(getAccountGroup());
    }

    public boolean isClaimGroup() {
        return CLAIM.equals(getAccountGroup());
    }

    public boolean isPremiumDeposit() {
        return AccountGroup.PREMIUM_DEPOSIT.equals(getAccountGroup());
    }

    public static class Mapper implements RowMapper<AccountEntity> {
        public AccountEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new AccountEntity(r);
        }
    }

    public String getAccountNo() {
        return accountNo;
    }

    public AccountEntity setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public String getAccountType() {
        return accountType;
    }

    public AccountEntity setAccountType(String accountType) {
        this.accountType = accountType;
        return this;
    }

    public String getName() {
        return name;
    }

    public AccountEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountEntity setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getCreditControllerPtUserId() {
        return creditControllerPtUserId;
    }

    public AccountEntity setCreditControllerPtUserId(String creditControllerPtUserId) {
        this.creditControllerPtUserId = creditControllerPtUserId;
        return this;
    }


    public String getPaymentMethod() {
        return paymentMethod;
    }

    public AccountEntity setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public AccountEntity setAccountGroup(AccountGroup accountGroup) {
        this.accountGroup = accountGroup;
        return this;
    }

    public AccountSum getAccountSum() {
        return accountSum;
    }

    public AccountEntity setAccountSum(AccountSum accountSum) {
        this.accountSum = accountSum;
        return this;
    }

    public boolean isAccountGroup(AccountGroup group){
        return this.getAccountGroup().equals(group);
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "accountNo='" + accountNo + '\'' +
                ", currency='" + currency + '\'' +
                ", accountGroup=" + getAccountGroup() +
                '}';
    }

    public LocalDate getLastTransDate() {
        return lastTransDate;
    }

    public AccountEntity setLastTransDate(LocalDate lastTransDate) {
        this.lastTransDate = lastTransDate;
        return this;
    }
}
