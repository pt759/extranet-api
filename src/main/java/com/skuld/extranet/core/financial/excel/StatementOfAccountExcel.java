package com.skuld.extranet.core.financial.excel;

import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoice;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoiceItem;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountVessel;
import com.skuld.extranet.excel.ExcelListConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.skuld.extranet.core.common.SkuldFormatter.formatLocaldateFor;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.GroupOther;
import static com.skuld.extranet.global.Global.remoteImgSkuldLogo;

public class StatementOfAccountExcel extends ExcelListConverter {

    final static Logger logger = LoggerFactory.getLogger(StatementOfAccountExcel.class);

    private static final String SheetName = "Statement Of Account";
    private AccountEntity account;
    private final int startRowForData;

    public static String getStaticFilename(String accountNo) {
        return "StatementOfAccount_" + accountNo + ".xlsx";
    }

    public StatementOfAccountExcel(List<StatementOfAccountVessel> list, String filename, AccountEntity account) {
        super(list, filename, SheetName);
        this.account = account;
        this.autoFilter = false;
        this.startRowForData = 5;
    }

    @Override
    protected Row populateRowHeader(Sheet sheet) {

        CellStyle headerStyleLeft = styleHelper.headerStyleWithSolidRedBackground();
        headerStyleLeft.setAlignment(HorizontalAlignment.LEFT);

        CellStyle headerStyleRight = styleHelper.headerStyleWithSolidRedBackground();
        headerStyleRight.setAlignment(HorizontalAlignment.RIGHT);

        Integer column = 0;
        Row rowTopHeader = sheet.createRow(0);
        Row rowHeader = sheet.createRow(startRowForData);
        styleHelper.createCellWithStyle(column++, "Vessel Name", headerStyleLeft, rowHeader);
        styleHelper.createCellWithStyle(column++, "Description", headerStyleLeft, rowHeader);
        styleHelper.createCellWithStyle(column++, "Invoice Date", headerStyleLeft, rowHeader);
        styleHelper.createCellWithStyle(column++, "Invoice No.", headerStyleLeft, rowHeader);
        styleHelper.createCellWithStyle(column++, "Item No.", headerStyleLeft, rowHeader);

        if (account.isPremiumGroup()) {
            styleHelper.createCellWithStyle(column++, "From", headerStyleLeft, rowHeader); // Premium only
            styleHelper.createCellWithStyle(column++, "To", headerStyleLeft, rowHeader);// Premium only
        } else if (account.isClaimGroup()) {
            styleHelper.createCellWithStyle(column++, "Event No.", headerStyleLeft, rowHeader);//Claim
            styleHelper.createCellWithStyle(column++, "Member Ref.", headerStyleLeft, rowHeader);//Claim
        }
        styleHelper.createCellWithStyle(column++, "Due Date", headerStyleLeft, rowHeader);
        styleHelper.createCellWithStyle(column++, "Currency", headerStyleLeft, rowHeader);

        styleHelper.createCellWithStyle(column++, "Original Amount", headerStyleRight, rowHeader);
        styleHelper.createCellWithStyle(column++, "Balance", headerStyleRight, rowHeader);
        styleHelper.createCellWithStyle(column++, "Due balance", headerStyleRight, rowHeader);

        // Headliner
        CellStyle createdStyle = styleHelper.styleGenericNew();
        createdStyle.setAlignment(HorizontalAlignment.RIGHT);
        createdStyle.setFont(styleHelper.normalFontBold());

        Row row = sheet.createRow(rowTopHeader.getRowNum() + 1);
        styleHelper.createCellWithStyle(column - 4, account.getName(), createdStyle, row);
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), column - 4, column - 1));

        row = sheet.createRow(rowTopHeader.getRowNum() + 2);
        styleHelper.createCellWithStyle(column - 1, account.getAccountNo(), createdStyle, row);

        row = sheet.createRow(rowTopHeader.getRowNum() + 3);
        styleHelper.createCellWithStyle(column - 1, formatLocaldateFor(LocalDate.now()), createdStyle, row);

        return rowHeader;
    }


    @Override
    protected void populateDataRows(Sheet sheet) {

        List<StatementOfAccountVessel> accountRows = (List<StatementOfAccountVessel>) list;

        CellStyle rowStyle = styleHelper.styleGeneric();

        int currentRow = startRowForData + 1;

        Set<String> currencies = new HashSet<>();

        for (StatementOfAccountVessel accountRow : accountRows) {

            for (StatementOfAccountInvoice invoice : accountRow.getInvoices()) {

                for (StatementOfAccountInvoiceItem item : invoice.getDetails()) {
                    Row row = sheet.createRow(currentRow++);
                    int column = 0;
                    currencies.add(item.getCurrency());
                    styleHelper.createCellWithStyle(column++, accountRow.getVesselName(), rowStyle, row);
                    styleHelper.createCellWithStyle(column++, item.getItemDescription(), rowStyle, row);
                    styleHelper.createCellWithStyle(column++, getInvoiceDateColumnValue(accountRow, item), rowStyle, row);
                    styleHelper.createCellWithStyle(column++, item.getInvoiceNo(), rowStyle, row);
                    styleHelper.createCellWithStyle(column++, item.getItemNo(), rowStyle, row);

                    if (account.isPremiumGroup()) {
                        styleHelper.createCellWithStyle(column++, formatLocaldateFor(item.getPeriodFrom()), rowStyle, row);
                        styleHelper.createCellWithStyle(column++, formatLocaldateFor(item.getPeriodTo()), rowStyle, row);
                    } else if (account.isClaimGroup()) {
                        styleHelper.createCellWithStyle(column++, item.getEventNo(), rowStyle, row);
                        styleHelper.createCellWithStyle(column++, item.getMemberRef(), rowStyle, row);
                    }
                    styleHelper.createCellWithStyle(column++, getDueDateColumnValue(item), rowStyle, row);
                    styleHelper.createCellWithStyle(column++, item.getCurrency(), rowStyle, row);
                    styleHelper.createNumericCellWithStyle(column++, item.getOriginalAmount().toString(), styleHelper.styleNumericThousand(), row);
                    styleHelper.createNumericCellWithStyle(column++, item.getBalance().toString(), styleHelper.styleNumericThousand(), row);
                    styleHelper.createNumericCellWithStyle(column++, blankIfZero(item.getDueBalance()), styleHelper.styleNumericThousand(), row);
                }
            }
        }

        int endRow = currentRow;
        for (String cur : currencies.stream().sorted().collect(Collectors.toList())) {
            populateSumRow(cur, startRowForData + 1, endRow, currentRow++, sheet);
        }


    }

    private String blankIfZero(BigDecimal value) {
        if (value.compareTo(BigDecimal.ZERO) == 0) {
            return "";
        } else {
            return value.toString();
        }

    }


    private String getDueDateColumnValue(StatementOfAccountInvoiceItem item) {
        if (StringUtils.isEmpty(item.getInvoiceNo())) {
            return "";
        } else {
            return formatLocaldateFor(item.getDueDate());
        }
    }

    private String getInvoiceDateColumnValue(StatementOfAccountVessel accountRow, StatementOfAccountInvoiceItem item) {
        if (GroupOther.equalsIgnoreCase(accountRow.getVesselName()) && StringUtils.isEmpty(item.getInvoiceNo())) {
            return formatLocaldateFor(item.getTransDate());
        } else {
            return formatLocaldateFor(item.getInvoiceDate());
        }

    }

    private void populateSumRow(String currency, int startFormulaRow, int endFormulaRow, int printRow, Sheet sheet) {

        Row row = sheet.createRow(printRow);
        styleHelper.createCellWithStyle(8, "Sum " + currency + " :", styleHelper.styleTwoBorderStyle(), row);
        createSumIfCell(currency, 9, startFormulaRow + 1, endFormulaRow, 8, styleHelper.styleTwoBorderStyle(), row);
        createSumIfCell(currency, 10, startFormulaRow + 1, endFormulaRow, 8, styleHelper.styleTwoBorderStyle(), row);
        createSumIfCell(currency, 11, startFormulaRow + 1, endFormulaRow, 8, styleHelper.styleTwoBorderStyle(), row);

    }

    private void createSumIfCell(String check, int column, int startRow, int endRow, int colCheck, CellStyle style, Row row) {

        Cell cell = styleHelper.createFormulaCell(column, style, row);

        String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
        String columnLetterForReference = CellReference.convertNumToColString(colCheck);
        String range = columnLetter + startRow + ":" + columnLetter + endRow;
        String checkRange = columnLetterForReference + startRow + ":" + columnLetterForReference + endRow;
        String cellFormula = "SUMIF(" + checkRange + ", \"" + check + "\", " + range + ")";

        cell.setCellType(CellType.FORMULA);
        cell.setCellFormula(cellFormula);


    }

    protected void addLogoImage(Sheet sheet) {


        try {
            URL url = new URL(remoteImgSkuldLogo);
            InputStream targetStream = url.openStream();
            byte[] bytes = IOUtils.toByteArray(targetStream);
            int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
            targetStream.close();
            CreationHelper helper = workbook.getCreationHelper();
            // Create the drawing patriarch.  This is the top level container for all shapes.
            Drawing drawing = sheet.createDrawingPatriarch();

            //add a picture shape
            ClientAnchor anchor = helper.createClientAnchor();
            //set top-left corner of the picture,
            //subsequent call of Picture#resize() will operate relative to it
            anchor.setCol1(0);
            anchor.setCol2(0);
            anchor.setRow1(2);
            anchor.setRow2(2);
            Picture pict = drawing.createPicture(anchor, pictureIdx);

            //auto-size picture relative to its top-left corner
            pict.resize();
        } catch (Exception e) {
            logger.error("Error drawing logo, but moving on: " + e.getMessage());
        }
    }


    @Override
    protected void populateFooterRow(Sheet sheet) {

    }

    public AccountEntity getAccount() {
        return account;
    }

    public StatementOfAccountExcel setAccount(AccountEntity account) {
        this.account = account;
        return this;
    }

    protected void autoSizeColumns(Sheet sheet) {
        for (int x = 0; x < sheet.getRow(startRowForData).getPhysicalNumberOfCells(); x++) {
            sheet.autoSizeColumn(x);
        }
    }
}
