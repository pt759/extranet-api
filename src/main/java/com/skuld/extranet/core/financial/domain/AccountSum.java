package com.skuld.extranet.core.financial.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.skuld.extranet.core.common.SkuldAmounts.amountDecimal;
import static com.skuld.extranet.core.common.SkuldAmounts.roundSafe;

public class AccountSum {

    private BigDecimal balance;
    private BigDecimal dueBalance;
    private String currency;
    private Integer itemRows;

    public AccountSum() {
        this.dueBalance = BigDecimal.ZERO;
        this.balance = BigDecimal.ZERO;
    }

    public AccountSum(String accountNumber, BigDecimal accountBalance, BigDecimal balance, String lastUpdated) {
        this.dueBalance = accountBalance;
        this.balance = balance;
    }

    public AccountSum(BigDecimal accountBalance, BigDecimal balance) {
        this.dueBalance = accountBalance;
        this.balance = balance;
    }

    public AccountSum(ResultSet r) throws SQLException {
        this.balance = r.getBigDecimal("balance");
        this.dueBalance = r.getBigDecimal("due_balance");
        this.currency = r.getString("currency");
        this.itemRows = r.getInt("item_count");
    }

    public static class Mapper implements RowMapper<AccountSum> {
        public AccountSum map(ResultSet r, StatementContext ctx) throws SQLException {
            return new AccountSum(r);
        }
    }




    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDueBalance() {
        return roundSafe(dueBalance, amountDecimal);
    }

    public AccountSum setDueBalance(BigDecimal dueBalance) {
        this.dueBalance = dueBalance;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return roundSafe(balance, amountDecimal);
    }

    public AccountSum setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountSum setCurrency(String currency) {
        this.currency = currency;
        return this;
    }






    public Integer getItemRows() {
        return itemRows;
    }

    public AccountSum setItemRows(Integer itemRows) {
        this.itemRows = itemRows;
        return this;
    }
}
