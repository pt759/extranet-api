package com.skuld.extranet.core.financial.domain.deposit.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.financial.domain.deposit.InstalmentType;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static com.skuld.extranet.core.common.SkuldSqlHelper.fromSqlDate;

public class InstallmentsEntity {


    private String invoiceNo;
    private LocalDate invoiceDueDate;
    private LocalDate invoiceDate;
    private LocalDate itemDueDate;
    private LocalDate itemTransDate;
    private String accountNo;
    private String accItemNo;
    private String itemId;
    private String itemText;
    private String currencyCode;
    private BigDecimal amt;
    private BigDecimal currencyAmount;
    private BigDecimal currencyBalance;
    private BigDecimal depositPaid;
    private String paymentMethod;
    private String fullyMatched;

    public InstallmentsEntity() {

    }


    public InstallmentsEntity(ResultSet r) throws SQLException {
        this.invoiceNo = r.getString("invoice_no");
        this.invoiceDueDate = fromSqlDate(r.getDate("invoice_due_date"));
        this.invoiceDate = fromSqlDate(r.getDate("invoice_date"));
        this.itemDueDate = fromSqlDate(r.getDate("item_due_date"));
        this.accountNo = r.getString("account_no");
        this.accItemNo = r.getString("acc_item_no");
        this.itemId = r.getString("item_id");
        this.itemText = r.getString("item_text");
        this.currencyCode = r.getString("currency_code");
        this.amt = r.getBigDecimal("amount");
        this.currencyAmount = r.getBigDecimal("currency_amount");
        this.depositPaid = r.getBigDecimal("deposit_paid");
        this.currencyBalance = r.getBigDecimal("currency_balance");
        this.paymentMethod = r.getString("payment_method");
        this.fullyMatched = r.getString("fully_matched");
        this.itemTransDate = fromSqlDate(r.getDate("trans_date"));

    }

    private static InstallmentsEntity buildOtherInstalment(ResultSet r) throws SQLException {
        InstallmentsEntity entity = new InstallmentsEntity();

        entity.accountNo = r.getString("account_no");
        entity.accItemNo = r.getString("acc_item_no");
        entity.itemText = r.getString("item_text");
        entity.currencyCode = r.getString("currency_code");
//        entity.depositPaid = r.getBigDecimal("deposit_paid");
        entity.depositPaid = BigDecimal.ZERO;
        entity.currencyAmount = r.getBigDecimal("currency_amount");
        entity.currencyBalance = r.getBigDecimal("currency_balance");
        entity.itemDueDate = fromSqlDate(r.getDate("item_due_date"));
        entity.itemTransDate = fromSqlDate(r.getDate("trans_date"));
        entity.invoiceNo = "";
        entity.setInvoiceDate(null);
        entity.setInvoiceDueDate(null);
        entity.setAmt(BigDecimal.ZERO);

        return entity;
    }

    public InstallmentsEntity setBalance(BigDecimal balance) {
        this.currencyBalance = balance;
        return this;
    }

    public boolean filterItemByInstalmentType(InstallmentsEntity entity) {
        if (entity.isInstalmentOther()) {
            return this.getCurrencyBalance().abs().compareTo(BigDecimal.ZERO) > 0 || this.getCurrencyAmount().abs().compareTo(BigDecimal.ZERO) > 0;
        } else {
            return this.getCurrencyBalance().abs().compareTo(BigDecimal.ZERO) > 0 || this.getDepositPaid().abs().compareTo(BigDecimal.ZERO) > 0;
        }
    }

    public static class Mapper implements RowMapper<InstallmentsEntity> {
        @Override
        public InstallmentsEntity map(ResultSet rs, StatementContext ctx) throws SQLException {
            return new InstallmentsEntity(rs);
        }
    }


    public static class MapperForOtherInstalment implements RowMapper<InstallmentsEntity> {
        @Override
        public InstallmentsEntity map(ResultSet rs, StatementContext ctx) throws SQLException {
            return InstallmentsEntity.buildOtherInstalment(rs);
        }
    }


    public String getInvoiceNo() {
        return invoiceNo;
    }

    public InstallmentsEntity setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public InstallmentsEntity setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public InstallmentsEntity setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public String getAccItemNo() {
        return accItemNo;
    }

    public InstallmentsEntity setAccItemNo(String accItemNo) {
        this.accItemNo = accItemNo;
        return this;
    }

    public String getItemId() {
        return itemId;
    }

    public InstallmentsEntity setItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    public String getItemText() {
        return itemText;
    }

    public InstallmentsEntity setItemText(String itemText) {
        this.itemText = itemText;
        return this;
    }

    public InstallmentsEntity setDescription(String itemText) {
        this.itemText = itemText;
        return this;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public InstallmentsEntity setCurrency(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public InstallmentsEntity setAmt(BigDecimal amt) {
        this.amt = amt;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getCurrencyAmount() {
        return currencyAmount;
    }

    public InstallmentsEntity setCurrencyAmount(BigDecimal currencyAmount) {
        this.currencyAmount = currencyAmount;
        return this;
    }

    public BigDecimal getCurrencyBalance() {
        return currencyBalance;
    }

    public InstallmentsEntity setCurrencyBalance(BigDecimal currencyBalance) {
        this.currencyBalance = currencyBalance;
        return this;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public InstallmentsEntity setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public String getFullyMatched() {
        return fullyMatched;
    }

    public InstallmentsEntity setFullyMatched(String fullyMatched) {
        this.fullyMatched = fullyMatched;
        return this;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public InstallmentsEntity setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public InstallmentsEntity setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public BigDecimal getDepositPaid() {
        return depositPaid;
    }

    public InstallmentsEntity setDepositPaid(BigDecimal depositPaid) {
        this.depositPaid = depositPaid;
        return this;
    }

    public LocalDate getItemDueDate() {
        return itemDueDate;
    }

    public InstallmentsEntity setItemDueDate(LocalDate itemDueDate) {
        this.itemDueDate = itemDueDate;
        return this;
    }

    public InstalmentType getInstalmentType() {
        return StringUtils.isEmpty(invoiceNo) ? InstalmentType.other : InstalmentType.normal;
    }

    public boolean isInstalmentOther() {
        return getInstalmentType().equals(InstalmentType.other);
    }

    @Override
    public String toString() {
        return "accItemNo='" + accItemNo + '\'';

    }

    public LocalDate getItemTransDate() {
        return itemTransDate;
    }

    public InstallmentsEntity setItemTransDate(LocalDate itemTransDate) {
        this.itemTransDate = itemTransDate;
        return this;
    }
}
