package com.skuld.extranet.core.financial.domain.invoice;

import com.skuld.extranet.core.financial.domain.AccountGroup;

public enum InvoiceType {

    Other("Other"),
    Premium("Premium"),
    PremiumDeposit("Premium deposit"),
    Claim("Claim"),
    // Do we really need the others here
    Deductible("Deductible"),
    Installment("Installment"),
    CreditNote("Credit note"),
    Payment("Payment"),
    PolicyInvoice("Policy Invoice"),
    //    Jan Peders suggestions, please see RESTAPI-423.
    Commission("Commission"),
    PolicyFlexInvoice("Policy flex Invoice"),
    Call("Call"),
    DepositCommission("Deposit commission"),
    TechnicalServices("Technical services"),
    DepositInvoice("Deposit invoice"),
    CreatedInAx("Created in ax"),
    Rebate("Rebate"),
    RebateCommission("Rebate commission"),
    MutualCredit("Mutual credit"),
    Invoice("Invoice");

    private final String value;

    InvoiceType(String a) {
        value = a;
    }

    @Override
    public String toString() {
        return value;
    }

    public String value() {
        return value;
    }

    public static InvoiceType fromString(String code) {
        for (InvoiceType b : InvoiceType.values()) {
            if (b.value().equalsIgnoreCase(code)) {
                return b;
            }
        }
        return Invoice;
    }

    public static InvoiceType from(AccountGroup group) {
        switch (group) {
            case PREMIUM:
                return Premium;
            case PREMIUM_DEPOSIT:
                return PremiumDeposit;
            case CLAIM:
                return Claim;
            default:
                return Other;
        }

    }

}
