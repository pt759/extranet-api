package com.skuld.extranet.core.financial.domain.statementofaccount;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.global.RESTendPoints.urlInvoiceAsBase64;

public class StatementOfAccountInvoiceItem {

    private String itemDescription;
    private String itemNo;

    private String invoiceNo;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDate;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDateDue;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate dueDate;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate transDate;

    // Premium Account
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate periodFrom;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate periodTo;

    private String currency;
    private BigDecimal originalAmount;
    private BigDecimal balance;
    private BigDecimal dueBalance;

    // Claims Only
    private String eventNo;
    private boolean eventNoAccess;
    private String memberRef;



    public String getInvoiceUrl() {
        if (StringUtils.isNotEmpty(invoiceNo)) {
            return urlInvoiceAsBase64(invoiceNo);
        } else {
            return "";
        }

    }

    public StatementOfAccountInvoiceItem() {
    }

    public static StatementOfAccountInvoiceItem fromAccItem(AccountItemEntity accItem) {

        return new StatementOfAccountInvoiceItem()

                .setItemNo(accItem.getItemNo())
                .setInvoiceNo(accItem.getInvoiceNo())
                .setItemDescription(accItem.getDescription())
                .setInvoiceDate(accItem.getInvoiceDate())
                .setInvoiceDateDue(accItem.getInvoiceDueDate())
                .setDueDate(accItem.getDueDate())

                .setPeriodFrom(accItem.getStartDate())
                .setPeriodTo(accItem.getEndDate())

                .setOriginalAmount(accItem.getOriginalAmount())
                .setBalance(accItem.getBalance())
                .setDueBalance(accItem.getDueBalance())
                .setCurrency(accItem.getCurrency())

                .setEventNo(accItem.getEventNo())
                .setMemberRef(accItem.getMemberRef())
                .setTransDate(accItem.getTransDate())
                ;
    }


    public String getItemDescription() {
        return itemDescription;
    }

    public StatementOfAccountInvoiceItem setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
        return this;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public StatementOfAccountInvoiceItem setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public StatementOfAccountInvoiceItem setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public LocalDate getInvoiceDateDue() {
        return invoiceDateDue;
    }

    public StatementOfAccountInvoiceItem setInvoiceDateDue(LocalDate invoiceDateDue) {
        this.invoiceDateDue = invoiceDateDue;
        return this;
    }

    public LocalDate getPeriodFrom() {
        return periodFrom;
    }

    public StatementOfAccountInvoiceItem setPeriodFrom(LocalDate periodFrom) {
        this.periodFrom = periodFrom;
        return this;
    }

    public LocalDate getPeriodTo() {
        return periodTo;
    }

    public StatementOfAccountInvoiceItem setPeriodTo(LocalDate periodTo) {
        this.periodTo = periodTo;
        return this;
    }

    public static List<StatementOfAccountInvoiceItem> fromAccItems(AccountItemEntity item, List<AccountItemEntity> accItems) {
        return accItems.stream()
                .filter(i->i.getInvoiceNoSafe().equalsIgnoreCase(item.getInvoiceNoSafe()))
                .map(i -> StatementOfAccountInvoiceItem.fromAccItem(i))
                .collect(Collectors.toList());
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public StatementOfAccountInvoiceItem setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return balance;
    }

    public StatementOfAccountInvoiceItem setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDueBalance() {
        return dueBalance;
    }

    public StatementOfAccountInvoiceItem setDueBalance(BigDecimal dueBalance) {
        this.dueBalance = dueBalance;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public StatementOfAccountInvoiceItem setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getItemNo() {
        return itemNo;
    }

    public StatementOfAccountInvoiceItem setItemNo(String itemNo) {
        this.itemNo = itemNo;
        return this;
    }

    public String getEventNo() {
        return eventNo;
    }

    public StatementOfAccountInvoiceItem setEventNo(String eventNo) {
        this.eventNo = eventNo;
        return this;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public StatementOfAccountInvoiceItem setMemberRef(String memberRef) {
        this.memberRef = memberRef;
        return this;
    }

    public LocalDate getTransDate() {
        return transDate;
    }

    public StatementOfAccountInvoiceItem setTransDate(LocalDate transDate) {
        this.transDate = transDate;
        return this;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public StatementOfAccountInvoiceItem setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public boolean isEventNoAccess() {
        return eventNoAccess;
    }

    public StatementOfAccountInvoiceItem setEventNoAccess(boolean eventNoAccess) {
        this.eventNoAccess = eventNoAccess;
        return this;
    }


}
