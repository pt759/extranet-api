package com.skuld.extranet.core.financial.domain.deposit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Declaration {

    private String vesselName;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate fromDate;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate toDate;
    private BigDecimal amount = new BigDecimal("0");

    private String currency;
    private String product;
    private String rebate;
    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    private BigDecimal taxAmount = new BigDecimal("0");
    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    private BigDecimal premiumInclTax = new BigDecimal("0");

    public static Declaration createFrom(CharterDeliveryReportEntity entity) {
        Declaration item = new Declaration();

        item.setVesselName(entity.getCurrentShipName())
                .setAmount(entity.getPremium())
                .setCurrency(entity.getCurrency())
                .setFromDate(entity.getEffStart())
                .setToDate(entity.getEffEnd())
                .setProduct(entity.getProduct())
                .setRebate(entity.getRebate())
                .setPremiumInclTax(entity.getPremiumInclTax())
                .setTaxAmount(entity.getTaxAmount())
        ;

        return item;

    }

    public String getVesselName() {
        return vesselName;
    }

    public Declaration setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public Declaration setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public Declaration setToDate(LocalDate toDate) {
        this.toDate = toDate;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getAmount() {
        return amount;
    }

    public Declaration setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public Declaration setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public Declaration setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getRebate() {
        return rebate;
    }

    public Declaration setRebate(String rebate) {
        this.rebate = rebate;
        return this;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public Declaration setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
        return this;
    }

    public BigDecimal getPremiumInclTax() {
        return premiumInclTax;
    }

    public Declaration setPremiumInclTax(BigDecimal premiumInclTax) {
        this.premiumInclTax = premiumInclTax;
        return this;
    }


}
