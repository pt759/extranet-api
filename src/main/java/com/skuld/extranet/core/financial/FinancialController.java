package com.skuld.extranet.core.financial;

import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalment;
import com.skuld.extranet.core.financial.domain.invoice.Invoice;
import com.skuld.extranet.core.financial.service.FinancialService;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import com.skuld.extranet.excel.ExcelListConverter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.skuld.extranet.global.RESTendPoints.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Financial")
@RestController
@RequestMapping(financial)
public class FinancialController extends SkuldController {
    final static Logger logger = LoggerFactory.getLogger(AzureAdUserService.class);

    @Autowired
    private FinancialService service;


    @Operation(summary = "Download Statement of Account Excel")
    @RequestMapping(
            path = financial_soa_excel_with_statCode_accountNo,
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity downloadStatementOfAccountToExcelBase64Container(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo) {
        Base64Container result = service.getStatementOfAccountBase64(statCode, accountNo);
        return responseEntityWrapper(result);
    }

    @Operation(summary = "Download Statement of Account Excel (File)")
    @RequestMapping(
            path = financial_soa_excel_file_with_statCode_accountNo
            , method = RequestMethod.GET)
    public void downloadStatementOfAccountToExcelFile(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo,
            HttpServletResponse response) {
        ExcelListConverter excel = service.getStatementOfAccountExcel(statCode, accountNo);
        excel.export(response);
    }


    @Operation(summary = "Download Statement of Account PDF (Base64)")
    @RequestMapping(
            path = financial_soa_pdf_with_statCode_id_itemType_Base64
            , method = RequestMethod.GET)
    public ResponseEntity downloadStatementOfAccountToPDFBase64(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo,
            @PathVariable(value = "itemType") String itemType,
            HttpServletResponse response) {
        Base64Container container = service.statementOfAccountAsPDF(statCode, accountNo, itemType, "Statement of Account.pdf");
        return responseEntityWrapper(container);
    }

    @Operation(summary = "Download Statement of Account PDF (File)")
    @RequestMapping(
            path = financial_soa_pdf_file_with_statCode_id_itemType
            , method = RequestMethod.GET)
    public ResponseEntity downloadStatementOfAccountToPDFFile(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo,
            @PathVariable(value = "itemType") String itemType,
            HttpServletResponse response) throws IOException {
        Object url = service.getStatementOfAccountUrl(statCode, accountNo, itemType);
        return buildResponseEntity(url);
    }

    @Operation(summary = "Download Instalment Report PDF (File)")
    @RequestMapping(
            path = financial_instalmentReport_pdf
            , method = RequestMethod.GET)
    public ResponseEntity downloadInstalmentReportToPDFFile(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo,
            HttpServletResponse response) throws IOException {
        Object url = service.getInstalmentReportUrl(statCode, accountNo);
        return buildResponseEntity(url);
    }

    @Operation(summary = "Download Instalment Report PDF (Base64)")
    @RequestMapping(
            path = financial_instalmentReport_pdf_Base64
            , method = RequestMethod.GET)
    public ResponseEntity downloadInstalmentReportPDFBase64(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo,
            HttpServletResponse response) {
        Base64Container container = service.InstalmentReportPDF(statCode, accountNo, "Instalment Report " + accountNo + ".pdf");
        return responseEntityWrapper(container);
    }

    @Operation(summary = "Overdue invoices for statcode")
    @RequestMapping(
            path = "/invoices/overdue/{statcode}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity openInvoices(
            @PathVariable(value = "statcode") String statCode) {
        List<Invoice> invoices = service.getOverdueInvoicesFor(statCode);
        return buildResponseEntity(invoices);
    }

    @Operation(summary = "Instalment overview")
    @RequestMapping(
            path = "/instalment/deposit/{statcode}/{accountno}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity openInvoices(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "accountno") String accountNo
    ) {
        List<DepositInstalment> instalments = service.getDepositInstalmentOverview(accountNo);
        return buildResponseEntity(instalments);
    }

}
