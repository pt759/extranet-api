package com.skuld.extranet.core.financial.domain;

public enum AccountStatus {
    open("Open"), active("Active"), passive("Passive"), notSet("notCalculated");

    private final String description;

    AccountStatus(String desc) {
        this.description = desc;
    }

    public static AccountStatus fromString(String code) {
        for (AccountStatus b : AccountStatus.values()) {
            if (b.description.equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return description;
    }
}
