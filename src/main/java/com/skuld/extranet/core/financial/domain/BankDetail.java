package com.skuld.extranet.core.financial.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BankDetail {
    private String bankCurrencyCode;
    private String accountNo;
    private String bankName;
    private String bankAccountNumber;
    private String ibanNumber;
    private String accountHolderName;
    private String bankCode;
    private String bankCodeType;
    private String bankAddress;
    private String bankIdNumber;

    public String getBankCurrencyCode() {
        return bankCurrencyCode;
    }

    public BankDetail setBankCurrencyCode(String bankCurrencyCode) {
        this.bankCurrencyCode = bankCurrencyCode;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public BankDetail setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public String getBankName() {
        return bankName;
    }

    public BankDetail setBankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public BankDetail setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
        return this;
    }

    public String getIbanNumber() {
        return ibanNumber;
    }

    public BankDetail setIbanNumber(String ibanNumber) {
        this.ibanNumber = ibanNumber;
        return this;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public BankDetail setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
        return this;
    }

    public String getBankCode() {
        return bankCode;
    }

    public BankDetail setBankCode(String bankCode) {
        this.bankCode = bankCode;
        return this;
    }

    public String getBankCodeType() {
        return bankCodeType;
    }

    public BankDetail setBankCodeType(String bankCodeType) {
        this.bankCodeType = bankCodeType;
        return this;
    }
    public String getBankAddress() {
        return bankAddress;
    }

    public BankDetail setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
        return this;
    }

    public String getBankIdNumber() {
        return bankIdNumber;
    }

    public BankDetail setBankIdNumber(String bankIdNumber) {
        this.bankIdNumber = bankIdNumber;
        return this;
    }

    public BankDetail(ResultSet r) throws SQLException {
        this.bankCurrencyCode = r.getString("bank_currency_code");
        this.accountNo = r.getString("bank_account_no_join");
        this.bankName = r.getString("bank_surname");
        this.bankAccountNumber = r.getString("bank_account_no");
        this.ibanNumber = r.getString("bank_iban_num");
        this.accountHolderName = r.getString("bank_pay_chan_holder");
        this.bankCode = r.getString("bank_code");
        this.bankCodeType = r.getString("bank_code_type");
        this.bankIdNumber = r.getString("bank_id_no");
    }

    public static class Mapper implements RowMapper<BankDetail> {
        public BankDetail map(ResultSet r, StatementContext ctx) throws SQLException {
            return new BankDetail(r);
        }
    }
}
