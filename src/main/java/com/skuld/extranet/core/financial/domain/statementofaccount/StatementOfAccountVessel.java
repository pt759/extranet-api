package com.skuld.extranet.core.financial.domain.statementofaccount;

import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class StatementOfAccountVessel {

    final static Logger logger = LoggerFactory.getLogger(StatementOfAccountVessel.class);

    private String vesselName;
    private List<StatementOfAccountTotal> invoiceTotals;
    private List<StatementOfAccountInvoice> invoices;


    public StatementOfAccountVessel() {
        invoices = new ArrayList<>();
        invoices = new ArrayList<>();
    }

    public String toString() {
        return vesselName + " (Invoices: " + invoices.size() + ")";
    }

    public void calculateInvoiceTotals() {
        invoiceTotals.stream().forEach(total -> {
            total.setBalance(getTotalForInvoices(StatementOfAccountInvoiceItem::getBalance, total.getCurrency()));
            total.setDueBalance(getTotalForInvoices(StatementOfAccountInvoiceItem::getDueBalance, total.getCurrency()));
            total.setOriginalAmount(getTotalForInvoices(StatementOfAccountInvoiceItem::getOriginalAmount, total.getCurrency()));
        });
    }

    private BigDecimal getTotalForInvoices(Function<StatementOfAccountInvoiceItem, BigDecimal> getterFunction, String currency) {
        return invoices.stream()
                .map(item ->
                        item.getTotalForInvoice(currency, getterFunction))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static StatementOfAccountVessel fromAccItem(AccountItemEntity item) {
        StatementOfAccountVessel soaRows = new StatementOfAccountVessel();
        soaRows.setVesselName(item.getVesselName());
//        soaRows.setCurrency(item.getCurrency());
        return soaRows;
    }

    public String getVesselName() {
        return vesselName == null ? "" : vesselName;
    }

    public StatementOfAccountVessel setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public List<StatementOfAccountInvoice> getInvoices() {
        return invoices;
    }

    public StatementOfAccountVessel setInvoices(List<StatementOfAccountInvoice> invoices) {
        this.invoices = invoices;
        return this;
    }

    public List<StatementOfAccountTotal> getInvoiceTotals() {
        return invoiceTotals;
    }

    public StatementOfAccountVessel setInvoiceTotals(List<StatementOfAccountTotal> invoiceTotals) {
        this.invoiceTotals = invoiceTotals;
        return this;
    }
}
