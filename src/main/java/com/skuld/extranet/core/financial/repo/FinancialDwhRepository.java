package com.skuld.extranet.core.financial.repo;

import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinancialDwhRepository extends DBDwhRepo {
    final static Logger logger = LoggerFactory.getLogger(FinancialDwhRepository.class);

    public List<CharterDeliveryReportEntity> getCharterDeclarations(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getCharterDeclarations(accountNo));
    }
}
