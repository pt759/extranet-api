package com.skuld.extranet.core.financial.domain.invoice;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class InvoiceItem {

    private String vesselName;
    private String description;
    private String itemNo;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDate;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate dueDate;

    private BigDecimal originalAmount;
    private BigDecimal balance;
    private BigDecimal dueBalance;



    public static List<InvoiceItem> fromAccItemDetails(String invoiceNo, List<AccountItemEntity> accItemDetails) {
        List<InvoiceItem> collect = accItemDetails.stream()
                .filter(i -> invoiceNo.equalsIgnoreCase(i.getInvoiceNoSafe()))
                .map(InvoiceItem::fromAccItemDetail)
                .collect(Collectors.toList());
        return collect;
    }


    public static List<InvoiceItem> fromAccItemDetails(List<AccountItemEntity> accItemDetails) {

        List<InvoiceItem> collect = accItemDetails.stream()
                .map(InvoiceItem::fromAccItemDetail)
                .collect(Collectors.toList());
        return collect;
    }

    public static InvoiceItem fromAccItemDetail(AccountItemEntity i) {
        return new InvoiceItem()
                .setItemNo(i.getItemNo())
                .setBalance(i.getBalance())
                .setDescription(i.getDescription())
                .setDueBalance(i.getDueBalance())
                .setDueDate(i.getDueDate())
                .setInvoiceDate(i.getInvoiceDate())
                .setOriginalAmount(i.getOriginalAmount())
                .setVesselName(i.getVesselName());
    }


    public String getVesselName() {
        return vesselName;
    }

    public InvoiceItem setVesselName(String vesselName) {
        this.vesselName = vesselName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public InvoiceItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getItemNo() {
        return itemNo;
    }

    public InvoiceItem setItemNo(String itemNo) {
        this.itemNo = itemNo;
        return this;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public InvoiceItem setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public InvoiceItem setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public InvoiceItem setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return balance;
    }

    public InvoiceItem setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDueBalance() {
        return dueBalance;
    }

    public InvoiceItem setDueBalance(BigDecimal dueBalance) {
        this.dueBalance = dueBalance;
        return this;
    }

    public boolean isItemOverdue() {
        return LocalDate.now().isAfter(getDueDate());
    }
}
