package com.skuld.extranet.core.financial.domain.entity;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.skuld.extranet.core.common.SkuldSqlHelper.nvl;

public class StatCodeAccountEntity {
    private String accountNo;
    private String statCode;
    private String policyHolderId;
    private String policyHolder;
    private String creditController;

    public StatCodeAccountEntity() {
    }

    public StatCodeAccountEntity(ResultSet r) throws SQLException {
        this.accountNo = r.getString("account_no");
        this.statCode = r.getString("stat_code");
        this.policyHolderId = r.getString("name_id");
        this.policyHolder = r.getString("policy_holder");
        this.creditController = nvl(r.getString("creditcontroller_pt_user_id"), "");
    }

    public static class Mapper implements RowMapper<StatCodeAccountEntity> {
        public StatCodeAccountEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new StatCodeAccountEntity(r);
        }
    }


    public String getAccountNo() {
        return accountNo;
    }

    public StatCodeAccountEntity setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public String getStatCode() {
        return statCode;
    }

    public StatCodeAccountEntity setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public String getCreditController() {
        return creditController;
    }

    public StatCodeAccountEntity setCreditController(String creditController) {
        this.creditController = creditController;
        return this;
    }

    public String getPolicyHolderId() {
        return policyHolderId;
    }

    public StatCodeAccountEntity setPolicyHolderId(String policyHolderId) {
        this.policyHolderId = policyHolderId;
        return this;
    }

    public String getPolicyHolder() {
        return policyHolder;
    }

    public StatCodeAccountEntity setPolicyHolder(String policyHolder) {
        this.policyHolder = policyHolder;
        return this;
    }

    @Override
    public String toString() {
        return "StatCodeAccountEntity{" +
                "accountNo='" + accountNo + '\'' +
                '}';
    }
}
