package com.skuld.extranet.core.financial.domain.deposit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.deposit.InstalmentType;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.global.RESTendPoints;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DepositInstalment {

    private String accountNo;
    private String invoiceNo;
    private InstalmentType instalmentType;

    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDate;

    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDueDate;

    private String currencyCode;

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    @JsonProperty("invoicedSum")
    public BigDecimal getInvoicedSum() {
        if (isOther()) {
            return BigDecimal.ZERO;
        } else {

            return getSumFor(DepositInstalmentItem::getInvoiced);
        }
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    @JsonProperty("paidSum")
    public BigDecimal getPaidSum() {

        if (isOther()) {
            return getBalanceSum();
        } else {
            return getSumFor(DepositInstalmentItem::getPaid);
        }

    }

    public boolean isOther() {
        return InstalmentType.other.equals(instalmentType);
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    @JsonProperty("balanceSum")
    public BigDecimal getBalanceSum() {
        return getSumFor(DepositInstalmentItem::getBalance);
    }

    @JsonProperty("invoiceUrl")
    public String getInvoiceUrl() {
        return StringUtils.isNotEmpty(invoiceNo) ? RESTendPoints.urlInvoiceAsBase64(invoiceNo) : "";
    }

    private List<DepositInstalmentItem> items;

    @Override
    public String toString() {
        return "DepositInstalmentOverview{" +
                "type='" + instalmentType + '\'' +
                "invoiceNo='" + invoiceNo + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", items size = " + items.size() +
                '}';
    }

    public DepositInstalment setItemsFilteredBy(InstallmentsEntity instalmentEntity, List<InstallmentsEntity> instalments) {
        // Filter Items
        List<DepositInstalmentItem> depositInstalmentItems = instalments.stream()
                .filter(instalment -> instalment.getInvoiceNo().equalsIgnoreCase(invoiceNo))
                .filter(x -> (x.filterItemByInstalmentType(instalmentEntity)))
                .map(instalment -> DepositInstalmentItem.from(instalment))
                .collect(Collectors.toList());

        this.setItems(depositInstalmentItems);
        return this;
    }

    public static DepositInstalment buildFrom(InstallmentsEntity entity) {
        return new DepositInstalment()
                .setInvoiceNo(entity.getInvoiceNo())
                .setInstalmentType(entity.getInstalmentType())
                .setAccountNo(entity.getAccountNo())
                .setCurrencyCode(entity.getCurrencyCode())
                .setInvoiceDate(entity.getInvoiceDate())
                .setInvoiceDueDate(entity.getInvoiceDueDate())
                ;
    }


    public String getInvoiceNo() {
        return invoiceNo;
    }

    public DepositInstalment setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public DepositInstalment setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }


    private BigDecimal getSumFor(Function<DepositInstalmentItem, BigDecimal> mapFunction) {
        if (CollectionUtils.isEmpty(items)) {
            return new BigDecimal("0");
        }

        List<DepositInstalmentItem> items = this.items;
        return items.stream().map(mapFunction).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public List<DepositInstalmentItem> getItems() {
        return items;
    }

    public DepositInstalment setItems(List<DepositInstalmentItem> items) {
        this.items = items;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public DepositInstalment setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public DepositInstalment setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    @JsonProperty("invoiceDueDateOriginal")
    public LocalDate getOriginalInvoiceDueDate() {
        return invoiceDueDate;
    }

    public LocalDate getInvoiceDueDate() {

        if (isDueDateHasChanged()) {
            Set<LocalDate> collect = items.stream()
                    .map(DepositInstalmentItem::getItemDueDate)
                    .collect(Collectors.toSet());

            if (collect.size() == 1) {
                return items.get(0).getItemDueDate();
            }
        }

        return invoiceDueDate;

    }

    public DepositInstalment setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    @JsonProperty("dueDateHasChanged")
    public boolean isDueDateHasChanged() {
        return items != null && invoiceDueDate != null && items.stream().anyMatch(i -> !invoiceDueDate.isEqual(i.getItemDueDate()));
    }

    public InstalmentType getInstalmentType() {
        return instalmentType;
    }

    public DepositInstalment setInstalmentType(InstalmentType instalmentType) {
        this.instalmentType = instalmentType;
        return this;
    }
}
