package com.skuld.extranet.core.financial.domain.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldSqlHelper;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

public class AccountItemEntity {
    private String accountNumber;
    private String itemNo;
    private String currency;
    private String vesselOrder;
    private String vesselName;
    private String description;
    private String invoiceNo;
    private String paymentMethod;
    private LocalDate invoiceDate;
    private LocalDate invoiceDueDate;
    private LocalDate dueDate;
    private LocalDate startDate;
    private LocalDate endDate;

    private String onHold;
    private BigDecimal originalAmount;
    private BigDecimal balance;
    private BigDecimal dueBalance;

    // Claims Only
    private String eventNo;
    private String memberRef;
    private LocalDate transDate;


    public static final String GroupOther = "Other";

    public boolean invoiceIsNullOrStartsNineSerie() {
        return StringUtils.isEmpty(invoiceNo);
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public AccountItemEntity setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public enum ItemType {
        All("A"),
        Due("D"),
        Unpaid("U");

        private final String value;

        ItemType(String a) {
            value = a;
        }

        @Override
        public String toString() {
            return value;
        }

        public String value() {
            return value;
        }

        public static ItemType fromString(String code) {
            for (ItemType b : ItemType.values()) {
                if (b.value().equalsIgnoreCase(code)) {
                    return b;
                }
            }
            return null;
        }

    }


    final static Logger logger = LoggerFactory.getLogger(AccountItemEntity.class);


    public String toString() {
        return groupByAccountNumberCurrencyVesselName();
    }

    public String groupByAccountNumberCurrencyVesselName() {
//        return accountNumber + "-" + currency + "-" + (vesselName != null ? vesselName : "");
        return accountNumber + "-" + (vesselName != null ? vesselName : "");
    }

    public AccountItemEntity() {
    }

    public AccountItemEntity(ResultSet r) throws SQLException {
        this.accountNumber = r.getString("i_account_no");
        this.currency = r.getString("i_currency_code");
        this.vesselOrder = r.getString("i_vessel_name_order");
        this.vesselName = getColumnStringOrNullValueString(r, "vessel_name", GroupOther);
        this.description = r.getString("description");
        this.invoiceNo = r.getString("payment_no");
        this.invoiceDate = SkuldSqlHelper.fromSqlDate(r.getDate("invoice_date"));
        this.invoiceDueDate = SkuldSqlHelper.fromSqlDate(r.getDate("invoice_due_date"));
        this.dueDate = SkuldSqlHelper.fromSqlDate(r.getDate("due_date"));
        this.startDate = SkuldSqlHelper.fromSqlDate(r.getDate("i_start_date"));
        this.endDate = SkuldSqlHelper.fromSqlDate(r.getDate("i_end_date"));
        this.paymentMethod = r.getString("i_payment_method");

        this.itemNo = r.getString("i_acc_item_no");
        this.onHold = r.getString("i_on_hold");
        this.originalAmount = r.getBigDecimal("i_currency_amt");
        this.dueBalance = r.getBigDecimal("i_due_balance");
        this.balance = r.getBigDecimal("i_currency_balance");

        // Claims only
        this.eventNo = r.getString("event_no");
        this.memberRef = r.getString("member_ref");
        this.transDate = SkuldSqlHelper.fromSqlDate(r.getDate("i_trans_date"));

        // Due Balance

    }

    private String getColumnStringOrNullValueString(ResultSet r, String columnName, String NullValueString) throws SQLException {
        String string = r.getString(columnName);
        return string != null ? string : NullValueString;
    }

    public String getItemNo() {
        return itemNo;
    }


    public static class Mapper implements RowMapper<AccountItemEntity> {
        public AccountItemEntity map(ResultSet r, StatementContext ctx) throws SQLException {
            return new AccountItemEntity(r);
        }
    }


    public String getAccountNumber() {
        return accountNumber;
    }

    public AccountItemEntity setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountItemEntity setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getVesselOrder() {
        return vesselOrder;
    }

    public AccountItemEntity setVesselOrder(String vesselOrder) {
        this.vesselOrder = vesselOrder;
        return this;
    }

    public String getVesselName() {
        return vesselName;
    }

    public AccountItemEntity setVesselName(String vesselName) {
        if (vesselName == null) {
            vesselName = "";
        } else {
            this.vesselName = vesselName;
        }

        return this;
    }

    public String getDescription() {
        return description;
    }

    public AccountItemEntity setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public String getInvoiceNoSafe() {
        return invoiceNo != null ? invoiceNo : "";
    }

    public boolean validInvoiceNumber(){
        return StringUtils.isNotEmpty(invoiceNo) && invoiceNo.startsWith("9");
    }

    public AccountItemEntity setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public AccountItemEntity setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public AccountItemEntity setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public AccountItemEntity setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public AccountItemEntity setEndDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public AccountItemEntity setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return balance;
    }

    public AccountItemEntity setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public AccountItemEntity setItemNo(String itemNo) {
        this.itemNo = itemNo;
        return this;
    }

    public String getEventNo() {
        return eventNo;
    }

    public AccountItemEntity setEventNo(String eventNo) {
        this.eventNo = eventNo;
        return this;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public AccountItemEntity setMemberRef(String memberRef) {
        this.memberRef = memberRef;
        return this;
    }

    public LocalDate getTransDate() {
        return transDate;
    }

    public AccountItemEntity setTransDate(LocalDate transDate) {
        this.transDate = transDate;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDueBalance() {
        return dueBalance;
    }

    public AccountItemEntity setDueBalance(BigDecimal dueBalance) {
        this.dueBalance = dueBalance;
        return this;
    }

    public static BigDecimal getTotalForAccountItems(AccountGroup accountType, Function<AccountItemEntity, BigDecimal> mapFunction, String currency, List<AccountItemEntity> accountItems) {
        BigDecimal total;
        switch(accountType) {
            case CLAIM:
                total = getTotalForAccountItemsWithoutOnHold(mapFunction, currency, accountItems);
                break;
            case PREMIUM:
                total = getTotalForAccountItemsWithOnHold(mapFunction, currency, accountItems);
                break;
            default:
                total = getTotalForAccountItemsWithoutOnHold(mapFunction, currency, accountItems);
                break;
        }
        return total;
    }

    public static BigDecimal getTotalForAccountItemsWithoutOnHold(Function<AccountItemEntity, BigDecimal> mapFunction, String currency, List<AccountItemEntity> accountItems) {
        BigDecimal total = accountItems.stream()
                .filter(item -> item.getCurrency().equalsIgnoreCase(currency) && !item.isOnHold())
                .map(mapFunction).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public static BigDecimal getTotalForAccountItemsWithOnHold(Function<AccountItemEntity, BigDecimal> mapFunction, String currency, List<AccountItemEntity> accountItems) {
        BigDecimal total = accountItems.stream()
                .filter(item -> item.getCurrency().equalsIgnoreCase(currency))
                .map(mapFunction).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public boolean isOnHold() {
        return onHold.equals("Y");
    }

    public boolean isNotOnHold(){
        return !isOnHold();
    }

    public String getOnHold() {
        return onHold;
    }

    public AccountItemEntity setOnHold(String onHold) {
        this.onHold = onHold;
        return this;
    }

    public AccountItemEntity setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }
}
