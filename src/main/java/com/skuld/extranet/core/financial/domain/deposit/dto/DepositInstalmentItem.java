package com.skuld.extranet.core.financial.domain.deposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class DepositInstalmentItem {

    private String invoiceNo;
    private String accItemNo;
    private String itemText;
    private BigDecimal invoiced;
    private BigDecimal paid;
    private BigDecimal balance;
    private BigDecimal originalAmount;

    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate itemDueDate;

    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate itemTransDate;

    @Override
    public String toString() {
        return "DepositInstalmentItem{" +
                "accItemNo='" + accItemNo + '\'' +
                ", itemText='" + itemText + '\'' +
                '}';
    }

    public static DepositInstalmentItem from(InstallmentsEntity p) {
        DepositInstalmentItem item = new DepositInstalmentItem()
                .setInvoiceNo(p.getInvoiceNo())
                .setAccItemNo(p.getAccItemNo())
                .setItemText(p.getItemText())
                .setInvoiced(p.getCurrencyAmount())
                .setOriginalAmount(p.getCurrencyAmount())
                .setPaid(p.getDepositPaid())
                .setBalance(p.getCurrencyBalance())
                .setItemDueDate(p.getItemDueDate())
                .setItemTransDate(p.getItemTransDate())
                ;

        return item;
    }


    public String getInvoiceNo() {
        return invoiceNo;
    }

    public DepositInstalmentItem setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    public String getAccItemNo() {
        return accItemNo;
    }

    public DepositInstalmentItem setAccItemNo(String accItemNo) {
        this.accItemNo = accItemNo;
        return this;
    }

    public String getItemText() {
        return itemText;
    }

    public DepositInstalmentItem setItemText(String itemText) {
        this.itemText = itemText;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getInvoiced() {
        return invoiced;
    }

    public DepositInstalmentItem setInvoiced(BigDecimal invoiced) {
        this.invoiced = invoiced;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getPaid() {
        return paid;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getOriginalAmount() {
        //TODO: Fix when fixing RESTAPI-639
        return originalAmount;
    }

    public DepositInstalmentItem setPaid(BigDecimal paid) {
        this.paid = paid;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return balance;
    }

    public DepositInstalmentItem setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public LocalDate getItemDueDate() {
        return itemDueDate;
    }

    public DepositInstalmentItem setItemDueDate(LocalDate itemDueDate) {
        this.itemDueDate = itemDueDate;
        return this;
    }

    public LocalDate getItemTransDate() {
        return itemTransDate;
    }

    public DepositInstalmentItem setItemTransDate(LocalDate itemTransDate) {
        this.itemTransDate = itemTransDate;
        return this;
    }

    public DepositInstalmentItem setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }
}
