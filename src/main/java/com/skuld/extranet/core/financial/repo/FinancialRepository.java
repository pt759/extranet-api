package com.skuld.extranet.core.financial.repo;


import com.skuld.common.domain.YesNo;
import com.skuld.extranet.core.common.repo.DBTiaRepo;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.BankDetail;
import com.skuld.extranet.core.financial.domain.deposit.entity.DepositStatusEntity;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import org.jdbi.v3.core.statement.OutParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinancialRepository extends DBTiaRepo {
    final static Logger logger = LoggerFactory.getLogger(FinancialRepository.class);


    public FinancialRepository() {
    }

    public List<AccountItemEntity> accountItems(String accountNo, ItemType itemType, YesNo deposit, YesNo onHold) {
        List<AccountItemEntity> list = getJdbi().withExtension(FinancialDao.class, dao -> dao.accountItems(accountNo, itemType.value(), deposit.value(), onHold.value()));
        return list;
    }

    public AccountEntity getAccount(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getAccount(accountNo));
    }

    public List<StatCodeAccountEntity> getStatCodeDepositAccounts(String statCode) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getStatCodeDepositAccounts(statCode));
    }

    public List<StatCodeAccountEntity> getStatCodePremiumAccounts(String statCode) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getStatCodePremiumAccounts(statCode));
    }

    public List<StatCodeAccountEntity> getStatCodeClaimAccounts(String statCode) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getStatCodeClaimAccounts(statCode));
    }

    public List<StatCodeAccountEntity> getDepositAccount(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getDepositAccount(accountNo));
    }

    public List<StatCodeAccountEntity> getPremiumAccount(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getPremiumAccount(accountNo));
    }

    public List<StatCodeAccountEntity> getClaimAccount(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getClaimAccount(accountNo));
    }

    public List<AccountSum> getAccountSum(String accountNumber, ItemType dueBalanceItemType, YesNo onHold, YesNo includeDeposit) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getAccountSum(accountNumber, dueBalanceItemType.value(), onHold.value(), includeDeposit.value()));
    }

    public List<BankDetail> getBankDetails(String accountNumber,String itemType, String deposit, String onHold) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getBankDetails(accountNumber,itemType, deposit, onHold));
    }

    public String getBankAddress(String bankIdNumber) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getBankAddress(bankIdNumber));
    }

    public String getPolicyHolder(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getPolicyHolder(accountNo));
    }

    public DepositStatusEntity getDepositAccountStatus(String accountNo) {
        OutParameters result = getJdbi().onDemand(FinancialDao.class).getDepositAccountStatus(accountNo);
        DepositStatusEntity status = DepositStatusEntity.mapper(result);
        return status;
    }

    public List<InstallmentsEntity> getInstalments(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getInstalments(accountNo));
    }

    public List<InstallmentsEntity> getInstalmentOthers(String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getInstalmentOthers(accountNo));
    }

    public String getPolicyFistStartDate(String statCode, String accountNo) {
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getStatCodePolicyFistStartDate(statCode, accountNo));
    }

    public List<String> getAccountProducts(String accountNo){
        return getJdbi().withExtension(FinancialDao.class, dao -> dao.getAccountProducts(accountNo));
    }
}
