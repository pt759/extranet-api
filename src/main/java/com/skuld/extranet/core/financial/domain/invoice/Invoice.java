package com.skuld.extranet.core.financial.domain.invoice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

import static com.skuld.extranet.global.RESTendPoints.urlInvoiceAsBase64;

public class Invoice {

    private String accountNo;
    private String policyHolderId;
    private String policyHolder;
    private String invoiceNo;
    private String currency;
    private InvoiceType invoiceType;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate dueDate;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDate;
    private BigDecimal originalAmount;
    private BigDecimal balance;
    private BigDecimal dueBalance;
    private boolean dueDateHasChanged;
    private List<InvoiceItem> invoiceItems;

    public Invoice() {
    }


    public static Invoice fromAccItem(AccountItemEntity item) {
        return new Invoice()
                .setInvoiceNo(item.getInvoiceNoSafe())
                .setAccountNo(item.getAccountNumber())
                .setDueDate(item.getInvoiceDueDate())
                .setInvoiceDate(item.getInvoiceDate())
                .setCurrency(item.getCurrency())
                ;
    }

    public static Invoice fromAccItem(AccountItemEntity item, List<AccountItemEntity> items) {
        return Invoice.fromAccItem(item)
                .setInvoiceItems(InvoiceItem.fromAccItemDetails(item.getInvoiceNoSafe(), items))
                ;
    }

    @JsonIgnore
    public String getInvoiceNoSafe() {
        return invoiceNo != null ? invoiceNo : "";
    }

    public String getPolicyHolder() {
        return policyHolder;
    }

    public Invoice setPolicyHolder(String policyHolder) {
        this.policyHolder = policyHolder;
        return this;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public Invoice setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    @JsonIgnore
    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    @JsonProperty("invoiceType")
    public String getInvoiceTypeAsString() {
        return invoiceType != null ? invoiceType.toString() : "";
    }

    public Invoice setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
        return this;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public Invoice setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    @JsonIgnore
    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getOriginalAmount() {
        return getSum(InvoiceItem::getOriginalAmount);
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return getSum(InvoiceItem::getBalance);
    }

    private BigDecimal getSum(Function<InvoiceItem, BigDecimal> mapFunction) {
        if (CollectionUtils.isEmpty(invoiceItems)) {
            return BigDecimal.ZERO;
        }
        return invoiceItems.stream().map(mapFunction).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDueBalance() {
        return getSum(InvoiceItem::getDueBalance);
    }

    @JsonProperty("invoiceUrl")
    public String getInvoiceUrl() {
        return urlInvoiceAsBase64(getInvoiceNo());
    }

    public String getCurrency() {
        return currency;
    }

    public Invoice setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getPolicyHolderId() {
        return policyHolderId;
    }

    public Invoice setPolicyHolderId(String policyHolderId) {
        this.policyHolderId = policyHolderId;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public Invoice setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public List<InvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }

    public Invoice setInvoiceItems(List<InvoiceItem> invoiceItems) {
        this.invoiceItems = invoiceItems;
        return this;
    }

    public boolean hasOverdueItem() {
        return invoiceItems.stream().anyMatch(i -> i.isItemOverdue());
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public Invoice setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public boolean isDueDateHasChanged() {
        return invoiceItems != null && dueDate != null && invoiceItems.stream().anyMatch(i -> !dueDate.isEqual(i.getDueDate()));
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "accountNo='" + accountNo + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                '}';
    }

    public Invoice setInvoiceType(AccountGroup accountGroup) {
        this.setInvoiceType(InvoiceType.from(accountGroup));
        return this;
    }
}
