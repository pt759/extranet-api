package com.skuld.extranet.core.financial.domain.statementofaccount;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;

import java.math.BigDecimal;

public class StatementOfAccountTotal {

    private BigDecimal originalAmount;
    private BigDecimal balance;
    private BigDecimal dueBalance;
    private String currency;

    public StatementOfAccountTotal() {
        this.originalAmount = BigDecimal.ZERO;
        this.balance = BigDecimal.ZERO;
        this.dueBalance = BigDecimal.ZERO;
        this.currency = "";
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public StatementOfAccountTotal setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getBalance() {
        return balance;
    }

    public StatementOfAccountTotal setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDueBalance() {
        return dueBalance;
    }

    public StatementOfAccountTotal setDueBalance(BigDecimal dueBalance) {
        this.dueBalance = dueBalance;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public StatementOfAccountTotal setCurrency(String currency) {
        this.currency = currency;
        return this;
    }
}
