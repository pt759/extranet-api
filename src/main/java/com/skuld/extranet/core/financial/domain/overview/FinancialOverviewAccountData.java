package com.skuld.extranet.core.financial.domain.overview;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.AccountStatus;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.skuld.extranet.core.financial.domain.AccountGroup.PREMIUM_DEPOSIT;

public class FinancialOverviewAccountData {

    final static Logger logger = LoggerFactory.getLogger(FinancialOverviewAccountData.class);

    @JsonProperty("accountNumber")
    private String accountNo;
    private String accountName;
    private String policyHolder;
    private String policyHolderId;
    private AccountGroup accountType;
    private AccountStatus accountStatus;

    private List<AccountSum> accountSum;
    private List<AccountSum> depositAccountSum;


    @JsonIgnore
    private LocalDate lastTransDate;

    public FinancialOverviewAccountData() {
        this.accountSum = new ArrayList<>();
        this.accountStatus = AccountStatus.notSet;
    }

    public String getAccountName() {
        return accountName;
    }

    public FinancialOverviewAccountData setAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public String getPolicyHolder() {
        return policyHolder;
    }

    public FinancialOverviewAccountData setPolicyHolder(String policyHolder) {
        this.policyHolder = policyHolder;
        return this;
    }


    public static FinancialOverviewAccountData from(AccountEntity account) {
        FinancialOverviewAccountData data = new FinancialOverviewAccountData();
        if (account != null) {
            data = new FinancialOverviewAccountData()
                    .setAccountNo(account.getAccountNo())
                    .setAccountName(account.getName())
                    .setPolicyHolder(null)
                    .setAccountType(account.getAccountGroup())
                    .setLastTransDate(account.getLastTransDate())
            ;

        }
        return data;
    }

    public AccountGroup getAccountType() {
        return accountType;
    }

    public FinancialOverviewAccountData setAccountType(AccountGroup accountType) {
        this.accountType = accountType;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public FinancialOverviewAccountData setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public FinancialOverviewAccountData setAccountSum(List<AccountSum> accountSum) {
        this.accountSum = accountSum;
        return this;
    }

    public List<AccountSum> getAccountSum() {
        return accountSum;
    }

    public boolean hasAccountBalance() {
        return (accountSum != null && accountSum.size() > 0);
    }

    public String getPolicyHolderId() {
        return policyHolderId;
    }

    public void setPolicyHolderId(String policyHolderId) {
        this.policyHolderId = policyHolderId;
    }

    @Override
    public String toString() {
        return "{" +
                "accountNo='" + accountNo + '\'' +
                "status='" + accountStatus + '\'' +
                "}";
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public boolean isStatus(AccountStatus status) {
        return status != null && status.equals(accountStatus);
    }

    public FinancialOverviewAccountData setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }

    public boolean isAccountActive(int lastTransDateLimit) {
        return !hasAccountBalance() && lastTransDate != null && lastTransDate.isAfter(LocalDate.now().minusDays(lastTransDateLimit));
    }

    public LocalDate getLastTransDate() {
        return lastTransDate;
    }

    public FinancialOverviewAccountData setLastTransDate(LocalDate lastTransDate) {
        this.lastTransDate = lastTransDate;
        return this;
    }

    public void calculateStatus(int daysSinceLastTransactionLimit, int daysSinceLastTransactionDepositLimit) {
        if (hasAccountBalance() || hasOpenItems()) {
            this.accountStatus = AccountStatus.open;
        } else if (isDeposit() && hasAccountBalanceForDeposit()) {
            this.accountStatus = AccountStatus.open;
        } else if (isDeposit() && isAccountActive(daysSinceLastTransactionDepositLimit)) {
            this.accountStatus = AccountStatus.active;
        } else if (!isDeposit() && isAccountActive(daysSinceLastTransactionLimit)) {
            this.accountStatus = AccountStatus.active;
        } else {
            this.accountStatus = AccountStatus.passive;
        }
    }

    public boolean hasOpenItems() {
        return accountSum != null && accountSum.stream().mapToInt(AccountSum::getItemRows).reduce(0, Integer::sum) > 0;
    }

    public boolean hasAccountBalanceForDeposit() {
        return (depositAccountSum != null && depositAccountSum.size() > 0);
    }


    public boolean isDeposit() {
        return PREMIUM_DEPOSIT.equals(accountType);
    }


    public List<AccountSum> getDepositAccountSum() {
        return depositAccountSum;
    }

    public FinancialOverviewAccountData setDepositAccountSum(List<AccountSum> depositAccountSum) {
        this.depositAccountSum = depositAccountSum;
        return this;
    }
}
