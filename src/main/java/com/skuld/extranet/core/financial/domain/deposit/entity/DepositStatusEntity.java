package com.skuld.extranet.core.financial.domain.deposit.entity;

import org.jdbi.v3.core.statement.OutParameters;

import java.math.BigDecimal;

public class DepositStatusEntity {

    private BigDecimal invoicedAmt;
    private BigDecimal paidAmt;
    private BigDecimal usedAmt;
    private String currency;

    public static DepositStatusEntity mapper(OutParameters r) {
        DepositStatusEntity entity =
                new DepositStatusEntity()
                        .setCurrency(r.getString("currency"))
                        .setInvoicedAmt(new BigDecimal(r.getDouble("invoicedAmt")))
                        .setUsedAmt(new BigDecimal(r.getDouble("usedAmt")))
                        .setPaidAmt(new BigDecimal(r.getDouble("paidAmt") * -1));
        return entity;
    }


    public BigDecimal getInvoicedAmt() {
        return invoicedAmt;
    }

    public DepositStatusEntity setInvoicedAmt(BigDecimal invoicedAmt) {
        this.invoicedAmt = invoicedAmt;
        return this;
    }

    public BigDecimal getPaidAmt() {
        return paidAmt;
    }

    public DepositStatusEntity setPaidAmt(BigDecimal paidAmt) {
        this.paidAmt = paidAmt;
        return this;
    }

    public BigDecimal getUsedAmt() {
        return usedAmt;
    }

    public DepositStatusEntity setUsedAmt(BigDecimal usedAmt) {
        this.usedAmt = usedAmt;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public DepositStatusEntity setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    @Override
    public String toString() {
        return "DepositStatusEntity{" +
                "invoicedAmt=" + invoicedAmt +
                ", paidAmt=" + paidAmt +
                ", usedAmt=" + usedAmt +
                ", currency='" + currency + '\'' +
                '}';
    }
}
