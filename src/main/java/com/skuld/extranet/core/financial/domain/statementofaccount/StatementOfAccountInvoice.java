package com.skuld.extranet.core.financial.domain.statementofaccount;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.global.SkuldPredicate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StatementOfAccountInvoice {

    private String invoiceNo;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate dueDate;
    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate invoiceDate;
    private boolean dueDateHasChanged;
    private List<StatementOfAccountInvoiceItem> details;




    public static List<StatementOfAccountInvoice> fromAccItems(List<AccountItemEntity> items) {
        List<StatementOfAccountInvoice> rowInvoices = items.stream()
                .filter(SkuldPredicate.distinctByKey(AccountItemEntity::getInvoiceNoSafe))
                .map(item -> StatementOfAccountInvoice.fromAccItem(item, items))
                .collect(Collectors.toList());

        return rowInvoices;

    }

    private static StatementOfAccountInvoice fromAccItem(AccountItemEntity item, List<AccountItemEntity> items) {
        return new StatementOfAccountInvoice()
                .setInvoiceNo(item.getInvoiceNo())
                .setDueDate(item.getInvoiceDueDate())
                .setInvoiceDate(item.getInvoiceDate())
                .setDetails(StatementOfAccountInvoiceItem.fromAccItems(item, items))
                ;
    }


    public String getInvoiceNo() {
        return invoiceNo;
    }

    public StatementOfAccountInvoice setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    public List<StatementOfAccountInvoiceItem> getDetails() {
        return details;
    }

    public StatementOfAccountInvoice setDetails(List<StatementOfAccountInvoiceItem> details) {
        this.details = details;
        return this;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public StatementOfAccountInvoice setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    @JsonIgnore
    public BigDecimal getTotalBalanceForInvoice(String currency) {
        return getTotalForInvoice(currency, StatementOfAccountInvoiceItem::getBalance);
    }

    @JsonIgnore
    public BigDecimal getTotalDueBalanceForInvoice(String currency) {
        return getTotalForInvoice(currency, StatementOfAccountInvoiceItem::getDueBalance);
    }

    @JsonIgnore
    public BigDecimal getTotalOriginalAmountForInvoice(String currency) {
        return getTotalForInvoice(currency, StatementOfAccountInvoiceItem::getOriginalAmount);
    }

    public BigDecimal getTotalForInvoice(String currency, Function<StatementOfAccountInvoiceItem, BigDecimal> mapFunction) {
        BigDecimal total = details.stream()
                .filter(item -> item.getCurrency().equalsIgnoreCase(currency))
                .map(mapFunction).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public StatementOfAccountInvoice setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public boolean isDueDateHasChanged() {
        return details!=null && dueDate !=null && details.stream().anyMatch(i -> !dueDate.isEqual(i.getDueDate()));
    }


}
