package com.skuld.extranet.core.financial.domain.deposit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldBigDecimalSerializer;
import com.skuld.extranet.core.financial.domain.deposit.entity.DepositStatusEntity;

import java.math.BigDecimal;

import static com.skuld.extranet.core.common.SkuldAmounts.nvl;
import static com.skuld.extranet.core.common.SkuldAmounts.round;


public class DepositStatus {

    private BigDecimal depositInvoiced;
    private BigDecimal depositPaid;
    private BigDecimal depositDeclared;
    private BigDecimal declaredSumAmountInclTax; // Should be equal to depositDeclared (comes from TIA)
    private BigDecimal declaredSumAmount;
    private BigDecimal declaredSumTax;

    private String currency;

    public static DepositStatus convertFrom(DepositStatusEntity entity) {
        return new DepositStatus()
                .setDepositInvoiced(entity.getInvoicedAmt())
                .setDepositPaid(entity.getPaidAmt())
                .setDepositDeclared(entity.getUsedAmt())
                .setDeclaredSumAmountInclTax(null)
                .setCurrency(entity.getCurrency());

    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDepositInvoiced() {
        return depositInvoiced;
    }

    public DepositStatus setDepositInvoiced(BigDecimal depositInvoiced) {
        this.depositInvoiced = depositInvoiced;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDepositPaid() {
        return depositPaid;
    }

    public DepositStatus setDepositPaid(BigDecimal depositPaid) {
        this.depositPaid = depositPaid;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDepositDeclared() {
        return depositDeclared;
    }

    public DepositStatus setDepositDeclared(BigDecimal depositDeclared) {
        this.depositDeclared = depositDeclared;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDeclaredSumAmountInclTax() {
        return declaredSumAmountInclTax;
    }

    public DepositStatus setDeclaredSumAmountInclTax(BigDecimal declaredSumAmountInclTax) {
        this.declaredSumAmountInclTax = declaredSumAmountInclTax;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public DepositStatus setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    @JsonProperty("depositBalance")
    public BigDecimal getDepositBalance() {

        if (depositInvoiced == null && depositPaid == null) {
            return null;
        }

        return round(nvl(depositInvoiced).add(nvl(depositPaid)), 2);
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDeclaredSumAmount() {
        return declaredSumAmount;
    }

    public DepositStatus setDeclaredSumAmount(BigDecimal declaredSumAmount) {
        this.declaredSumAmount = declaredSumAmount;
        return this;
    }

    @JsonSerialize(using = SkuldBigDecimalSerializer.class)
    public BigDecimal getDeclaredSumTax() {
        return declaredSumTax;
    }

    public DepositStatus setDeclaredSumTax(BigDecimal declaredSumTax) {
        this.declaredSumTax = declaredSumTax;
        return this;
    }


}
