package com.skuld.extranet.core.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class AzureApiAdGroupDTO {
    private String displayName;
    private String objectId;
    private String description;
}
