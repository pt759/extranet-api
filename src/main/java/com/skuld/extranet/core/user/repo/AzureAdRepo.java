package com.skuld.extranet.core.user.repo;

import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.dto.AzureApiAdGroupDTO;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

@Log4j2
@Data
@Service
public class AzureAdRepo {

    private RestTemplate restTemplate;
    @Value("${azureapi.url}")
    private String url;
    @Value("${mail.membermail}")
    private String memberMailUrl;
    @Value("${mail.create.user}")
    private String mailCreateUserUrl;

    @Autowired
    public AzureAdRepo(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public AzureAdGroup createGroup(String groupName, String description) {
        AzureAdGroup group = AzureAdGroup.builder().name(groupName).description(description).build();

        ResponseEntity<AzureAdGroup> response = this.restTemplate.postForEntity(url + "/group/create", group,
                AzureAdGroup.class);

        return response.getBody();
    }

    public String deleteGruop(String groupId) {
        this.restTemplate.delete(url + "/group/delete?id=" + groupId, HttpMethod.DELETE, null, String.class);
        return "200";
    }

    public AzureAdGroup getGroupById(String groupId) {
        AzureApiAdGroupDTO group = null;
        log.info("Checking if group " + groupId + " exists");

        try {
            group = this.restTemplate.getForObject(url + "/group/id/{id}", AzureApiAdGroupDTO.class, groupId);
        } catch (HttpServerErrorException e) {
            log.error("Error checking group " + e.getLocalizedMessage());
        }
        return AzureAdGroup.from(group);
    }

    public AzureAdGroup getGroupByName(String groupName) {
        AzureApiAdGroupDTO group = null;

        try {
            group = this.restTemplate.getForObject(url + "/group/name/{name}", AzureApiAdGroupDTO.class, groupName);
        } catch (HttpServerErrorException e) {
            log.error("Error checking group " + e.getLocalizedMessage());
        } catch (ResourceAccessException e) {
            log.error("Can not connect to REST sevice " + e.getLocalizedMessage());
        }
        return AzureAdGroup.from(group);
    }

    public List<AzureAdUser> getGroupMembers(String groupId) {
        AzureAdUser[] members = this.restTemplate.getForObject(url + "/group/" + groupId + "/members", AzureAdUser[].class);
        return Arrays.asList(members);
    }

    public String removeFromGroup(String userId, String groupId) {
        ResponseEntity<String> result = this.restTemplate.exchange(url + "/group/" + groupId + "/remove/" + userId,
                HttpMethod.DELETE, null, String.class);

        return result.getBody();
    }

    public AzureAdUser getUserByEmail(String email) {
        return this.restTemplate.getForObject(url + "/user/signinname/{name}", AzureAdUser.class, email);
    }

    public AzureAdUser getUserById(String userId) {
        return this.restTemplate.getForObject(url + "/user/id/{id}", AzureAdUser.class, userId);
    }

    public AzureAdUser createAzureAdUser(String firstName, String lastName, String email) {
        AzureAdUser azureAdUser = new AzureAdUser(firstName, lastName, email);

        ResponseEntity<AzureAdUser> newUser = this.restTemplate.postForEntity(url + "/user/create", azureAdUser, AzureAdUser.class);
        return newUser.getBody();
    }

    public void deleteUser(String userId) {
        this.restTemplate.delete(url + "/user/delete/" + userId, HttpMethod.DELETE, null, String.class);
    }

    public String addUserToGroup(String userId, String groupId) {
        ResponseEntity<String> status = this.restTemplate.postForEntity(url + "/group/" + groupId + "/add/" + userId,
                HttpMethod.POST, String.class);

        return status.getBody();
    }

    public List<AzureAdGroup> getUserAzureGroups(String userId) {
        AzureAdGroup[] groups = this.restTemplate.getForObject(url + "/usergroups/id/" + userId, AzureAdGroup[].class);
        return Arrays.asList(groups);
    }

    public String updateUser(AzureAdUser user) {
        ClientHttpRequestFactory requestFactory = getSimpleClientHttpRequestFactory();
        HttpEntity<AzureAdUser> requestUpdate = new HttpEntity<>(user, null);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        String updateUSerUrl = url + "/user/" + user.getUserId() + "/update?";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(updateUSerUrl)
                .queryParam("AccountEnabled", user.isAccountEnabled())
                .queryParam("DisplayName", user.getDisplayName())
                .queryParam("GivenName", user.getFirstName())
                .queryParam("Surname", user.getSurname());

        String result = this.restTemplate.patchForObject(builder.build().encode().toUri(), null, String.class);
        log.debug("requestUpdate: " + requestUpdate.toString());
        return result;
    }

    public ResponseEntity<String> sendCreateUserMailTo(String recipientEmail, String userMail) {
        String sendMailUrl = mailCreateUserUrl.replace("#usermail#", userMail).replace("#recipient#", recipientEmail);
        log.info("Sending mail to recipient: " + recipientEmail + " for created user ( " + userMail + ") using: " + url);
        ResponseEntity<String> status = sendInvitationMail(sendMailUrl);
        return status;
    }

    public ResponseEntity<String> sendMailToMember(String email) {
        log.info("Sending mail to user: " + email);
        ResponseEntity<String> status = sendInvitationMail(memberMailUrl + email);
        return status;
    }

    private ResponseEntity<String> sendInvitationMail(String sendMailUrl) {
        return restTemplate.postForEntity(sendMailUrl, HttpMethod.POST, String.class);
    }

    private ClientHttpRequestFactory getSimpleClientHttpRequestFactory() {
        final int timeout = 5;
        final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout * 1000);
        return clientHttpRequestFactory;
    }

    public AzureAdUser[] getUserFilter(String filter) {
        return this.restTemplate.getForObject(url + "/user/" + filter, AzureAdUser[].class, filter);
    }
}
