package com.skuld.extranet.core.user.repo;

import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.user.domain.BusinessUnitStatCodeGroup;
import com.skuld.extranet.core.user.domain.StatCode;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface StatCodeDao extends ClosableDao {
    @SqlQuery(" select * " +
            " from v_group_stat_codes gs " +
            " where lower(group_name) = lower(:groupName) " +
            " order by gs.name")
    @RegisterRowMapper(StatCode.Mapper.class)
    List<StatCode> groupStatCodes(String groupName);

    @SqlQuery("select distinct * from v_group_stat_codes gs where stat_code = :statCode")
    @RegisterRowMapper(StatCode.Mapper.class)
    StatCode getStatCode(String statCode);

    @SqlQuery("select group_name " +
            "  from v_group_stat_codes gs " +
            "       left join v_stat_code_business_groups bu on gs.group_name = bu.login_group " +
            " where bu.login_group is null and gs.stat_code = :statCode")
    @RegisterRowMapper(StatCode.Mapper.class)
    List<String> getStatCodesConnectedGroups(String statCode);

    @SqlQuery("select * from v_business_unit_statcode_group where dept_no = :departmentNumber")
    @RegisterRowMapper(StatCodeGroup.Mapper.class)
    List<StatCodeGroup> getBuStatCodeGroups(String departmentNumber);

    @SqlQuery("select * from v_stat_code_business_groups ")
    @RegisterRowMapper(BusinessUnitStatCodeGroup.Mapper.class)
    List<BusinessUnitStatCodeGroup> getBusinessUnitGroups();
}
