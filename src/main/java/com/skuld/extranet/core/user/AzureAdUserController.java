package com.skuld.extranet.core.user;

import com.skuld.common.SkuldController;
import com.skuld.common.dto.InitializationGroupDTO;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.domain.CreateUserEntity;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Azure ", description = "Azure user management")
@RestController
@RequestMapping("/aad")
public class AzureAdUserController extends SkuldController {
    final static Logger logger = LoggerFactory.getLogger(AzureAdUserController.class);

    @Autowired
    private AzureAdUserService azureAdUserService;

    @Deprecated
    @Operation(summary = "Get Azure User by email. Used by swagger only")
    @RequestMapping(path = "user/email/{email}/", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getUser(@PathVariable(value = "email") String email) {
        AzureAdUser user = azureAdUserService.getUserByEmail(email);
        return buildResponseEntity((user));
    }

    @Deprecated
    @Operation(summary = "Get Azure User by subject. Used by swagger only")
    @RequestMapping(path = "user/id/{id}/", method = RequestMethod.GET, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getUserById(@PathVariable(value = "id") String id) {
        return buildResponseEntity(azureAdUserService.getUserById(id));
    }

    @Deprecated
    @Operation(summary = "Create Azure User. Used by swagger only")
    @RequestMapping(path = "user/v0/", method = RequestMethod.POST, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity createUser(
            @RequestParam(value = "givenName", required = false) String firstName,
            @RequestParam(value = "surname", required = false) String lastName,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "groupId") String groupId
    ) {
        AzureAdUser user = azureAdUserService.createUser(firstName, lastName, email, groupId);
        return buildResponseEntity((user));
    }

    @Operation(summary = "Create Azure User ")
    @RequestMapping(path = "user/{azuregroupid}", method = RequestMethod.POST, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity createUser(
            @RequestBody CreateUserEntity createUser,
            @PathVariable(value = "azuregroupid") String azureGroupId
    ) {
        AzureAdUser user = azureAdUserService.createUserWithMailNotification(createUser);
        return buildResponseEntity(user);
    }

    @Operation(summary = "TIA create Azure user and group")
    @RequestMapping(path = "tiaUser/", method = RequestMethod.POST, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity tiaCreateUser(
            @RequestParam(value = "givenName") String firstName,
            @RequestParam(value = "surname") String lastName,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "groupName") String groupName,
            @RequestParam(value = "groupDescription") String groupDescription,
            @RequestParam(value = "requestUser") String requestUser) {
        CodeValue staus = azureAdUserService.createUserFromTia(firstName, lastName, email, groupName, groupDescription, requestUser);
        return buildResponseEntity((staus));
    }

    @Deprecated
    @Operation(summary = "Update Azure User")
    @RequestMapping(path = "user/update/", method = RequestMethod.PATCH, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity updateUser(@RequestParam(value = "id") String id,
                                     @RequestParam(value = "givenName", required = false) String firstName,
                                     @RequestParam(value = "surname", required = false) String surName,
                                     @RequestParam(value = "enabled", required = false) boolean isEnabled) {
        AzureAdUser user = new AzureAdUser();
        user.setUserId(id);
        user.setFirstName(firstName);
        user.setSurname(surName);
        user.setAccountEnabled(isEnabled);
        String result = azureAdUserService.updateUser(user);
        return buildResponseEntity((result));
    }

    @Operation(summary = "Initialize User for closed content")
    @GetMapping(path = "user/app/init/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity skuldInitUser() {
        List<InitializationGroupDTO> initializationGroupDTOS = azureAdUserService.initializeUser();

        return buildResponseEntity(initializationGroupDTOS);
    }

    @Operation(summary = "Accept agreement for using skuld.com")
    @PostMapping(path = "user/agreement/{azureUserId}",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity acceptAgreement(
            @PathVariable(value = "azureUserId") String azureUserId) {
        List<InitializationGroupDTO> initializationGroupDTOS = azureAdUserService.acceptAgreement(azureUserId);

        return buildResponseEntity(initializationGroupDTOS);
    }


    @Deprecated
    @Operation(summary = "Block sign-in for Azure user.")
    @GetMapping(path = "user/delete/{userId}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity deleteUser(@PathVariable(value = "userId") String userId) {
        CodeValue status = azureAdUserService.disableUser(userId);
        return buildResponseEntity(status);
    }

    @Operation(summary = "Get list of user by filter")
    @GetMapping(path = "user/{filter}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getUserFilter(@PathVariable(value = "filter") String filter) {
        List<AzureAdUser> users = azureAdUserService.getUserFilter(filter);
        return buildResponseEntity(users);
    }

    @Operation(summary = "Add employee to Skuld.com",
            description = "Searches for user (pt  only) in AD. Creates user and adds user to BU all groups in Azure.")
    @PostMapping(path = "user/employee/{email}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity addSkuldEmployee(@PathVariable(value = "email") String email) {
        CodeValue status = azureAdUserService.addSkuldEmployee(email);
        return buildResponseEntity(status);
    }

    @Operation(summary = "Remove employee from skuld.com",
            description = "Searches for user (pt  only) in AD, If found removes user from all Azure groups and deletes user.<BR>" +
                    "<H1>Users are permanently deleted automatically 30 days after they are deleted.<H1>")
    @DeleteMapping(path = "user/employee/{email}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity removeSkuldEmployee(@PathVariable(value = "email") String email) {
        CodeValue status = azureAdUserService.removeSkuldEmployee(email);
        return buildResponseEntity(status);
    }
}
