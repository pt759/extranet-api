package com.skuld.extranet.core.user;

import com.skuld.extranet.core.common.SimpleResponeBuilder;
import com.skuld.extranet.core.user.domain.TiaUser;
import com.skuld.extranet.core.user.service.TiaUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users/tia")
public class TiaUserController {

    @Autowired
    private TiaUserService userService;

    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity users() {
        List<TiaUser> tiaUsers = userService.tiaUsers();
        return SimpleResponeBuilder.buildResponseEntity(tiaUsers);
    }

    @RequestMapping(path = "handler", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity handler(@RequestParam Map<String, String> requestParams) {
        String userId = requestParams.get("handlerid");

        if (userId != null) {
            TiaUser handler = userService.getUser(userId);
            return SimpleResponeBuilder.buildResponseEntity(handler);
        }
        return null;
    }

    @RequestMapping(path = "/{ptuser}/",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity getUser(@PathVariable(value = "ptuser") String ptuser) {
        TiaUser tiaUser = userService.getUser(ptuser);
        return SimpleResponeBuilder.buildResponseEntity(tiaUser);
    }




}
