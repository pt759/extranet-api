package com.skuld.extranet.core.user.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TiaParty {

    private String idNumber;
    private String name;
    private String firstName;
    private String lastName;
    private String country;
    private String building;
    private String streetNumber;
    private String street;
    private String city;
    private String stateProvince;
    private String postCode;

    public String getIdNumber() {
        return idNumber;
    }

    public TiaParty setIdNumber(String idNumber) {
        this.idNumber = idNumber;
        return this;
    }

    public String getName() {
        return name;
    }

    public TiaParty setName(String name) {
        this.name = name;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public TiaParty setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public TiaParty setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public TiaParty setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getBuilding() {
        return building;
    }

    public TiaParty setBuilding(String building) {
        this.building = building;
        return this;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public TiaParty setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public TiaParty setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getCity() {
        return city;
    }

    public TiaParty setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public TiaParty setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public TiaParty setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }


    public TiaParty() {
    }

    public TiaParty(ResultSet r) throws SQLException {
        this.idNumber = r.getString("ID_NO");
        this.name = r.getString("NAME");
        this.firstName = r.getString("FORENAME");
        this.lastName = r.getString("SURNAME");
        this.country = r.getString("COUNTRY");
        this.building = r.getString("BUILDING_NAME");
        this.streetNumber = r.getString("STREET_NO");
        this.street = "STREET";
        this.city = r.getString("CITY");
    }
    public static class Mapper implements RowMapper<TiaParty> {
        public TiaParty map(ResultSet r, StatementContext ctx) throws SQLException {
            return new TiaParty(r);
        }
    }
}
