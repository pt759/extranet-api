package com.skuld.extranet.core.user.service;

import com.skuld.common.dto.InitializationGroupDTO;
import com.skuld.common.security.SecurityContextHelper;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.common.repo.DBTiaRepo;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.domain.BusinessUnitStatCodeGroup;
import com.skuld.extranet.core.user.domain.CreateUserEntity;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.repo.AzureAdRepo;
import com.skuld.extranet.ldap.LDAPService;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.profiles.service.ProfileClientService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.skuld.common.SkuldUtil.Sleep;
import static com.skuld.extranet.global.GlobalText.*;

@Log4j2
@Service
public class AzureAdUserService extends DBTiaRepo {


    private final AzureAdGroupService azureAdGroupService;
    private final AzureAdRepo azureAdRepo;
    private final StatCodeService statCodeService;
    private final LDAPService ldapService;
    private final ProfileClientService profileClientService;
    private final SecurityContextHelper securityContextHelper;

    public AzureAdUserService(AzureAdGroupService azureAdGroupService,
                              AzureAdRepo azureAdRepo,
                              StatCodeService statCodeService,
                              LDAPService ldapService,
                              ProfileClientService profileClientService) {
        this.azureAdGroupService = azureAdGroupService;
        this.azureAdRepo = azureAdRepo;
        this.statCodeService = statCodeService;
        this.ldapService = ldapService;
        this.profileClientService = profileClientService;
        this.securityContextHelper = new SecurityContextHelper();
    }

    public static boolean validEmail(String email) {
        EmailValidator validator = EmailValidator.getInstance();

        try {
            return validator.isValid(email);
        } catch (Exception e) {
            log.error("Error validating email: " + email);
            return false;
        }
    }

    public AzureAdUser getUserByEmail(String email) {
        AzureAdUser user = null;

        if (validEmail(email)) {
            user = azureAdRepo.getUserByEmail(email);
        }

        if (user == null) {
            log.error("Aad User not found");
        }
        return user;
    }

    public AzureAdUser getUserById(String userId) {
        AzureAdUser azureAdUser = azureAdRepo.getUserById(userId);

        if (Optional.ofNullable(azureAdUser).isPresent()) {
            log.debug("Getting user with id: " + userId);
        } else {
            log.error("Azure User with id " + userId + "not found");
        }

        return azureAdUser;
    }

    public CodeValue createUserFromTia(String firstName, String lastName, String email, String groupName,
                                       String groupDescription, String tiaUser) {

        AzureAdUser user = azureAdRepo.getUserByEmail(email);
        AzureAdGroup group = azureAdRepo.getGroupByName(groupName);
        CodeValue status = new CodeValue();
        String response;

        if (group == null) {
            log.debug("Creating group " + groupName + " " + groupDescription);
            group = azureAdRepo.createGroup(groupName, groupDescription);
            log.debug("Azure group created with id: " + group.getGroupId());
        }

        try {
            if (validEmail(email)) {
                if (user == null) {
                    log.debug("Creating user " + firstName + " " + lastName + " with email " + email);
                    user = azureAdRepo.createAzureAdUser(firstName, lastName, email);
                }

                if (user != null && group != null) {
                    log.debug("Adding user " + email + " to group " + group.getGroupId());
                    response = azureAdRepo.addUserToGroup(user.getUserId(), group.getGroupId());
                    status.setCode(OK_CODE);
                }

            }
        } catch (Exception e) {
            status.setCode("Error");
            status.setDescription("Error creating user " + email + " and adding to group: " + groupName);
            log.error("Error creating Aad user: " + e.getMessage());
            log.error(e.getMessage());
            status.setDescription(e.getMessage());
        }
        return status;
    }

    public AzureAdUser createUserWithMailNotification(CreateUserEntity createUserEntity) {
        AzureAdUser azureAdUser = azureAdRepo.getUserByEmail(createUserEntity.getEmail());
        CodeValue code;

        if (azureAdUser == null) {
            log.debug("Creating user: " + createUserEntity.getEmail() + " and adding to group " + createUserEntity.getGroupId());
            azureAdUser = azureAdRepo.createAzureAdUser(createUserEntity.getFirstName(), createUserEntity.getLastName(), createUserEntity.getEmail());
        }

        if (createUserEntity.getGroupId() != null) {
            try {
                code = azureAdGroupService.addUserToGroup(azureAdUser.getUserId(), createUserEntity.getGroupId());

                if (code.getCode() == OK_CODE) {
                    azureAdUser.setEmail(createUserEntity.getEmail());
                    sendMailWhenUserHasBeenCreated(createUserEntity);
                }
            } catch (Exception e) {
                log.error("Error creating AzureAdUser user: " + e.getMessage());
            }
        }
        return azureAdUser;
    }

    private void sendMailWhenUserHasBeenCreated(CreateUserEntity createUserEntity) {
        if (createUserEntity.isConfirmationMailToUser()) {
            azureAdRepo.sendMailToMember(createUserEntity.getEmail());
        }

        if (createUserEntity.hasRecipients()) {
            for (String recipientEmail : createUserEntity.getRecipients()) {
                azureAdRepo.sendCreateUserMailTo(recipientEmail, createUserEntity.getEmail());
            }
        }
    }

    public AzureAdUser createUser(String firstName, String lastName, String email, String groupId) {
        AzureAdUser user = null;
        if (validEmail(email)) {
            try {
                log.debug("Creating user " + firstName + " " + lastName + " with email " + email);
                user = azureAdRepo.createAzureAdUser(firstName, lastName, email);

                if (user != null) {
                    log.debug("Adding userid " + user.getUserId() + " to group " + groupId);
                    azureAdRepo.addUserToGroup(user.getUserId(), groupId);
                    azureAdRepo.sendMailToMember(email);
                }
            } catch (Exception e) {
                log.error("Error creating Aad user: " + e.getMessage());
            }
        }
        return user;
    }

    private CodeValue disableUser(String userId, boolean enabledStatus) {
        CodeValue codeValue;

        AzureAdUser user = azureAdRepo.getUserById(userId);
        List<AzureAdGroup> groups = azureAdRepo.getUserAzureGroups(userId);

        if (user != null && groups != null && groups.size() > 0) {
            log.info("Can not disable user " + user.getUserId() + ". Belongs to " + groups.size() + " group(s)");
            codeValue = new CodeValue(ERROR_CODE, USER_HAS_GROUP_MEMBERSHIPS);
        }

        if (user != null) {
            log.debug("Disable login for user: " + user.getUserId());
            user.setAccountEnabled(enabledStatus);
            String result = azureAdRepo.updateUser(user);
            codeValue = new CodeValue(OK_CODE, USER_SUCCESSFULLY_SUSPENDED);
        } else
            codeValue = new CodeValue(NOT_FOUND_CODE, USER_NOT_FOUND);
        return codeValue;
    }

    public CodeValue enableUser(String userId) {
        return disableUser(userId, true);
    }


    public CodeValue disableUser(String userId) {
        return disableUser(userId, false);
    }

    public String removeFromGroup(String userId, String groupId) {
        RequestEntity<String> result = null;
        AzureAdUser user = null;
        List<AzureAdGroup> groups = azureAdRepo.getUserAzureGroups(userId);

        log.debug("Delete user: " + userId);

        if (userId != null) {
            try {
                azureAdRepo.deleteUser(userId);
                return OK_CODE;
            } catch (Exception e) {
                return ERROR_CODE;
            }
        } else
            return ERROR_CODE;
    }

    public CodeValue deleteUserByEmail(String email) {
        CodeValue result;
        AzureAdUser user = azureAdRepo.getUserByEmail(email);

        if (user != null) {
            azureAdRepo.deleteUser(user.getUserId());
            result = new CodeValue(OK_CODE, OK_VALUE, USER_SUCCESSFULLY_DELETED);
        } else {
            result = new CodeValue(ERROR_CODE, ERROR_VALUE, USER_NOT_FOUND);
        }
        return result;
    }

    public CodeValue deleteUser(String userId) {
        CodeValue result;

        if (StringUtils.isNotEmpty(userId)) {
            azureAdRepo.deleteUser(userId);
            result = new CodeValue(OK_CODE, OK_VALUE, USER_SUCCESSFULLY_DELETED);
        } else {
            result = new CodeValue(ERROR_CODE, ERROR_VALUE, USER_NOT_FOUND);
        }
        return result;
    }

    public String updateUser(AzureAdUser user) {

        if (user.getUserId() == null && user.getEmail() != null) {
            user.setUserId(azureAdRepo.getUserByEmail(user.getEmail()).getUserId());
        }

        if (user.getUserId() != null) {
            return azureAdRepo.updateUser(user);
        } else
            return null;
    }

    public List<InitializationGroupDTO> initializeUser() {
        return profileClientService.initializeUser();
    }


    public List<StatCodeDTO> getStatCodesForGroup(String statCodeGroupLogin) {
        return statCodeService.getStatCodesForGroupCached(statCodeGroupLogin);
    }

    public List<AzureAdUser> getUserFilter(String filter) {
        log.debug("Searching for users starting with: " + filter);
        AzureAdUser[] users = azureAdRepo.getUserFilter(filter);
        return Arrays.asList(users);
    }

    public AzureAdUser addSkuldEmployee(LDAPUser ldapUser) {
        CreateUserEntity createUserEntity = new CreateUserEntity()
                .setFirstName(ldapUser.getGivenName())
                .setLastName(ldapUser.getSurName())
                .setEmail(ldapUser.getEmail());

        AzureAdUser azureAdUser = createUserWithMailNotification(createUserEntity);

        if (azureAdUser != null) {
            List<BusinessUnitStatCodeGroup> businessUnitLogin = statCodeService.getBusinessUnitLogin();

            businessUnitLogin.stream().forEach(businessUnit -> {
                AzureAdGroup azureAdGroup = azureAdGroupService.getGroupByName(businessUnit.getBusinessUnitLoginGroup());
                azureAdGroupService.addUserToGroup(azureAdUser.getUserId(), azureAdGroup.getGroupId());
                Sleep(1000);
            });
        }
        return azureAdUser;
    }

    public CodeValue addSkuldEmployee(String email) {
        CodeValue codeValue;

        try {
            addSkuldEmployee(ldapService.findFirstUserByMail(email));
            codeValue = new CodeValue(OK_CODE, OK_VALUE);
        } catch (NamingException e) {
            codeValue = new CodeValue(ERROR_CODE, ERROR_VALUE);
            log.error(e.getMessage());
        }
        return codeValue;
    }

    public CodeValue removeSkuldEmployee(String email) {
        CodeValue status;
        try {
            AzureAdUser azureAdUser = azureAdRepo.getUserByEmail(email);
            List<AzureAdGroup> azureAdGroups = azureAdRepo.getUserAzureGroups(azureAdUser.getUserId());

            for (AzureAdGroup azureAdGroup : azureAdGroups) {
                azureAdGroupService.removeUserFromGroup(azureAdUser.getUserId(), azureAdGroup.getGroupId());
            }
            deleteUser(azureAdUser.getUserId());
            status = new CodeValue(OK_CODE, USER_SUCCESSFULLY_DELETED);
        } catch (NullPointerException e) {
            log.error("Unable to remove user: " + email);
            status = new CodeValue(ERROR_CODE, USER_NOT_FOUND);
        }

        return status;
    }

    public List<InitializationGroupDTO> acceptAgreement(String userid) {
        return profileClientService.acceptAgreement(userid);
    }
}