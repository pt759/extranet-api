package com.skuld.extranet.core.user.domain;


import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.claim.domain.Views;
import com.skuld.extranet.core.tia.client.dto.TiaUserDTO;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class TiaUser {

    @JsonView({Views.ClaimDetail.class, UserView.SimpleUser.class})
    private String userName;
    private String tiaUserName;
    private String ptUserId; //ptXXX
    private String emailWithUserId; //Email

    @JsonView({Views.ClaimDetail.class, UserView.SimpleUser.class})
    private String email; //EmailFull

    private String broker;
    private List<String> stat_codes;
    private List<AzureAdGroup> group;


    @JsonView({Views.ClaimDetail.class, UserView.SimpleUser.class})
    private String title;

    @JsonView({Views.ClaimDetail.class, UserView.SimpleUser.class})
    private String phone;

    @JsonView({Views.ClaimDetail.class, UserView.SimpleUser.class})
    private String profilePicture;

    public static TiaUser from(TiaUserDTO tiaUserDTO) {
        return TiaUser.builder()
                .userName(tiaUserDTO.getUser_name())
                .tiaUserName(tiaUserDTO.getUser_id())
                .ptUserId(tiaUserDTO.getPt_user_id())
                .emailWithUserId(tiaUserDTO.getEmail_with_userid())
                .email(tiaUserDTO.getEmail())
                .phone(tiaUserDTO.getPhone_extent())
                .title(tiaUserDTO.getDescription())
                .build();
    }
}



