package com.skuld.extranet.core.user.repo;

import com.skuld.extranet.core.common.repo.DBTiaRepo;
import com.skuld.extranet.core.user.domain.BusinessUnitStatCodeGroup;
import com.skuld.extranet.core.user.domain.StatCode;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatCodeRepository extends DBTiaRepo {
    final static Logger logger = LoggerFactory.getLogger(StatCodeRepository.class);

    public List<StatCode> getStatCodeGroupStatCodes(String groupName) {
        return getJdbi().withExtension(StatCodeDao.class, dao -> dao.groupStatCodes(groupName));
    }

    public StatCode getStatCode(String statCode) {
        return getJdbi().withExtension(StatCodeDao.class, dao -> dao.getStatCode(statCode));
    }

    public List<StatCodeGroup> getBrokersStatCodeGroups() {
        return getJdbi().withExtension(UserAdminDao.class, dao -> dao.getBrokersStatCodeGroups());
    }

    public StatCodeGroup getStatCodeGroup(String login) {
        return getJdbi().withExtension(UserAdminDao.class, dao -> dao.getStatCodeGroup(login));
    }

    public List<StatCodeGroup> getBuStatCodeGroups(String deptNo) {
        return getJdbi().withExtension(StatCodeDao.class, dao -> dao.getBuStatCodeGroups(deptNo));
    }

    public List<String> getStatCodeConnectedGroups(String statCode) {
        return getJdbi().withExtension(StatCodeDao.class, dao -> dao.getStatCodesConnectedGroups(statCode));
    }

    public List<BusinessUnitStatCodeGroup> getBusinessUnitLoginGroups() {
        return getJdbi().withExtension(StatCodeDao.class, dao -> dao.getBusinessUnitGroups());
    }
}

