package com.skuld.extranet.core.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.domain.YesNo;
import com.skuld.extranet.core.user.domain.StatCode;
import com.skuld.extranet.core.user.domain.UserView;
import com.skuld.extranet.spa.pages.domain.ProductId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatCodeDTO {


    @JsonView({UserView.AzureGroupAdmin.class})
    private String statCode;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String statCodeName;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String statCodeManager;

    @JsonIgnore
    private boolean hasBroker;

    private YesNo declarations;
    private YesNo financials;
    private YesNo claimsOnline;
    private YesNo statistics;
    private YesNo vessels;
    private YesNo tasks;

    private String name;

    @JsonIgnore
    private List<ProductId> productId;

    public boolean hasBroker() {
        return this.hasBroker;
    }

    public static StatCodeDTO from(StatCode statCode) {
        StatCodeDTO dto = StatCodeDTO.builder()
                .statCode(statCode.getStatCode())
                .statCodeName(statCode.getStatCodeName())
                .statCodeManager(statCode.getStatCodeManager())
                .hasBroker(hasServiceAgentNo(statCode.getServiceAgentNo()))
                .declarations(statCode.getDeclarations())
                .financials(statCode.getFinancials())
                .productId(statCode.getProductId())
                .name(statCode.getStatCodeName())
                .build();
        return dto;
    }

    private static boolean hasServiceAgentNo(Long serviceAgentNo) {
        boolean hasBroker = false;
        hasBroker = Optional.ofNullable(serviceAgentNo).isPresent() && serviceAgentNo != 0;

        return hasBroker;
    }
}
