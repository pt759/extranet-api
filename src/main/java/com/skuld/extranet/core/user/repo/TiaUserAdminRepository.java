package com.skuld.extranet.core.user.repo;

import com.skuld.extranet.core.common.repo.DBTiaRepo;
import com.skuld.extranet.core.user.domain.BusinessUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TiaUserAdminRepository extends DBTiaRepo {

    final static Logger logger = LoggerFactory.getLogger(StatCodeRepository.class);

    public List<BusinessUnit> getBusinessUnits(){
        return getJdbi().withExtension(UserAdminDao.class, dao -> dao.getBusinessUnitList());
    }
}
