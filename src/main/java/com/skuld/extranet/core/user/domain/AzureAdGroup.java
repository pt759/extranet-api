package com.skuld.extranet.core.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.domain.YesNo;
import com.skuld.extranet.core.user.dto.AzureApiAdGroupDTO;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AzureAdGroup {

    @JsonView({UserView.AzureInitUser.class, UserView.AzureGroupAdmin.class})
    @JsonProperty("displayName")
    private String name;

    @JsonIgnore
    private String mailNickname;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String groupId;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String description;

    private List<StatCodeDTO> statCodes;

    @JsonView({UserView.AzureGroupAdmin.class})
    @JsonProperty("broker")
    private boolean isBroker;

    private AzureAdUser user;

    public AzureAdGroup() {
        this.statCodes = new ArrayList<>();
    }

    public static AzureAdGroup from(AzureApiAdGroupDTO group) {
        return AzureAdGroup.builder()
                .description(group.getDescription())
                .groupId(group.getObjectId())
                .name(group.getDisplayName())
                .build();
    }

    public boolean hasStatCode(String statCodeToAccess) {
        for (StatCodeDTO statCode : statCodes) {
            if (statCodeToAccess.equalsIgnoreCase(statCode.getStatCode())) {
                return true;
            }
        }
        return false;
    }
}
