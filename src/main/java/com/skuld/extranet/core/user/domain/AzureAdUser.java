package com.skuld.extranet.core.user.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

@Data
public class AzureAdUser {

    private String userName; // Email

    @JsonProperty("givenName")
    private String firstName;
    private String surname;

    @JsonView({UserView.AzureGroupAdmin.class,
            UserView.DeclarationContacts.class})

    private String displayName;

    @JsonView({UserView.AzureGroupAdmin.class,
            UserView.DeclarationContacts.class})
    private String email;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String userId;

    private String passWord;
    private String jobTitle;
    private String streetAddress;
    private String city;
    private String country;
    private String userType;
    private String department;
    private String street;
    private String postalCode;
    private String officePhone;
    private String authPhone;
    private String authAlternateEmail;
    private boolean userAdmin;

    @JsonProperty("TelephoneNumber")
    private String phone;

    @JsonProperty("AccountEnabled")
    private boolean accountEnabled;

    public AzureAdUser() {
        super();
    }

    public AzureAdUser(String username) {
        this.setUserName(getEmail());
    }

    public AzureAdUser(String firstName, String surname, String email) {
        this.firstName = firstName;
        this.surname = surname;
        this.displayName = firstName + " " + surname;
        this.email = email;
    }

    public String getId() {
        return userId;
    }

    public AzureAdUser setId(String userId) {
        this.userId = userId;
        return this;
    }
}