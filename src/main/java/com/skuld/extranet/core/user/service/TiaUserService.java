package com.skuld.extranet.core.user.service;


import com.skuld.extranet.core.tia.client.service.TiaCommonService;
import com.skuld.extranet.core.user.domain.TiaUser;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@NoArgsConstructor
@Service
public class TiaUserService {

    private TiaCommonService tiaCommonService;

    @Autowired
    public TiaUserService(TiaCommonService tiaCommonService) {
        this.tiaCommonService = tiaCommonService;
    }

    private final List<TiaUser> tiaUserCache = new ArrayList<>();

    public List<TiaUser> tiaUsers() {
        if (tiaUserCache.size() == 0) {
            log.info("Users are not in cache, reloading.");
            tiaCommonService.getTiaUsers().ifPresent(
                    items -> tiaUserCache.addAll(items.stream().map(TiaUser::from).collect(Collectors.toList()))
            );
        }

        return tiaUserCache;
    }

    public TiaUser getUser(String userName) {
        if (StringUtils.isEmpty(userName)) {
            return TiaUser.builder().build();
        }

        List<TiaUser> tiaUsers = tiaUsers();
        return matchUser(userName, tiaUsers);
    }

    public TiaUser matchUser(String userName, List<TiaUser> tiaUsers) {
        for (TiaUser tiaUser : tiaUsers) {
            if (matchOn(userName, tiaUser)) {
                return tiaUser;
            }
        }
        return null;
    }

    private boolean matchOn(String matchString, TiaUser tiaUser) {
        return matchString.equalsIgnoreCase(tiaUser.getPtUserId())
                || (matchString.equalsIgnoreCase(tiaUser.getTiaUserName()))
                || (matchString.equalsIgnoreCase(tiaUser.getEmailWithUserId())
                || (matchString.equalsIgnoreCase(tiaUser.getEmail())));
    }


}
