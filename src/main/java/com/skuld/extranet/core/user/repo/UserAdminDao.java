package com.skuld.extranet.core.user.repo;

import com.skuld.extranet.core.common.ClosableDao;
import com.skuld.extranet.core.user.domain.BusinessUnit;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface UserAdminDao extends ClosableDao {

    @SqlQuery("select * from v_business_units")
    @RegisterRowMapper(BusinessUnit.Mapper.class)
    List<BusinessUnit> getBusinessUnitList();

    @SqlQuery("select * " +
            " from v_broker_statcode_groups " +
            " where login = :login " +
            " union all " +
            " select login, group_name " +
            " from v_business_unit_statcode_group " +
            " where login = :login")
    @RegisterRowMapper(StatCodeGroup.Mapper.class)
    StatCodeGroup getStatCodeGroup(@Bind("login") String loginGroup);

    @SqlQuery("select * from v_broker_statcode_groups")
    @RegisterRowMapper(StatCodeGroup.Mapper.class)
    List<StatCodeGroup> getBrokersStatCodeGroups();

}
