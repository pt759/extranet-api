package com.skuld.extranet.core.user.service;

import com.skuld.extranet.core.user.domain.BusinessUnit;
import com.skuld.extranet.core.user.domain.BusinessUnitStatCodeGroup;
import com.skuld.extranet.core.user.domain.StatCode;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.repo.StatCodeRepository;
import com.skuld.extranet.core.user.repo.TiaUserAdminRepository;
import com.skuld.extranet.reference.entity.AppReferenceRepo;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.CachingConfig.STAT_CODES_FOR_GROUP;
import static com.skuld.extranet.CachingConfig.STAT_CODE_BUSINESS_GROUPS;
import static com.skuld.extranet.global.GlobalText.BROKER_BUSINESS_UNIT_ID;
import static com.skuld.extranet.global.GlobalText.BROKER_BUSINESS_UNIT_NAME;
import static java.util.Objects.isNull;

@Log4j2
@NoArgsConstructor
@Component
@Service
public class StatCodeService {

    private StatCodeRepository statCodeRepository;
    private TiaUserAdminRepository tiaUserAdminRepository;

    @Autowired
    public StatCodeService(StatCodeRepository statCodeRepository,
                           TiaUserAdminRepository tiaUserAdminRepository,
                           AppReferenceRepo appReferenceRepo) {
        this.statCodeRepository = statCodeRepository;
        this.tiaUserAdminRepository = tiaUserAdminRepository;
    }

    public StatCode getStatCode(String statcode) {
        return statCodeRepository.getStatCode(statcode);
    }

    @Cacheable(STAT_CODES_FOR_GROUP)
    public List<StatCodeDTO> getStatCodesForGroupCached(String statCodeGroupLogin) {
        List<StatCode> statCodes = statCodeRepository.getStatCodeGroupStatCodes(statCodeGroupLogin);

        List<StatCodeDTO> filteredStatCodes = statCodes
                .stream()
                .filter(statCode -> !isNull(statCode.getStatCode()))
                .map(StatCodeDTO::from)
                .collect(Collectors.toList());
        return filteredStatCodes;
    }

    public List<StatCodeGroup> getBrokerStatCodesGroupsCached() {
        return statCodeRepository.getBrokersStatCodeGroups();
    }

    public List<BusinessUnit> getBusinessUnits() {
        List<BusinessUnit> businessUnits = tiaUserAdminRepository.getBusinessUnits();
        BusinessUnit brokers = new BusinessUnit(BROKER_BUSINESS_UNIT_NAME, BROKER_BUSINESS_UNIT_ID);
        businessUnits.add(brokers);
        return businessUnits;
    }

    public List<StatCodeGroup> getBusinessUnitStatCodeGroups(String deptNo) {

        return statCodeRepository.getBuStatCodeGroups(deptNo);
    }

    public List<StatCodeGroup> getBrokersStatCodeGroups() {
        return statCodeRepository.getBrokersStatCodeGroups();
    }

    public StatCodeGroup getStatCodeGroup(String login) {

        return statCodeRepository.getStatCodeGroup(login);
    }

    public List<String> getStatCodeConnectedGroups(String statCode) {

        return statCodeRepository.getStatCodeConnectedGroups(statCode);
    }

    public boolean isDirectMember(List<StatCodeDTO> statCodes) {
        return statCodes.stream().map(StatCodeDTO::hasBroker).allMatch(broker -> broker == false);
    }

    public boolean isBroker(String groupName) {
        boolean broker = getBrokerStatCodesGroupsCached()
                .stream()
                .anyMatch(x -> groupName.equalsIgnoreCase(x.getLoginId()));
        return broker;
    }

    public boolean isSkuldBusinessUnit(String groupName) {
        return getBusinessUnitLogin()
                .stream()
                .anyMatch(x -> groupName.equalsIgnoreCase(x.getBusinessUnitLoginGroup()));
    }

    @Cacheable(STAT_CODE_BUSINESS_GROUPS)
    public List<BusinessUnitStatCodeGroup> getBusinessUnitLogin() {
        return statCodeRepository.getBusinessUnitLoginGroups();
    }

    public List<StatCodeGroup> getAllStatCodesGroupsCached() {
        List<StatCodeGroup> allStatCodeGroups = new ArrayList<>();
        List<BusinessUnit> businessUnits = getBusinessUnits();

        businessUnits.stream().forEach(businessUnit -> {
            allStatCodeGroups.addAll(getBusinessUnitStatCodeGroups(businessUnit.getDepartmentNumber()));
        });

        allStatCodeGroups.addAll(getBrokerStatCodesGroupsCached());
        return allStatCodeGroups;
    }

    public StatCodeService setStatCodeRepository(StatCodeRepository statCodeRepository) {
        this.statCodeRepository = statCodeRepository;
        return this;
    }

    public StatCodeService setTiaUserAdminRepository(TiaUserAdminRepository tiaUserAdminRepository) {
        this.tiaUserAdminRepository = tiaUserAdminRepository;
        return this;
    }

    public void initBusinessUnitStatCodeCache() {
        log.debug("Initiating stat code cache for BusinessUnits");
        List<BusinessUnitStatCodeGroup> businessUnitLogin = getBusinessUnitLogin();
        businessUnitLogin.parallelStream().forEach(businessUnit -> {
            getStatCodesForGroupCached(businessUnit.getBusinessUnitLoginGroup());
        });
    }
}
