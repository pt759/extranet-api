package com.skuld.extranet.core.user.domain;

import lombok.Data;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;


@Data
public class BusinessUnitStatCodeGroup {
    private String businessUnitGroupName;
    private String businessUnitDepartmentNo;
    private String businessUnitLoginGroup;

    public BusinessUnitStatCodeGroup(ResultSet r) throws SQLException {
        this.businessUnitGroupName = r.getString("dept_name");
        this.businessUnitDepartmentNo = r.getString("dept_no");
        this.businessUnitLoginGroup = r.getString("login_group");
    }

    public static class Mapper implements RowMapper<BusinessUnitStatCodeGroup> {
        public BusinessUnitStatCodeGroup map(ResultSet r, StatementContext ctx) throws SQLException {
            return new BusinessUnitStatCodeGroup(r);
        }
    }
}
