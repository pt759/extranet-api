package com.skuld.extranet.core.user.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.skuld.extranet.core.tia.client.dto.TiaBusinessUnitStatCodeGroupDTO;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserAdminBusinessUnitDTO {

    @JsonProperty("name")
    private String departmentName;

    @JsonProperty("deptNo")
    private String departmentNumber;

    public static UserAdminBusinessUnitDTO from(TiaBusinessUnitStatCodeGroupDTO tiaBusinessUnitStatCodeGroupDTO) {
        return UserAdminBusinessUnitDTO.builder()
                .departmentName(tiaBusinessUnitStatCodeGroupDTO.getDept_name())
                .departmentNumber(tiaBusinessUnitStatCodeGroupDTO.getDept_no())
                .build();
    }
}
