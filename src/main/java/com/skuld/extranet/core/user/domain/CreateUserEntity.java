package com.skuld.extranet.core.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class CreateUserEntity {

    private String firstName;
    private String lastName;
    private String email;
    private String groupId;
    private boolean confirmationMailToUser;
    private List<String> recipients;

    public CreateUserEntity() {
        this.recipients = new ArrayList<>();
        this.confirmationMailToUser = false;
    }

    public String getFirstName() {
        return firstName;
    }

    public CreateUserEntity setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public CreateUserEntity setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public CreateUserEntity setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public boolean isConfirmationMailToUser() {
        return confirmationMailToUser;
    }

    public CreateUserEntity setConfirmationMailToUser(boolean confirmationMailToUser) {
        this.confirmationMailToUser = confirmationMailToUser;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CreateUserEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public CreateUserEntity setRecipients(List<String> recipients) {
        this.recipients = recipients;
        return this;
    }

    @JsonIgnore
    public boolean hasRecipients() {
        return recipients!=null && recipients.size()>0;
    }

    public String toString(){
        return firstName + " " + lastName + " (" + email + ") - " + confirmationMailToUser;
    }

}
