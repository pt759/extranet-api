package com.skuld.extranet.core.user.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BusinessUnit {

	@JsonView({UserView.AzureGroupAdmin.class})
	@JsonProperty("name")
	private String departmentName;

	@JsonView({UserView.AzureGroupAdmin.class})
	@JsonProperty("deptNo")
	private String departmentNumber;

	public BusinessUnit(ResultSet r) throws SQLException {
		this.departmentName = r.getString("dept_name");
		this.departmentNumber = r.getString("dept_no");
	}

	public static class Mapper implements RowMapper<BusinessUnit> {
        public BusinessUnit map(ResultSet r, StatementContext ctx) throws SQLException {
            return new BusinessUnit(r);
        }
    }
}
