package com.skuld.extranet.core.user.domain;

public interface UserView {

    interface SimpleUser {
    }
    
    interface AzureInitUser {
    }

    interface AzureGroupAdmin {

    }

    interface DeclarationContacts {

    }
}
