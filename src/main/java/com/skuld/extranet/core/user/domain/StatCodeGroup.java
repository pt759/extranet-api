package com.skuld.extranet.core.user.domain;

import com.fasterxml.jackson.annotation.JsonView;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StatCodeGroup {

	@JsonView({ UserView.AzureGroupAdmin.class })
	private String loginId;

	@JsonView({ UserView.AzureGroupAdmin.class })
	private String groupName;
	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginName() {
		return loginId;
	}

	public void setLoginName(String loginName) {
		this.loginId = loginName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public StatCodeGroup() {
	}

	public StatCodeGroup(ResultSet r) throws SQLException {
		this.loginId = r.getString("login");
		this.groupName = r.getString("group_name");
	}

	public static class Mapper implements RowMapper<StatCodeGroup> {
		public StatCodeGroup map(ResultSet r, StatementContext ctx) throws SQLException {
			return new StatCodeGroup(r);
		}
	}
}
