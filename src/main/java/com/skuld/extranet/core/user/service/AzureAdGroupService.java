package com.skuld.extranet.core.user.service;

import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.repo.AzureAdRepo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.skuld.extranet.global.GlobalText.*;

@Service
public class AzureAdGroupService {

    final static Logger logger = LoggerFactory.getLogger(AzureAdUserService.class);

    @Autowired
    private AzureAdRepo repo;

    @Autowired
    private StatCodeService statCodeService;

    public List<AzureAdUser> getMembersByGroupName(String groupName) {

        AzureAdGroup azureAdGroup = repo.getGroupByName(groupName);

        if (azureAdGroup != null) {
            return getGroupMembersById(azureAdGroup.getGroupId(), false);
        }
        return null;
    }

    public List<AzureAdUser> getGroupMembersById(String groupId) {
        return getGroupMembersById(groupId, false);
    }

    public List<AzureAdUser> getGroupMemberWithSkuldsById(String groupId) {
        return getGroupMembersById(groupId, true);
    }

    public List<AzureAdUser> getGroupMembersById(String groupId, boolean showSkuld) {
        List<AzureAdUser> members = repo.getGroupMembers(groupId);

        if (Optional.ofNullable(members).isPresent()) {
            if (showSkuld) {
                return members.stream().collect(Collectors.toList());
            } else {
                return members.stream().filter(u -> !u.getEmail().contains("@skuld.com") && !StringUtils.isEmpty(u.getEmail()))
                        .collect(Collectors.toList());
            }
        }

        return members;
    }

//    public List<AzureAdUser> getGroupMembersById(String groupId, boolean showSkuld) {
//        List<AzureAdUser> members = new ArrayList<>();
//            if (groupId != null) {
//                members = repo.getGroupMembers(groupId);
//
//                if (Optional.ofNullable(members).isPresent()) {
//                    if (showSkuld) {
//                        return members.stream().collect(Collectors.toList());
//                    } else {
//                        return members.stream().filter(u -> !u.getEmail().contains("@skuld.com" ) &&  !StringUtils.isEmpty(u.getEmail()))
//                                .collect(Collectors.toList());
//                    }
//                }
//            }
//        return members;
//    }


    public AzureAdGroup createGroup(String groupName, String description) {
        AzureAdGroup newGroup = null;

        if (repo.getGroupByName(groupName) == null) {
            logger.info("Creating group " + groupName + " " + description);
            newGroup = repo.createGroup(groupName, description);

            if (newGroup == null) {
                logger.error("ERROR: Group " + groupName + " exists");
                return null;
            }
        } else {
            logger.error("Group " + groupName + " exists.");
        }
        return newGroup;
    }

    public CodeValue deleteGroupByName(String groupName) {
        CodeValue codeValue;
        AzureAdGroup azureAdGroup = repo.getGroupByName(groupName);

        if (azureAdGroup != null)
            codeValue = deleteGroup(azureAdGroup.getGroupId());
        else
            codeValue = new CodeValue(NOT_FOUND_CODE, GROUP_NOT_FOUND);

        return codeValue;
    }

    public CodeValue deleteGroup(String groupId) {
        CodeValue status;

        try {
            List<AzureAdUser> members = repo.getGroupMembers(groupId);

            if (members != null && members.size() == 0) {
                repo.deleteGruop(groupId);
                status = new CodeValue(OK_CODE, OK_VALUE, GROUP_DELETED);
            } else {
                logger.error("Can not delte group. " + groupId + "has " + members.size() + " members");
                status = new CodeValue(ERROR_DELETING_GROUP, ERROR_CODE, CAN_NOT_DELETE_GROUP_HAS_MEMBERS);
            }
        } catch (Exception e) {
            logger.error(ERROR_DELETING_GROUP + " " + e.getMessage());
            status = new CodeValue(ERROR_CODE, ERROR_VALUE, ERROR_DELETING_GROUP);
        }
        return status;
    }

    public CodeValue removeUserFromGroup(String userId, String groupId) {
        String result = null;
        CodeValue status = new CodeValue();

        AzureAdUser user = repo.getUserById(userId);
        AzureAdGroup group = repo.getGroupById(groupId);

        logger.debug("Removing user " + userId + " from group " + groupId);

        if (user != null & group != null) {
            result = repo.removeFromGroup(user.getUserId(), group.getGroupId());

            switch (result) {
                case AZURE_OK_CODE:
                    status = new CodeValue(OK_CODE, OK_VALUE, USER_REMOVED_FROM_GROUP);
                    break;
                case NOT_FOUND_CODE:
                    status = new CodeValue(NOT_FOUND_CODE, NOT_FOUND_VALUE, USER_NOT_FOUND);
                    break;

                default:
                    break;
            }
        } else {
            logger.error("UserId or groupId missing");
            status.setDescription(USER_NOT_FOUND);
            status.setCode(ERROR_CODE);
        }

        return status;
    }

    public CodeValue removeUserFromGroupByName(String email, String groupName) {
        if (email != null && groupName != null) {
            return removeUserFromGroup(email, repo.getGroupByName(groupName).getGroupId());
        }
        return new CodeValue(ERROR_CODE, ERROR_CODE, NOT_FOUND_VALUE);
    }

    public AzureAdGroup getGroupByName(String groupName) {
        logger.debug("Getting id for group: " + groupName);

        AzureAdGroup group = repo.getGroupByName(groupName);
        return group;
    }

    public AzureAdGroup getGroupbyId(String groupId) {

        AzureAdGroup group = repo.getGroupById(groupId);

        if (group == null) {
            logger.info("Group does not exist ");
        }

        return group;
    }

    public CodeValue addUserToGroup(String userId, String groupId) {
        String result;
        CodeValue status = new CodeValue();
        List<AzureAdGroup> azureAdGroupList = getUserAzureGroups(userId);
        boolean isMemberOfGroup = azureAdGroupList.stream().anyMatch(group -> group.getGroupId().equalsIgnoreCase(groupId));

        if (!isMemberOfGroup) {
            result = repo.addUserToGroup(userId, groupId);

            switch (result) {
                case AZURE_OK_CODE:
                    status = new CodeValue(OK_CODE, OK_VALUE, ADD_TO_GROUP_SUCCESS_VALUE);
                    break;
                default:
                    status = new CodeValue(ERROR_CODE, AZURE_ERROR, COULD_NOT_ADD_USER_TO_GROUP);
                    break;
            }
        } else {
            status = new CodeValue(ERROR_CODE, AZURE_ERROR, COULD_NOT_ADD_USER_TO_GROUP);
        }

        return status;
    }

    public List<AzureAdGroup> getUserAzureGroups(String userId) {
        return repo.getUserAzureGroups(userId);
    }

    public AzureAdGroupService setRepo(AzureAdRepo repo) {
        this.repo = repo;
        return this;
    }

    public AzureAdGroupService setStatCodeService(StatCodeService statCodeService) {
        this.statCodeService = statCodeService;
        return this;
    }
}
