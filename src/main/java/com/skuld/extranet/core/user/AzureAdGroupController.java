package com.skuld.extranet.core.user;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.UserView;
import com.skuld.extranet.core.user.service.AzureAdGroupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Log4j2
@Tag(name = "Azure ", description = "Azure group management")
@RestController
@RequestMapping("/aad/group")
public class AzureAdGroupController extends SkuldController {

    private final AzureAdGroupService groupService;

    public AzureAdGroupController(AzureAdGroupService groupService) {
        this.groupService = groupService;
    }

    @Operation(summary = "Get Group Members by Azure Ad group id")
    @RequestMapping(
            path = "members/{azuregroupid}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getGroupMembersById(
            @PathVariable(value = "azuregroupid") String azureGroupId
    ) {
        return buildResponseEntity(groupService.getGroupMembersById(azureGroupId));
    }

    @JsonView({UserView.DeclarationContacts.class})
    @Operation(summary = "Get Group Members by group name")
    @RequestMapping(
            path = "contacts/{azuregroupname}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getGroupMembersByName(
            @PathVariable(value = "azuregroupname") String azureGroupName
    ) {
        return buildResponseEntity(groupService.getMembersByGroupName(azureGroupName));
    }

    @Operation(summary = "Get Group Members")
    @GetMapping(
            path = "contacts/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity getGroupMembers(
            @RequestParam("azuregroupid") String azureGroupId,
            @RequestParam(value = "showSkuld", defaultValue = "false") Boolean showSkuld
    ) {
        return buildResponseEntity(groupService.getGroupMembersById(azureGroupId, showSkuld));
    }

    @Operation(summary = "Create Azure Gruop")
    @RequestMapping(
            path = "add/",
            method = RequestMethod.POST,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity createGroup(
            @RequestParam(value = "groupName") String groupName,
            @RequestParam(value = "groupDescription") String groupDescription
    ) {
        AzureAdGroup group = groupService.createGroup(groupName, groupDescription);
        return buildResponseEntity((group));
    }

    @Operation(summary = "Add member to group")
    @RequestMapping(
            path = "member/{azuregroupid}/{userId}",
            method = RequestMethod.POST,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity addToGroup(
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "azuregroupid") String groupId
    ) {
        CodeValue result = groupService.addUserToGroup(userId, groupId);
        return buildResponseEntity(result);
    }

    @Operation(summary = "Remove member from group")
    @RequestMapping(
            path = "member/{azuregroupid}/{userId}",
            method = RequestMethod.DELETE,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity removeFromGroup(
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "azuregroupid") String groupId
    ) {
        CodeValue result = groupService.removeUserFromGroup(userId, groupId);
        return buildResponseEntity(result);
    }

}
