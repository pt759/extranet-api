package com.skuld.extranet.core.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.domain.YesNo;
import com.skuld.extranet.spa.pages.domain.ProductId;
import lombok.Data;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.skuld.common.domain.YesNo.No;

@Data
public class StatCode {

    @JsonView({UserView.AzureGroupAdmin.class})
    private String statCode;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String statCodeName;

    @JsonView({UserView.AzureGroupAdmin.class})
    private String statCodeManager;

    @JsonIgnore
    private Long serviceAgentNo;

    private YesNo declarations;
    private YesNo financials;

    @JsonIgnore
    private List<ProductId> productId;

    public StatCode() {
        this.declarations = No;
    }

    public StatCode(ResultSet r) throws SQLException {
        this();
        this.statCode = r.getString("STAT_CODE");
        this.statCodeName = r.getString("name");
        this.declarations = YesNo.convertStringToYesNo(r.getString("ONLINE_DECLARATION"));
        this.productId =  ProductId.fromCommaString(r.getString("PRODUCTS"));
        this.statCodeManager = r.getString("GROUP_MANAGER");
        this.serviceAgentNo = r.getLong("SERVICE_AGENT_NO");
    }

    public static class Mapper implements RowMapper<StatCode> {
        public StatCode map(ResultSet r, StatementContext ctx) throws SQLException {
            return new StatCode(r);
        }
    }

    @Override
    public String toString() {
        return "StatCode{" +
                "statCode='" + statCode + '\'' +
                ", name='" + statCodeName + '\'' +
                '}';
    }

    public String getStatCode() {
        return statCode;
    }

    public StatCode setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    public String getName() {
        return statCodeName;
    }

    public StatCode setName(String name) {
        this.statCodeName = name;
        return this;
    }
}
