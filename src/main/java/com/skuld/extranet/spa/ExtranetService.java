package com.skuld.extranet.spa;


import com.skuld.common.domain.Base64Container;
import com.skuld.errorhandling.MessageLevel;
import com.skuld.errorhandling.exceptions.SkuldErrorException;
import com.skuld.extranet.core.accountOverview.AccountOverviewService;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.archive.service.ArchiveService;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.common.PolicyService;
import com.skuld.extranet.core.common.SkuldWebUtil;
import com.skuld.extranet.core.common.domain.Base64ContainerForExcel;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.common.domain.CodeValueWithList;
import com.skuld.extranet.core.common.domain.GraphData;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntityVersion;
import com.skuld.extranet.core.memberstatistics.service.MemberStatisticsService;
import com.skuld.extranet.core.vessel.VesselService;
import com.skuld.extranet.core.vessel.domain.GrossPremium;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselPolicyLine;
import com.skuld.extranet.global.SkuldPredicate;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.spa.pages.ClaimListPage;
import com.skuld.extranet.spa.pages.StatisticsPage;
import com.skuld.extranet.spa.pages.VesselDetailPage;
import com.skuld.extranet.spa.pages.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ExtranetService {
    final static Logger logger = LoggerFactory.getLogger(ExtranetService.class);

    @Autowired
    MemberStatisticsService msService;

    @Autowired
    ClaimService claimService;

    @Autowired
    VesselService vesselService;

    @Autowired
    AccountOverviewService accountService;

    @Autowired
    ArchiveService archiveService;

    private DocumentService documentService;

    private PolicyService policyService;

    public ExtranetService() {
        policyService = new PolicyService();
        documentService = new DocumentService();
    }

    public ExtranetService setPolicyService(PolicyService policyService) {
        this.policyService = policyService;
        return this;
    }

    /*
            All values which are based of Statcode for Statistics Page.
            */
    public StatisticsPage statisticsPage(String statCode) {
        StatisticsPage statisticsPage = new StatisticsPage();

        logger.info("statisticsPage->Start");
        // Master data
        List<StatisticsEntity> masterStatisticsEntity = msService.getStatistics(statCode);
        logger.info("statisticsPage-> Loaded Statistics");

        List<ClaimStatisticsEntity> claimStatisticsFiveYear = msService.statisticsClaimFiveYear(statCode);
        logger.info("statisticsPage-> Loaded Claim Five Year");

        List<ClaimStatisticsEntity> claimStatisticsCurrentYear = msService.statisticsClaimSixYear(statCode);
        logger.info("statisticsPage-> Loaded Claim Current Year");

        // Filter
        StatisticPageFilter filter = createFilterForStatisticsPage(masterStatisticsEntity);
        statisticsPage.setFilter(filter);
        statisticsPage.setCurrentPolicyYear(policyService.currentPolicyYearAsString());

        // Table on Page for all products
        // filter on product
        for (CodeValue codeValue : filter.getProducts()) {
            logger.info("statisticsPage->-> Product " + codeValue.getCode() + " start");
            String productCode = codeValue.getCode();
            StatisticsProductData productDataForPage = new StatisticsProductData();

            productDataForPage.setProductCode(productCode);


            List<StatisticsEntity> rowsForProduct = getProductStatisticsEntityFrom(masterStatisticsEntity, productCode);

            List productYearFilter = getProductYearFilter(rowsForProduct);

            List<StatisticsTableEntity> tableDataForProductSplitOnClassActual = splitListBasedOnClassAndVersion(rowsForProduct, StatisticsEntityVersion.actual);
            List<StatisticsTableEntity> tableDataForProductSplitOnClassDistributed = splitListBasedOnClassAndVersion(rowsForProduct, StatisticsEntityVersion.distributed);
            List<StatisticsTableEntity> tableDataForProductSplitOnClassFiveYear = splitListBasedOnClassAndVersion(rowsForProduct, StatisticsEntityVersion.fiveYear);


            Map<String, List<StatisticsTableEntity>> tables = productDataForPage.getTables();
            tables.put(StatYearType.statYears.getCode(), tableDataForProductSplitOnClassActual);
            tables.put(StatYearType.distributed.getCode(), tableDataForProductSplitOnClassDistributed);
            tables.put(StatYearType.fiveYears.getCode(), tableDataForProductSplitOnClassFiveYear);

            // Claim Count for 5 year statistics / Column Graph
            List<ClaimStatisticsEntity> claimStatisticsByProductAndGrouped = getClaimStatisticsByProductAndGrouped(productCode, 5, claimStatisticsFiveYear);

            List<ClaimStatisticsEntity> list = getClaimStatisticsByProductAndGrouped(productCode, 4, claimStatisticsCurrentYear);
            list.add(ClaimStatisticsEntity.createTotalClaimEntry(list));


            productDataForPage.setPdfUrls(msService.getMemberStatisticsUrls(statCode, Product.fromString(productCode)));
            productDataForPage.setExcelUrls(msService.getExcelUrlForStatistics(statCode, Product.fromString(productCode)));
            productDataForPage.setTotalClaimFiveYearByGroup(claimStatisticsByProductAndGrouped);
            productDataForPage.setTotalClaimsForCurrentYearByGroup(list);
            productDataForPage.setLossRatio5YearStatistics(getLossRatio5Year(statCode, productCode));
            productDataForPage.setStatYearsFilter(productYearFilter);

            statisticsPage.getProductDataMap().put(productCode, productDataForPage);

        }
        logger.info("statisticsPage->-> Page data Generation is Done");

        return statisticsPage;
    }

    private List<GraphData> getLossRatio5Year(String statCode, String productCode) {
        return msService.getLossRatio5Years(statCode, productCode);
    }

    private List<StatisticsEntity> getProductStatisticsEntityFrom(List<StatisticsEntity> masterStatisticsEntity, String productCode) {
        return masterStatisticsEntity.stream()
                .filter(line -> productCode.equalsIgnoreCase(line.getProduct()))
                .collect(Collectors.toList());
    }

    public List<ClaimStatisticsEntity> getClaimStatisticsByProductAndGrouped(String productCode, Integer groups, List<ClaimStatisticsEntity> claimStatisticsFiveYear) {
        List<ClaimStatisticsEntity> list = msService.filteredClaimStatisticsEntityOnProduct(claimStatisticsFiveYear, productCode);
        return ClaimStatisticsEntity.groupListWithLimitOf(groups, list);
    }

    private StatisticPageFilter createFilterForStatisticsPage(List<StatisticsEntity> masterStatisticsEntity) {
        List<CodeValue> products = getProductsForPage(masterStatisticsEntity);
        StatisticPageFilter filter = new StatisticPageFilter();
        filter.setProducts(products);
        return filter;
    }

    protected List getProductYearFilter(List<StatisticsEntity> rows) {
        List<String> years = rows.stream()
                .filter(SkuldPredicate.distinctByKey(StatisticsEntity::getPolicyYear))
                .map(line -> line.getPolicyYear())
                .sorted()
                .collect(Collectors.toList());

        List statYearsChoices = new ArrayList<>();

        statYearsChoices.add(StatYearType.distributed.getCodeValue());
        statYearsChoices.add(StatYearType.fiveYears.getCodeValue());

        CodeValueWithList codeValueWithList = new CodeValueWithList(StatYearType.statYears.getCodeValue(), years);
        statYearsChoices.add(codeValueWithList);
        return statYearsChoices;
    }

    private List<CodeValue> getProductsForPage(List<StatisticsEntity> masterStatisticsEntity) {
        return masterStatisticsEntity.stream()
                .filter(SkuldPredicate.distinctByKey(StatisticsEntity::getProduct))
                .map(line -> new CodeValue(line.getProduct(), line.getProduct(), line.getProduct()))
                .collect(Collectors.toList());
    }

    public List<StatisticsTableEntity> splitListBasedOnClassAndVersion(List<StatisticsEntity> list, StatisticsEntityVersion version) {

        List<StatisticsEntity> rows;
        if (version.equals(StatisticsEntityVersion.fiveYear)) {
            rows = list.stream().filter(line -> policyService.isValidFiveYearPolicyYear(line.getPolicyYear())).collect(Collectors.toList());
        } else {
            rows = list;
        }

        Map<String, List<StatisticsEntity>> map = rows.stream()
                .filter(line -> version.dbFilterValue().equalsIgnoreCase(line.getVersion()))
                .collect(Collectors.groupingBy(StatisticsEntity::getProductClass));

        List<StatisticsTableEntity> tables = new ArrayList();
        for (Map.Entry<String, List<StatisticsEntity>> entry : map.entrySet()) {
            StatisticsTableEntity table = new StatisticsTableEntity();
            List<StatisticsEntity> value = entry.getValue();


            table.setData(value);
            table.setTitle(entry.getKey());
            table.setName(entry.getKey());
            tables.add(table);
        }


        return tables;
    }

    public String getStatisticsAsBase64(String statCode, Product product, StatisticsEntityVersion version) {
        StatisticsPage page = statisticsPage(statCode);

        if (page == null) {
            return null;
        }

        StatYearType statYearTypeMapped = StatisticsEntityVersion.convert(version);

        StatisticsProductData statisticsProductData = page.getProductDataMap().get(product.toString());
        List<StatisticsTableEntity> tables = statisticsProductData.getDataTable(statYearTypeMapped);

        String data = null;
        if (tables.size() > 0) {
            String sheetName = getSheetName(product, version);
            String filename = "Member Statistics.xlsx";
            StatisticsExcelConverter excel = new StatisticsExcelConverter(filename, sheetName);
            data = excel.getExcelAsBase64Text(tables);
        }
        return data;

    }


    public VesselDetailPage vesselDetailPage(String vesselId, String statCode) {

        VesselDetailPage page = new VesselDetailPage();

        Vessel vessel = findVessel(vesselId, statCode);

        page.setVessel(vessel);
        // Because of performance, this has been moved to separate endpoint.
//        List<DocumentRef> pageDocuments = getDocumentsForVessel(vessel);
//        page.setDocuments(pageDocuments);

        List<CodeValue> lossRatio = new ArrayList<>();
        page.setLossRatios(lossRatio);

        List<GrossPremium> grossPremiums = vesselService.grossPremiumsFor(vesselId, statCode);
        page.setVesselGrossPremiums(grossPremiums);

        LDAPUser ldapUser = accountService.getAccountManager(vessel.getStatCode());
        page.setAccountManager(ldapUser);

        return page;

    }

    // Replaced documents in vesselDetailPage
    public List<DocumentRef> vesselDetailDocuments(String vesselId, String statCode) {

        Vessel vessel = findVessel(vesselId, statCode);

        return getDocumentsForVessel(vessel);
    }

    private Vessel findVessel(String vesselId, String statCode) {
        Vessel vessel = vesselService.find(vesselId, statCode);
        if (vessel == null) {
            throw new SkuldErrorException("/vessel/detail/documents", "Did not find vessel", "Unable to find vesselid " + vesselId + " for " + statCode, MessageLevel.ERROR, null);
        }
        return vessel;
    }

    public List<DocumentRef> getDocumentsForVessel(Vessel vessel) {
        List<DocumentRef> allDocuments = new ArrayList<>();

        //Finding Vessels from DWH that we should check.
        List<VesselPolicyLine> list = vesselService.findVesselPolicyLine(vessel);

        // For Each Vessel,
        // get list of documents from TIA based on AgrLineNo
        for (VesselPolicyLine vesselPolicyLine : list) {
            List<DocumentRef> docs = archiveService.getDocumentsReferenceArgNo(vessel.getStatCode(), vesselPolicyLine.getPolicy_line_id());
            allDocuments.addAll(docs);
        }

        allDocuments.stream().forEach(
                doc -> doc
                        .setVesselName(vessel.getVesselName())
                        .setUrl(documentService.convertDocumentUrl(vessel.getStatCode(), doc)))
        ;

        allDocuments = documentService.sortDocumentListAfterFilename(allDocuments);

        return allDocuments;
    }


    public void setAccountOverviewService(AccountOverviewService accountOverviewService) {
        this.accountService = accountOverviewService;
    }

    public void setVesselService(VesselService vesselService) {
        this.vesselService = vesselService;
    }

    public ExtranetService setMsService(MemberStatisticsService msService) {
        this.msService = msService;
        return this;
    }

    public ExtranetService setClaimService(ClaimService claimService) {
        this.claimService = claimService;
        return this;
    }

    public void setArchiveService(ArchiveService archiveService) {
        this.archiveService = archiveService;
    }

    public Base64Container getExcelAsBase64(String statcode, String product, String version) {

        Product thisProduct = Product.fromString(SkuldWebUtil.urlDecode(product));
        StatisticsEntityVersion statVersion = StatisticsEntityVersion.fromString(version);
        String data = "";
        String filename = "Member Statistics.xlsx";
        if (thisProduct != null && statVersion != null) {
            List<StatisticsTableEntity> tableData = getStatisticsForTableAsList(statcode, thisProduct, statVersion);
            if (tableData != null && tableData.size() > 0) {
                String sheetName = getSheetName(thisProduct, statVersion);
                StatisticsExcelConverter excel = new StatisticsExcelConverter(filename, sheetName);
                data = excel.getExcelAsBase64Text(tableData);
            }
        }

        Base64Container result = Base64ContainerForExcel.getBase64ContainerWithExcel(data, filename);
        return result;
    }

    public void getExcelAsFile(String statcode, String product, String version, HttpServletResponse response) {
        Product thisProduct = Product.fromString(SkuldWebUtil.urlDecode(product));
        StatisticsEntityVersion statVersion = StatisticsEntityVersion.fromString(version);

        if (thisProduct != null && statVersion != null) {
            List<StatisticsTableEntity> tables = getStatisticsForTableAsList(statcode, thisProduct, statVersion);
            if (tables != null && tables.size() > 0) {
                String filename = "Member Statistics.xlsx";
                String sheetName = getSheetName(thisProduct, statVersion);
                StatisticsExcelConverter excel = new StatisticsExcelConverter(filename, sheetName);
                excel.export(tables, response);
            } else {
                logger.info("Excel as File: No data found for : statcode:" + statcode + ", product:" + product + ", version:" + version);
            }
        }
    }

    private String getSheetName(Product thisProduct, StatisticsEntityVersion statVersion) {
        return statVersion.getDescription() + " " + " - " + thisProduct.description();
    }


    public List<StatisticsTableEntity> getStatisticsForTableAsList(String statcode, Product product, StatisticsEntityVersion version) {

        StatisticsPage page = statisticsPage(statcode);

        if (page == null) {
            return null;
        }

        StatYearType statYearTypeMapped = StatisticsEntityVersion.convert(version);

        StatisticsProductData statisticsProductData = page.getProductDataMap().get(product.toString());
        List<StatisticsTableEntity> tables = statisticsProductData.getDataTable(statYearTypeMapped);
        return tables;


    }


    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

    @Deprecated
    public ClaimListPage claimsList(String statCode) {

        List<Claim> claimDetailVS = claimService.claimListAll(statCode);

        List<Product> distinctProducts = getDistinctProductsSorted(claimDetailVS);

        List<UrlLink> urls = getClaimListUrls(statCode, distinctProducts);

        ClaimListPage page = new ClaimListPage();
        page.setClaims(claimDetailVS);
        page.setUrlDownloads(urls);

        return page;
    }

    @Deprecated
    protected List<Product> getDistinctProductsSorted(List<Claim> claimDetailVS) {
        List<Product> distinctProducts = claimDetailVS.stream()
                .map(Claim::getProduct)
                .distinct()
                .collect(Collectors.toList());
        Collections.sort(distinctProducts);
        return distinctProducts;
    }

    @Deprecated
    protected List<UrlLink> getClaimListUrls(String statCode, List<Product> products) {

        List<UrlLink> urls = new ArrayList<>();
        urls.add(claimService.getListOfClaimExcelUrls(statCode));

        for (Product product : products) {
            urls.addAll((claimService.getListOfClaimUrls(statCode, product)));
        }
        return urls;
    }
}
