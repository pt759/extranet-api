package com.skuld.extranet.spa;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.common.SkuldController;
import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.security.service.ContentFilterService;
import com.skuld.extranet.spa.pages.ClaimListPage;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverviewPage;
import com.skuld.extranet.spa.pages.PageEntityView;
import com.skuld.extranet.spa.pages.StatisticsPage;
import com.skuld.extranet.spa.pages.VesselDetailPage;
import com.skuld.extranet.spa.pages.domain.SearchResult;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.skuld.extranet.core.common.SkuldWebUtil.converterStringToIntWithDefaultValue;
import static com.skuld.extranet.global.RESTendPoints.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Log4j2
@Tag(name = "Extranet", description = "Page Endpoint for Extranet")
@OpenAPIDefinition(info =
@Info(title = "Page Endpoint for Extranet",
        version = "1.0",
        description = ""))
@RestController
@RequestMapping(extranet)
@CrossOrigin
public class ExtranetController extends SkuldController {

    private static final String listLimitDefault = "10";
    private final ExtranetService extranetService;
    private final SearchService searchService;
    private final ContentFilterService contentFilterService;

    @Autowired
    public ExtranetController(ExtranetService extranetService,
                              SearchService searchService,
                              ContentFilterService contentFilterService) {
        this.extranetService = extranetService;
        this.searchService = searchService;
        this.contentFilterService = contentFilterService;
    }

    @Operation(summary = "Statistics Page")
    @GetMapping(
            path = "/statistics/{statcode}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity statisticsPage(@PathVariable(value = "statcode") String statCode) {
        log.info("Load StatisticsPage for statcode = " + statCode);
        StatisticsPage page = extranetService.statisticsPage(statCode);
        return buildResponseEntity(page);
    }

    @Operation(summary = "Vessel Detail Page")
    @GetMapping(path = "/vessel/detail/{statcode}/{vesselId}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity vesselDetailPage(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "vesselId") String vesselId
    ) {
        log.info("Load Vessel details for = " + vesselId + " , statcode=" + statCode);
        VesselDetailPage page = extranetService.vesselDetailPage(vesselId, statCode);
        return buildResponseEntity(page);
    }

    @Operation(summary = "Vessel Detail Documents")
    @ApiResponse(content = @Content(schema = @Schema(implementation = DocumentRef.class)))
    @GetMapping(path = "/vessel/detail/documents/{statcode}/{vesselId}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity vesselDetailDocuments(
            @PathVariable(value = "statcode") String statCode,
            @PathVariable(value = "vesselId") String vesselId
    ) {
        log.info("Load vesselDetailDocuments for vesselid = " + vesselId + " , statcode=" + statCode);

        final List<DocumentRef> documentRefs = extranetService.vesselDetailDocuments(vesselId, statCode);
        Map map = new HashMap();
        map.put("DOCUMENTS", documentRefs);
        return buildResponseEntity(map);
    }

    @Operation(summary = "Client Overview Page")
    @GetMapping(
            path = "/client/overview/{statcode}/",
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity clientOverviewPage(
            @PathVariable(value = "statcode") String statCode) {
        ClientOverviewPage page = searchService.clientOverviewPage(statCode);

        return buildResponseEntity(contentFilterService.filterClientOverviewPage(page));
    }

    @Operation(summary = "Export Statistics list to Excel")
    @GetMapping(
            path = statistics_excel_StatCode_Product_Version,
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity downloadStatisticsToExcel(@PathVariable String statcode,
                                                    @PathVariable String product,
                                                    @PathVariable String version
    ) {
        Base64Container container = extranetService.getExcelAsBase64(statcode, product, version);
        return responseEntityWrapper(container);
    }

    @Operation(summary = "Export Statistics list to Excel")
    @GetMapping(
            path = statistics_excel_file_StatCode_Product_Version
    )
    public void downloadStatisticsToExcelDirect(@PathVariable String statcode,
                                                @PathVariable String product,
                                                @PathVariable String version,
                                                HttpServletResponse response) {
        extranetService.getExcelAsFile(statcode, product, version, response);
    }

    @Operation(summary = "Client Search")
    @JsonView(PageEntityView.SearchList.class)
    @GetMapping(
            path = "/client/search/{statcode}/",
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity searchClient(@PathVariable(value = "statcode") String statCode,
                                       @RequestParam(value = "query") String query) {
        SearchResult result = searchService.searchClient(query, statCode);
        if (result == null) {
            result = new SearchResult();
        }
        return buildResponseEntity(result);
    }

    @Operation(summary = "Broker Search")
    @JsonView(PageEntityView.SearchListBroker.class)
    @GetMapping(
            path = "/broker/search/{statcodegrouplogin}/",
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity search(@PathVariable(value = "statcodegrouplogin") String statCodeGroupLogin,
                                 @RequestParam(value = "query") String query,
                                 @RequestParam(value = "limit", required = false, defaultValue = listLimitDefault) String limit
    ) {
        int listLimit = converterStringToIntWithDefaultValue(limit, listLimitDefault);
        SearchResult result = searchService.searchBroker(query, statCodeGroupLogin, listLimit);
        return buildResponseEntity(result);
    }

    @Deprecated
    @Operation(summary = "Old Claims Page")
    @JsonView(PageEntityView.ClaimListPage.class)
    @GetMapping(
            path = "/claims/list/{statcode}/",
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity claimListPage(@PathVariable(value = "statcode") String statCode
    ) {
        ClaimListPage page = extranetService.claimsList(statCode);
        log.info("Claims on page: " + page.getClaims().size());
        return buildResponseEntity(page);
    }


}
