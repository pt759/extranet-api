package com.skuld.extranet.spa.pages.domain;

public enum ProductClass {

    PI("P&I"),
    FDD("FD&D");

    private final String value;


    ProductClass(String value) {
        this.value = value;
    }

    public static ProductClass fromString(String code) {
        for (ProductClass b : ProductClass.values()) {
            if (b.value().equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

}
