package com.skuld.extranet.spa.pages.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ProductId {
    Charterer("C"),
    Yachts("Z"),
    Owner("O"),

    undefined("Undefined");

    private final String value;


    ProductId(String value) {
        this.value = value;
    }

    public static ProductId fromString(String value) {
        for (ProductId item : ProductId.values()) {
            if (item.value().equalsIgnoreCase(value)) {
                return item;
            }
        }
        return undefined;
    }

    public static List<ProductId> fromCommaString(String products) {
        List<ProductId> productIds = new ArrayList<>();

        List<String> listOfProducts = Arrays.asList(products.split(","));
        listOfProducts.stream().forEach(x -> {
            productIds.add(fromString(x));
        });
        return productIds;
    }

    public String value() {
        return value;
    }

    public String description() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static class Mapper implements RowMapper<ProductId> {
        public ProductId map(ResultSet r, StatementContext ctx) throws SQLException {
            return fromString(r.getString("product"));
        }
    }

}
