package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.core.common.domain.CodeValue;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class ReportGenerator {

    List<CodeValue> accounts;
    List<CodeValue> policyHolders;
    List<Report> reports;

    public ReportGenerator(List<Report> reports) {
        this.reports = reports;
    }

    public ReportGenerator() {
        this.reports = new ArrayList<>();
    }

    public List<Report> getReports() {
        return reports;
    }

    public ReportGenerator setReports(List<Report> reports) {
        this.reports = reports;
        return this;
    }

    public List<CodeValue> getAccounts() {
        return accounts;
    }

    public ReportGenerator setAccounts(List<CodeValue> accounts) {
        this.accounts = accounts;
        return this;
    }

    public List<CodeValue> getPolicyHolders() {
        return policyHolders;
    }

    public ReportGenerator setPolicyHolders(List<CodeValue> policyHolders) {
        this.policyHolders = policyHolders;
        return this;
    }
}
