package com.skuld.extranet.spa.pages;

import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.vessel.domain.GrossPremium;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.ldap.domain.LDAPUser;
import lombok.Data;

import java.util.List;

@Data
public class VesselDetailPage {
    Vessel vessel;
    List<GrossPremium> vesselGrossPremiums;
    List<CodeValue> lossRatios;
    LDAPUser accountManager;
}
