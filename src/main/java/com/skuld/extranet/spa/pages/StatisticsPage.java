package com.skuld.extranet.spa.pages;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.skuld.extranet.spa.pages.domain.StatisticPageFilter;
import com.skuld.extranet.spa.pages.domain.StatisticsProductData;

import java.util.HashMap;
import java.util.Map;

public class StatisticsPage {

    private StatisticPageFilter filter;

    private String currentPolicyYear;

    @JsonProperty("pageMap")
    Map<String, StatisticsProductData> productDataMap;


    public StatisticsPage() {
        this.productDataMap = new HashMap<>();
        filter = new StatisticPageFilter();
    }


    public Map<String, StatisticsProductData> getProductDataMap() {
        return productDataMap;
    }

    public StatisticsPage setProductDataMap(Map<String, StatisticsProductData> productDataMap) {
        this.productDataMap = productDataMap;
        return this;
    }

    public StatisticPageFilter getFilter() {
        return filter;
    }

    public StatisticsPage setFilter(StatisticPageFilter filter) {
        this.filter = filter;
        return this;
    }

    public String getCurrentPolicyYear() {
        return currentPolicyYear;
    }

    public StatisticsPage setCurrentPolicyYear(String currentPolicyYear) {
        this.currentPolicyYear = currentPolicyYear;
        return this;
    }
}
