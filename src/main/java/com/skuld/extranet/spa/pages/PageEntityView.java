package com.skuld.extranet.spa.pages;

import com.skuld.extranet.core.accountOverview.domain.AccountView;
import com.skuld.extranet.core.claim.domain.Views;
import com.skuld.extranet.core.user.domain.UserView;
import com.skuld.extranet.core.vessel.domain.VesselView;
import com.skuld.extranet.ldap.domain.LdapUserViews;

public class PageEntityView {


    public interface UrlLinks {

    }

    public interface SearchList extends VesselView.Entered, Views.ClaimList, LdapUserViews.LdapBasic, AccountView.Basic {
    }

    public interface SearchListBroker extends SearchList {
    }


    public interface ClientView extends VesselView.Entered, Views.ClaimSimpleListOpen, LdapUserViews.LdapBasic, UserView.SimpleUser {
    }

    public interface ClaimListPage extends Views.ClaimList, UrlLinks {

    }

    public interface VesselDetailPage extends LdapUserViews.LdapBasic, VesselView.Detail, VesselView.GrossPremium, VesselView.Documents {
    }


}
