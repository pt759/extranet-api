package com.skuld.extranet.spa.pages.domain;

import com.skuld.extranet.core.common.domain.CodeValue;

public enum StatYearType {

    distributed("DISTRIBUTED", "Distributed", ""),
    fiveYears("5YEAR", "Five year", ""),
    sixYears("6YEAR", "Five year plus current", ""),
    statYears("STAT", "Stat years", ""),
    current("CURRENT", "Current year", "")
    ;

    private final CodeValue codeValue;

    StatYearType(String code, String value, String description) {
        this.codeValue = new CodeValue(code, value, description);
    }

    @Override
    public String toString() {
        return codeValue.getValue();
    }

    public CodeValue getCodeValue() {
        return this.codeValue;
    }

    public String getCode() {
        return this.codeValue.getCode();
    }

    public static StatYearType fromString(String code) {
        for (StatYearType b : StatYearType.values()) {
            if (b.getCode().equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }

    public String getValue() {
        return toString();
    }
}
