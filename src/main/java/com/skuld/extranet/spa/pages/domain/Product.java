package com.skuld.extranet.spa.pages.domain;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public enum Product {
    charter("Charterer"),
    crewManager("Crew Managers"),
    fixedOwner("Fixed Owner"),
    hullChart("P&I/Hull Chart."),
    owner("Owner"),
    ownerMutual("Owner Mutual"),
    skuldRigs("Skuld rigs"),
    undefined("Undefined");

    private final String value;


    Product(String value) {
        this.value = value;
    }

    public static Product fromString(String value) {
        for (Product item : Product.values()) {
            if (item.value().equalsIgnoreCase(value)) {
                return item;
            }
        }
        return undefined;
    }

    public String value() {
        return value;
    }

    public String description() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static class Mapper implements RowMapper<Product> {
        public Product map(ResultSet r, StatementContext ctx) throws SQLException {
            return fromString(r.getString("product"));
        }
    }

}
