package com.skuld.extranet.spa.pages.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.spa.pages.PageEntityView;

public class UrlLink {

    public static String CODE_PDF = "PDF";
    public static String CODE_EXCEL = "EXCEL";

    public static String CODE_SOA = "Statement of account as ";
    public static String CODE_INSTALMENT_REPORT = "Instalment Report as ";


    @JsonView(PageEntityView.UrlLinks.class)
    private String title;

    @JsonView(PageEntityView.UrlLinks.class)
    private String url;

    @JsonView(PageEntityView.UrlLinks.class)
    private String code;


    public UrlLink() {
    }

    public UrlLink(String title, String url, String code) {
        this.title = title;
        this.url = url;
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public UrlLink setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public UrlLink setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getCode() {
        return code;
    }

    public UrlLink setCode(String code) {
        this.code = code;
        return this;
    }


}
