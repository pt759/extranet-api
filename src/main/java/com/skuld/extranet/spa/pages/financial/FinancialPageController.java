package com.skuld.extranet.spa.pages.financial;

import com.skuld.common.SkuldController;
import com.skuld.common.domain.YesNo;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Financial", description = "Financial Pages for Module")
@RestController
@RequestMapping("/financialpagecontroller/")
public class FinancialPageController extends SkuldController {

    @Autowired
    private FinancialPageService service;

    @Operation(summary = "Financial Overview of Accounts")
    @RequestMapping(
            path = "/soa/{statcode}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity financialOverview(
            @PathVariable(value = "statcode") String statCode) {
        FinancialOverviewPage page = service.getFinancialOverview(statCode);
        return buildResponseEntity(page);
    }

    @Operation(summary = "Statement of Account")
    @RequestMapping(
            path = "/soa/{statcode}/{accountno}/{itemtype}/{deposit}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity statementOfAccount(@PathVariable(value = "statcode") String statCode,
                                             @PathVariable(value = "accountno") String accountNo,
                                             @PathVariable(value = "itemtype") String itemType,
                                             @PathVariable(value = "deposit") String deposit
                                             ) {
        StatementOfAccountPage page = service.getStatementOfAccount(statCode, accountNo, AccountItemEntity.ItemType.fromString(itemType), YesNo.fromString(deposit));
        return buildResponseEntity(page);
    }

    @Operation(summary = "Statement of Account Deposit Account")
    @RequestMapping(
            path = "/deposit/{statcode}/{accountno}/{itemtype}/{deposit}/",
            method = RequestMethod.GET,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity statementOfAccountDeposit(@PathVariable(value = "statcode") String statCode,
                                             @PathVariable(value = "accountno") String accountNo,
                                             @PathVariable(value = "itemtype") String itemType,
                                             @PathVariable(value = "deposit") String deposit
                                             ) {
        StatementOfPremiumDepositAccountPage page = service.getDepositStatementOfAccount(accountNo);
        return buildResponseEntity(page);
    }

}
