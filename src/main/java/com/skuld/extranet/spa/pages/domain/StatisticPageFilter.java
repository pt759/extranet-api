package com.skuld.extranet.spa.pages.domain;

import com.skuld.extranet.core.common.domain.CodeValue;

import java.util.List;

public class StatisticPageFilter {

    private List<CodeValue> products;


    public List<CodeValue> getProducts() {
        return products;
    }

    public StatisticPageFilter setProducts(List<CodeValue> products) {
        this.products = products;
        return this;
    }

}
