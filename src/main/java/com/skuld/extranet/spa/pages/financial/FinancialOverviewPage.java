package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.core.financial.domain.invoice.Invoice;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccounts;
import com.skuld.extranet.ldap.domain.LDAPUser;

import java.util.ArrayList;
import java.util.List;

public class FinancialOverviewPage {

    private FinancialOverviewAccounts financialOverviewAccounts;
    private List<LDAPUser> creditControllers;
    private List<Invoice> openInvoices;

    public FinancialOverviewPage() {
        this.creditControllers = null;
        this.financialOverviewAccounts = new FinancialOverviewAccounts();
        this.openInvoices = new ArrayList<>();
    }

    public List<LDAPUser> getCreditControllers() {
        return creditControllers;
    }

    public FinancialOverviewPage setCreditControllers(List<LDAPUser> creditControllers) {
        this.creditControllers = creditControllers;
        return this;
    }

    public FinancialOverviewAccounts getFinancialOverviewAccounts() {
        return financialOverviewAccounts;
    }

    public FinancialOverviewPage setFinancialOverviewAccounts(FinancialOverviewAccounts financialOverviewAccounts) {
        this.financialOverviewAccounts = financialOverviewAccounts;
        return this;
    }

    public List<Invoice> getOpenInvoices() {
        return openInvoices;
    }

    public FinancialOverviewPage setOpenInvoices(List<Invoice> openInvoices) {
        this.openInvoices = openInvoices;
        return this;
    }
}
