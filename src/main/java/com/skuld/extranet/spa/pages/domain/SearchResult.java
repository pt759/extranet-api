package com.skuld.extranet.spa.pages.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.spa.pages.PageEntityView;

import java.util.ArrayList;
import java.util.List;

public class SearchResult {


    @JsonView(PageEntityView.SearchList.class)
    Boolean resultIsBroker;

    @JsonView(PageEntityView.SearchList.class)
    String statCode;

    @JsonView(PageEntityView.SearchList.class)
    List<AccountOverview> clients;

    @JsonView(PageEntityView.SearchList.class)
    List<Vessel> vessels;

    @JsonView(PageEntityView.SearchList.class)
    List<Claim> claims;

    public SearchResult() {
        this.claims = new ArrayList<>();
        this.vessels = new ArrayList<>();
        this.clients = new ArrayList<>();
    }

    public SearchResult setClaims(List<Claim> claims) {
        this.claims = claims;
        return this;
    }

    public List<Vessel> getVessels() {
        return vessels;
    }

    public SearchResult setVessels(List<Vessel> vessels) {
        this.vessels = vessels;
        return this;
    }

    public List<AccountOverview> getClients() {
        return clients;
    }

    public SearchResult setClients(List<AccountOverview> clients) {
        this.clients = clients;
        return this;
    }

    public Boolean isBroker() {
        return resultIsBroker;
    }

    public SearchResult setBrokerFlag(Boolean flag) {
        resultIsBroker = flag;
        return this;
    }

    public String getStatCode() {
        return statCode;
    }

    public SearchResult setStatCode(String statCode) {
        this.statCode = statCode;
        return this;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "resultIsBroker=" + resultIsBroker +
                ", statCode='" + statCode + '\'' +
                ", claims=" + claims.size() +
                ", vessels=" + vessels.size() +
                ", clients=" + clients.size() +
                '}';
    }

    public List<Claim> getClaims() {
        return claims;
    }
}
