package com.skuld.extranet.spa.pages.ClientOverview;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.global.SkuldPredicate;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonus;
import com.skuld.extranet.spa.pages.PageEntityView;
import lombok.Data;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class ClientOverviewPage {

    @JsonView(PageEntityView.ClientView.class)
    List<VesselListDTO> vessels;

    @JsonView(PageEntityView.ClientView.class)
    List<Claim> openClaims;

    @JsonView(PageEntityView.ClientView.class)
    LDAPUser accountManager;


    @JsonView(PageEntityView.ClientView.class)
    List<LDAPUser> claimHandlers;

    @JsonView(PageEntityView.ClientView.class)
    MemberPerformanceBonus memberPerformanceBonus;

    ClientOverViewSummary clientOverViewSummary;


    public List<LDAPUser> updateClaimHandlers() {
        if (openClaims == null) {
            return null;
        }
        List<LDAPUser> handlers = openClaims
                .stream()
                .filter(SkuldPredicate.distinctByKey(Claim::getEventHandlerMail))
                .map(line -> line.getEventHandlerUser())
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return handlers;

    }

    public ClientOverviewPage setOpenClaims(List<Claim> openClaims) {
        this.openClaims = openClaims;
        this.claimHandlers = updateClaimHandlers();
        return this;
    }



}
