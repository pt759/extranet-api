package com.skuld.extranet.spa.pages.financial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skuld.extranet.core.common.SkCommon;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.BankDetail;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccountData;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountTotal;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountVessel;
import com.skuld.extranet.spa.pages.domain.UrlLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.getTotalForAccountItems;

public class StatementOfAccountPage {

    final static Logger logger = LoggerFactory.getLogger(StatementOfAccountPage.class);

    private final String name = "Statement of Account";
    private String lastUpdated = SkCommon.todo;
    private List<UrlLink> urlDownloads;
    private FinancialOverviewAccountData accountInformation;
    private List<BankDetail> bankDetails;
    private List<StatementOfAccountTotal> vesselTotals;//By currency
    private List<StatementOfAccountVessel> vessels;

    @JsonIgnore
    private List<AccountItemEntity> accountItems;


    public StatementOfAccountPage() {
        this.urlDownloads = new ArrayList<>();
    }

    public List<UrlLink> getUrlDownloads() {
        return urlDownloads;
    }

    public StatementOfAccountPage setUrlDownloads(List<UrlLink> urlDownloads) {
        this.urlDownloads = urlDownloads;
        return this;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public StatementOfAccountPage setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public String getName() {
        return name;
    }

    public List<StatementOfAccountVessel> getVessels() {
        return vessels;
    }

    public FinancialOverviewAccountData getAccountInformation() {
        return accountInformation;
    }

    public StatementOfAccountPage setAccountInformation(FinancialOverviewAccountData accountInformation) {
        this.accountInformation = accountInformation;
        return this;
    }

    public StatementOfAccountPage setVessels(List<StatementOfAccountVessel> vessels) {
        this.vessels = vessels;
        return this;
    }

    public List<BankDetail> getBankDetails() {
        return bankDetails;
    }

    public StatementOfAccountPage setBankDetails(List<BankDetail> bankDetails) {
        this.bankDetails = bankDetails;
        return this;
    }

    public List<StatementOfAccountTotal> getVesselTotals() {
        return vesselTotals;
    }

    public StatementOfAccountPage setVesselTotals(List<StatementOfAccountTotal> vesselTotals) {
        this.vesselTotals = vesselTotals;
        return this;
    }


    public List<StatementOfAccountTotal> calculateTotalsAndSortOnCurrency(AccountGroup accountType, List<StatementOfAccountVessel> vessels) {
        List<String> currencies = distinctCurrenciesSorted(vessels);

        List<StatementOfAccountTotal> totals = new ArrayList<>();

        currencies.stream()
                .forEach(currency -> {
                    StatementOfAccountTotal total = new StatementOfAccountTotal();
                    total.setCurrency(currency);
                    total.setOriginalAmount(getTotalForAccountItems(accountType, AccountItemEntity::getOriginalAmount, currency, accountItems));
                    total.setBalance(getTotalForAccountItems(accountType, AccountItemEntity::getBalance, currency, accountItems));
                    total.setDueBalance(getTotalForAccountItems(accountType, AccountItemEntity::getDueBalance, currency, accountItems));
                    totals.add(total);
                });


        return totals;
    }

    protected List<String> distinctCurrenciesSorted(List<StatementOfAccountVessel> rows) {
        List<String> currencies = new ArrayList<>();
        for (StatementOfAccountVessel row : rows) {
            List<StatementOfAccountTotal> invoiceTotals = row.getInvoiceTotals();
            for (StatementOfAccountTotal invoiceTotal : invoiceTotals) {
                currencies.add(invoiceTotal.getCurrency());
            }
        }

        return currencies.stream().distinct().sorted().collect(Collectors.toList());
    }

    public List<AccountItemEntity> getAccountItems() {
        return accountItems;
    }


    public StatementOfAccountPage setAccountItems(List<AccountItemEntity> accItems) {
        this.accountItems = accItems;
        return this;
    }


}
