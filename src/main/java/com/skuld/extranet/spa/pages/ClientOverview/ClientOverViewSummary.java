package com.skuld.extranet.spa.pages.ClientOverview;

import com.skuld.extranet.core.accountOverview.dto.VesselsSummaryDTO;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ClientOverViewSummary {
    private ClientClaimsSummaryDTO clientClaimsSummaryDTO;
    private VesselsSummaryDTO vesselsSummaryDTO;
}
