package com.skuld.extranet.spa.pages;

import com.fasterxml.jackson.annotation.JsonView;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.spa.pages.domain.UrlLink;

import java.util.List;

public class ClaimListPage {

    @JsonView(PageEntityView.ClaimListPage.class)
    List<UrlLink> urlDownloads;

    @JsonView(PageEntityView.ClaimListPage.class)
    List<Claim> claims;




    public List<Claim> getClaims() {
        return claims;
    }

    public ClaimListPage setClaims(List<Claim> claims) {
        this.claims = claims;
        return this;
    }

    public List<UrlLink> getUrlDownloads() {
        return urlDownloads;
    }

    public ClaimListPage setUrlDownloads(List<UrlLink> urlDownloads) {
        this.urlDownloads = urlDownloads;
        return this;
    }
}
