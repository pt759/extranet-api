package com.skuld.extranet.spa.pages.financial;

public class Report {
    private String name;
    private String url;

    public Report(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Report setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Report setUrl(String url) {
        this.url = url;
        return this;
    }
}
