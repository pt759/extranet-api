package com.skuld.extranet.spa.pages.financial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.skuld.extranet.core.common.SkuldLocalDateSerializer;
import com.skuld.extranet.core.financial.domain.deposit.Declaration;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalment;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositStatus;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.ldap.domain.LDAPUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class StatementOfPremiumDepositAccountPage {

    private static final Logger logger = LoggerFactory.getLogger(StatementOfPremiumDepositAccountPage.class.getName());

    private LDAPUser creditController;
    private DepositStatus depositStatus;
    private List<DepositInstalment> instalments = new ArrayList<>();
    private List<InstallmentsEntity> installmentTotals = new ArrayList<>();

    @JsonSerialize(using = SkuldLocalDateSerializer.class)
    private LocalDate declarationLastUpdated;

    private List<Declaration> declarations = new ArrayList<>();
    private List<Declaration> declarationTotals = new ArrayList<>();


    public List<Declaration> getDeclarations() {
        return declarations;
    }

    public StatementOfPremiumDepositAccountPage setDeclarations(List<Declaration> declarations) {
        this.declarations = declarations;
        return this;
    }

    public LDAPUser getCreditController() {
        return creditController;
    }

    public StatementOfPremiumDepositAccountPage setCreditController(LDAPUser creditController) {
        this.creditController = creditController;
        return this;
    }

    @JsonIgnore
    public List<InstallmentsEntity> getInstallmentTotals() {
        return installmentTotals;
    }

    public StatementOfPremiumDepositAccountPage setInstallmentTotals(List<InstallmentsEntity> installmentTotals) {
        this.installmentTotals = installmentTotals;
        return this;
    }

    @JsonIgnore
    public BigDecimal sumDeclarationsBy(String currency, Function<Declaration, BigDecimal> mapFunction ) {

        BigDecimal total = declarations.stream()
                .filter(item -> item.getCurrency().equalsIgnoreCase(currency))
                .map(mapFunction).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public StatementOfPremiumDepositAccountPage setDeclarationTotals(List<Declaration> declarationTotals) {
        this.declarationTotals = declarationTotals;
        return this;
    }

    @JsonIgnore
    public List<Declaration> getDeclarationTotals() {
        return declarationTotals;
    }

    public void setDepositStatus(DepositStatus status) {
        this.depositStatus = status;
    }

    public DepositStatus getDepositStatus() {
        return depositStatus;
    }

    public List<DepositInstalment> getInstalments() {
        return instalments;
    }

    public StatementOfPremiumDepositAccountPage setInstalments(List<DepositInstalment> instalments) {
        this.instalments = instalments;
        return this;
    }

    public LocalDate getDeclarationLastUpdated() {
        return LocalDate.now().minusDays(1);
    }

    public void updateDeclaredValues(DepositStatus status) {
        String currency = status.getCurrency();
        status.setDeclaredSumAmount(sumDeclarationsBy(currency, Declaration::getAmount));
        status.setDeclaredSumTax(sumDeclarationsBy(currency, Declaration::getTaxAmount));
        status.setDeclaredSumAmountInclTax(sumDeclarationsBy(currency, Declaration::getPremiumInclTax));

    }
}
