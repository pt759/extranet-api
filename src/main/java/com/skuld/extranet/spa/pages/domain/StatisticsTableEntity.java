package com.skuld.extranet.spa.pages.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

public class StatisticsTableEntity {

    private String name;
    private String title;
    private Product product;
    private String currency;
    List<StatisticsEntity> data;

    public String getName() {
        return name;
    }

    public StatisticsTableEntity setName(String name) {
        this.name = name;
        return this;
    }

    public List<StatisticsEntity> getData() {
        return data;
    }

    public StatisticsTableEntity setData(List<StatisticsEntity> data) {
        this.data = data;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public StatisticsTableEntity setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getCurrency() {
        if (StringUtils.isEmpty(currency) && !isEmpty(data)) {
            StatisticsEntity statisticsEntity = data.stream().filter(line -> line.getCurrency() != null).findAny().orElse(null);
            this.currency = statisticsEntity != null ? statisticsEntity.getCurrency() : "USD";
        }
        return currency;
    }

    public String getTableProduct(){
        return getProduct().description();
    }

    @JsonIgnore
    public Product getProduct() {
        if (product == null && data != null && data.size() > 0) {
            product = Product.fromString(data.get(0).getProduct());
        }
        return product;
    }


    public StatisticsTableEntity setCurrency(String currency) {
        this.currency = currency;
        return this;
    }


}
