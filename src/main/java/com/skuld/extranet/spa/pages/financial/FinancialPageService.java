package com.skuld.extranet.spa.pages.financial;

import com.skuld.common.domain.YesNo;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.BankDetail;
import com.skuld.extranet.core.financial.domain.deposit.Declaration;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositStatus;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccountData;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccounts;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoice;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoiceItem;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountTotal;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountVessel;
import com.skuld.extranet.core.financial.service.FinancialService;
import com.skuld.extranet.global.SkuldPredicate;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.UrlLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.extranet.core.financial.domain.AccountStatus.active;
import static com.skuld.extranet.core.financial.domain.AccountStatus.open;
import static com.skuld.extranet.global.RESTendPoints.*;
import static com.skuld.extranet.spa.pages.domain.UrlLink.CODE_INSTALMENT_REPORT;
import static com.skuld.extranet.spa.pages.domain.UrlLink.CODE_SOA;

@Service
public class FinancialPageService {

    final static Logger logger = LoggerFactory.getLogger(FinancialPageService.class);

    @Value("${financial.account.active.daysSinceLastTransactionLimit}")
    private int daysSinceLastTransactionLimit;

    @Value("${financial.account.active.daysSinceLastTransactionDepositLimit}")
    private int daysSinceLastTransactionDepositLimit;


    private FinancialService financialService;

    @Autowired
    public FinancialPageService(FinancialService financialService){
        this.financialService = financialService;
    }

    public FinancialOverviewPage getFinancialOverview(String statCode) {
        FinancialOverviewPage page = new FinancialOverviewPage();

        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodeAccountsCached(statCode);
        List<AccountEntity> allAccounts = financialService.getAllAccountsFor(statCodeAccounts);
        FinancialOverviewAccounts financialOverviewAccountsFor = getFinancialOverviewAccountsFor(statCodeAccounts, allAccounts);

        page.setFinancialOverviewAccounts(financialOverviewAccountsFor);
        page.setCreditControllers(getFinancialCreditControllersFor(statCodeAccounts));
        return page;
    }

    public StatementOfAccountPage getStatementOfAccount(String statCode, String accountNumber, AccountItemEntity.ItemType itemType, YesNo deposit) {
        StatementOfAccountPage soa = new StatementOfAccountPage();
        try {

            FinancialOverviewAccountData overviewAccount = financialService.getOverviewAccount(accountNumber);
            soa.setAccountInformation(overviewAccount);

            List<AccountItemEntity> accItems = financialService.getAccountItemsFor(accountNumber, itemType, deposit);
            List<StatementOfAccountVessel> vessels = financialService.convertAccItemsToAccountVessel(accItems);

            updateVesselAccessToEvent(statCode, vessels);

            soa.setAccountItems(accItems);
            soa.setVessels(vessels);

            List<StatementOfAccountTotal> totals = soa.calculateTotalsAndSortOnCurrency(overviewAccount.getAccountType(), soa.getVessels());
            soa.setVesselTotals(totals);

            soa.setUrlDownloads(generateDownloadUrls(statCode, accountNumber, itemType));

            List<String> currencies = financialService.currenciesForInvoices(accItems);
            List<BankDetail> bankDetails = financialService.getBankDetailsByCurrencies(accountNumber, itemType, deposit, financialService.onHoldItems(accountNumber), currencies);
            soa.setBankDetails(bankDetails);
        } catch (Exception ex) {
            logger.info("Exception: " + ex.getMessage());
        }
        return soa;
    }

    private void updateVesselAccessToEvent(String statCode, List<StatementOfAccountVessel> vessels) {
        vessels.stream().forEach(v->updateInvoiceAccessToEvent(v.getInvoices(),statCode));
    }

    private void updateInvoiceAccessToEvent(List<StatementOfAccountInvoice> invoices, String statCode) {
        invoices.stream().forEach(i-> updateInvoiceItemsAccessToEvent(i.getDetails(),statCode));
    }

    private void updateInvoiceItemsAccessToEvent(List<StatementOfAccountInvoiceItem> items, String statCode) {
        items.stream().forEach(i -> updateInvoiceItemAccessToEvent(i, statCode));
    }

    private void updateInvoiceItemAccessToEvent(StatementOfAccountInvoiceItem item, String statCode) {
        item.setEventNoAccess(financialService.doStatCodeHaveAccessToEvent(statCode,item.getEventNo()));
    }

    private List<UrlLink> generateDownloadUrls(String statCode, String accountNo, AccountItemEntity.ItemType itemType) {
        List<UrlLink> list = new ArrayList<>();
        list.add(new UrlLink("Statement of Account", urlStatementOfAccount(statCode, accountNo, itemType), CODE_SOA + UrlLink.CODE_PDF));
        list.add(new UrlLink("Statement of Account", urlStatementOfAccountExcel(statCode, accountNo), CODE_SOA + UrlLink.CODE_EXCEL));

        if (getAccountProducts(accountNo).contains(Product.ownerMutual)) {
            list.add(new UrlLink("Instalment report", urlInstallmentReport(statCode, accountNo), CODE_INSTALMENT_REPORT + UrlLink.CODE_PDF));
        }
        return list;
    }

    public FinancialPageService setFinancialService(FinancialService financialService) {
        this.financialService = financialService;
        return this;
    }

    private List<LDAPUser> getFinancialCreditControllersFor(List<StatCodeAccountEntity> statCodeAccounts) {
        return financialService.getCreditControllersForStatCode(statCodeAccounts);
    }

    protected FinancialOverviewAccounts getFinancialOverviewAccountsFor(List<StatCodeAccountEntity> statCodeAccounts, List<AccountEntity> allAccounts) {
        List<FinancialOverviewAccountData> allOverviewAccountData = getFinancialOverviewAccountData(statCodeAccounts, allAccounts);
        FinancialOverviewAccounts overviewAccounts = new FinancialOverviewAccounts();
        overviewAccounts.setAllAccounts(allOverviewAccountData);
        return overviewAccounts;
    }

    protected List<FinancialOverviewAccountData> getFinancialOverviewAccountData(List<StatCodeAccountEntity> statCodeAccounts, List<AccountEntity> allAccounts) {
        List<FinancialOverviewAccountData> allOverviewAccountData = new ArrayList<>();
        statCodeAccounts.stream()
                .forEach(statCodeAccountEntity -> {
                            FinancialOverviewAccountData account = findOverviewAccountData(statCodeAccountEntity, allAccounts);
                            if (account.isStatus(open) || account.isStatus(active)) {
                                allOverviewAccountData.add(account);
                            }
                        }
                );
        return allOverviewAccountData;
    }

    protected FinancialOverviewAccountData findOverviewAccountData(StatCodeAccountEntity statCodeAccountEntity, List<AccountEntity> allAccounts) {
        AccountEntity account = financialService.matchAccount(statCodeAccountEntity.getAccountNo(), allAccounts);
        List<AccountSum> accountSum = financialService.getAccountSumSortedByCurrency(statCodeAccountEntity.getAccountNo());
        FinancialOverviewAccountData accountData = FinancialOverviewAccountData.from(account);
        accountData.setPolicyHolder(statCodeAccountEntity.getPolicyHolder());
        accountData.setPolicyHolderId(statCodeAccountEntity.getPolicyHolderId());
        accountData.setAccountSum(accountSum);

        if (accountData.isDeposit() && !accountData.hasAccountBalance()){
            List<AccountSum> depositBalanceSums = financialService.getAccountSumSortedByCurrencyForDepositBalance(account.getAccountNo());
            accountData.setDepositAccountSum(depositBalanceSums);
        }

        accountData.calculateStatus(daysSinceLastTransactionLimit,daysSinceLastTransactionDepositLimit);

        return accountData;
    }

    @Deprecated
    public ReportGenerator getReportGenerator(List<FinancialOverviewAccountData> financialOverviewAccounts, List<AccountEntity> allAccounts) {

        ReportGenerator generator = new ReportGenerator();
        List<CodeValue> accounts = new ArrayList<>();
        List<CodeValue> policyHolders = new ArrayList<>();

        financialOverviewAccounts.stream().forEach(i -> {
            AccountEntity account = financialService.matchAccount(i.getAccountNo(), allAccounts);
            accounts.add(new CodeValue(account.getAccountNo(), account.getName()));
            policyHolders.add(new CodeValue(i.getPolicyHolderId(), i.getPolicyHolder()));
        });

        generator.setAccounts(accounts);
        generator.setPolicyHolders(distinctPolicyHolders(policyHolders));
        generator.setReports(financialService.getReports());

        return generator;
    }

    private List<CodeValue> distinctPolicyHolders(List<CodeValue> policyHolder) {
        return policyHolder.stream()
                .filter(SkuldPredicate.distinctByKey(CodeValue::getValue))
                .collect(Collectors.toList());
    }

    public StatementOfPremiumDepositAccountPage getDepositStatementOfAccount(String accountNo) {

        StatementOfPremiumDepositAccountPage page = new StatementOfPremiumDepositAccountPage();
        page.setDeclarations(getDeclarationsFiltered(accountNo));
        DepositStatus depositAccountStatus = financialService.getDepositAccountStatus(accountNo);
        page.updateDeclaredValues(depositAccountStatus);
        page.setDepositStatus(depositAccountStatus);
        page.setInstalments(financialService.getDepositInstalmentOverview(accountNo));

        page.setCreditController(findCreditController(accountNo));

        return page;

    }

    private LDAPUser findCreditController(String accountNo) {
        AccountEntity account = financialService.getAccount(accountNo);
        return financialService.findCreditController(account.getCreditControllerPtUserId());

    }

    private List<Product> getAccountProducts(String accountNo) {
        return financialService.getAccountProducts(accountNo);
    }

    protected List<Declaration> getDeclarationsFiltered(String accountNo) {

        List<CharterDeliveryReportEntity> charterDeclarations = financialService.getAllCharterDeclarations(accountNo);

        List<Declaration> list = new ArrayList<>();
        charterDeclarations.stream()
                .filter(item -> item.getPremium() != null && item.getPremium().abs().compareTo(BigDecimal.ZERO) > 0)
                .forEach(item -> {
                    Declaration declaration = Declaration.createFrom(item);
                    list.add(declaration);
                });

        return list.stream().sorted(Comparator.comparing(Declaration::getVesselName)).collect(Collectors.toList());

    }

    public void setDaysSinceLastTransactionLimit(int daysLimit) {
        this.daysSinceLastTransactionLimit = daysLimit;
    }

    public void setDaysSinceLastTransactionDepositLimit(int daysLimit) {
        this.daysSinceLastTransactionDepositLimit = daysLimit;
    }
}
