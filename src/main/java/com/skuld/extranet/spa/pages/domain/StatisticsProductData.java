package com.skuld.extranet.spa.pages.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skuld.extranet.core.common.domain.CodeValueWithList;
import com.skuld.extranet.core.common.domain.GraphData;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatisticsProductData {

    private String productCode;
    private List<CodeValueWithList> statYearsFilter;
    private Map<String, List<StatisticsTableEntity>> tables;

    @JsonIgnore
    private List<ClaimStatisticsEntity> totalClaimsForCurrentYearByGroup;

    private List<ClaimStatisticsEntity> totalClaimFiveYearByGroup;

    private List<GraphData> lossRatio5YearStatistics;
    private List<UrlLink> pdfUrls;
    private List<UrlLink> excelUrls;


    public StatisticsProductData() {


        this.tables = new HashMap<>();
        this.pdfUrls = new ArrayList<>();
        this.excelUrls = new ArrayList<>();
    }

    public String getProductCode() {
        return productCode;
    }

    public StatisticsProductData setProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public List<ClaimStatisticsEntity> getTotalClaimsForCurrentYearByGroup() {
        return totalClaimsForCurrentYearByGroup;
    }

    public StatisticsProductData setTotalClaimsForCurrentYearByGroup(List<ClaimStatisticsEntity> totalClaimsForCurrentYearByGroup) {
        this.totalClaimsForCurrentYearByGroup = totalClaimsForCurrentYearByGroup;
        return this;
    }

    public List<ClaimStatisticsEntity> getTotalClaimFiveYearByGroup() {
        return totalClaimFiveYearByGroup;
    }

    public StatisticsProductData setTotalClaimFiveYearByGroup(List<ClaimStatisticsEntity> totalClaimFiveYearByGroup) {
        this.totalClaimFiveYearByGroup = totalClaimFiveYearByGroup;
        return this;
    }

    public List<GraphData> getLossRatio5YearStatistics() {
        return lossRatio5YearStatistics;
    }

    public StatisticsProductData setLossRatio5YearStatistics(List<GraphData> lossRatio5YearStatistics) {
        this.lossRatio5YearStatistics = lossRatio5YearStatistics;
        return this;
    }

    public Map<String, List<StatisticsTableEntity>> getTables() {
        return tables;
    }

    public StatisticsProductData setTables(Map<String, List<StatisticsTableEntity>> tables) {
        this.tables = tables;
        return this;
    }

    public List<StatisticsTableEntity> getDataTable(StatYearType type) {
        return tables == null ? null : tables.get(type.getCode());
    }

    public List<UrlLink> getPdfUrls() {
        return pdfUrls;
    }

    public StatisticsProductData setPdfUrls(List<UrlLink> pdfUrls) {
        this.pdfUrls = pdfUrls;
        return this;
    }

    public List<UrlLink> getExcelUrls() {
        return excelUrls;
    }

    public StatisticsProductData setExcelUrls(List<UrlLink> excelUrls) {
        this.excelUrls = excelUrls;
        return this;
    }

    public List<CodeValueWithList> getStatYearsFilter() {
        return statYearsFilter;
    }

    public StatisticsProductData setStatYearsFilter(List<CodeValueWithList> statYearsFilter) {
        this.statYearsFilter = statYearsFilter;
        return this;
    }


}
