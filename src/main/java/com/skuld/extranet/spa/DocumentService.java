package com.skuld.extranet.spa;

import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.global.RESTendPoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;


public class DocumentService {
    final static Logger logger = LoggerFactory.getLogger(DocumentService.class);

    public List<DocumentRef> sortDocumentListAfterFilename(List<DocumentRef> list) {
        return list.stream().sorted(comparing(DocumentRef::getFileName)).collect(Collectors.toList());
    }

    protected Map<String, List<DocumentRef>> groupDocumentsByProgramId(List<DocumentRef> docs) {
        Map<String, List<DocumentRef>> programIdDocuments = docs.stream().collect(Collectors.groupingBy(DocumentRef::getProgramIdAndCladd));

        updateWithMostRecentVersionForStartDate(programIdDocuments);

        //Sort each
        programIdDocuments.forEach((k, v) -> {
            v = v.stream().sorted(comparing(DocumentRef::getRequestId).reversed()).collect(Collectors.toList());
            programIdDocuments.put(k, v);
        });

        return programIdDocuments;
    }

    private void updateWithMostRecentVersionForStartDate(Map<String, List<DocumentRef>> programIdDocuments) {
        programIdDocuments.forEach((key, docByProgramId) -> {

            List<DocumentRef> documents = new ArrayList<>();
            List<String> startDateList = getDistinctStartDates(docByProgramId);
            for (String startDate : startDateList) {
                DocumentRef maxRequestIdDocument = docByProgramId.stream()
                        .filter(line -> line.getStartDateAsString().equalsIgnoreCase(startDate))
                        .max(Comparator.comparing(DocumentRef::getRequestIdAsInt)).orElse(null);
                documents.add(maxRequestIdDocument);

            }
            programIdDocuments.put(key, documents);
        });
    }

    private List<String> getDistinctStartDates(List<DocumentRef> docByProgramId) {
        return docByProgramId.stream().map(DocumentRef::getStartDateAsString).distinct().collect(Collectors.toList());
    }

    public String convertDocumentUrl(String statCode, DocumentRef document) {
        return RESTendPoints.archive + RESTendPoints.archive_StatCode_DocumentKey
                .replace(RESTendPoints.pathVariableStatCode, statCode)
                .replace(RESTendPoints.pathVariableDocumentKey, document.getRequestId());
    }

}
