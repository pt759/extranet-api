package com.skuld.extranet.spa;

import com.skuld.extranet.core.accountOverview.AccountOverviewService;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.accountOverview.dto.VesselsSummaryDTO;
import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.api.vessel.service.VesselClientService;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.claim.service.ClaimsClientService;
import com.skuld.extranet.core.common.SkCommon;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonusService;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverViewSummary;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverviewPage;
import com.skuld.extranet.spa.pages.domain.SearchResult;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@Service
public class SearchService {

    private final ClaimService claimService;
    private final ClaimsClientService claimsClientService;
    private final VesselClientService vesselClientService;
    private final AccountOverviewService accountOverviewService;
    private final AzureAdUserService azureAdUserService;
    private final MemberPerformanceBonusService memberPerformanceBonusService;

    @Autowired
    public SearchService(ClaimService claimService,
                         ClaimsClientService claimsClientService, VesselClientService vesselClientService,
                         AccountOverviewService accountOverviewService,
                         AzureAdUserService azureAdUserService,
                         MemberPerformanceBonusService memberPerformanceBonusService) {
        this.claimService = claimService;
        this.claimsClientService = claimsClientService;
        this.vesselClientService = vesselClientService;
        this.accountOverviewService = accountOverviewService;
        this.azureAdUserService = azureAdUserService;
        this.memberPerformanceBonusService = memberPerformanceBonusService;
    }

    public SearchResult searchBroker(String webQuery, String statCodeGroupLogin, int limit) {
        SearchResult mainSearchResult = new SearchResult();
        // make a search for every client the broker has

        if (StringUtils.isNotBlank(webQuery)) {

            String query = webQuery.toLowerCase();

            List<StatCodeDTO> statCodes = azureAdUserService.getStatCodesForGroup(statCodeGroupLogin);
            List<StatCodeDTO> statCodesFiltered = statCodes
                    .stream()
                    .filter(line -> line.getStatCodeName().toLowerCase().contains(query))
                    .collect(Collectors.toList());
            List<AccountOverview> clientResult = accountOverviewService.convertToAccountOverview(statCodesFiltered);

            List<Claim> allClaims = new ArrayList<>();
            List<Vessel> allVessels = new ArrayList<>();
            List<StatCodeDTO> allStatCodes = new ArrayList<>();
            List<SearchResult> searchClientResult = searchInAllClients(query, statCodes);

            for (SearchResult searchResult : searchClientResult) {
                allClaims.addAll(searchResult.getClaims());
                allVessels.addAll(searchResult.getVessels());
                allStatCodes.add(findStatCode(searchResult.getStatCode(), statCodes));
            }

            findStatNameForAll(allVessels, allStatCodes);

            mainSearchResult.setBrokerFlag(true);
            mainSearchResult.setVessels(sortLimitCombinedVesselResult(limit, allVessels));
            mainSearchResult.setClaims(sortLimitCombinedClaimResult(limit, allClaims));
            mainSearchResult.setClients(sortLimitClientResult(limit, clientResult));
        }
        return mainSearchResult;
    }

    private void findStatNameForAll(List<Vessel> allVessels, List<StatCodeDTO> statCodes) {
        allVessels.stream().forEach(line -> line.setStatCodeName(findStatCodeName(line.getStatCode(), statCodes)));
    }

    private String findStatCodeName(String statCode, List<StatCodeDTO> statCodes) {
        return statCodes.stream()
                .filter(line -> line.getStatCode()
                        .equalsIgnoreCase(statCode))
                .map(StatCodeDTO::getStatCodeName)
                .findAny()
                .orElse(SkCommon.unknown);
    }

    private StatCodeDTO findStatCode(String statCode, List<StatCodeDTO> statCodes) {
        for (StatCodeDTO code : statCodes) {
            if (code.getStatCode().equalsIgnoreCase(statCode)) {
                return code;
            }
        }
        return null;
    }

    private List<AccountOverview> sortLimitClientResult(int limit, List<AccountOverview> list) {
        // Sort and Limit Combined Result Clients
        return list.stream()
                .sorted(Comparator.comparing(AccountOverview::getClientName))
                .limit(limit)
                .collect(Collectors.toList());
    }

    private List<Claim> sortLimitCombinedClaimResult(int limit, List<Claim> claims) {
        // Sort and Limit Combined Result Claims
        return claims.stream()
                .sorted(Comparator.comparing(Claim::getIncidentDate)
                        .reversed())
                .limit(limit)
                .collect(Collectors.toList());
    }

    private List<Vessel> sortLimitCombinedVesselResult(int limit, List<Vessel> vessels) {
        // Sort and Limit Combined Result Vessel
        return vessels.stream()
                .sorted(Comparator.comparing(Vessel::getVesselName))
                .limit(limit)
                .collect(Collectors.toList());
    }

    private List<SearchResult> searchInAllClients(String query, List<StatCodeDTO> statCodes) {
        // Search each of the Brokers Client for Claims and Vessels
        List<SearchResult> brokerSearchResult = new ArrayList<>();
        for (StatCodeDTO clientStatCode : statCodes) {
            log.info("Searching for statcode: " + clientStatCode);
            SearchResult res = searchClient(query, clientStatCode.getStatCode());
            log.info("Result: " + res.toString());
            brokerSearchResult.add(res);
        }
        return brokerSearchResult;
    }

    public SearchResult searchClient(String query, String statCode) {
        if (StringUtils.isEmpty(StringUtils.trim(query)) || StringUtils.isEmpty(StringUtils.trim(statCode))) {
            log.info("Search {" + query + "} is null ( Statcode: " + statCode + " )");
            return null;
        }

        String search = StringUtils.upperCase(query).trim();
        log.info("Searching for " + statCode + " with : '" + search + "'");

        SearchResult searchResult = new SearchResult();
        searchResult.setStatCode(statCode);
        searchResult.setBrokerFlag(false);

        List openClaims = findOpenClaims(search, statCode);
        searchResult.setClaims(openClaims);

        List<Vessel> vessels = findVessels(statCode, search);
        log.info("Vessels found: " + vessels.size());
        searchResult.setVessels(vessels);
        return searchResult;
    }

    private List<Vessel> findVessels(String statCode, String search) {
        return vesselClientService.searchForVessel(new VesselFilter(statCode, search));
    }

    private List findOpenClaims(String query, String statCode) {
        List<Claim> claimsForStatCode = claimService.claimListOpen(statCode);
        List<Claim> filteredClaims = claimService.filterClaimsOn(query, claimsForStatCode);
        return filteredClaims;
    }

    public ClientOverviewPage clientOverviewPage(String statCode) {
        ClientOverviewPage page = new ClientOverviewPage();

        page.setClientOverViewSummary(getClientSummary(statCode));
        page.setVessels(vesselClientService.vesselsFor(statCode));
        page.setOpenClaims(claimService.claimListOpen(statCode));
        page.setMemberPerformanceBonus(memberPerformanceBonusService.getMemberPerformanceBonus(statCode));
        page.setClientOverViewSummary(getClientSummary(statCode));
        LDAPUser ldapUser = accountOverviewService.getAccountManager(statCode);
        page.setAccountManager(ldapUser);
        return page;
    }

    private ClientOverViewSummary getClientSummary(String statCode) {
        return ClientOverViewSummary.builder()
                .clientClaimsSummaryDTO(getClaimsOverviewSummary(statCode))
                .vesselsSummaryDTO(getVesselSummary(statCode))
                .build();
    }

    private VesselsSummaryDTO getVesselSummary(String statCode) {
        return VesselsSummaryDTO.builder()
                .totalVessels(getNumberOfVesselsFor(statCode))
                .totalNetPremium(getStatCodeNetPremium(statCode))
                .build();
    }

    private String getStatCodeNetPremium(String statCode) {
        return accountOverviewService.getTotalNetPremium(Set.of(statCode));
    }

    private long getNumberOfVesselsFor(String statCode) {
        return vesselClientService.vesselsFor(statCode)
                .stream()
                .map(VesselListDTO::getImo)
                .distinct()
                .count();
    }

    protected ClientClaimsSummaryDTO getClaimsOverviewSummary(String statCode) {
        return claimsClientService.getClaimOverviewSummary(statCode);
    }
}
