package com.skuld.extranet.spa;

import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import com.skuld.extranet.excel.ExcelFormat;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.StatisticsTableEntity;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class StatisticsExcelConverter extends ExcelFormat {

    final static Logger logger = LoggerFactory.getLogger(StatisticsExcelConverter.class);

    public StatisticsExcelConverter(String filename, String sheetName) {
        super(filename, sheetName);
    }

    public void export(List<StatisticsTableEntity> data, HttpServletResponse response) {
        try {
            response.addHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");
            streamWorkbookFrom(data, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            logger.error("export(): " + e.getMessage());
        }
    }

    public String getExcelAsBase64Text(List<StatisticsTableEntity> data) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Workbook workBook = createSheet(getSheetName(), data);
            workBook.write(out);
            workBook.close();
            out.close();

            String base64Text = Base64.encodeBase64String(out.toByteArray());
//            logger.info("Base64Text: " + base64Text);
            return base64Text;
        } catch (Exception e) {
            logger.error("getExcelAsBase64Text(): " + e.getMessage());
        }

        return "";
    }

    public void streamWorkbookFrom(List<StatisticsTableEntity> data, OutputStream outputStream) throws IOException {
        Workbook workBook = createSheet(getSheetName(), data);
        workBook.write(outputStream);
        workBook.close();
        outputStream.close();
    }

    public Workbook createSheet(String sheetName, List<StatisticsTableEntity> tableEntities) {

        StatisticsTableEntity firstTableEntity = tableEntities.get(0);

        logger.info("**** FDD must come last, or find the correct table here");
        List<StatisticsEntity> list = firstTableEntity.getData();
        Product tableProduct = firstTableEntity.getProduct();

        Sheet sheet = workbook.createSheet(sheetName);
        Integer currentRow = 0;
        String title1 = list.get(0).getProductClass();
        currentRow = populateRowTitle(currentRow, title1+ " (" + firstTableEntity.getCurrency() + ")", sheet);
        currentRow = populateRowHeader(currentRow, tableProduct, sheet);
        currentRow = populateDataRows(currentRow, tableProduct, list, sheet);
        currentRow = populateSumRows(currentRow, title1, tableProduct, list, sheet);
        Integer sumRow1 = currentRow;

        if (tableEntities.size() > 1) {
            currentRow++; // eXtra Space.

            list = tableEntities.get(1).getData();
            String title2 = list.get(0).getProductClass();
            currentRow = populateRowTitle(currentRow, title2+ " (" + firstTableEntity.getCurrency() + ")", sheet);
            currentRow = populateRowHeader(currentRow, tableProduct, sheet);
            currentRow = populateDataRows(currentRow, tableProduct, list, sheet);
            currentRow = populateSumRows(currentRow, title2, tableProduct, list, sheet);
            Integer sumRow2 = currentRow;

            // Total sum row
            currentRow = populateTotalSumRows("Total " + title1 + " and " + title2, sumRow1, sumRow2, tableProduct, currentRow, sheet);
        }

        autoSize(1, sheet);
        return workbook;
    }

    private Integer populateTotalSumRows(String title, Integer sumRow1, Integer sumRow2, Product tableProduct, Integer currentRow, Sheet sheet) {
        currentRow++;
        Row row = sheet.createRow(currentRow);

        CellStyle style = styleHelper.styleTwoBorderStyle();

        Integer column = 0;
        styleHelper.createCellWithStyle(column++, title, style, row);
        createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
        createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
        createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
        createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
        createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
        if (tableProduct.equals(Product.owner)) {
            createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
            createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
            createSumOfTwoTables(sumRow1, sumRow2, column++, style, row);
        }
        createLossRatioCell(column++, tableProduct, styleHelper.styleTwoBorderPercentage(), row);

        return currentRow++;
    }


    private void createSumOfTwoTables(Integer sumRow1, Integer sumRow2, Integer column, CellStyle style, Row row) {
        Cell cell = styleHelper.createFormulaCell(column, style, row);
        String columnReference = CellReference.convertNumToColString(cell.getColumnIndex());

        String cellFormula = columnReference + sumRow1 + "+" + columnReference + sumRow2;

        cell.setCellType(CellType.FORMULA);
        cell.setCellFormula(cellFormula);
    }


    private Integer populateSumRows(Integer currentRow, String title, Product tableProduct, List<StatisticsEntity> table, Sheet sheet) {

        Integer startRow = (currentRow) - (table.size() - 1);
        Integer endRow = currentRow;

        Row row = sheet.createRow(currentRow);

        Integer column = 0;//start from second column - we don't want to include Year in Sum.

        styleHelper.createCellWithStyle(column++, "Total " + title, styleHelper.styleTwoBorderStyle(), row);
        createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
        createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
        createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
        createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
        createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
        if (tableProduct.equals(Product.owner)) {
            createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
            createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
            createSumCell(column++, startRow, endRow, styleHelper.styleTwoBorderStyle(), row);
        }

        createLossRatioCell(column++, tableProduct, styleHelper.styleTwoBorderPercentage(), row);

        return ++currentRow;
    }

    private void createLossRatioCell(Integer column, Product tableProduct, CellStyle style, Row row) {
        Cell cell = styleHelper.createFormulaCell(column, style, row);

        Integer currentFormulaRow = cell.getRow().getRowNum() + 1;
        String cellFormula;
        if (tableProduct.equals(Product.owner)) {
            cellFormula = "SUM(G3:I3) /F3".replace("3", currentFormulaRow.toString());
        } else {
            cellFormula = "(E3+F3)/D3".replace("3", currentFormulaRow.toString());
        }
        cell.setCellType(CellType.FORMULA);
        cell.setCellFormula(cellFormula);
    }



    protected Integer populateRowTitle(Integer currentRow, String title, Sheet sheet) {
        styleHelper.createCellWithStyle(0, title, styleHelper.headerStyleSimple(), sheet.createRow(currentRow));
        return ++currentRow;
    }

    protected Integer populateRowHeader(Integer currentRow, Product tableProduct, Sheet sheet) {
        Row rowHeader = sheet.createRow(currentRow);
        CellStyle style = styleHelper.headerStyleWithSolidRedBackground();

        Integer column = 0;

        styleHelper.createCellWithStyle(column++, "Policy year", style, rowHeader);
        styleHelper.createCellWithStyle(column++, "Weighted GT", style, rowHeader);
        styleHelper.createCellWithStyle(column++, "Weighted no Vessels", style, rowHeader);
        styleHelper.createCellWithStyle(column++, "Net Premium", style, rowHeader);

        if (tableProduct.equals(Product.owner)) {
            styleHelper.createCellWithStyle(column++, "Excess Reinsurance", style, rowHeader);
            styleHelper.createCellWithStyle(column++, "Net Retained premium", style, rowHeader);
        }

        styleHelper.createCellWithStyle(column++, "Payments", style, rowHeader);
        styleHelper.createCellWithStyle(column++, "Reserves", style, rowHeader);

        if (tableProduct.equals(Product.owner)) {
            styleHelper.createCellWithStyle(column++, "Pool Claims", style, rowHeader);
        }
        Cell cell = styleHelper.createCellWithStyle(column++, "Loss Ratio", style, rowHeader);
        return ++currentRow;
    }

    protected Integer populateDataRows(Integer currentRow, Product tableProduct, List<StatisticsEntity> table, Sheet sheet) {

        for (StatisticsEntity data : table) {
            Row row = sheet.createRow(currentRow);
            Integer column = 0;
            currentRow++;
            styleHelper.createCellWithStyle(column++, data.getPolicyYear(), styleHelper.styleGeneric(), row);
            styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getWeightedGt()), styleHelper.styleNumericThousand(), row);
            styleHelper.createNumericCellWithStyle(column++, Double.toString(data.getWeightedNoVessels()), styleHelper.styleNumericThousand(), row);
            styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getNetPremium()), styleHelper.styleNumericThousand(), row);
            if (tableProduct.equals(Product.owner)) {
                styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getExcessReinsurance()), styleHelper.styleNumericThousand(), row);
                styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getNetRetainedPremium()), styleHelper.styleNumericThousand(), row);
            }
            styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getPayments()), styleHelper.styleNumericThousand(), row);
            styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getReserves()), styleHelper.styleNumericThousand(), row);
            if (tableProduct.equals(Product.owner)) {
                styleHelper.createNumericCellWithStyle(column++, Long.toString(data.getPoolClaims()), styleHelper.styleNumericThousand(), row);
            }
            styleHelper.createNumericCellWithStyle(column++, data.getLossRatio().toString(), styleHelper.stylePercentage(), row);
        }
        return currentRow;
    }

}
