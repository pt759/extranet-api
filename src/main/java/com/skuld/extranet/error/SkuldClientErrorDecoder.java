package com.skuld.extranet.error;

import com.skuld.errorhandling.MessageLevel;
import com.skuld.errorhandling.exceptions.SkuldClientException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.nio.charset.Charset;

public class SkuldClientErrorDecoder implements ErrorDecoder {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Exception decode(String methodKey, Response response) {

        String body = getResponseBody(methodKey, response);

        if (response.status() == 500) {

            final SkuldClientException skuldClientException = new SkuldClientException(
                    "client/decode/not-implemented"
                    , "Unable to perform request"
                    , "" + HttpStatus.valueOf(response.status())
                    , MessageLevel.ERROR
                    , response.request().toString());

            return skuldClientException;
        }

        return new SkuldClientException("/client/decode/", "Unable to perform request", "" + HttpStatus.valueOf(response.status()) + " for " + methodKey, MessageLevel.ERROR, body);

    }

    public String getResponseBody(String methodKey, Response response) {
        String body;
        try {
            body = IOUtils.toString(response.body().asReader(Charset.defaultCharset()));
            logger.debug("Got {} response from {}, response body: {}", response.status(), methodKey, body);
        } catch (IOException e) {
            body = "";
            logger.debug("Got {} response from {}, response body could not be read", response.status(), methodKey);
        }
        return body;
    }


}

