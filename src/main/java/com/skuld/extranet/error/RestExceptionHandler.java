package com.skuld.extranet.error;

import com.skuld.errorhandling.dto.ApiMessageDTO;
import com.skuld.errorhandling.exceptions.SkuldClientException;
import com.skuld.errorhandling.exceptions.SkuldErrorException;
import com.skuld.errorhandling.exceptions.SkuldException;
import com.skuld.errorhandling.exceptions.SkuldValidationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.skuld.errorhandling.MessageLevel.ERROR;
import static com.skuld.global.GlobalText.txtSystemAdministrator;

@Log4j2
@ControllerAdvice
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    //     All Exceptions
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {

        ApiMessageDTO apiMessage = ApiMessageDTO.builder()
                .message(txtSystemAdministrator)
                .debugMessage(ex.getMessage())
                .messageLevel(ERROR)
                .url(request.getDescription(false))
                .build();

        log.error(apiMessage.getLogId() + ", " + apiMessage.toString() + ", " + ex.getClass() + ",", ex);

        return new ResponseEntity(apiMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {


        ApiMessageDTO apiMessage = ApiMessageDTO.builder()
                .message(txtSystemAdministrator)
                .debugMessage(ex.getMessage())
                .messageLevel(ERROR)
                .url(request.getDescription(false))
                .build();

        log.error("handleMethodArgumentNotValid: " + apiMessage.getLogId() + ", " + apiMessage.toString() + ", " + ex.getClass() + ",", ex);
        return new ResponseEntity(apiMessage, HttpStatus.BAD_REQUEST);
    }

    @Override
    public final ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiMessageDTO apiMessage = ApiMessageDTO.builder()
                .message(txtSystemAdministrator)
                .debugMessage("Some object is missing getters/setters:" + ";" + ex.getMessage())
                .messageLevel(ERROR)
                .url(request.getDescription(false)).build();

        log.error("handleHttpMediaTypeNotAcceptable: " + apiMessage.getLogId() + ", " + apiMessage.toString() + ", " + ex.getClass() + ",", ex);

        return new ResponseEntity<>(apiMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({
            SkuldClientException.class,
            SkuldValidationException.class,
            SkuldErrorException.class}
    )
    public final ResponseEntity<ApiMessageDTO> handleSkuldException(SkuldException ex, WebRequest request) {
        ApiMessageDTO apiMessage = fromException(ex.getMessage(), ex, request);
        log.error(ex.getClass().getSimpleName() + " : " + apiMessage.getLogId());
        log.error(apiMessage.toString(), ex.getData());
        return new ResponseEntity<>(apiMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ApiMessageDTO fromException(String title, SkuldException ex, WebRequest request) {
        return ApiMessageDTO.builder()
                .title(title)
                .type(ex.getType())
                .message(ex.getMessage())
                .debugMessage(ex.getSystemMessage())
                .messageLevel(ex.getMessageLevel())
                .url(request.getDescription(false))
                .data(ex.getData() instanceof Exception ? null : ex.getData())
                .build();
    }

}
