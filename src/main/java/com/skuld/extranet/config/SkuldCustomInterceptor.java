package com.skuld.extranet.config;

import com.skuld.common.security.service.JwtService;
import com.skuld.extranet.security.service.UserAccessService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.skuld.extranet.global.GlobalText.NO_DATA_ACCESS;
import static com.skuld.extranet.global.GlobalText.NO_ENDPOINT_ACCESS;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

@Log4j2
public class SkuldCustomInterceptor implements HandlerInterceptor {

    private final JwtService jwtService;

    @Autowired
    private UserAccessService userAccessService;

    public SkuldCustomInterceptor(UserAccessService userAccessService,
                                  JwtService jwtService) {
        this.userAccessService = userAccessService;
        this.jwtService = jwtService;
    }

    public SkuldCustomInterceptor() {
        this.jwtService = new JwtService();
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        if (jwtByPass(httpServletRequest)) {
            return true;
        }
        return hasTokenAccess(httpServletRequest, httpServletResponse);
    }

    protected boolean hasTokenAccess(HttpServletRequest httpServletRequest, HttpServletResponse response) throws IOException {
        boolean hasAccess;
        if (jwtService.verifyTokenFromRequest(httpServletRequest)) {
            hasAccess = userAccessService.hasAccess(httpServletRequest);
            if (hasAccess == false) {
                log.info(userAccessService.getUrl(httpServletRequest) + " " + NO_DATA_ACCESS);
                response.sendError(SC_FORBIDDEN, NO_DATA_ACCESS);
            }
        } else {
            hasAccess = false;
            log.info(userAccessService.getUrl(httpServletRequest) + " " + NO_ENDPOINT_ACCESS);
            response.sendError(SC_UNAUTHORIZED, NO_ENDPOINT_ACCESS);
        }
        return hasAccess;
    }

    protected boolean jwtByPass(HttpServletRequest httpServletRequest) {
        boolean jwtIsValid = false;

        if (isEndpointOpen(httpServletRequest) || hasApiKey(httpServletRequest) || isAccessesControlDisabled() || isTestToken(httpServletRequest)) {
            jwtIsValid = true;

            log.info(userAccessService.getUrl(httpServletRequest) + " is open by open-endpoint, api-key  or disabled access control. " + " Host: " + userAccessService.getRemoteAddress(httpServletRequest));
        }
        return jwtIsValid;
    }

    private boolean isTestToken(HttpServletRequest httpServletRequest) {
        String authToken = jwtService.tokenFromRequest(httpServletRequest);

        return jwtService.tokenIsUsedForTest(authToken);
    }

    private boolean hasApiKey(HttpServletRequest request) {
        try {
            return userAccessService.hasApiKey(request);
        } catch (NullPointerException e) {
            return false;
        }
    }

    private boolean isEndpointOpen(HttpServletRequest httpServletRequest) {
        return userAccessService.isEndpointOpen(httpServletRequest);
    }

    private boolean isAccessesControlDisabled() {
        return !userAccessService.isAccessControlEnabled();
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    }


}
