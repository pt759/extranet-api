package com.skuld.extranet.config;


import com.skuld.common.security.authentication.UserSecurityDetails;
import com.skuld.common.security.service.JwtService;
import com.skuld.errorhandling.MessageLevel;
import com.skuld.errorhandling.exceptions.SkuldAccessException;
import com.skuld.extranet.profiles.service.ProfileClientService;
import com.skuld.extranet.security.service.UserAccessService;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.skuld.common.security.authentication.SecurityValues.apiServiceUSerEmail;
import static com.skuld.common.security.authentication.SecurityValues.apiServiceUserAzureId;

@Log4j2
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private final JwtService jwtService;
    private final UserAccessService userAccessService;

    public JwtAuthenticationFilter(UserAccessService userAccessService, ProfileClientService profileClientService) {
        this.userAccessService = userAccessService;
        this.jwtService = new JwtService();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        if (userAccessService.isEndpointOpen(request) && !userAccessService.isAccessControlEnabled()){
            log.debug("JwtAuthenticationFilter: " + userAccessService.isEndpointOpen(request));
            createAnonymousContext(request);
        }
        else
            createUserContext(request);

        chain.doFilter(request, response);
    }

    private void createAnonymousContext(HttpServletRequest request) {
        UserSecurityDetails userSecurityDetails = UserSecurityDetails.builder().email(apiServiceUSerEmail).build();
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                apiServiceUserAzureId, null, null);
        auth.setDetails(userSecurityDetails);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }


    private void createUserContext(HttpServletRequest request) {
        try {
            if (!jwtService.verifyTokenFromRequest(request) && userAccessService.isAccessControlEnabled()) {
                log.error("Token not valid: " + jwtService.tokenFromRequest(request));
                throw new SkuldAccessException("CreateUserContext", "Please re-authenticate", "Token not valid", MessageLevel.ERROR, null);
            }

            String authToken = jwtService.tokenFromRequest(request);
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    null, null, null);
            auth.setDetails(UserSecurityDetails.from(authToken));
            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch (Exception e) {
            log.error("Error creating user security context: " + e.getMessage());
            SecurityContextHolder.clearContext();
        }
    }
}