package com.skuld.extranet.config;

import com.skuld.common.security.SecurityContextHelper;
import com.skuld.extranet.error.SkuldClientErrorDecoder;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import static com.skuld.common.security.authentication.SecurityValues.authorizationHeader;
import static com.skuld.global.GlobalKeys.TOKEN_PREFIX;

@Log4j2
public class ProfileClientConfiguration {

    @Value("${skuld.api.vessels.api-key}")
    String apiKey;

    @Value("${security.accessControl}")
    private boolean accessControlEnabled;

    @Bean
    public RequestInterceptor requestInterceptor() {
            return requestTemplate -> requestTemplate.header(authorizationHeader, getAuthorization());
    }

    private String getAuthorization() {
        SecurityContextHelper securityContextHelper = new SecurityContextHelper();
        String token = securityContextHelper.getToken();

        if (token.contains(TOKEN_PREFIX))
            return securityContextHelper.getToken();
        else
            return TOKEN_PREFIX + securityContextHelper.getToken();
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new SkuldClientErrorDecoder();
    }
}
