package com.skuld.extranet.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ProfileManager {

    @Autowired
    Environment environment;

    public String getCurrentEnvironment(){
        return environment.getProperty("spring.profiles.active");
    }
    public void getActiveProfiles() {
        for (final String profileName : environment.getActiveProfiles()) {
            System.out.println("Currently active profile - " + profileName);
        }
    }
}
