package com.skuld.extranet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SkuldApiConfig implements WebMvcConfigurer {

    @Bean
    public SkuldCustomInterceptor  skuldCustomInterceptor(){
        return new SkuldCustomInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(skuldCustomInterceptor()).addPathPatterns("/**");
    }
}