package com.skuld.extranet.global;

import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.spa.pages.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class RESTendPoints {

    private static final Logger logger = LoggerFactory.getLogger(RESTendPoints.class.getName());

    public static String apiHost;

    @Value("${api.host.url}")
    public void setHost(String host) {
        apiHost = host;
    }

    public static String wrapHost(String url) {
        return apiHost + url;
    }

    public static final String vesselSearch = "/vesselsearch";
    public static final String correspondentSearch = "/correspondents";
    public static final String tiaCreateUSer = "/aad/tiaUser";
    public static final String tiaUsers = "/users/tia";
    public static final String pathVariableStatCode = "{statcode}";
    public static final String pathVariableProduct = "{product}";
    public static final String pathVariableVersion = "{version}";
    public static final String pathVariableAccountNo = "{accountno}";

    public static final String pathVariableInvoiceNo = "{invoiceno}";
    public static final String pathVariableDocumentKey = "{documentKey}";
    public static final String pathVariableAgrLineNo = "{agrLineNo}";
    public static final String pathVariableItemType = "{itemType}";

    // Archive
    public static final String archive = "/archive";
    public static final String archive_StatCode_DocumentKey = "/" + pathVariableStatCode + "/" + pathVariableDocumentKey + "/";
    public static final String archive_invoice_with_InvoiceNo = "/invoice/" + pathVariableInvoiceNo + "/";
    public static final String archive_invoice_file_with_InvoiceNo = "/invoice/" + "/file/" + pathVariableInvoiceNo + "/";
    public static final String archive_StatCode_AgrLineNo = "/list/" + pathVariableStatCode + "/" + pathVariableAgrLineNo + "/";

    // extranet
    public static final String extranet = "/extranet";
    public static final String statistics_excel_StatCode_Product_Version = "/statistics/excel/" + pathVariableStatCode + "/" + pathVariableProduct + "/" + pathVariableVersion + "/";
    public static final String statistics_excel_file_StatCode_Product_Version = "/statistics/excel/file/" + pathVariableStatCode + "/" + pathVariableProduct + "/" + pathVariableVersion + "/";

    // Statistics
    public static final String statisticsPage = "/statistics";
    public final static String pdf_StatCode_Version_Product = "/pdf/" + pathVariableStatCode + "/" + pathVariableVersion + "/" + pathVariableProduct + "/";

    // Claims
    public static final String claims = "/claims";
    public static final String claims_open_pdf_StatCode_Product = "/list/pdf/open/" + pathVariableStatCode + "/" + pathVariableProduct + "/";
    public static final String claims_all_pdf_StatCode_Product = "/list/pdf/all/" + pathVariableStatCode + "/" + pathVariableProduct + "/";
    public static final String claims_open_excel_StatCode = "/list/excel/all/" + pathVariableStatCode + "/";
    public static final String claims_open_excel_file_StatCode = "/list/excel/all/file/" + pathVariableStatCode + "/";

    //Vessel
    public static final String vesselPage = "/vessels";

    //Financial
    public static final String financial = "/financialcontroller";
    public static final String financialPage = "/financialpagecontroller";

    public static final String financial_soa_pdf_with_statCode_id_itemType_Base64 = "/soa/pdf/" + pathVariableStatCode + "/" + pathVariableAccountNo + "/" + pathVariableItemType;
    public static final String financial_soa_pdf_file_with_statCode_id_itemType = "/soa/pdf/file/" + pathVariableStatCode + "/" + pathVariableAccountNo + "/" + pathVariableItemType;

    public static final String financial_instalmentReport_pdf_Base64 = "/instalment/pdf/" + pathVariableStatCode + "/" + pathVariableAccountNo;
    public static final String financial_instalmentReport_pdf = "/instalment/pdf/file/" + pathVariableStatCode + "/" + pathVariableAccountNo;

    public static final String financial_soa_excel_with_statCode_accountNo = "/soa/excel/" + pathVariableStatCode + "/" + pathVariableAccountNo;
    public static final String financial_soa_excel_file_with_statCode_accountNo = "/soa/excel/file/" + pathVariableStatCode + "/" + pathVariableAccountNo;

    //User (Contact Sync and Find Email info)
    public static final String users = "/users/";

    //Member Performance
    public static final String memberPerformance = "/member_performance";
    public static final String member_performance_pdf_statCode_pdf_base64 = "/pdf/" + pathVariableStatCode + "/";
    public static final String member_performance_pdf_file_statCode = "/pdf/file/" + pathVariableStatCode + "/";

    public static String urlForOpenClaimAsPDF(String statCode, Product product) {
        return claims + claims_open_pdf_StatCode_Product
                .replace(pathVariableStatCode, statCode)
                .replace(pathVariableProduct, product.value())
                ;
    }

    public static String urlForAllClaimAsPDF(String statCode, Product product) {
        return claims + claims_all_pdf_StatCode_Product
                .replace(pathVariableStatCode, statCode)
                .replace(pathVariableProduct, product.value())
                ;
    }

    public static String urlForAllClaimsAsExcel(String statCode) {
        return claims + claims_open_excel_StatCode.replace(pathVariableStatCode, statCode);
    }

    public static String urlStatementOfAccount(String statcode, String accountNo, AccountItemEntity.ItemType itemType) {
        String s = wrapHost(financial + financial_soa_pdf_with_statCode_id_itemType_Base64
                .replace(pathVariableStatCode, statcode)
                .replace(pathVariableAccountNo, accountNo)
                .replace(pathVariableItemType, itemType.value())
        );

        logger.info(s);
        return s;
    }

    public static String urlInstallmentReport(String statcode, String accountNo) {
        String s = wrapHost(financial + financial_instalmentReport_pdf_Base64
                .replace(pathVariableStatCode, statcode)
                .replace(pathVariableAccountNo, accountNo));

        logger.info(s);
        return s;
    }

    public static String urlMemberPerformancePdf(String statcode) {
        return wrapHost(memberPerformance + member_performance_pdf_statCode_pdf_base64)
                .replace(pathVariableStatCode, statcode);
    }


    public static String urlStatementOfAccount() {
        return wrapHost(financial + financial_soa_pdf_with_statCode_id_itemType_Base64);
    }

    @Deprecated
    public static String urlInstallmentReport() {
        return wrapHost(financial + financial_instalmentReport_pdf_Base64);
    }

    public static String urlStatementOfAccountExcel(String statcode, String accountNo) {
        return wrapHost(financial + financial_soa_excel_with_statCode_accountNo
                .replace(pathVariableStatCode, statcode)
                .replace(pathVariableAccountNo, accountNo
                ));
    }

    public static String urlInvoiceAsBase64(String invoiceNo) {
        return wrapHost(archive + archive_invoice_with_InvoiceNo
                .replace(pathVariableInvoiceNo, invoiceNo));
    }


}
