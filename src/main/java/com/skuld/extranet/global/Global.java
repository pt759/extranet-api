package com.skuld.extranet.global;

public class Global {

    public static final String urlForPolicyB2C_1A_signup_signin = "https://b2cskuld.b2clogin.com/b2cskuld.onmicrosoft.com/discovery/v2.0/keys?p=B2C_1A_signup_signin";
    public static final String url_TEST_ForPolicyB2C_1A_signup_signin = "https://b2cskuld.b2clogin.com/b2cskuld.onmicrosoft.com/discovery/v2.0/keys?p=B2C_1A_TEST_signup_signin";
    public static final String url_QA_ForPolicyB2C_1A_signup_signin = "https://b2cskuld.b2clogin.com/b2cskuld.onmicrosoft.com/discovery/v2.0/keys?p=B2C_1A_QA_signup_signin";
    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String AZURE_USER_ADMIN_GROUP_ID = "9f851020-f9b3-4415-86cb-2241b680bc34";
    public static final String AZURE_USER_FINANCIALS_GROUP_ID = "6fc5e5a0-5f96-4707-89bf-64a85a4659a9";
    public static final String AZURE_YACHT_CREW_LIABILITY_GROUP_ID = "e232534a-ee77-48ad-b0a9-3e3c84075af6";
    public static final String appReferenceQuoteOnlineGroup = "QUOTE_ONLINE_GROUP";
    public static final String appReferenceClaimsOnlineGroup = "CLAIMS_ONLINE_GROUP";
    public static String APPLICATION_EXCEL_VALUE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //Images
    public static String remoteImgSkuldLogo ="http://mobile.skuld.com:41897/images/logo_skuld_small.png";
    public static String imgSkuldLogo = "/images/logo_skuld_small.png";
    public static String imgFileName = "/images/skuld_logo_front.gif";
    public static String imgWave = "/images/sea_trail2.jpg";
    public static String imgSkuldLogoRedTransparent = "/images/skuld_logo_red_transparent.png";
}
