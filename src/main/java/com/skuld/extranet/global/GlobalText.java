package com.skuld.extranet.global;

public class GlobalText {
    // TODO: Move it to a property file
    public final static String NO_ENDPOINT_ACCESS = "You do not have access to this endpoint";
    public final static String NO_DATA_ACCESS = "No data access";

    public final static String OK_CODE = "200";
    public final static String OK_VALUE = "200";

    public final static String NOT_FOUND_VALUE = "Not found";
    public final static String NOT_FOUND_CODE = "404";

    public final static String ERROR_CODE = "500";
    public final static String ERROR_VALUE = "Server error";

    public final static String AZURE_OK_CODE = "204";
    public final static String AZURE_ERROR = "Azure Error";

    public final static String ADD_TO_GROUP_SUCCESS_VALUE = "User added from group";
    public final static String COULD_NOT_ADD_USER_TO_GROUP = "Could not add user to group";
    
    public final static String GROUP_DELETED = "Group deleted";
    public final static String ERROR_DELETING_GROUP = "Error deleting group";
    public final static String GROUP_NOT_FOUND = "Group not found";
    public final static String CAN_NOT_DELETE_GROUP_HAS_MEMBERS = "Can not delete group. Has members";
    
    public final static String USER_REMOVED_FROM_GROUP = "User removed from group";

    public final static String USER_SUCCESSFULLY_SUSPENDED = "User successfully suspended in Azure";
    public final static String USER_HAS_GROUP_MEMBERSHIPS = "Can not suspend user. Is member of group(s)";

    public final static String USER_SUCCESSFULLY_DELETED = "User successfully removed from Azure";
    public final static String USER_NOT_FOUND = "User not found";
    
    public final static String COULD_NOT_CONNECT_TO_DATABASE = "Could not connect to database";
    public final static String DB_CONNECTION_ERROR_CODE = "503";
    
    public final static	String BROKER_BUSINESS_UNIT_NAME = "Brokers";
    public final static	String BROKER_BUSINESS_UNIT_ID = "0";

    public final static String netPremium = "Net Premium: ";
    public final static String mainCovers = "Main Covers ";

    
}
