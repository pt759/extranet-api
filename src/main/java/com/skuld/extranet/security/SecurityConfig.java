package com.skuld.extranet.security;

import com.skuld.extranet.config.JwtAuthenticationFilter;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

import static com.skuld.common.security.GlobalAccess.*;

@Log4j2
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    private final JwtAuthenticationFilter jwtAuthenticationFilter;

    public SecurityConfig(JwtAuthenticationFilter jwtAuthenticationFilter) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(swaggerDocs);
        web.ignoring().antMatchers(swaggerJson);
        web.ignoring().antMatchers(swaggerUI);
        web.ignoring().antMatchers(error);
    }

    protected void configure(HttpSecurity http) throws Exception {
        log.info("Configure CORS");
        http.cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, authenticationUrl).permitAll()
                .antMatchers(HttpMethod.GET, swaggerUI).permitAll()
                .antMatchers(HttpMethod.GET, apiDocsUrl).permitAll();

        http.addFilterBefore(
                jwtAuthenticationFilter,
                UsernamePasswordAuthenticationFilter.class
        );
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList(
                "http://localhost:52167",
                "http://localhost:8100",
                "http://localhost",
                "http://localhost:65328",
                "http://localhost:65438",
                "http://localhost:1234",
                "https://www.skuld.com",
                "https://PT-SXD7E-075",
                "https://PT-SXD7E-074",
                "https://api.skuld.com",
                "https://login.test.int.skuld.com",
                "https://login.qa.int.skuld.com",
                "https://login.skuld.com"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
        configuration.setExposedHeaders(Arrays.asList("x-auth-token"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
