package com.skuld.extranet.security.service;


import com.skuld.common.security.SecurityContextHelper;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverviewPage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentFilterService {

    private final SecurityContextHelper securityContextHelper;

    public ContentFilterService() {
        this.securityContextHelper = new SecurityContextHelper();
    }

    public ClientOverviewPage filterClientOverviewPage(ClientOverviewPage page) {

        if (securityContextHelper.isCrewingManager()) {
            setCrewingManagerOverviewPage(page);
        }
        return page;
    }

    private void setCrewingManagerOverviewPage(ClientOverviewPage page) {
        page.setAccountManager(null);
        page.setMemberPerformanceBonus(null);
        page.setVessels(null);
        page.setVessels(null);
    }

    public List<AccountOverview> filterAccountOverview(List<AccountOverview> membersForLogin) {
        if (securityContextHelper.isCrewingManager()) {
            membersForLogin.forEach(member -> {
                member.setCurrency(null);
                member.setGrossPremium(null);
            });
        }

        return membersForLogin;
    }
}
