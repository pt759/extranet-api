package com.skuld.extranet.security.service;


import com.skuld.common.security.SecurityContextHelper;
import com.skuld.common.security.service.JwtUtil;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.financial.service.FinancialService;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.service.AzureAdGroupService;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import com.skuld.extranet.core.user.service.StatCodeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.skuld.common.security.GlobalAccess.*;
import static com.skuld.common.security.authentication.SecurityValues.apiKeyHeader;
import static com.skuld.common.security.authentication.SecurityValues.authorizationHeader;
import static com.skuld.extranet.global.Global.AZURE_USER_ADMIN_GROUP_ID;
import static com.skuld.extranet.global.RESTendPoints.*;
import static com.skuld.extranet.security.service.JwtSecConstant.SecurityConstants.*;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;


@Log4j2
@Service
public class UserAccessService {

    private final UserGroupAccessService userGroupAccessService;
    private final StatCodeService statCodeService;
    private final AzureAdUserService azureAdUserService;
    private final AzureAdGroupService azureAdGroupService;
    private final ClaimService claimService;
    private final FinancialService financialService;
    private final SecurityContextHelper securityContextHelper;

    @Value("${financialPage.enabled}")
    private Boolean financialPageVisible;

    @Value("${security.accessControl}")
    private boolean accessControl;

    @Value("${security.ip.whitelist}")
    private String whiteListedIp;

    @Value("${skuld.api.extranet.api-key}")
    private String apiKey;

    @Autowired
    public UserAccessService(UserGroupAccessService userGroupAccessService,
                             StatCodeService statCodeService,
                             AzureAdUserService azureAdUserService,
                             AzureAdGroupService azureAdGroupService,
                             ClaimService claimService,
                             FinancialService financialService) {
        this.userGroupAccessService = userGroupAccessService;
        this.statCodeService = statCodeService;
        this.azureAdUserService = azureAdUserService;
        this.azureAdGroupService = azureAdGroupService;
        this.claimService = claimService;
        this.financialService = financialService;
        this.securityContextHelper = new SecurityContextHelper();
    }

    public boolean accessToStatCode(String authToken, String statCodeToAccess) {
        boolean accessToStatCode = false;
        try {
            List<AzureAdGroup> groups = userGroupAccessService.getUsersGroupsFromToken(authToken);
            accessToStatCode = groups.stream().anyMatch(group -> group.hasStatCode(statCodeToAccess));
        } catch (Exception e) {
            log.error("Error checking for access: " + e.getMessage());

        }

        return accessToStatCode;
    }


    public boolean hasAccess(HttpServletRequest httpRequest) {
        String auth_token = null;
        log.debug("Checking access to page and data");
        String servletPath = httpRequest.getServletPath();
        Map<String, String> pathVariables = (Map) httpRequest.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        auth_token = httpRequest.getHeader(authorizationHeader);

        return pageAccess(servletPath) && dataAccess(pathVariables, auth_token);
    }

    public boolean dataAccess(Map<String, String> pathVariables, String auth_token) {
        boolean dataAccess = true;

        for (Map.Entry<String, String> entry : pathVariables.entrySet()) {
            log.debug("Path key: " + entry.getKey().toLowerCase() + " value: " + entry.getValue());
            switch (entry.getKey().toLowerCase()) {
                case HEADER_STAT_CODE_GROUP_LOGIN:
                    dataAccess = accessToGroup(auth_token, entry.getValue());
                    break;
                case HEADER_STATCODE:
                    dataAccess = accessToStatCode(auth_token, entry.getValue());
                    break;
                case HEADER_EVENT_NO:
                    dataAccess = accessToEvent(auth_token, pathVariables.get("statcode"), pathVariables.get("eventno"));
                    break;
                case HEADER_AZURE_GROUP_ID:
                    dataAccess = accessToAzureGroup(pathVariables.get(HEADER_AZURE_GROUP_ID));
                    break;
                case PATH_ACCOUNT_NO:
                    dataAccess = accessToAccount(entry.getValue());
                    break;

                default:
                    break;
            }
            if (!dataAccess) {
                log.error("No access to " + entry.getKey().toLowerCase() + " : " + entry.getValue() + "Token: " + auth_token);
                break;
            }
        }
        return dataAccess;
    }

    public boolean pageAccess(String page) {
        boolean pageAccess = true;
        String authToken = securityContextHelper.getToken();

        log.debug("Page: " + page);
        if (page.contains(financialPage)) {
            pageAccess = isSkuldEmployee() || (memberOfBrokerGroup(authToken) && financialPageVisible);
        }

        if (securityContextHelper.isCrewingManager() && (page.contains(vesselPage) || page.contains(statisticsPage))) {
            pageAccess = false;
        }

        if (!pageAccess) {
            log.error("No access to page" + page + " : " + "Token: " + authToken);
        }
        return pageAccess;
    }


    public boolean accessToAccount(String account) {
        boolean access;
        List<StatCodeDTO> statCodeDTOS = new ArrayList<>();

        securityContextHelper.getUserSecurityDetails()
                .getAzureGroupNames()
                .stream()
                .forEach(statCodeGroup -> {
                    statCodeDTOS.addAll(statCodeService.getStatCodesForGroupCached(statCodeGroup));
                });

        List<StatCodeDTO> distinctStatCodeList = statCodeDTOS.stream().distinct().collect(Collectors.toList());
        access = financialService.checkStatCodeAccountsAccess(distinctStatCodeList, account);

        return access;
    }

    private boolean isSkuldEmployee() {
        return securityContextHelper.isSkuldEmployee();
    }


    private boolean memberOfBrokerGroup(String authToken) {
        List<AzureAdGroup> groups = userGroupAccessService.getUsersGroupsFromToken(authToken);
        return groups.stream().anyMatch(group -> group.isBroker());
    }

    private boolean accessToEvent(String auth_token, String statCode, String claimEventNo) {
        AzureAdUser user = azureAdUserService.getUserById(JwtUtil.getUserFromToken(auth_token));
        Claim claim = claimService.claimEvent(statCode, claimEventNo);
        boolean access = true;

        if (claim != null && claim.getConflictEvent().equalsIgnoreCase("Y") && user.getEmail().contains(SKULD_EMAIL_DOMAIN)) {
            log.info("User " + user.getEmail() + " has no access to event " + claimEventNo + ". Is conflict case: " + claim.getConflictEvent());
            access = false;
        }
        return access;
    }

    public boolean accessToGroup(String authToken, String statCodeGroupLogin) {
        List<String> groups = JwtUtil.getStatCodeGroupsFromToken(authToken);
        return groups.stream().anyMatch(x -> statCodeGroupLogin.equalsIgnoreCase(x));
    }

    public boolean accessToAzureGroup(String azureGroupId) {
        List<String> groupIds = securityContextHelper.getAzureGroupIds();
        return groupIds
                .stream()
                .anyMatch(group ->
                        azureGroupId.equals(group) ||
                        group.equals(AZURE_USER_ADMIN_GROUP_ID));
    }

    public boolean isAccessControlEnabled() {
        return accessControl;
    }

    public boolean isEndpointOpen(HttpServletRequest request) {
        boolean jwtIsValid = false;
        String servletPath = request.getServletPath();
        String host = getRemoteAddress(request);
        List<String> ipList = Arrays.asList(whiteListedIp.split(","));

        if (servletPath.startsWith(vesselSearch) ||
                servletPath.startsWith(correspondentSearch) ||
                servletPath.startsWith(users) ||
                servletPath.startsWith(tiaCreateUSer) && ipList.stream().anyMatch(host::contains) ||
                servletPath.startsWith(tiaUsers) && ipList.stream().anyMatch(host::contains) ||
                servletPath.startsWith(archive) && ipList.stream().anyMatch(host::contains) ||
                servletPath.startsWith(authenticationUrl) && ipList.stream().anyMatch(host::contains) ||
                servletPath.startsWith(swaggerUI) && ipList.stream().anyMatch(host::contains) ||
                servletPath.startsWith(apiDocsUrl) && ipList.stream().anyMatch(host::contains)
        ) {
            jwtIsValid = true;
            log.info(getUrl(request) + " is open by origin host or url");
        }

        return jwtIsValid;
    }

    public String getRemoteAddress(HttpServletRequest request) {
        String ipFromHeader = request.getHeader("X-FORWARDED-FOR");
        if (ipFromHeader != null && ipFromHeader.length() > 0) {
            log.debug("ip from proxy - X-FORWARDED-FOR : " + ipFromHeader);
            return ipFromHeader;
        }
        return request.getRemoteAddr();
    }

    public String getUrl(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        String host = getRemoteAddress(request);

        return host + servletPath;
    }

    public UserAccessService setWhiteListedIp(String whiteListedIp) {
        this.whiteListedIp = whiteListedIp;
        return this;
    }

    public boolean hasApiKey(HttpServletRequest request) {
        return apiKey.equals(request.getHeader(apiKeyHeader));
    }
}
