package com.skuld.extranet.security.service;

import com.skuld.common.security.service.JwtUtil;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.service.StatCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserGroupAccessService {

    @Autowired
    private StatCodeService statCodeService;

    public List<AzureAdGroup> getUsersGroupsFromToken(String authToken) {
        List<AzureAdGroup> userGroups = extractUserGroupFrom(authToken);
        userGroups.forEach(group -> group.setStatCodes(statCodeService.getStatCodesForGroupCached(group.getName())));
        userGroups.forEach(group -> group.setBroker(statCodeService.isBroker(group.getName())));

        return userGroups;
    }

    private List<AzureAdGroup> extractUserGroupFrom(String authToken) {
        List<AzureAdGroup> groups = new ArrayList<>();
        List<String> groupNames = JwtUtil.getStatCodeGroupsFromToken(authToken);

        groupNames.stream().forEach(groupName -> groups.add(AzureAdGroup.builder().name(groupName).build()));
        return groups;
}
}
