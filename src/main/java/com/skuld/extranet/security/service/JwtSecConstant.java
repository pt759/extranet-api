package com.skuld.extranet.security.service;

public class JwtSecConstant {

    public class SecurityConstants {
        public static final String SECRET = "SecretKeyToGenJWTs";
        public static final long EXPIRATION_TIME = 864_000_000; // 10 days
        public static final String HEADER_STATCODE = "statcode";
        public static final String HEADER_EVENT_NO = "eventno";
		public static final String HEADER_STAT_CODE_GROUP_LOGIN = "statcodegrouplogin";
		public static final String HEADER_AZURE_GROUP_ID = "azuregroupid";
        public static final String PATH_ACCOUNT_NO = "accountno";
		public static final String SKULD_EMAIL_DOMAIN = "@skuld.com";
    }
}

