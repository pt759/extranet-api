package com.skuld.extranet.reference.entity;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.skuld.extranet.CachingConfig.APP_REFERENCE;

@Repository
public interface AppReferenceRepo extends CrudRepository<AppReference, Long> {

    @Cacheable(APP_REFERENCE)
    List<AppReference> findByDomain(String domain);

    AppReference findByCode(String code);

    Optional<AppReference> findById(Long id);
}
