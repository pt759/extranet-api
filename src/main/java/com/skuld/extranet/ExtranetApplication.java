package com.skuld.extranet;

import com.zaxxer.hikari.HikariDataSource;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.spring4.JdbiFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.config.EnableHypermediaSupport;

import javax.sql.DataSource;
import java.util.UUID;

@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@EnableFeignClients
@EnableCaching
@SpringBootApplication
public class ExtranetApplication extends SpringBootServletInitializer {

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        SpringApplication.run(ExtranetApplication.class, args);
    }

    @Autowired
    @Bean
    public Jdbi jdbi(DataSource dataSource) {
        synchronized (Jdbi.class) {
            return Jdbi.create(dataSource);
        }
    }

    @Autowired
    @Bean
    public JdbiFactoryBean dbiFactory(DataSource dataSource) {
        JdbiFactoryBean dbiFactoryBean = new JdbiFactoryBean();
        dbiFactoryBean.setDataSource(dataSource);
        return dbiFactoryBean;
    }

    @Primary
    @Bean
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driverClassName"));
        dataSource.setJdbcUrl(env.getProperty("tia.datasource.url"));
        dataSource.setUsername(env.getProperty("tia.datasource.username"));
        dataSource.setPassword(env.getProperty("tia.datasource.password"));
        dataSource.setPoolName("tia_dataSource_" + UUID.randomUUID().toString());
        return dataSource;
    }

    @Bean(name = "dwhDatasource")
    public DataSource dataSourceDwh() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driverClassName"));
        dataSource.setJdbcUrl(env.getProperty("dwh.datasource.url"));
        dataSource.setUsername(env.getProperty("dwh.datasource.username"));
        dataSource.setPassword(env.getProperty("dwh.datasource.password"));
        dataSource.setPoolName("dwhDataSource_" + UUID.randomUUID().toString());
        return dataSource;
    }

//    @PostConstruct
//    public void setSkuldKeyStore(){
//        System.setProperty("javax.net.ssl.trustStore", env.getProperty("security.keyStore.name"));
//        System.setProperty("javax.net.ssl.trustStorePassword", env.getProperty("security.keyStore.password"));
//    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ExtranetApplication.class);
    }
}
