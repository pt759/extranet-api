package com.skuld.extranet.profiles.client;

import com.skuld.common.dto.InitializationGroupDTO;
import com.skuld.extranet.config.ProfileClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@FeignClient(name = "profile.client", url = "${skuld.api.profiles.url}", configuration = ProfileClientConfiguration.class)
public interface ProfileClient {

    @GetMapping(value = "/initialize")
    List<InitializationGroupDTO> initializeUser();

    @PostMapping(value = "/initialize/acceptAgreement/{azureUserId}/")
    void acceptAgreement(@PathVariable("azureUserId") String azureUserId);

    @PutMapping(value = "/initialize/acceptAgreement/{azureUserId}/")
    void updateUserAcceptance(@PathVariable("azureUserId") String azureUserId);
}
