package com.skuld.extranet.profiles.service;

import com.skuld.common.dto.InitializationGroupDTO;
import com.skuld.extranet.profiles.client.ProfileClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class ProfileClientService {

    private final ProfileClient profileClient;


    public ProfileClientService(ProfileClient profileClient) {
        this.profileClient = profileClient;
    }

    public List<InitializationGroupDTO> initializeUser() {
        List<InitializationGroupDTO> initializationGroupDTOS = profileClient.initializeUser();
        return initializationGroupDTOS;
    }

    public List<InitializationGroupDTO> acceptAgreement(String azureUserId){
        profileClient.acceptAgreement(azureUserId);
        return initializeUser();
    }

    public void updateUserAcceptance(String azureUserId){
        profileClient.acceptAgreement(azureUserId);
    }
}