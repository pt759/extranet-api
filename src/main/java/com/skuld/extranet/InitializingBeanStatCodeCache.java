package com.skuld.extranet;

import com.skuld.extranet.core.user.service.StatCodeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Log4j2
@Component
@Profile("!dev")
public class InitializingBeanStatCodeCache implements InitializingBean {


    private final Environment environment;
    private final StatCodeService statCodeService;

    @Autowired
    public InitializingBeanStatCodeCache(Environment environment,
                                         StatCodeService statCodeService) {
        this.environment = environment;
        this.statCodeService = statCodeService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.debug("Cashing BU statcodes at startup");
        statCodeService.initBusinessUnitStatCodeCache();
        log.info(Arrays.asList(environment.getDefaultProfiles()));
    }
}