package com.skuld.extranet.excel;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ExcelFormat {
    final static Logger logger = LoggerFactory.getLogger(ExcelFormat.class);
    private final String sheetName;

    protected Workbook workbook;
    protected String filename = "";

    protected ExcelStyleHelper styleHelper;

    public ExcelFormat(String filename, String sheetName) {
        this.workbook = new XSSFWorkbook();
        this.filename = filename;
        this.sheetName = sheetName;

        styleHelper = new ExcelStyleHelper(workbook);

    }



    public String getSheetName() {
        return sheetName;
    }

    public Workbook getWorkbook() {
        return workbook;
    }

    public ExcelFormat setWorkbook(Workbook workbook) {
        this.workbook = workbook;
        return this;
    }

    public String getFilename() {
        return filename;
    }

    public ExcelFormat setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    protected void autoSize(Sheet sheet) {
        autoSize(0, sheet);
    }

    protected void autoSize(Integer row, Sheet sheet) {
        //Auto size all the columns
        for (int x = 0; x < sheet.getRow(row).getPhysicalNumberOfCells(); x++) {
            sheet.autoSizeColumn(x);
        }
    }



    protected void createSumCell(Integer column, Integer startRow, Integer endRow, CellStyle style, Row row) {

        Cell cell = styleHelper.createFormulaCell(column, style, row);

        String columnReference = CellReference.convertNumToColString(cell.getColumnIndex());
        String cellFormula = "SUM(" + columnReference + startRow + ":" + columnReference + endRow + ")";

        cell.setCellType(CellType.FORMULA);
        cell.setCellFormula(cellFormula);

//        logger.info(cellFormula);
    }


}
