package com.skuld.extranet.excel;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.awt.Color;

public class ExcelStyleHelper {

    protected CellStyle cellStyle;
    protected CellStyle cellStyleWithTopBottomBorder;

    protected XSSFDataFormat dataFormat;

    private CellStyle styleTwoBorder;
    private CellStyle styleNumericThousand;
    private CellStyle styleNumericThousandWithTwoDecimals;
    private CellStyle styleNoDecimal;
    private CellStyle stylePercentage;
    private CellStyle styleTwoBorderPercentage;

    public XSSFColor WHITE = new XSSFColor(Color.WHITE);
    public XSSFColor BLACK = new XSSFColor(Color.BLACK);
    private XSSFCellStyle headerStyle;
    private XSSFCellStyle headerStyleSimple;
    private org.apache.poi.ss.usermodel.Font normalFontBold;

    private final Workbook workbook;

    public ExcelStyleHelper(Workbook workbook) {
        this.workbook = workbook;

        dataFormat = (XSSFDataFormat) workbook.createDataFormat();
    }

    public CellStyle styleGeneric() {
        if (this.cellStyle == null) {
            this.cellStyle = styleGenericNew();
        }
        return this.cellStyle;
    }

    public CellStyle styleGenericNew() {
        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        return style;
    }

    public XSSFColor colorSkuldRed_e71324() {
        return new XSSFColor(new java.awt.Color(231, 19, 36));
    }

    public XSSFColor colorBlack() {
        return new XSSFColor(new java.awt.Color(231, 19, 36));
    }

    public XSSFCellStyle headerStyleSimple() {
        if (headerStyleSimple == null) {
            XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
            style.setFont(headerFont(BLACK));
            headerStyleSimple = style;
        }
        return headerStyleSimple;
    }

    public XSSFCellStyle headerStyleWithSolidRedBackground() {
//        if (headerStyle == null) {
            XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
            style.setFont(headerFont(WHITE));

            style.setFillForegroundColor(colorSkuldRed_e71324());
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            headerStyle = style;
//        }
        return headerStyle;
    }


    protected CellStyle styleNoDecimal() {
        if (styleNoDecimal == null) {
            CellStyle style = workbook.createCellStyle();
            style.setDataFormat(getNumericNoDecimal());
            styleNoDecimal = style;
        }
        return styleNoDecimal;
    }

    public CellStyle stylePercentage() {
        if (this.stylePercentage == null) {
            CellStyle style = workbook.createCellStyle();
            style.setDataFormat(getDataFormatPercentage());
            this.stylePercentage = style;
        }
        return this.stylePercentage;
    }

    protected CellStyle cellStyleWithTopBottomBorder() {
        if (this.cellStyleWithTopBottomBorder == null) {
            CellStyle style = workbook.createCellStyle();
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderBottom(BorderStyle.DOUBLE);
            this.cellStyleWithTopBottomBorder = style;
        }
        return this.cellStyleWithTopBottomBorder;
    }

    public CellStyle styleNumericThousand() {
        if (styleNumericThousand == null) {
            CellStyle style = workbook.createCellStyle();
            style.setDataFormat(getNumericThousand());
            styleNumericThousand = style;
        }
        return styleNumericThousand;
    }

    public CellStyle styleNumericThousandWithTwoDecimals() {
        if (styleNumericThousandWithTwoDecimals == null) {
            CellStyle style = workbook.createCellStyle();
            style.setDataFormat(getNumericThousand());
            styleNumericThousandWithTwoDecimals = style;
        }
        return styleNumericThousand;
    }



    protected XSSFFont headerFont(XSSFColor color) {
        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");

        font.setColor(color);
        font.setFontHeightInPoints((short) 11);
        font.setBold(true);
        return font;
    }

    public Cell createCellWithStyle(int column, String txt, CellStyle style, Row row) {
        Cell cell = row.createCell(column);
        cell.setCellStyle(style);
        cell.setCellValue(txt);
        return cell;
    }

    public Cell createFormulaCell(int column, CellStyle style, Row row) {
        Cell cell = createCellWithStyle(column, "", style, row);
        return cell;
    }

    public void createNumericCellWithStyle(int column, String value, CellStyle style, Row row) {

        Cell cell = createCellWithStyle(column, value, style, row);
        try {
            Double thisValue = Double.valueOf(value);
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(thisValue);
        } catch (Exception ex) {
            cell.setCellValue("");
        }
    }


    public CellStyle styleTwoBorderStyle() {
        if (styleTwoBorder == null) {
            CellStyle style = workbook.createCellStyle();
            style.setDataFormat(styleNumericThousand().getDataFormat());
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderBottom(BorderStyle.THIN);
            style.setFont(normalFontBold());
            this.styleTwoBorder = style;
        }
        return styleTwoBorder;
    }

    public CellStyle styleTwoBorderPercentage() {
        if (styleTwoBorderPercentage == null) {
            CellStyle style = workbook.createCellStyle();
            style.setDataFormat(stylePercentage().getDataFormat());
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderBottom(BorderStyle.THIN);
            style.setFont(normalFontBold());
            this.styleTwoBorderPercentage = style;
        }
        return styleTwoBorderPercentage;
    }

    public Font normalFontBold() {
        if (normalFontBold == null) {
            XSSFFont font = (XSSFFont) workbook.createFont();
            font.setBold(true);
            normalFontBold = font;
        }
        return normalFontBold;
    }


    public XSSFDataFormat getDataFormat() {
        return dataFormat;
    }

    public short getNumericThousand() {
        return getDataFormat().getFormat("#,##0.00");
    }


    public short getNumericNoDecimal() {
        return getDataFormat().getFormat("##0");
    }

    public short getDataFormatPercentage() {
        return getDataFormat().getFormat("0%");
    }

}
