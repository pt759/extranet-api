package com.skuld.extranet.excel;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.List;

public abstract class ExcelListConverter extends ExcelFormat {
    final static Logger logger = LoggerFactory.getLogger(ExcelListConverter.class);

    protected List<? extends Object> list;

    protected boolean autoFilter = true;


    public ExcelListConverter(List<? extends Object> list, String filename, String sheetName) {
        super(filename, sheetName);
        this.list = list;
    }


    public String getExcelAsBase64Text() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Workbook workBook = convertListToSheet(getSheetName());
            workBook.write(out);
            workBook.close();
            out.close();

            String base64Text = Base64.encodeBase64String(out.toByteArray());
//            logger.info("Base64Text: " + base64Text);
            return base64Text;
        } catch (Exception e) {
            logger.error("error: " + e.getMessage());
        }

        return "";
    }

    public void export(HttpServletResponse response) {

        response.addHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");

        ServletOutputStream outputStream;
        try {
            outputStream = response.getOutputStream();
            Workbook workBook = convertListToSheet(getSheetName());
            workBook.write(outputStream);
            workBook.close();
            outputStream.close();
            response.flushBuffer();
        } catch (Exception e) {
            logger.error("Failed to generate Excel: " + e.getMessage());
        }
    }

    public Workbook convertListToSheet(String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);

        Row rowHeader = populateRowHeader(sheet);
        populateDataRows(sheet);
        populateFooterRow(sheet);

        if (autoFilter && rowHeader != null) {
            sheet.setAutoFilter(new CellRangeAddress(rowHeader.getRowNum(), rowHeader.getRowNum(), rowHeader.getFirstCellNum(), rowHeader.getLastCellNum() - 1));
        }

        //Auto size all the columns
        autoSizeColumns(sheet);


        addLogoImage(sheet);

        return workbook;
    }

    protected void addLogoImage(Sheet sheet){
    }

    protected void autoSizeColumns(Sheet sheet) {
        for (int x = 0; x < sheet.getRow(0).getPhysicalNumberOfCells(); x++) {
            sheet.autoSizeColumn(x);
        }
    }

    protected abstract void populateFooterRow(Sheet sheet);

    protected abstract Row populateRowHeader(Sheet sheet);

    protected abstract void populateDataRows(Sheet sheet);

    public ExcelListConverter noAutofilter() {
        autoFilter = false;
        return this;
    }


    public List<? extends Object> getList() {
        return list;
    }

    public ExcelListConverter setList(List<? extends Object> list) {
        this.list = list;
        return this;
    }
}
