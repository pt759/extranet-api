package com.skuld.extranet.tia;

import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.tia.client.dto.TiaStatCodeDTO;
import com.skuld.extranet.core.tia.client.service.TiaPartyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static com.skuld.common.global.GlobalTestValues.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TiaPartyServiceTest {

    @Autowired
    private TiaPartyService tiaPartyService;

    @Test
    @WithMockCustomUser(token = employeeToken)
    void get_stat_codes_via_ords() {
        Optional<List<TiaStatCodeDTO>> statCode = tiaPartyService.getStatCode(africaExpressStatCode);
        assertTrue(statCode.get().size() > 1);

    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    void get_stat_codes_for_stat_code_group() {
        Optional<List<TiaStatCodeDTO>> statCodesForGroup = tiaPartyService.getStatCodesForGroup(cpbHodingsAzureDisplayName);
        assertTrue(statCodesForGroup.get().size() > 5);
    }
}
