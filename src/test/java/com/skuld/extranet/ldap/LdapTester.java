package com.skuld.extranet.ldap;

import com.skuld.extranet.ldap.domain.LDAPUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchResult;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class LdapTester {


    @Autowired
    private LDAPService ldapService;

    @Test
    public void findUserByMail3() throws NamingException {
        LDAPUser user = ldapService.findFirstUserByMail("tawana.tannock@skuld.com");
        assertThat(user).isNotNull();
        System.out.println(user);
    }

    @Test
    public void findUserWithAllInfo() throws NamingException {
        NamingEnumeration<SearchResult> userWithAllInfo = ldapService.findUserWithAllInfo("tawana.tannock@skuld.com", LDAPAttributes.mail);
        assertThat(userWithAllInfo).isNotNull();
    }

    @Test
    public void testCacheForAllUsers() throws NamingException {
        StopWatch watch = new StopWatch();
        watch.start("a");
        List<LDAPUser> list1 = ldapService.allLdapUsersFromAD();
        watch.stop();

        watch.start("b");
        List<LDAPUser> list2 = ldapService.allLdapUsersFromAD();
        watch.stop();

        watch.start("c");
        List<LDAPUser> list3 = ldapService.allLdapUsersFromAD();
        watch.stop();
        System.out.println(watch.prettyPrint());

    }
}