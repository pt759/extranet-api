package com.skuld.extranet.ldap.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LDAPUserTest {

    LDAPUser user = new LDAPUser();

    @Test
    public void testInvalidPhoneIsNotReturned() {
        user.setDirectPhone("telephoneNumber: 0");
        assertThat(user.getDirectPhone()).isEqualTo("");

        user.setDirectPhone("telephoneNumber: +47");
        assertThat(user.getDirectPhone()).isEqualTo("");

        user.setDirectPhone("telephoneNumber: 0047");
        assertThat(user.getDirectPhone()).isEqualTo("");

    }

    @Test
    public void testValidPhoneNumber() {
        //Norwegian Mobile
        user.setDirectPhone("telephoneNumber: 004747343247");
        assertThat(user.getDirectPhone()).isEqualTo("+47 473 43 247");

        user.setDirectPhone("telephoneNumber: +4747343247");
        assertThat(user.getDirectPhone()).isEqualTo("+47 473 43 247");

        user.setDirectPhone("telephoneNumber: 4747343247");
        assertThat(user.getDirectPhone()).isEqualTo("+47 473 43 247");

        //German
        user.setDirectPhone("telephoneNumber: +49 1704813991");
        assertThat(user.getDirectPhone()).isEqualTo("+49 170 4813991");

        //UK Mobile
        user.setDirectPhone("telephoneNumber: +447864606652");
        assertThat(user.getDirectPhone()).isEqualTo("+44 7864 606652");
    }


}