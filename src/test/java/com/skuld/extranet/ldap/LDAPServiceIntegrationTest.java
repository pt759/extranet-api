package com.skuld.extranet.ldap;

import com.skuld.extranet.ldap.domain.LDAPUser;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;

import javax.naming.NamingException;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@EnableCaching
class LDAPServiceIntegrationTest {


    @Autowired
    private LDAPService service;

    @Test
    public void findUser() throws NamingException {
        List<LDAPUser> list = service.findUser("pt99", LDAPAttributes.sAMAccountName);
        assertThat(list.size()).isEqualTo(1);
    }

    @Test
    public void findUserForOddbjorn() throws NamingException {
        List<LDAPUser> list = service.findUser("pt184", LDAPAttributes.sAMAccountName);
        assertThat(list.size()).isEqualTo(1);
        LDAPUser user = list.get(0);
        assertThat(user.getMobilePhone()).isEqualToIgnoringCase("+47 473 43 247");
    }

    @Test
    public void findUserWithNoResult() throws NamingException {
        List<LDAPUser> list = service.findUser("pt184", LDAPAttributes.mail);
        assertThat(list.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnNullUser() throws NamingException {
        LDAPUser user = service.findFirstUserByMail("pt184");
        assertThat(user).isNull();
    }

    @Disabled
    @DisplayName("Pt user no linger exists in AD")
    @Test
    public void shouldFindDisabledUser() throws NamingException {
        LDAPUser user = service.findFirstUserBySamaAccount("pt83");//Helle Lehmann
        assertThat(user).isNull();
//        assertThat(user.isDisabled()).isTrue();
    }


    @Test
    public void findUserByMail() throws NamingException {
        List<LDAPUser> list = service.findUser("oddbjorn.lona@skuld.com", LDAPAttributes.mail);
        assertThat(list.size()).isEqualTo(1);
        assertThat(list.get(0).getEmail()).isEqualTo("oddbjorn.lona@skuld.com");
    }

    @DisplayName("Should only call the AD once")
    @Test
    public void testAllCache() {
        List<LDAPUser> ldapUsers1 = service.allLdapUsersFromAD();
        List<LDAPUser> ldapUsers2 = service.allLdapUsersFromAD();
    }

    @Disabled
    @DisplayName("Display formatted phone numbers for all users")
    @Test
    public void testRetrieveAllUsersFromADAndShowPhoneNumbers() {
        List<LDAPUser> list = service.allLdapUsersFromAD();

        list.stream()
                .filter(item -> item.isValidSkuldUser())
                .filter(item -> !item.isDisabled())
                .sorted(Comparator.comparing(LDAPUser::getDepartment))
                .forEach(item -> {
                            System.out.println(item.getName() + ": " + item.getDirectPhone() + ";" + item.getMobilePhone());
                        }
                );
    }

}