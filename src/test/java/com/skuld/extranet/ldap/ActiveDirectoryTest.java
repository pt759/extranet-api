package com.skuld.extranet.ldap;

import com.skuld.extranet.ldap.domain.LDAPUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchResult;
import java.util.List;

import static com.skuld.extranet.ldap.LDAPAttributes.sAMAccountName;

@SpringBootTest
class ActiveDirectoryTest {

    @Autowired
    private ActiveDirectory ad;

    @Test
    public void testADQuery() throws NamingException {
        String searchValue = "pt184";
        String searchBy = sAMAccountName;

        ad.openConnection();
        NamingEnumeration<SearchResult> result = ad.searchUser(searchValue, searchBy);

        if (result.hasMore()) {
            List<LDAPUser> ldapUsers = LDAPUser.listOfUsers(result);
            for (LDAPUser ldapUser : ldapUsers) {
                System.out.println(ldapUser);
            }

        } else {
            System.out.println("No search result found!");
        }

        ad.closeLdapConnection();
    }
}