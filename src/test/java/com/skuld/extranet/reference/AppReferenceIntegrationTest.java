package com.skuld.extranet.reference;

import com.skuld.extranet.reference.entity.AppReference;
import com.skuld.extranet.reference.entity.AppReferenceRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class AppReferenceIntegrationTest {

    @Autowired
    private AppReferenceRepo referenceRepo;

    @Test
    void get_app_reference_values() {
        List<AppReference> appReferences = referenceRepo.findByDomain("ACCESS_CONTROL");
        assertThat(appReferences.size()).isEqualTo(1);
    }

    @Test
    public void save_and_delete_reference() {
        AppReference entity = AppReference.builder().domain("TestDomain").code("1").value("2").description("3").build();
        AppReference savedEntity = referenceRepo.save(entity);
        assertThat(savedEntity.getDescription().equalsIgnoreCase(entity.getDescription())).isTrue();

        Optional<AppReference> entityById = referenceRepo.findById(savedEntity.getId());
        referenceRepo.delete(entityById.get());
        assertThat(referenceRepo.findById(entityById.get().getId()).isPresent()).isFalse();
    }
}
