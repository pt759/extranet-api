package com.skuld.extranet.memberPerformanceBonus.pdf;

import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonus;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class MemberPerformancePdfTest {

    MemberPerformancePdf pdf = new MemberPerformancePdf("member_performance_test.pdf");

    @Test
    public void pdfMemberPerformanceBonus() {
        MemberPerformanceBonus memberPerformanceBonus = new MemberPerformanceBonus()
                .setStatCodeNames(Arrays.asList("Stena Line", "Stena Tanker Fleet", "Stena black and white"))
                .setPerformanceYear(2017)
                .setStatCode("2018.01")
                .setStatCodeName("Stena Line ")
                .setAmount(new BigDecimal("80000"))
                .setBalance(new BigDecimal("20000"))
                ;
        pdf.write(memberPerformanceBonus);
        assertFileExists(pdf.getTmpFileName());
        // If you want to inspect the file, remove this  statement
        pdf.removeFile();

    }

    private void assertFileExists(String filename) {
        File tmp = new File(filename);
        assertThat((tmp.exists() && !tmp.isDirectory())).isTrue();
    }

}