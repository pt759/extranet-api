package com.skuld.extranet.memberPerformanceBonus.pdf;

import com.skuld.extranet.core.common.SkuldWebUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.skuld.extranet.global.Global.imgFileName;

public class PDFBoxProofOfConceptTester {

    PDFBoxProofOfConceptTester tester;

    @Disabled
    @Test
    public void pdfWithText() throws IOException {
        tester.writePDF();
    }

    @Disabled
    @Test
    public void pdfWithImage() throws IOException, URISyntaxException {
        tester.writePdfWithImage();
    }

    public void writePdfWithImage() throws IOException, URISyntaxException {

        try (PDDocument doc = new PDDocument()) {

            PDPage page = new PDPage();
            doc.addPage(page);


            PDImageXObject pdImage = PDImageXObject.createFromFile(SkuldWebUtil.getImageFilePath(imgFileName, this), doc);

            int width = pdImage.getWidth();
            int height = pdImage.getHeight();

            float offset = 1f;


            try (PDPageContentStream cont = new PDPageContentStream(doc, page)) {

                PDRectangle mediaBox = page.getMediaBox();
                float yOffset = mediaBox.getHeight() - (height / 2);

                cont.drawImage(pdImage, offset, yOffset, width / 2, height / 2);
            }

            doc.save("c:/temp/image.pdf");
        }
    }

    public void writePDF() throws IOException {
        try (PDDocument doc = new PDDocument()) {

            PDPage myPage = new PDPage();
            doc.addPage(myPage);

            try (PDPageContentStream cont = new PDPageContentStream(doc, myPage)) {

                cont.beginText();

                cont.setFont(PDType1Font.TIMES_ROMAN, 12);
                cont.setLeading(14.5f);

                cont.newLineAtOffset(25, 700);
                String line1 = "World War II (often abbreviated to WWII or WW2), "
                        + "also known as the Second World War,";
                cont.showText(line1);

                cont.newLine();

                String line2 = "was a global war that lasted from 1939 to 1945, "
                        + "although related conflicts began earlier.";
                cont.showText(line2);
                cont.newLine();

                String line3 = "It involved the vast majority of the world's "
                        + "countries—including all of the great powers—";
                cont.showText(line3);
                cont.newLine();

                String line4 = "eventually forming two opposing military "
                        + "alliances: the Allies and the Axis.";
                cont.showText(line4);
                cont.newLine();

                cont.endText();
            }

            doc.save("c:\\temp\\wwii.pdf");
        }

    }
}
