package com.skuld.extranet.profiles.service;

import com.skuld.common.dto.InitializationGroupDTO;
import com.skuld.common.security.authentication.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.employeeToken;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ProfileClientServiceIntegrationTest {

    @Autowired
    private ProfileClientService profileClientService;

    @Test
    @WithMockCustomUser(token = employeeToken)
    void initialize_user_from_profile_api(){
        List<InitializationGroupDTO> initializationGroupDTOS = profileClientService.initializeUser();
        assertTrue(initializationGroupDTOS.size()>0);

    }
}