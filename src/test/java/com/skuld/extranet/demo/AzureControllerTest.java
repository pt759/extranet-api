package com.skuld.extranet.demo;


import com.skuld.common.security.service.JwtService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AzureControllerTest {
    JwtService jwtService;

    @BeforeEach
    public void setup() {
        jwtService = new JwtService();
    }

    @DisplayName("Put in a new fresh token, and you can validate it here.")
    @Disabled
    @Test
    public void validateJWT() {
        String accessToken = "<fresh token>";
        assertThat(jwtService.verifyToken(accessToken)).isTrue();
    }
}