package com.skuld.extranet.spa;

import com.skuld.extranet.core.accountOverview.AccountOverviewService;
import com.skuld.extranet.core.api.vessel.service.VesselClientService;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.claim.service.ClaimsClientService;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonusService;
import com.skuld.extranet.spa.pages.domain.SearchResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class SearchServiceTest {

    SearchService service;

    ClaimService claimService = mock(ClaimService.class);
    AccountOverviewService accountService = mock(AccountOverviewService.class);
    AzureAdUserService azureAdUserService = mock(AzureAdUserService.class);
    MemberPerformanceBonusService memberPerformanceBonusService = mock(MemberPerformanceBonusService.class);
    VesselClientService vesselClientService = mock(VesselClientService.class);
    private ClaimsClientService claimsClientService = mock(ClaimsClientService.class);

    @BeforeEach
    void setUp() {
        service = new SearchService(claimService, claimsClientService, vesselClientService, accountService, azureAdUserService, memberPerformanceBonusService);
    }

    @Test
    public void search_with_null_values_should_return_null() {
        SearchResult searchResult = service.searchClient(null, null);
        assertThat(searchResult).isNull();
    }

    @Test
    public void search_with_empty_values_should_return_null() {
        SearchResult searchResult = service.searchClient(" ", null);
        assertThat(searchResult).isNull();
    }

    @Test
    public void brokerSearchShouldReturnBlankWhenQueryStringIsBlank() {
        String queryString = " ";
        SearchResult result = service.searchBroker(queryString, "cpbhodin", 10);
        assertThat(result.getClients().size()).isEqualTo(0);
        assertThat(result.getVessels().size()).isEqualTo(0);
        assertThat(result.getClaims().size()).isEqualTo(0);
    }

    @Test
    public void should_filter_claims_on_vessel_name() {

        String vesselName = "SOMETHING";
        String statCode = "10.10";

        Claim claim1 = Claim.builder().claEventNo("1").vesselName(vesselName).imoNumber("").build();
        Claim claim2 = Claim.builder().claEventNo("2").vesselName(vesselName).imoNumber("").build();
        Claim claim3 = Claim.builder().claEventNo("3").vesselName("").imoNumber("").build();
        List<Claim> claimsForStatCode = new ArrayList<Claim>(Arrays.asList(claim1, claim2, claim3));
        when(claimService.claimListOpen(any())).thenReturn(claimsForStatCode);
        when(claimService.filterClaimsOn(any(), any())).thenCallRealMethod();

        Vessel vessel1 = new Vessel();
        Vessel vessel2 = new Vessel();
        Vessel vessel3 = new Vessel();
        List<Vessel> vessels = new ArrayList<>(Arrays.asList(vessel1, vessel2, vessel3));
        when(vesselClientService.searchForVessel(any())).thenReturn(vessels);


        SearchResult result = service.searchClient(vesselName, statCode);

        assertThat(result.getClaims()).size().isEqualTo(2);
        assertThat(result.getVessels()).size().isEqualTo(3);

    }


    @Nested
    @DisplayName("Search Client")
    class SearchClientTest {

        String claEventNo = "2000";
        String imoNumber = "123456789";

        @BeforeEach
        public void setup() {

            Claim claim1 = Claim.builder()
                    .claEventNo("10000")
                    .vesselName("bc")
                    .imoNumber(imoNumber)
                    .build();

            Claim claim2 = Claim.builder()
                    .claEventNo(claEventNo)
                    .vesselName("ha")
                    .imoNumber("123")
                    .build();

            Vessel vessel1 = Vessel.builder()
                    .imo_number(imoNumber)
                    .vesselName("abc")
                    .build();

            Vessel vessel2 = Vessel.builder()
                    .imo_number("4444444")
                    .vesselName("helo")
                    .build();

            List<Claim> claims = Arrays.asList(claim1, claim2);
            List<Vessel> vessels = Arrays.asList(vessel1, vessel2);
            when(claimService.claimListOpen(any())).thenReturn(claims);
            when(claimService.filterClaimsOn(any(), any())).thenCallRealMethod();

            when(vesselClientService.searchForVessel(any())).thenReturn(vessels);
        }


        @Test
        public void searchresult_filter_claim_event_no() {

            SearchResult result = service.searchClient(claEventNo, "xx");

            List<Claim> claims = result.getClaims();
            assertThat(claims.size()).isEqualTo(1);

            assertThat(claims)
                    .extracting("claEventNo")
                    .contains(claEventNo)
            ;

            List<Vessel> vessels = result.getVessels();
            assertThat(vessels.size()).isEqualTo(2);
        }
    }
}
