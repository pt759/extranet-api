package com.skuld.extranet.spa;

import com.google.common.collect.Iterables;
import com.skuld.extranet.ExtranetApplication;
import com.skuld.extranet.core.claim.repo.ClaimDetailRepository;
import com.skuld.extranet.core.claim.service.ClaimService;
import com.skuld.extranet.core.common.PolicyService;
import com.skuld.extranet.core.common.SkCommon;
import com.skuld.extranet.core.common.domain.CodeValueWithList;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntityVersion;
import com.skuld.extranet.core.memberstatistics.repo.StatisticsRepo;
import com.skuld.extranet.core.memberstatistics.service.MemberStatisticsService;
import com.skuld.extranet.spa.pages.domain.ProductClass;
import com.skuld.extranet.spa.pages.domain.StatisticsTableEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ExtranetApplication.class)
class ExtranetServiceTest {

    @Autowired
    private ExtranetService service;

    @Autowired
    MemberStatisticsService msService;

    @Autowired
    ClaimService claimService;

    HttpServletResponse httpResponse = mock(HttpServletResponse.class);

    @BeforeEach
    public void setup() {

        StatisticsRepo statisticsRepoMock = mock(StatisticsRepo.class);

        ClaimDetailRepository claimRepo = mock(ClaimDetailRepository.class);
        claimService = new ClaimService(claimRepo);

        service.setMsService(msService);
        service.setClaimService(claimService);
        service.setDocumentService(new DocumentService());

    }

    @Test
    @DisplayName("Get Claim Five Year Statistics by Group and Product, and add other group at the end")
    public void getClaimFiveYearGrouped() {
        String statCode = "'1000.0";
        String product = "PRODUCT1";
        Integer limit = 7;

        List<ClaimStatisticsEntity> claimList = new ArrayList<>();
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE1").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(20));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE2").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(10));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE3").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(5));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE4").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE5").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE6").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE7").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE8").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));

        List<ClaimStatisticsEntity> list = service.getClaimStatisticsByProductAndGrouped(product, limit, claimList);

        assertThat(list.size()).isEqualTo(7);
        ClaimStatisticsEntity last = Iterables.getLast(list);
        assertThat(last.getClaimsType()).isEqualTo(SkCommon.group_others_code);
        assertThat(last.getClaims()).isEqualTo(4);
    }

    @Test
    public void getClaimFiveYearGroupedWhenTotalListisLessThanLimit() {
        String statCode = "'1000.0";
        String product = "PRODUCT1";
        Integer limit = 7;

        List<ClaimStatisticsEntity> claimList = new ArrayList<>();
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE1").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(20));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE2").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(10));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE3").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(5));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE4").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE5").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));

        List<ClaimStatisticsEntity> list = service.getClaimStatisticsByProductAndGrouped(product, limit, claimList);

        assertThat(list.size()).isEqualTo(5);
        ClaimStatisticsEntity last = Iterables.getLast(list);
        assertThat(last.getClaimsType()).isEqualTo(list.get(4).getClaimsType());
    }

    @Test
    public void splitListBasedOnClassAndVersionTest() {

        List<StatisticsEntity> list = new ArrayList<>();
        StatisticsEntity entity1 = new StatisticsEntity().setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.distributed.toString());
        StatisticsEntity entity2 = new StatisticsEntity().setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.distributed.toString());
        StatisticsEntity entity3 = new StatisticsEntity().setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.distributed.toString());

        StatisticsEntity entity4 = new StatisticsEntity().setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity5 = new StatisticsEntity().setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity6 = new StatisticsEntity().setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());

        StatisticsEntity entity7 = new StatisticsEntity().setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.fiveYear.toString());
        StatisticsEntity entity8 = new StatisticsEntity().setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.fiveYear.toString());
        StatisticsEntity entity9 = new StatisticsEntity().setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.fiveYear.toString());


        list.addAll(Arrays.asList(entity1, entity2, entity3, entity4, entity5, entity6, entity7, entity8, entity9));

        List<StatisticsTableEntity> statisticsTableEntities = service.splitListBasedOnClassAndVersion(list, StatisticsEntityVersion.distributed);

        assertThat(statisticsTableEntities.size()).isEqualTo(2);
        StatisticsTableEntity piTable = statisticsTableEntities.get(0);
        StatisticsTableEntity fddTable = statisticsTableEntities.get(1);

        List<StatisticsEntity> piTableData = statisticsTableEntities.get(0).getData(); // P&I
        List<StatisticsEntity> fddTableData = statisticsTableEntities.get(1).getData(); // FDD


        assertThat(piTable.getName()).isEqualTo(ProductClass.PI.toString());
        assertThat(fddTable.getName()).isEqualTo(ProductClass.FDD.toString());
        assertThat(piTableData.size()).isEqualTo(2);

        assertThat(piTableData).extracting("version").containsOnly(StatisticsEntityVersion.distributed.toString());
        assertThat(piTableData).extracting("productClass").containsOnly(ProductClass.PI.toString());

        assertThat(fddTableData.size()).isEqualTo(1);
        assertThat(fddTableData).extracting("version").containsOnly(StatisticsEntityVersion.distributed.toString());
        assertThat(fddTableData).extracting("productClass").containsOnly(ProductClass.FDD.toString());

    }


    @DisplayName("Five Year Table should have five year back in time, excluding current Policy year")
    @Test
    public void splitListBasedOnClassAndVersionTestFiveYear() {

        PolicyService policyService = new PolicyService().setCurrentPolicyYear(2017);
        service.setPolicyService(policyService);

        List<StatisticsEntity> list = new ArrayList<>();
        // There is no such thing as fiveyear in
        StatisticsEntity entity1 = new StatisticsEntity().setPolicyYear("2012").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity2 = new StatisticsEntity().setPolicyYear("2013").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity3 = new StatisticsEntity().setPolicyYear("2014").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity4 = new StatisticsEntity().setPolicyYear("2015").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity5 = new StatisticsEntity().setPolicyYear("2016").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity6 = new StatisticsEntity().setPolicyYear("2017").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString());

        StatisticsEntity entity7 = new StatisticsEntity().setPolicyYear("2014").setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity8 = new StatisticsEntity().setPolicyYear("2015").setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity9 = new StatisticsEntity().setPolicyYear("2016").setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());
        StatisticsEntity entity10 = new StatisticsEntity().setPolicyYear("2017").setProductClass(ProductClass.FDD.toString()).setVersion(StatisticsEntityVersion.actual.toString());


        list.addAll(Arrays.asList(entity1, entity2, entity3, entity4, entity5, entity6, entity7, entity8, entity9, entity10));

        List<StatisticsTableEntity> statisticsTableEntities = service.splitListBasedOnClassAndVersion(list, StatisticsEntityVersion.fiveYear);
        assertThat(statisticsTableEntities.size()).isEqualTo(2); // both PI and FDD

        StatisticsTableEntity piTable = statisticsTableEntities.get(0);
        StatisticsTableEntity fddTable = statisticsTableEntities.get(1);

        assertThat(piTable.getData().size()).isEqualTo(5);
        assertThat(fddTable.getData().size()).isEqualTo(3);

    }

    @Test
    public void testChoosePolicyYearSorted() {
        List<StatisticsEntity> list = new ArrayList<>();
        list.add(new StatisticsEntity().setPolicyYear("2015").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString()));
        list.add(new StatisticsEntity().setPolicyYear("2013").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString()));
        list.add(new StatisticsEntity().setPolicyYear("2014").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString()));
        list.add(new StatisticsEntity().setPolicyYear("2012").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString()));
        list.add(new StatisticsEntity().setPolicyYear("2016").setProductClass(ProductClass.PI.toString()).setVersion(StatisticsEntityVersion.actual.toString()));

        List productYearFilter = service.getProductYearFilter(list);
        CodeValueWithList codeValueWithList = (CodeValueWithList) productYearFilter.get(2);
        List<String> years = (List<String>) codeValueWithList.getList();
        assertThat(years.get(0)).isEqualToIgnoringCase("2012");
        assertThat(years.get(1)).isEqualToIgnoringCase("2013");
        assertThat(years.get(2)).isEqualToIgnoringCase("2014");
        assertThat(years.get(3)).isEqualToIgnoringCase("2015");
        assertThat(years.get(4)).isEqualToIgnoringCase("2016");
    }


}