package com.skuld.extranet.spa;

import com.skuld.extranet.ExtranetApplication;
import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.common.domain.CodeValueWithList;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntityVersion;
import com.skuld.extranet.core.memberstatistics.service.MemberStatisticsService;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.spa.pages.StatisticsPage;
import com.skuld.extranet.spa.pages.VesselDetailPage;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.StatYearType;
import com.skuld.extranet.spa.pages.domain.StatisticsProductData;
import com.skuld.extranet.spa.pages.domain.StatisticsTableEntity;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static com.skuld.extranet.spa.pages.domain.ProductClass.FDD;
import static com.skuld.extranet.spa.pages.domain.ProductClass.PI;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExtranetApplication.class)
class ExtranetServiceIntegrationTest {
    final static Logger logger = LoggerFactory.getLogger(ExtranetServiceIntegrationTest.class);


    @Autowired
    private ExtranetService service;

    @Autowired
    private MemberStatisticsService msService;

    @Test
    @DisplayName("Test Statistics Page - Content 2")
    public void statisticPageTest() {
        StatisticsPage page = service.statisticsPage("1692.1");

        StatisticsProductData statisticsProductData = page.getProductDataMap().get(Product.owner.toString());
        List<StatisticsTableEntity> tables = statisticsProductData.getTables().get(StatYearType.statYears.getCode());
        assertThat(tables.size()).isEqualTo(2);

        StatisticsTableEntity table = tables.get(0);
        assertThat(table.getName()).isEqualTo(PI.value());
        assertThat(table.getData().size()).isGreaterThan(0);

        StatisticsTableEntity tableFDD = tables.get(1);
        assertThat(tableFDD.getName()).isEqualTo(FDD.value());
        assertThat(tableFDD.getData().size()).isGreaterThan(0);
    }

    @Test
    @DisplayName("Test Statistics Page - Filter")
    public void statisticPageTestFilter() {
        StatisticsPage page = service.statisticsPage("1692.1");
        assertThat(page.getFilter().getProducts().size()).isEqualTo(1);
        StatisticsProductData statisticsProductData = page.getProductDataMap().get(Product.owner.toString());
        List<CodeValueWithList> statYearsFilter = statisticsProductData.getStatYearsFilter();
        assertThat(statYearsFilter.size()).isEqualTo(3);
    }


    @Test
    @DisplayName("Test Statistics Page - Content for Claim five year statistics")
    public void statisticPageTestClaims5Year() {
        List<ClaimStatisticsEntity> claimStatisticsFiveYear = msService.statisticsClaimFiveYear("1692.1");
        List<ClaimStatisticsEntity> list = service.getClaimStatisticsByProductAndGrouped(Product.owner.toString(), 7, claimStatisticsFiveYear);
        assertThat(list.size()).isGreaterThan(4);
    }

    @Test
    @DisplayName("Test Vessel Detail Page - content of detail page ")
    public void vesselDetailPageTestForSpecificVessel() {
        VesselDetailPage vesselDetailPage = service.vesselDetailPage("111933", africaExpressStatCode);
        assertThat(vesselDetailPage.getVessel()).isNotNull();
    }

    @Disabled //No documents for 2021
    @Test
    public void should_find_documents_for_africa_express() {
        List<DocumentRef> vesselDetailPage = service.vesselDetailDocuments("1770473", africaExpressStatCode);
        assertThat(vesselDetailPage.size()).isGreaterThan(0);
    }


    @Disabled //TODO RESTAPI-686
    @Test
    @DisplayName("Vessel Detail Page for Specific vessel for Frontline Management : Takes about 13 seconds in production")
    public void vesselDetailPageTestForVesselForFrontlineManagement() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("vesselDetailPage");
        VesselDetailPage vesselDetailPage = service.vesselDetailPage("174149", "7249.0");
        assertThat(vesselDetailPage.getVessel()).isNotNull();
        stopWatch.stop();
        logger.info("vesselDetailPage total milliseconds: " + stopWatch.getTotalTimeMillis());
    }

    @Disabled //No documents for 2021
    @Test
    public void should_find_documents_for() {
        List<DocumentRef> vesselDetailPage = service.vesselDetailDocuments("174149", "7249.0");
        assertThat(vesselDetailPage.size()).isGreaterThan(0);
    }

    @Disabled //TODO RESTAPI-686
    @Test
    public void getDocumentForVessel111933_6554_50() {
        Vessel vessel = new Vessel();
        vessel.setVesselId("111933").setStatCode(africaExpressStatCode).setVesselName("KingKong");
        List<DocumentRef> documents = service.getDocumentsForVessel(vessel);
        assertThat(documents.size()).isGreaterThan(0);
    }

    @Disabled //TODO RESTAPI-686
    @Test
    public void getDocumentForVesse6801() {
        Vessel vessel = new Vessel();
        vessel.setVesselId("6801").setStatCode("5461.1");
        List<DocumentRef> documents = service.getDocumentsForVessel(vessel);
        assertThat(documents.size()).isEqualTo(8);
    }

    @Test
    @DisplayName("Test Statistics Page - Download")
    public void statisticsDownloadTest() throws Exception {
        StatisticsPage page = service.statisticsPage("1692.1");

        StatisticsExcelConverter excel = new StatisticsExcelConverter("Member Statistics.xlsx", "Statistics");

        File file = new File("memberstatistics.xlsx");

        if (file.exists()) {
            file.delete();
        }

        FileOutputStream outputStream = new FileOutputStream(file);
        StatisticsProductData mutual_liability = page.getProductDataMap().get(Product.owner.value());
        List<StatisticsTableEntity> tables = mutual_liability.getTables().get(StatYearType.statYears.getCode());

        excel.streamWorkbookFrom(tables, outputStream);

        assertThat(file.exists()).isEqualTo(true);
    }

    @Test
    public void getStatisticsAsBase64TEst() {
        String statisticsAsBase64 = service.getStatisticsAsBase64("1692.1", Product.owner, StatisticsEntityVersion.actual);
        assertThat(statisticsAsBase64).isNotNull();

    }
}
