package com.skuld.extranet.spa;

import com.skuld.extranet.core.archive.domain.DocumentRef;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class DocumentServiceTest {

    DocumentService service;

    @BeforeEach
    public void setup(){
        service = new DocumentService();
    }

    @Test
    public void testConvertURlDocuments() {
        String statCode = "10.0";
        DocumentRef document = new DocumentRef().setRequestId("1000");
        String s = service.convertDocumentUrl(statCode, document);
        assertThat(s).containsIgnoringCase("1000");
        assertThat(s).containsIgnoringCase("10.0");
    }

    @Test
    public void testGroupDocumentsByProgramId(){
        List<DocumentRef> docs = new ArrayList<>();
        docs.add(new DocumentRef().setProgramId("RS100").setRequestDate(LocalDate.parse("2018-02-22")).setRequestId("15926661").setFileName("A").setProductClass("FDD"));//
        docs.add(new DocumentRef().setProgramId("RS100").setRequestDate(LocalDate.parse("2018-02-20")).setRequestId("15926412").setFileName("A").setProductClass("FDD"));//
        docs.add(new DocumentRef().setProgramId("RS100").setRequestDate(LocalDate.parse("2018-02-22")).setRequestId("15926663").setFileName("A").setProductClass("PI"));//
        docs.add(new DocumentRef().setProgramId("RS10").setRequestDate(LocalDate.parse("2018-01-22")).setRequestId("15926610").setFileName("B").setProductClass("PI"));//
        docs.add(new DocumentRef().setProgramId("RS100").setRequestDate(LocalDate.parse("2018-02-22")).setRequestId("15926662").setFileName("A").setProductClass("PI"));//
        docs.add(new DocumentRef().setProgramId("RS100").setRequestDate(LocalDate.parse("2018-02-22")).setRequestId("15926661").setFileName("A").setProductClass("PI"));//
        docs.add(new DocumentRef().setProgramId("RS101").setRequestDate(LocalDate.parse("2018-02-22")).setRequestId("15926611").setFileName("C").setProductClass("PI"));//

        Map<String, List<DocumentRef>> map = service.groupDocumentsByProgramId(docs);

        assertThat(map.size()).isEqualTo(4);
        List<DocumentRef> rs100 = map.get("RS100 PI");
        assertThat(rs100.get(0).getRequestId()).isEqualTo("15926663");
    }
}