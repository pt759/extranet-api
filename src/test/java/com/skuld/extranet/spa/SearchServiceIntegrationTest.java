package com.skuld.extranet.spa;

import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.ldap.domain.LDAPUser;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverviewPage;
import com.skuld.extranet.spa.pages.domain.SearchResult;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static com.skuld.common.global.GlobalTestValues.employeeToken;
import static com.skuld.extranet.core.claim.domain.EventStatus.closed;
import static com.skuld.extranet.core.claim.domain.EventStatus.open;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
class SearchServiceIntegrationTest {

    String statCodeGroupLoginAonRiskSolution = "aonrisksoluti";


    private final SearchService searchService;

    @Autowired
    SearchServiceIntegrationTest(SearchService searchService) {
        this.searchService = searchService;
        }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void clientSearchEmptyString() {
        SearchResult result1 = searchService.searchClient("", "1692.1");
        SearchResult result2 = searchService.searchClient("", "");
        SearchResult result3 = searchService.searchClient(" ", " ");

        assertThat(result1).isNull();
        assertThat(result2).isNull();
        assertThat(result3).isNull();
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void brokerSearchOnlyContainOpenClaims() {
        String queryString = "S";
        SearchResult result = searchService.searchBroker(queryString, "africaex", 8);

        assertThatClaimsIsOpen(result.getClaims());
        assertClaimNameContains(queryString, result.getClaims());
        assertClientNameContains(queryString, result.getClients());
        assertVesselNameContains(queryString, result.getVessels());

    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void brokerSearchIncludeClient() {
        String queryString = "africa";
        SearchResult result = searchService.searchBroker(queryString, "cpbhodin", 10);
        assertThat(result.getClients().size()).isEqualTo(1);
        assertClientNameContains(queryString, result.getClients());
        assertThat(result.getVessels().size()).isEqualTo(0);
        assertThat(result.getClaims().size()).isGreaterThanOrEqualTo(0);
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void brokerSearchWithLowerCase() {
        int limit = 8;
        SearchResult result = searchService.searchBroker("s", "aonrisksoluti", limit);
        List<Vessel> vessels = result.getVessels();
        assertThat(vessels.size()).isGreaterThan(0);
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void brokerSearchWithLimit() {
        int limit = 1;
        SearchResult result = searchService.searchBroker("s", "africaex", limit);
        assertThat(result.getVessels().size()).isGreaterThan(0).isLessThanOrEqualTo(limit);
        assertThat(result.getClaims().size()).isGreaterThan(0).isLessThanOrEqualTo(limit);
        assertThat(result.getClients().size()).isGreaterThan(0).isLessThanOrEqualTo(limit);
    }


    @DisplayName("Broker search should include Stat Code and Name in vessel")
    @Test
    @WithMockCustomUser(token = employeeToken)
    public void brokerSearchVesselIncludeStatName() {
        int limit = 1;
        SearchResult result = searchService.searchBroker("s", statCodeGroupLoginAonRiskSolution, limit);
        assertThat(result.getVessels().size()).isGreaterThan(0).isLessThanOrEqualTo(limit);
        Vessel vessel = result.getVessels().get(0);
        assertThat(vessel.getStatCode()).isNotNull();
        assertThat(vessel.getStatCodeName()).isNotNull();
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void brokerSearchWithLimitOf10() {
        int limit = 10;
        SearchResult result = searchService.searchBroker("s", statCodeGroupLoginAonRiskSolution, limit);
        List<Vessel> vessels = result.getVessels();
        assertThat(vessels.size()).isGreaterThan(0)
                .isLessThanOrEqualTo(limit);
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void clientSearchOnlyContainOpenClaims() {
        SearchResult result = searchService.searchClient("S", "22280.00");
        List<Claim> claims = result.getClaims();
        assertThatClaimsIsOpen(claims);
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void clientOverViewPageTest() {
        ClientOverviewPage page = searchService.clientOverviewPage("0769.0");
        assertThat(page).isNotNull();
        assertThat(page.getAccountManager()).isNotNull();
        assertThat(page.getMemberPerformanceBonus().getAmount()).isNotNull();
        List<LDAPUser> claimHandlers = page.getClaimHandlers();

        assertThat(claimHandlers.size()).isGreaterThan(0);
        LDAPUser actual = claimHandlers.get(0);
        assertThat(actual.getSAMAccountName()).isNotNull();
    }


    @Test
    @WithMockCustomUser(token = employeeToken)
    public void clientOverViewPageTestFor33287_00() {
        ClientOverviewPage page = searchService.clientOverviewPage("33287.00");
        List<LDAPUser> claimHandlers = page.getClaimHandlers();

        assertThat(claimHandlers.size()).isGreaterThan(1);
        LDAPUser actual = claimHandlers.get(0);
        assertThat(actual.getSAMAccountName()).isNotNull();
    }

    @Test
    void
    get_claim_summary_for_overview_page(){
        ClientClaimsSummaryDTO summaryDTO = searchService.getClaimsOverviewSummary(africaExpressStatCode);
        assertTrue(summaryDTO.getVesselsWithClaim()>1);
        assertTrue(summaryDTO.getOpenClaims()>1);
    }



    private void assertThatClaimsIsOpen(List<Claim> claims) {
        assertThat(claims)
                .extracting("status")
                .contains(open.toString())
                .doesNotContain(closed.toString());
    }

    private void assertClaimNameContains(String queryString, List<Claim> list) {
        assertThat(list.size()).isGreaterThan(0);
        for (Claim claim : list) {
            assertThat(claim.getVesselName()).contains(queryString);
        }
    }

    private void assertClientNameContains(String queryString, List<AccountOverview> list) {
        assertThat(list.size()).isGreaterThan(0);
        for (AccountOverview accountOverview : list) {
            assertThat(accountOverview.getClientName()).containsIgnoringCase(queryString);
        }
    }

    private void assertVesselNameContains(String queryString, List<Vessel> list) {
        assertThat(list.size()).isGreaterThan(0);
        for (Vessel vessel : list) {
            assertThat(vessel.getVesselName()).containsIgnoringCase(queryString);
        }
    }
}
