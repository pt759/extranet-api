package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoice;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountInvoiceItem;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountTotal;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountVessel;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

import static com.skuld.common.domain.YesNo.No;
import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static com.skuld.extranet.GlobalForTest.africaExpressAccountNo535775;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.GroupOther;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType.All;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType.Unpaid;
import static org.assertj.core.api.Assertions.assertThat;

@Log4j2
@SpringBootTest
public class FinancialPageServiceStatementOfAccountIntegrationTest {

    @Autowired
    private FinancialPageService financialPageService;


    @DisplayName("No other accounts")
    @Test
    public void getStatementOfAccountContainsRowsWithOther() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount("21722.00", "545349", Unpaid, No);
        assertThat(page.getAccountInformation()).isNotNull();
        List<StatementOfAccountVessel> rows = page.getVessels();
        assertThat(rows.size()).isGreaterThan(0);
//        assertThat(rows).extracting("vesselName").contains(GroupOther);
    }

    @Test
    void test_statement_of_account_overview_performance() {
        long startTime = System.nanoTime();
        financialPageService.getStatementOfAccount("37951.00", "586969", Unpaid, No);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000;

        log.debug("Financial overview duration ms: " + duration);
        log.warn("This is getting slower, I needed to turn this up a bit, because doing something else, but need to check this out!!");
        assertThat(duration).isLessThan(6000);
    }

    @Disabled //No open items
    @Test
    public void testPremiumAccount() {

        StatementOfAccountPage page = financialPageService.getStatementOfAccount(africaExpressStatCode, africaExpressAccountNo535775, Unpaid, No);
        assertThat(page.getAccountInformation()).isNotNull();
        assertThat(page.getAccountInformation().getAccountType()).isEqualByComparingTo(AccountGroup.PREMIUM);
        assertThat(page.getUrlDownloads().size()).isEqualTo(2);
        assertThat(page.getVessels().size()).isGreaterThan(0);
        assertThat(page.getBankDetails().size()).isGreaterThan(0);
    }

    @Disabled
    @Test
    public void testClaimAccount() {
        // Claim Account
        // http://localhost:8080/financialpagecontroller/soa/6554.50/540682/U/N/
        StatementOfAccountPage page = financialPageService.getStatementOfAccount(africaExpressStatCode, "540682", Unpaid, No);
        assertThat(page.getVessels().size()).isGreaterThan(0);
        assertThat(page.getAccountInformation()).isNotNull();
        assertThat(page.getAccountInformation().getAccountType()).isEqualByComparingTo(AccountGroup.CLAIM);
        assertThat(page.getUrlDownloads().size()).isEqualTo(2);
    }

    @Disabled
    @DisplayName("Find  Totals")
    @Test
    public void testTotals() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount(africaExpressStatCode, "584678", Unpaid, No);
        List<StatementOfAccountVessel> rows = page.getVessels();
        assertThat(rows.size()).isEqualTo(2);
        String vesselName = "JACAMAR ARROW";
        assertThat(rows).filteredOn("vesselName", vesselName).size().isEqualTo(1);
        StatementOfAccountVessel row = rows.stream().filter(item -> item.getVesselName().equalsIgnoreCase(vesselName)).findFirst().orElse(null);
        assertThat(row).isNotNull();
    }


    @Disabled //Flaky
    @DisplayName("Others should be sorted to last in list - .non_owner_account_568323_has_2_SOA_report_urlNeed to find an account with other group")
    @Test
    public void testTotalsForOffshoreOtherLastInList() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount("10822.00", "581743", Unpaid, No);
        List<StatementOfAccountVessel> rows = page.getVessels();
        assertThat(rows.size()).isEqualTo(5);
        assertThat(rows.get(4).getVesselName()).isEqualTo(GroupOther);
    }

    @Disabled
    @DisplayName("Find Totals for Fred Olsen Energy")
    @Test
    public void testTotalsForOffshore() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount("10822.00", "581743", Unpaid, No);
        List<StatementOfAccountVessel> rows = page.getVessels();
        assertThat(rows.size()).isEqualTo(5);

        String vesselName = "BLACKFORD DOLPHIN";
        assertThat(rows).filteredOn("vesselName", vesselName).size().isEqualTo(1);
        StatementOfAccountVessel vessels = rows.stream().filter(item -> item.getVesselName().equalsIgnoreCase(vesselName)).findFirst().orElse(null);
        assertThat(vessels).isNotNull();
        assertThat(vessels.getInvoiceTotals().size()).isEqualTo(2);
        List<StatementOfAccountTotal> invoiceTotals = vessels.getInvoiceTotals();
        assertThat(invoiceTotals.size()).isEqualTo(2);

        StatementOfAccountTotal statementOfAccountRowTotal = findStatementOfAccountTotal("GBP", invoiceTotals);
        assertThat(statementOfAccountRowTotal.getCurrency()).isEqualTo("GBP");
        assertThat(statementOfAccountRowTotal.getOriginalAmount()).isEqualByComparingTo(new BigDecimal("34062.81"));
        assertThat(statementOfAccountRowTotal.getBalance()).isEqualByComparingTo(new BigDecimal("34062.81"));
        assertThat(statementOfAccountRowTotal.getDueBalance()).isEqualByComparingTo(new BigDecimal("13150.00"));

        statementOfAccountRowTotal = findStatementOfAccountTotal("USD", invoiceTotals);
        assertThat(statementOfAccountRowTotal.getCurrency()).isEqualTo("USD");
        assertThat(statementOfAccountRowTotal.getOriginalAmount()).isEqualByComparingTo(new BigDecimal("-5091.7"));
        assertThat(statementOfAccountRowTotal.getBalance()).isEqualByComparingTo(new BigDecimal("-5091.7"));
        assertThat(statementOfAccountRowTotal.getDueBalance()).isEqualByComparingTo(new BigDecimal("-5091.7"));

    }


    @Disabled
    @DisplayName("TotalVessel and AccountInformation should have equal Balance and Due Balance per currency")
    @Test
    public void testThatAccountInfoTotalsAndTotalsAreEqual() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount("10822.00", "581743", Unpaid, No);
        List<StatementOfAccountTotal> vesselTotals = page.getVesselTotals();
        List<AccountSum> accountSums = page.getAccountInformation().getAccountSum();
        assertThat(vesselTotals.size()).isEqualTo(accountSums.size());

        assertTotalAndSumIsEqual("USD", vesselTotals, accountSums);
        assertTotalAndSumIsEqual("GBP", vesselTotals, accountSums);
    }

    private void assertTotalAndSumIsEqual(String currency, List<StatementOfAccountTotal> vesselTotals, List<AccountSum> accountSums) {
        StatementOfAccountTotal statementOfAccountTotal = findStatementOfAccountTotal(currency, vesselTotals);
        AccountSum accountSum = findAccount(currency, accountSums);
        assertThat(statementOfAccountTotal.getBalance()).isEqualByComparingTo(accountSum.getBalance());
        assertThat(statementOfAccountTotal.getDueBalance()).isEqualByComparingTo(accountSum.getDueBalance());
        assertThat(statementOfAccountTotal.getCurrency()).isEqualTo(accountSum.getCurrency());
    }

    private AccountSum findAccount(String currency, List<AccountSum> list) {
        return list.stream().filter(item -> item.getCurrency().equalsIgnoreCase(currency)).findFirst().orElse(null);
    }

    private StatementOfAccountTotal findStatementOfAccountTotal(String currency, List<StatementOfAccountTotal> vesselTotals) {
        return vesselTotals.stream().filter(item -> item.getCurrency().equalsIgnoreCase(currency)).findFirst().orElse(null);
    }

    @Disabled
    @DisplayName("Multiple Currencies gives multiple totals - but 586811 only has EUR open")
    @Test
    public void testStatementOfAccountMultipleCurrenciesAndTotals() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount("anystring", "586811", All, No);
        List<StatementOfAccountVessel> rows = page.getVessels();
        assertThat(rows.size()).isGreaterThan(0);
        assertThat(rows).filteredOn("vesselName", "AIRONE").size().isEqualTo(1);

        StatementOfAccountVessel airone = rows.stream().filter(item -> item.getVesselName().equalsIgnoreCase("AIRONE")).findFirst().orElse(null);
        assertThat(airone).isNotNull();
        assertThat(airone.getInvoiceTotals().size()).isEqualTo(2);
    }

    @Disabled //mv_charter_delivery_report is missing account_no
    @DisplayName("Test Deposit Account Page")
    @Test
    public void testDeposit() {
        StatementOfPremiumDepositAccountPage page = financialPageService.getDepositStatementOfAccount("582099");
        assertThat(page.getDeclarations().size()).isGreaterThan(0);
        assertThat(page.getCreditController()).isNotNull();
    }

    @Disabled
    @Test
    public void mutual_credit_should_be_under_vessel_not_other() {
        StatementOfAccountPage page = financialPageService.getStatementOfAccount("7669.0", "574339", All, No);

        assertThat(page.getVessels()).size().isGreaterThan(0);
        StatementOfAccountVessel aline_b = page.getVessels().stream().filter(item -> item.getVesselName().equalsIgnoreCase("ALINE B")).findFirst().orElse(null);
        StatementOfAccountInvoice statementOfAccountInvoice = aline_b.getInvoices().stream().filter(item -> item.getInvoiceNo().equalsIgnoreCase("9901652692")).findFirst().orElse(null);
        StatementOfAccountInvoiceItem mutual = statementOfAccountInvoice.getDetails().stream().filter(item -> item.getItemDescription().contains("Mutual")).findFirst().orElse(null);
        assertThat(mutual).isNotNull();
    }

    @Disabled //Flaky
    @DisplayName("Exclude Instalment report url for non owner accounts")
    @Test
    public void non_owner_account_568323_has_2_SOA_report_urls(){
        String accountNo = "568323";
        String statCode = "32136.00";
        StatementOfAccountPage statementOfAccountPage = financialPageService.getStatementOfAccount(statCode,accountNo, AccountItemEntity.ItemType.Unpaid, No);
        assertThat(statementOfAccountPage.getUrlDownloads().size()).isEqualTo(2);
    }
}

