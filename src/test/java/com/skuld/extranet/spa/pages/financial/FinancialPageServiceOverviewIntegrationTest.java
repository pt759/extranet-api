package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.AccountStatus;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccountData;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccounts;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.skuld.extranet.GlobalForTest.creditControlEmail;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FinancialPageServiceOverviewIntegrationTest {

    @Autowired
    private FinancialPageService financialPageService;

    private final String accountNo = "535775";
    private final String africaExpressStatCode = "6554.50";

    @Disabled //Flaky
    @Test
    public void financial_overview_does_not_throw_exception() {
        FinancialOverviewPage page = financialPageService.getFinancialOverview(africaExpressStatCode);
        assertThat(page.getCreditControllers().size()).isGreaterThan(0);
        assertThat(page.getFinancialOverviewAccounts().getAllAccounts().size()).isGreaterThan(0);
    }

    @Disabled //Flaky
    @Test
    public void testFinancialOverviewPageForDefaultCreditController() {
        FinancialOverviewPage page = financialPageService.getFinancialOverview("35104.00");
        assertThat(page.getCreditControllers().size()).isEqualTo(1);
        assertThat(page.getCreditControllers().get(0).getEmail().equalsIgnoreCase(creditControlEmail));
        assertThat(page.getFinancialOverviewAccounts().getAllAccounts().size()).isGreaterThan(0);
    }

    @Disabled //Flaky
    @DisplayName("Attic Forrest have one premium account")
    @Test
    public void testGetOverviewForAtticForrest() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("17270.00");


        assertThat(financialOverview).isNotNull();
        List<FinancialOverviewAccountData> premiumAccounts = financialOverview.getFinancialOverviewAccounts().getPremiumAccounts();
        assertThat(premiumAccounts.size()).isEqualTo(1);
    }

    @Disabled //No open items
    @DisplayName("Attic Forrest has one claim account")
    @Test
    public void get_overview_for_Attic_Forrest_1_claims_account() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("17270.00");
        assertThat(financialOverview).isNotNull();
        List<FinancialOverviewAccountData> accounts = financialOverview.getFinancialOverviewAccounts().getClaimAccounts();
        assertThat(accounts.size()).isEqualTo(1);
    }

    @Disabled //Flaky
    @DisplayName("Bulk Trading has Claim accounts ")
    @Test
    public void testGetServiceSupplierAccountForStenaBulk() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("7534.0");
        List<FinancialOverviewAccountData> allAccounts = financialOverview.getFinancialOverviewAccounts().getClaimAccounts();
        assertThat(allAccounts).filteredOn("accountNo", "515009").size().isGreaterThan(0);
    }

    @Disabled //Flaky
    @DisplayName("Wallenius Wilhelmsen LogisticTrading has Claim accounts ")
    @Test
    public void wallenius_have_claim_account() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("37048.00");
        List<FinancialOverviewAccountData> allAccounts = financialOverview.getFinancialOverviewAccounts().getClaimAccounts();
        assertThat(allAccounts).filteredOn("accountNo", "587570").size().isEqualTo(1);
    }

    @Disabled //Flaky
    @DisplayName("Mercuria should have Claim Account ")
    @Test
    public void testGetMercuriaAccounts() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("21108.00");
        List<FinancialOverviewAccountData> accounts = financialOverview.getFinancialOverviewAccounts().getClaimAccounts();
        assertThat(accounts).size().isGreaterThan(0);
    }

    @Disabled //Flaky
    @DisplayName("Account for Reports - Africa Express Line Ltd.")
    @Test
    public void testGetAccountsForAfricaExpressLine() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview(africaExpressStatCode);
        List<FinancialOverviewAccountData> accounts = financialOverview.getFinancialOverviewAccounts().getAllAccounts();
        assertThat(accounts.size()).isEqualTo(financialOverview.getFinancialOverviewAccounts().getAllAccounts().size());
    }

    @Disabled
    //Run in PROD
    @Test
    public void copenhagen_merchants_should_have_one_deposit_account() {
        FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("36948.00");
        FinancialOverviewAccounts accounts = financialOverview.getFinancialOverviewAccounts();
        assertThat(accounts.getPremiumAccounts().size()).isEqualTo(2);
        assertThat(accounts.getPremiumAccounts())
                .filteredOn(x -> x.getAccountType().equals(AccountGroup.PREMIUM_DEPOSIT))
                .extracting("accountStatus")
                .containsOnly(AccountStatus.open);
    }

    @Disabled //No open items
    @Nested
    class africaExpressOverview {

        @DisplayName("Deposit Account For Africa Express Line Ltd. has given values.")
        @Test
        public void depositAccountForAfricaExpress() {
            FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview(africaExpressStatCode);
            List<FinancialOverviewAccountData> accounts = financialOverview.getFinancialOverviewAccounts().getPremiumAccounts();
            assertThat(accounts).filteredOn("accountNo", "586266").size().isEqualTo(1);

            Optional<FinancialOverviewAccountData> first = accounts.stream().filter(x -> x.getAccountNo().equals("586266")).findFirst();
            assertThat(first.isPresent());
            FinancialOverviewAccountData account = first.get();

            assertThat(account.getAccountSum().size()).isEqualTo(1);
            assertThat(account.getAccountSum().get(0).getBalance()).isEqualByComparingTo(new BigDecimal("17966.00"));
            assertThat(account.getAccountSum().get(0).getDueBalance()).isEqualByComparingTo(new BigDecimal("17966.00"));
        }

        @DisplayName("Africa Express Line Ltd. deposit account for current year.")
        @Test
        public void depositAccountsForAfricaExpress() {
            FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview(africaExpressStatCode);
            List<FinancialOverviewAccountData> accounts = financialOverview.getFinancialOverviewAccounts().getPremiumAccounts();
            assertThat(accounts)
                    .filteredOn("accountType", AccountGroup.PREMIUM_DEPOSIT)
                    .extracting("accountNo").contains("586266");
        }


        @DisplayName("Africa Express Line Ltd. deposit account for current year " +
                "and closed account for previous year when extending DepositLastTransDate to 1020 days.")
        @Test
        public void depositAccountsForAfricaExpressWithPreviousYearAccount() {
            financialPageService.setDaysSinceLastTransactionDepositLimit(720);
            FinancialOverviewPage financialOverview = financialPageService.getFinancialOverview("6554.50");
            List<FinancialOverviewAccountData> accounts = financialOverview.getFinancialOverviewAccounts().getPremiumAccounts();
            assertThat(accounts)
                    .filteredOn("accountType", AccountGroup.PREMIUM_DEPOSIT)
                    .extracting("accountNo").containsOnly("582067", "586266");
        }

        @Test
        public void testGetFinancialOverviewAccountsFor() {
            List<AccountEntity> allAccounts = List.of(new AccountEntity().setAccountNo(accountNo).setAccountType("1"));

            StatCodeAccountEntity entity = new StatCodeAccountEntity().setStatCode(africaExpressStatCode).setAccountNo(accountNo);
            List<StatCodeAccountEntity> statCodeAccountEntities = new ArrayList<>();
            statCodeAccountEntities.add(entity);
            FinancialOverviewAccounts result = financialPageService.getFinancialOverviewAccountsFor(statCodeAccountEntities, allAccounts);
            assertThat(result.getAllAccounts().size()).isGreaterThan(0);
            assertThat(result.getPremiumAccounts().size()).isGreaterThan(0);
        }

        @DisplayName("Account Overview List only contain accounts with Balance not equal to zero.")
        @Test
        public void testAccountListContainsOnlyBalanceOfNotZero() {
            List<AccountEntity> allAccounts = List.of(new AccountEntity().setAccountNo(accountNo).setAccountType("1"));
            StatCodeAccountEntity entity = new StatCodeAccountEntity().setStatCode(africaExpressStatCode).setAccountNo(accountNo);
            List<StatCodeAccountEntity> statCodeAccountEntities = new ArrayList<>();
            statCodeAccountEntities.add(entity);
            FinancialOverviewAccounts result = financialPageService.getFinancialOverviewAccountsFor(statCodeAccountEntities, allAccounts);
            assertThat(result.getPremiumAccounts()).filteredOn(item -> item.hasAccountBalance()).size().isGreaterThan(0);
        }
    }


}
