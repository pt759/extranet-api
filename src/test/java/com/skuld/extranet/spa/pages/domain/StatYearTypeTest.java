package com.skuld.extranet.spa.pages.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class StatYearTypeTest {

    @Test
    public void testStatYearTYype(){
        StatYearType type = StatYearType.fiveYears;
        assertThat(StatYearType.fromString("5YEAR")).isEqualTo(type);

    }

}