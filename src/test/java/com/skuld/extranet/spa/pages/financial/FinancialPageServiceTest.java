package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.ExtranetApplication;
import com.skuld.extranet.core.financial.domain.AccountGroup;
import com.skuld.extranet.core.financial.domain.AccountStatus;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.deposit.Declaration;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccountData;
import com.skuld.extranet.core.financial.domain.overview.FinancialOverviewAccounts;
import com.skuld.extranet.core.financial.service.FinancialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.skuld.extranet.GlobalForTest.africaExpressAccountNo535775;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntityTest.accountNo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ExtranetApplication.class)
class FinancialPageServiceTest {

    FinancialService financialService = mock(FinancialService.class);
    FinancialPageService service = new FinancialPageService(financialService);

    private final String nameOfAccount = "Name of Account";
    private final String policyHolder = "Oddbjorn";
    private final String ptUserName = "pt184";
    private List<AccountEntity> allAccounts;
    private AccountEntity matchAccount;
    List<AccountSum> listAccountSums;

    @BeforeEach
    public void setup() {

        //service.setFinancialService(financialService);

        AccountSum accountSum = new AccountSum()
                .setBalance(new BigDecimal("100"))
                .setDueBalance(new BigDecimal("40"))
                .setCurrency("USD");

        listAccountSums = new ArrayList<>(Arrays.asList(accountSum));

        when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(listAccountSums);

        matchAccount = new AccountEntity()
                .setAccountNo(africaExpressAccountNo535775)
                .setName(nameOfAccount)
                .setAccountType("1")
                .setCurrency("USD")
                .setCreditControllerPtUserId(ptUserName)
                .setAccountSum(accountSum);
        when(financialService.matchAccount(any(), anyList())).thenReturn(matchAccount);


        allAccounts = new ArrayList<>();
        AccountEntity accountEntity = new AccountEntity().setAccountNo(accountNo);
        allAccounts.add(accountEntity);

    }

    @Test
    public void testFindAccountInfo() {
        StatCodeAccountEntity statCodeAccountEntity = new StatCodeAccountEntity().setAccountNo(accountNo).setPolicyHolder(policyHolder);
        FinancialOverviewAccountData accountInfo = service.findOverviewAccountData(statCodeAccountEntity, allAccounts);

        assertThat(accountInfo.getAccountNo()).isEqualTo(accountNo);
        assertThat(accountInfo.getAccountName()).isEqualTo(nameOfAccount);
        assertThat(accountInfo.getPolicyHolder()).isEqualTo(policyHolder);
    }

    @Test
    public void testGetFinancialOverviewAccountsFor() {
        StatCodeAccountEntity entity = new StatCodeAccountEntity().setAccountNo(accountNo).setPolicyHolder(policyHolder);
        List<StatCodeAccountEntity> list = new ArrayList<>();
        list.add(entity);

        FinancialOverviewAccounts accounts = service.getFinancialOverviewAccountsFor(list, allAccounts);
        assertThat(accounts.getAllAccounts().size()).isGreaterThan(0);

        List<FinancialOverviewAccountData> premiumAccounts = accounts.getPremiumAccounts();
        assertThat(premiumAccounts.size()).isGreaterThan(0);

        FinancialOverviewAccountData row1 = premiumAccounts.get(0);
        assertThat(row1.getPolicyHolder()).isEqualTo(policyHolder);
        assertThat(row1.getAccountName()).isEqualTo(nameOfAccount);
        assertThat(row1.getAccountNo()).isEqualTo(accountNo);
    }

    @DisplayName("Declarations should not show those that have premium of Zero")
    @Test
    public void testDeclarationsListShouldOnlyReturnPremiumNotZeroOrNull() {

        List<CharterDeliveryReportEntity> charterDeclarations = new ArrayList<>();
        CharterDeliveryReportEntity entity1 = new CharterDeliveryReportEntity().setPremium(BigDecimal.ZERO);
        CharterDeliveryReportEntity entity2 = new CharterDeliveryReportEntity().setPremium(null);
        CharterDeliveryReportEntity entity3 = new CharterDeliveryReportEntity().setPremium(new BigDecimal(1));

        charterDeclarations.addAll(Arrays.asList(entity1, entity2, entity3));

        when(financialService.getAllCharterDeclarations(any())).thenReturn(charterDeclarations);

        List<Declaration> declarations = service.getDeclarationsFiltered("1");

        assertThat(declarations).size().isEqualTo(1);
    }


    @DisplayName("Declarations should show rebate items and sort on vesselname")
    @Test
    public void declarations_should_sort_and_show_rebate() {

        List<CharterDeliveryReportEntity> charterDeclarations = new ArrayList<>();
        CharterDeliveryReportEntity entity1 = new CharterDeliveryReportEntity().setCurrentShipName("E").setPremium(new BigDecimal("100"));
        CharterDeliveryReportEntity entity2 = new CharterDeliveryReportEntity().setCurrentShipName("C").setPremium(new BigDecimal("100"));
        CharterDeliveryReportEntity entity3 = new CharterDeliveryReportEntity().setCurrentShipName("D").setPremium(new BigDecimal("100"));
        CharterDeliveryReportEntity entity4 = new CharterDeliveryReportEntity().setCurrentShipName("C").setPremium(new BigDecimal("100"));
        CharterDeliveryReportEntity entity5 = new CharterDeliveryReportEntity().setCurrentShipName("A").setPremium(new BigDecimal("100"));

        charterDeclarations.addAll(Arrays.asList(entity1, entity2, entity3,entity4,entity5));

        when(financialService.getAllCharterDeclarations(any())).thenReturn(charterDeclarations);

        List<Declaration> declarations = service.getDeclarationsFiltered("1");

        assertThat(declarations).size().isEqualTo(5);
        assertThat(declarations.get(0).getVesselName()).isEqualTo("A");
    }



    @Nested
    @DisplayName("Account Status Tests")
    class accountStatusTests {

        private StatCodeAccountEntity statCodeAccountEntity;
        private AccountEntity accountEntity;
        List<StatCodeAccountEntity> statCodeAccounts;

        @BeforeEach
        public void setup() {
            statCodeAccountEntity = new StatCodeAccountEntity()
                    .setAccountNo(accountNo)
                    .setPolicyHolder(policyHolder);

            statCodeAccounts = new ArrayList<>(Arrays.asList(statCodeAccountEntity));

            accountEntity = allAccounts.get(0);
            accountEntity.setAccountType("1");

            when(financialService.matchAccount(any(), any())).thenReturn(accountEntity);
        }

        @DisplayName("Open Accounts are Account with accountSum.size>0")
        @Test
        public void openAccountsValidating() {


            when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(listAccountSums);
            service.setDaysSinceLastTransactionLimit(10);
            accountEntity.setLastTransDate(LocalDate.now().minusDays(5));

            List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

            assertThat(list).extracting("accountStatus").containsOnly(AccountStatus.open);
        }

        @DisplayName("Active Accounts are Account without accountSum, but transaction within day limit ")
        @Test
        public void activeAccountsValidating() {

            when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(new ArrayList<>());
            service.setDaysSinceLastTransactionLimit(10);
            accountEntity.setLastTransDate(LocalDate.now().minusDays(5));

            List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

            assertThat(list).extracting("accountStatus").containsOnly(AccountStatus.active);
        }


        @DisplayName("Passive Accounts are Account without accountSum and no transactions within day limit")
        @Test
        public void passiveAccountsValidating() {

            when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(new ArrayList<>());
            service.setDaysSinceLastTransactionLimit(10);
            accountEntity.setLastTransDate(LocalDate.now().minusDays(15));

            List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

            assertThat(list.size()).isEqualTo(0);
        }

        @Nested
        class DepositAccountIs {
            @BeforeEach
            public void beforeEach() {
                accountEntity
                        .setAccountType("1")
                        .setPaymentMethod("DEPOSIT");
                assertThat(accountEntity.getAccountGroup()).isEqualTo(AccountGroup.PREMIUM_DEPOSIT);

                service.setDaysSinceLastTransactionLimit(180);
                service.setDaysSinceLastTransactionDepositLimit(60);

//                when(financialService.matchAccount(any(), any())).thenReturn(accountEntity);
            }

            @Test
            public void open_when_balance_not_zero() {

                when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(listAccountSums);
                accountEntity.setLastTransDate(LocalDate.now().minusDays(70));

                List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

                assertThat(list).extracting("accountStatus").containsOnly(AccountStatus.open);

            }

            @Test
            public void active_when_balanze_zero_and_transaction_within_60_days() {

                when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(new ArrayList<>());
                service.setDaysSinceLastTransactionLimit(0);
                accountEntity.setLastTransDate(LocalDate.now().minusDays(10));
                service.setDaysSinceLastTransactionDepositLimit(60);

                List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

                assertThat(list).extracting("accountStatus").containsOnly(AccountStatus.active);

            }

            @Test
            public void passive_when_balanze_zero_and_no_transactions_within_60_days() {

                when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(new ArrayList<>());
                service.setDaysSinceLastTransactionDepositLimit(60);
                accountEntity.setLastTransDate(LocalDate.now().minusDays(70));

                List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

                assertThat(list.size()).isEqualTo(0);

            }

            @DisplayName("Deposit accounts have a different day limit than normal claim and premium accounts")
            @Test
            public void different_with_day_limit_separate_from_claim_and_premium_accounts() {

                when(financialService.getAccountSumSortedByCurrency(any())).thenReturn(new ArrayList<>());
                service.setDaysSinceLastTransactionLimit(0);
                accountEntity.setLastTransDate(LocalDate.now().minusDays(30));
                service.setDaysSinceLastTransactionDepositLimit(60);

                List<FinancialOverviewAccountData> list = service.getFinancialOverviewAccountData(statCodeAccounts, allAccounts);

                assertThat(list).extracting("accountStatus").containsOnly(AccountStatus.active);

            }
        }

    }

}
