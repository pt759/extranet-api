package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountTotal;
import com.skuld.extranet.core.financial.domain.statementofaccount.StatementOfAccountVessel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StatementOfAccountPageTest {

    StatementOfAccountPage page = new StatementOfAccountPage();

    @Test
    public void testDistinctSorted(){
        StatementOfAccountTotal total1 = new StatementOfAccountTotal().setCurrency("USD");
        List<StatementOfAccountTotal> statementOfAccountTotals1 = new ArrayList<>(Arrays.asList(total1));
        StatementOfAccountVessel vessel1 = new StatementOfAccountVessel().setInvoiceTotals(statementOfAccountTotals1);

        StatementOfAccountTotal total2 = new StatementOfAccountTotal().setCurrency("EUR");
        List<StatementOfAccountTotal> statementOfAccountTotals2 = new ArrayList<>(Arrays.asList(total2));
        StatementOfAccountVessel vessel2 = new StatementOfAccountVessel().setInvoiceTotals(statementOfAccountTotals2);

        List<StatementOfAccountVessel> statementOfAccountVessels = new ArrayList<>(Arrays.asList(vessel1,vessel2));

        List<String> strings = page.distinctCurrenciesSorted(statementOfAccountVessels);
        assertThat(strings.get(0)).isEqualTo("EUR");
    }

}