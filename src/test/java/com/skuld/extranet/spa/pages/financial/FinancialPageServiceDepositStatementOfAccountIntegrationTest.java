package com.skuld.extranet.spa.pages.financial;

import com.skuld.extranet.core.financial.domain.deposit.Declaration;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialPageServiceDepositStatementOfAccountIntegrationTest {

    @Autowired
    private FinancialPageService service;

    //TODO: Should fix this
    @Disabled //No open items
    @DisplayName("Test Deposit Account Page")
    @Test
    public void testDeposit() {
        StatementOfPremiumDepositAccountPage page = service.getDepositStatementOfAccount("586247");
        assertThat(page.getInstalments()).size().isGreaterThan(0).withFailMessage("Instalment");
        assertThat(page.getDeclarations().size()).isGreaterThanOrEqualTo(0);
    }@DisplayName("Test Deposit Account Page")


    @Test
    public void testDepositAccountForXoShipping() {
        StatementOfPremiumDepositAccountPage page = service.getDepositStatementOfAccount("586444");
        assertThat(page.getInstalments()).size().isGreaterThan(0).withFailMessage("Instalment");
        assertThat(page.getDeclarations().size()).isGreaterThanOrEqualTo(0);
        assertThat(page.getCreditController()).isNotNull();
    }

    @Disabled //Missing account_no in mv_charter_delivery_report
    @DisplayName("Test For Account no 586187")
    @Test
    public void testForAccount() {
        List<Declaration> declarations = service.getDeclarationsFiltered("586187");

        List<Declaration> ams_pegasus_iii = declarations.stream().filter(x -> x.getVesselName().equalsIgnoreCase("AMS Pegasus III")).collect(Collectors.toList());

        assertThat(ams_pegasus_iii).size().isGreaterThan(0);
    }

    @Disabled // Account_no missing from mv_charter_delivery_report
    @DisplayName("Filtered should remove all those with Zero Premium")
    @Test
    public void filterDeclarations() {
        List<Declaration> declarations = service.getDeclarationsFiltered("583174");
        assertThat(declarations.size()).isGreaterThan(0);
        assertThat(declarations)
                .extracting("amount").doesNotContain(BigDecimal.ZERO);
    }


}
