package com.skuld.extranet.core.memberstatistics.repo;

import com.skuld.extranet.core.common.domain.GraphData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
class StatisticsRepoIntegrationTest {

    @Autowired
    StatisticsRepo repo = new StatisticsRepo();


    @Test
    public void testLossRatio() {
        List<GraphData> owner = repo.lossRatio5Year(africaExpressStatCode, "Owner");
        assertThat(owner).isNotNull();
        assertThat(owner.size()).isGreaterThan(0);
    }

}