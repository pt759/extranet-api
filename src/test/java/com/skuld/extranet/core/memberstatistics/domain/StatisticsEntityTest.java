package com.skuld.extranet.core.memberstatistics.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class StatisticsEntityTest {


    StatisticsEntity entity = new StatisticsEntity();

    @Test
    public void testWeightedNoVessels2Decimals(){
        entity.setWeightedNoVessels(100.123455);
        assertThat(entity.getWeightedNoVessels()).isEqualTo(100.12);
    }

    @Test
    public void testWeightedNoVessels2DecimalsZero(){
        entity.setWeightedNoVessels(0.0);
        assertThat(entity.getWeightedNoVessels()).isEqualTo(0.0);
    }

    @Test
    public void testWeightedNoVessels2DecimalsRoundToInteger(){
        entity.setWeightedNoVessels(1234.9988);
        assertThat(entity.getWeightedNoVessels()).isEqualTo(1235.00);
    }


}