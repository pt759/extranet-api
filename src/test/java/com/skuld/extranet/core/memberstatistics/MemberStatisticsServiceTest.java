package com.skuld.extranet.core.memberstatistics;

import com.skuld.common.domain.Base64Container;
import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.common.SkuldWebUtil;
import com.skuld.extranet.core.memberstatistics.domain.ClaimStatisticsEntity;
import com.skuld.extranet.core.memberstatistics.domain.StatisticsEntityVersion;
import com.skuld.extranet.core.memberstatistics.service.MemberStatisticsService;
import com.skuld.extranet.spa.pages.domain.Product;
import com.skuld.extranet.spa.pages.domain.StatYearType;
import com.skuld.extranet.spa.pages.domain.UrlLink;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static com.skuld.common.global.GlobalTestValues.employeeToken;
import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest
class MemberStatisticsServiceTest {

    @Autowired
    private MemberStatisticsService service;

    @Test
    @DisplayName("getClaimFiveYearStatisticsByProductTest")
    public void getClaimFiveYearStatisticsByProductTest() {
        String statCode = "'1000.0";
        String product = "PRODUCT1";

        List<ClaimStatisticsEntity> claimList = new ArrayList<>();
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE1").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(20));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE2").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(10));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE3").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(5));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE4").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE5").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE6").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE7").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT1").setClaimsType("TYPE8").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(2));

        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT2").setClaimsType("TYPE1").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(3));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT2").setClaimsType("TYPE2").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(14));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT2").setClaimsType("TYPE3").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(24));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT3").setClaimsType("TYPE1").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(12));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT3").setClaimsType("TYPE2").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(40));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT3").setClaimsType("TYPE3").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(20));
        claimList.add(new ClaimStatisticsEntity().setProduct("PRODUCT3").setClaimsType("TYPE4").setStatCode(statCode).setPolicyYear("5YEAR").setClaims(30));

        List<ClaimStatisticsEntity> list = service.filteredClaimStatisticsEntityOnProduct(claimList, product);

        assertThat(list.size()).isEqualTo(8);

    }


    @Test
    public void memberStatisticsUrlsTest() {

        List<UrlLink> list = service.getMemberStatisticsUrls("10002", Product.owner);
        assertThat(list.size()).isEqualTo(3);

        UrlLink urlLink = list.get(0);
        assertThat(urlLink.getUrl()).contains(StatYearType.distributed.getCode()).contains(Product.owner.description());

        urlLink = list.get(1);
        assertThat(urlLink.getUrl()).contains(StatYearType.fiveYears.getCode()).contains(Product.owner.description());

        urlLink = list.get(2);
        assertThat(urlLink.getUrl()).contains(StatYearType.sixYears.getCode()).contains(Product.owner.description());

    }


    @Test
    public void pdfUrlForDistributed() {

        //http://accurate/_vti_bin/reportserver?http://accurate/SSRS/MS2015/MemberStatistics.rdl&rs:Format=pdf
        // &stat_version=DISTRIBUTED
        // &stat_currency=STAT
        // &product_line=Charterer
        // &stat_name=AUTO%20CHARTERING
        // &stat_code=32136.00

        UrlLink urlLink = service.convertPDFEndpointToUrl("1000", StatYearType.distributed, Product.fixedOwner);
        assertThat(urlLink.getUrl()).isNotNull();
        assertThat(urlLink.getUrl()).contains("1000").contains("DISTRIBUTED").doesNotContain("{");
    }


    @Test
    public void pdfUrlForFiveYear() {
        UrlLink urlLink = service.convertPDFEndpointToUrl("1000", StatYearType.fiveYears, Product.fixedOwner);
        assertThat(urlLink.getUrl()).isNotNull();
        assertThat(urlLink.getUrl()).contains("1000").contains("5YEAR").doesNotContain("{");
    }

    @Test
    public void ExcelUrlForFiveYear() {
        UrlLink urlLink = service.convertExcelEndpointToUrl("1000.1", Product.fixedOwner, StatisticsEntityVersion.actual);
        assertThat(urlLink.getUrl()).isNotNull();
        assertThat(urlLink.getUrl())
                .contains("1000.1")
                .contains(SkuldWebUtil.urlEncode(Product.fixedOwner.toString()))
                .contains(StatisticsEntityVersion.actual.toString())
                .doesNotContain("{")
                .doesNotContain("//");
        assertThat(urlLink.getTitle()).isEqualTo(StatisticsEntityVersion.actual.getDescription());
    }

    @Test
    public void ExcelUrlForDistributed() {
        UrlLink urlLink = service.convertExcelEndpointToUrl("1000.1", Product.owner, StatisticsEntityVersion.distributed);
        assertThat(urlLink.getUrl()).isNotNull();
        assertThat(urlLink.getUrl())
                .contains("1000.1")
                .contains(SkuldWebUtil.urlEncode(Product.owner.toString()))
                .contains(StatisticsEntityVersion.distributed.toString())
                .doesNotContain("{");
        assertThat(urlLink.getTitle()).isEqualTo(StatisticsEntityVersion.distributed.getDescription());
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void should_download_customer_statistics() {
        String statCode = "1341.0";
        String productLine = "Owner";
        String statVersion = "5YEAR";
        String statCurrency = "STAT";
        String statName = "STATSRAAD LEHMKUHL";
        String fileNameInContainer = "AnyFile.txt";

        final Base64Container base64Container = service.getMemberStatisticsPdf(statCode, statVersion, productLine, fileNameInContainer);
        AssertionsForClassTypes.assertThat(base64Container.getData()).isNotBlank();
        AssertionsForClassTypes.assertThat(base64Container.getFilename()).isEqualTo(fileNameInContainer);

    }

}
