package com.skuld.extranet.core.archive.service;

import com.skuld.extranet.core.archive.domain.DocumentRef;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;

@SpringBootTest
class ArchiveServiceIntegrationTest {

    @Autowired
    private ArchiveService service;

    @Disabled //Document removed from archive in Tia-t
    @DisplayName("Get documents for a vessel")
    @Test
    public void testArgLineNoService() {
        List<DocumentRef> list = service.getDocumentsReferenceArgNo(africaExpressStatCode, "1724979");
        MatcherAssert.assertThat(list.size(), is(greaterThan(0)));
    }


}