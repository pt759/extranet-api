package com.skuld.extranet.core.archive.service;

import com.skuld.extranet.core.archive.domain.ArchiveEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by pt184 on 8/2/2016.
 */
public class PDFDownloadToLocal {

    final static Logger logger = LoggerFactory.getLogger(PDFDownloadToLocal.class);

    public void writePDF(String filename, Blob blob) {
        try {
            InputStream is = blob.getBinaryStream();
            FileOutputStream fos = new FileOutputStream(filename);
            int b = 0;
            while ((b = is.read()) != -1) {
                fos.write(b);
            }

        } catch (IOException e) {
            logger.error("IO Exception: ", e);
        } catch (SQLException e) {
            logger.error("SQL Exception: ", e);
        }
    }


    public void writePDF(String filename, ArchiveEntity entity) {
        try {
            entity.getBlobAsBytes();
            FileOutputStream fos = new FileOutputStream(filename);
            fos.write(entity.getBlobAsBytes());
        } catch (IOException e) {
            logger.error("IO Exception: ", e);
        }
    }


    public void writePDF(String filename, InputStream is) {
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            int b = 0;
            while ((b = is.read()) != -1) {
                fos.write(b);
            }

        } catch (IOException e) {
            logger.error("IO Exception: ", e);
        }
    }
}
