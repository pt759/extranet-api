package com.skuld.extranet.core.archive.domain;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

class DocumentRefTest {


    @Test
    public void testProgramExists(){
        List<String> list = new ArrayList<>();
        list.add("100");
        list.add("200");
        list.add("300");
        assertThat(new DocumentRef().setProgramId("100").programIdInList(list)).isTrue();
        assertThat(new DocumentRef().setProgramId("300").programIdInList(list)).isTrue();
        assertThat(new DocumentRef().setProgramId("400").programIdInList(list)).isFalse();
    }

}