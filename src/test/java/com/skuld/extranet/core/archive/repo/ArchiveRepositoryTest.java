package com.skuld.extranet.core.archive.repo;

import com.skuld.extranet.ExtranetApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExtranetApplication.class)
class ArchiveRepositoryTest {

    @Autowired
    ArchiveRepository repo;

    @BeforeEach
    public void setup() {
    }

    @Test
    public void find_request_id_for_invoice() {
        String invoiceNo = "9901664490";
        String requestIdForInvoice = repo.getRequestIdForInvoice(invoiceNo);
        assertThat(requestIdForInvoice).isEqualToIgnoringCase("15944677");

    }

}
