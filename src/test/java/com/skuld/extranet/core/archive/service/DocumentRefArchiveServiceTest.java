package com.skuld.extranet.core.archive.service;

import com.skuld.extranet.core.archive.domain.DocumentRef;
import com.skuld.extranet.core.archive.repo.ArchiveRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DocumentRefArchiveServiceTest {

    ArchiveService service = new ArchiveService();
    ArchiveRepository mockRepo;

    @BeforeEach
    public void setup() {
        mockRepo = mock(ArchiveRepository.class);
        service.setRepo(mockRepo);
    }

    @Test
    public void testGetDocuments() {
        List<DocumentRef> docs = new ArrayList<>();
        docs.add(new DocumentRef().setProgramId("RS100"));
        docs.add(new DocumentRef().setProgramId("RS101"));
        docs.add(new DocumentRef().setProgramId("RS102"));
        docs.add(new DocumentRef().setProgramId("RS104"));

        List<DocumentRef> renewal = new ArrayList<>();
        renewal.add(new DocumentRef().setProgramId("RS102"));
        renewal.add(new DocumentRef().setProgramId("RS103"));

        when(mockRepo.getAgrDocuments(anyString(), anyString())).thenReturn(docs);
        when(mockRepo.getAgrDocuments(anyString(), anyString())).thenReturn(docs);
        when(mockRepo.getRenewalDocuments(any(), anyString())).thenReturn(renewal);

        List<DocumentRef> documents = service.getDocumentsReferenceArgNo("100", "100");

        assertThat(documents).asList().size().isEqualTo(5);
    }

  @Test
    public void testGetDocuments2() {
        List<DocumentRef> docs = new ArrayList<>();

        List<DocumentRef> renewal = new ArrayList<>();
        renewal.add(new DocumentRef().setProgramId("RS102"));
        renewal.add(new DocumentRef().setProgramId("RS103"));

        when(mockRepo.getAgrDocuments(anyString(), anyString())).thenReturn(docs);
        when(mockRepo.getRenewalDocuments(any(), anyString())).thenReturn(renewal);

        List<DocumentRef> documents = service.getDocumentsReferenceArgNo("100", "100");
        assertThat(documents).asList().size().isEqualTo(2);
    }
}