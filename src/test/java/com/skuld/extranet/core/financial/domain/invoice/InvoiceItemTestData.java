package com.skuld.extranet.core.financial.domain.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class InvoiceItemTestData {


    public static List<InvoiceItem> invoiceItems() {

        InvoiceItem item1 = new InvoiceItem().setDueDate(LocalDate.now().minusDays(10)).setOriginalAmount(new BigDecimal("10"));
        InvoiceItem item2 = new InvoiceItem().setDueDate(LocalDate.now()).setOriginalAmount(new BigDecimal("10"));
        InvoiceItem item3 = new InvoiceItem().setDueDate(LocalDate.now().plusDays(10)).setOriginalAmount(new BigDecimal("10"));
        List<InvoiceItem> items = new ArrayList<>(Arrays.asList(item1, item2, item3));
        return items;

    }

}