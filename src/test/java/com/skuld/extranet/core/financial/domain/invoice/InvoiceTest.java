package com.skuld.extranet.core.financial.domain.invoice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class InvoiceTest {

    Invoice invoice;

    @BeforeEach
    public void setup() {
        List<InvoiceItem> invoiceItems = InvoiceItemTestData.invoiceItems();
        invoice = new Invoice().setInvoiceItems(invoiceItems);
    }

    @Test
    public void testInvoiceItemOverdue() {
        assertThat(invoice.hasOverdueItem()).isTrue();
    }

    @Test
    public void invoiceHasOverdueItemOneItemIsInThePast() {

        invoice.getInvoiceItems().stream()
                .forEach(i -> i.setDueDate(LocalDate.now().minusDays(2)))
        ;

        assertThat(invoice.hasOverdueItem()).isTrue();
    }

}