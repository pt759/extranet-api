package com.skuld.extranet.core.financial.domain.invoice;

import java.util.ArrayList;
import java.util.Arrays;

public class InvoiceTestData {
    public static Invoice invoice1;
    public static Invoice invoice2;
    public static Invoice invoice3;

    public static ArrayList<Invoice> invoices() {

        invoice1 = new Invoice().setInvoiceItems(InvoiceItemTestData.invoiceItems());
        invoice2 = new Invoice().setInvoiceItems(InvoiceItemTestData.invoiceItems());
        invoice3 = new Invoice().setInvoiceItems(InvoiceItemTestData.invoiceItems());

        return new ArrayList<>(Arrays.asList(invoice1, invoice2, invoice3));

    }
}
