package com.skuld.extranet.core.financial.domain.statementofaccount;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StatementOfAccountInvoiceTest {
    StatementOfAccountInvoice invoice = new StatementOfAccountInvoice();
    LocalDate now = LocalDate.now();
    List<StatementOfAccountInvoiceItem> items;

    @BeforeEach
    public void setup() {

        items = new ArrayList<>();
        invoice.setDetails(items);

    }

    @DisplayName("Due date has NOT changed if all items have same due date as Invoice.")
    @Test
    public void dueDateHasNotChanged() {
        items.add(new StatementOfAccountInvoiceItem().setDueDate(now));
        invoice.setDueDate(now);

        assertThat(invoice.isDueDateHasChanged()).isFalse();

    }
    @DisplayName("Due date has changed if any item due date are different than invoice due date")
    @Test
    public void dueDateHasChangedWhenItemDueAreInThePast() {
        items.add(new StatementOfAccountInvoiceItem().setDueDate(now.minusDays(1)));
        invoice.setDueDate(now);
        assertThat(invoice.isDueDateHasChanged()).isTrue();
    }

    @DisplayName("Due date has changed if any item due date are different than invoice due date")
    @Test
    public void dueDateHasChangedWhenItemDueAreInTheFuture() {
        items.add(new StatementOfAccountInvoiceItem().setDueDate(now.plusDays(1)));
        invoice.setDueDate(now);
        assertThat(invoice.isDueDateHasChanged()).isTrue();
    }

}
