package com.skuld.extranet.core.financial.domain.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccountItemEntityTest {

    public static String invoiceNo1_valid = "9901667013";
    public static String invoiceNo2_valid = "9901667012";
    public static String invoiceNo3_invalid = "801667012";
    public static String accountNo = "535775";

    public static List<AccountItemEntity> accountItemEntities() {
        List<AccountItemEntity> accItems = new ArrayList<>();

        AccountItemEntity accItem1 = new AccountItemEntity()
                .setAccountNumber(accountNo)
                .setCurrency("USD")
                .setVesselName("Beta Pepsi")
                .setDescription("P&I Trading Tax 19.00%")
                .setInvoiceNo(invoiceNo1_valid)
                .setOriginalAmount(new BigDecimal("287.32"))
                .setBalance(new BigDecimal("287.32"))
                .setDueDate(LocalDate.now().minusDays(1))
                .setInvoiceDueDate(LocalDate.now().minusDays(1))
                ;


        AccountItemEntity accItem2 = new AccountItemEntity().setAccountNumber(accountNo).setOnHold("N").setCurrency("USD").setVesselName("Alpha Cola").setDescription("P&I Trading").setInvoiceNo(invoiceNo1_valid).setOriginalAmount(new BigDecimal("1587.32")).setBalance(new BigDecimal("157.32")).setDueDate(LocalDate.now().minusDays(1)).setInvoiceDueDate(LocalDate.now().minusDays(1));
        AccountItemEntity accItem3 = new AccountItemEntity().setAccountNumber(accountNo).setOnHold("N").setCurrency("USD").setVesselName("Congo Diamond").setDescription("FD&D Trading Tax 19.00%").setInvoiceNo(invoiceNo2_valid).setOriginalAmount(new BigDecimal("267.32")).setBalance(new BigDecimal("267.32")).setDueDate(LocalDate.now().plusDays(1)).setInvoiceDueDate(LocalDate.now().plusDays(1));
        AccountItemEntity accItem4 = new AccountItemEntity().setAccountNumber(accountNo).setOnHold("N").setCurrency("USD").setVesselName("Zelda Unboxed").setDescription("FD&D Trading").setInvoiceNo(invoiceNo2_valid).setOriginalAmount(new BigDecimal("1307.32")).setBalance(new BigDecimal("1307.32")).setDueDate(LocalDate.now().plusDays(1)).setInvoiceDueDate(LocalDate.now().plusDays(1));
        AccountItemEntity accItem5 = new AccountItemEntity().setAccountNumber(accountNo).setOnHold("N").setCurrency("USD").setVesselName("Zelda Unboxed").setDescription("FD&D Trading").setInvoiceNo(invoiceNo2_valid).setOriginalAmount(new BigDecimal("1307.32")).setBalance(new BigDecimal("1307.32")).setDueDate(LocalDate.now().plusDays(1)).setInvoiceDueDate(LocalDate.now().plusDays(1));
        AccountItemEntity accItem6 = new AccountItemEntity().setAccountNumber(accountNo).setOnHold("N").setCurrency("USD").setVesselName("Zelda Clickshare").setDescription("FD&D Trading").setInvoiceNo(invoiceNo3_invalid).setOriginalAmount(new BigDecimal("1307.32")).setBalance(new BigDecimal("1307.32")).setDueDate(LocalDate.now().minusDays(1)).setInvoiceDueDate(LocalDate.now().minusDays(1));
        accItems.addAll(Arrays.asList(accItem1, accItem2, accItem3, accItem4, accItem5, accItem6));

        return accItems;
    }

}