package com.skuld.extranet.core.financial.domain.deposit.dto;

import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class DepositInstalmentItemTest {

    @Test
    public void mappingFromEntityToDTODefinition() {

        InstallmentsEntity entity = new InstallmentsEntity()
                .setCurrencyBalance(bd("200"))
                .setCurrencyAmount(bd("300"))
                .setDepositPaid(bd("400"))

                ;

        DepositInstalmentItem fromItem = DepositInstalmentItem.from(entity);

        assertThat(fromItem.getBalance()).isEqualByComparingTo(entity.getCurrencyBalance());
        assertThat(fromItem.getInvoiced()).isEqualByComparingTo(entity.getCurrencyAmount());
        assertThat(fromItem.getPaid()).isEqualByComparingTo(entity.getDepositPaid());
        assertThat(fromItem.getOriginalAmount()).isEqualByComparingTo(entity.getCurrencyAmount());

    }

    private BigDecimal bd(String s) {
        return new BigDecimal(s);
    }

}