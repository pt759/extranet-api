package com.skuld.extranet.core.financial.domain.memberPerformanceBonus;

import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonus;
import com.skuld.extranet.memberPerformanceBonus.MemberPerformanceBonusService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class MemberPerformanceBonusTest {

    @Autowired
    private MemberPerformanceBonusService memberPerformanceBonusService;
    String statCode = "14864.0";

    @Test
    @DisplayName("Get member performance info for stat code")
    public void testGetStatCodesInPerformanceGroup() {
        MemberPerformanceBonus memberPerformancesBonus = memberPerformanceBonusService.getMemberPerformanceBonus(statCode);
        assertThat(memberPerformancesBonus.getAmount()).isNotNull();
        assertThat(memberPerformancesBonus.getStatCodeNames().size()).isEqualTo(1);
    }

    @Test
    public void noDownloadUrlWhenBalanceIsZero(){
        MemberPerformanceBonus bonus = new MemberPerformanceBonus().setStatCode("65432.00");

        bonus.setAmount(new BigDecimal("12000")).setBalance(new BigDecimal("0"));
        assertThat(bonus.getDownloadUrl()).isNotEmpty();

        bonus.setAmount(new BigDecimal("16000")).setBalance(null);
        assertThat(bonus.getDownloadUrl()).isNullOrEmpty();

        bonus.setAmount(new BigDecimal("5000")).setBalance(new BigDecimal("5000"));
        assertThat(bonus.getDownloadUrl()).isNullOrEmpty();
    }

    @Test
    public void onlyPayableWhenZeroBalance(){
        MemberPerformanceBonus bonus = new MemberPerformanceBonus().setStatCode("65432.00");

        bonus.setAmount(new BigDecimal("15000")).setBalance(new BigDecimal("0"));
        assertThat(bonus.isPayable()).isTrue();

        bonus.setAmount(new BigDecimal("15000")).setBalance(null);
        assertThat(bonus.isPayable()).isFalse();

        bonus.setAmount(new BigDecimal("5000")).setBalance(new BigDecimal("5000"));
        assertThat(bonus.isPayable()).isFalse();

    }


}
