package com.skuld.extranet.core.financial.service;


import com.skuld.extranet.ExtranetApplication;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.deposit.InstalmentType;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalment;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalmentItem;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntityTest;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import com.skuld.extranet.core.financial.domain.invoice.Invoice;
import com.skuld.extranet.core.financial.domain.invoice.InvoiceTestData;
import com.skuld.extranet.core.financial.repo.FinancialRepository;
import com.skuld.extranet.ldap.domain.LDAPUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.skuld.common.domain.YesNo.No;
import static com.skuld.extranet.GlobalForTest.creditControlEmail;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType.Unpaid;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntityTest.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ExtranetApplication.class)
class FinancialServiceTest {


    @Autowired
    private FinancialService financialService;

    FinancialRepository mockFinancialRepository = mock(FinancialRepository.class);
    List<AccountItemEntity> accItems;

    @BeforeEach
    public void setup() {
        AccountEntity account = new AccountEntity().setAccountType("1");
        accItems = AccountItemEntityTest.accountItemEntities();

        when(mockFinancialRepository.accountItems(any(), any(), any(), any())).thenReturn(accItems);
        when(mockFinancialRepository.getAccount(any())).thenReturn(account);
        financialService.setFinancialRepository(mockFinancialRepository);
    }

    @Test
    public void should_sort_account_currency() {

        AccountSum accountSum1 = new AccountSum().setCurrency("USD").setBalance(new BigDecimal("100.0")).setDueBalance(new BigDecimal("50.0"));
        AccountSum accountSum2 = new AccountSum().setCurrency("DKK").setBalance(new BigDecimal("100.0")).setDueBalance(new BigDecimal("50.0"));
        List<AccountSum> accountSums = new ArrayList<>(Arrays.asList(accountSum1, accountSum2));

        when(mockFinancialRepository.getAccountSum(any(), any(), any(), any())).thenReturn(accountSums);

        List<AccountSum> data = financialService.getAccountSumSortedByCurrency("");

        assertThat(data.get(0).getCurrency()).isEqualTo("DKK");
        assertThat(data.get(1).getCurrency()).isEqualTo("USD");
    }

    @Test
    public void testOnlyReturnNineSerieInvoiec() {
        List<AccountItemEntity> items = financialService.getAccItems("12345", Unpaid, No);
        assertThat(items.size()).isEqualTo(5);
    }

    @Test
    public void testGetInvoicesForAccount() {
        List<Invoice> invoicesForAccount = financialService.getInvoicesForAccount("535775");
        assertThat(invoicesForAccount.size()).isEqualTo(2);
    }

    @DisplayName("Test that we will receive an empty list when no items are sent in")
    @Test
    public void testGetInvoicesForAccountsAcceptNullLists() {

        List<Invoice> invoicesForAccounts = financialService.convertAccItemsToInvoices(null);
        assertThat(invoicesForAccounts).isNotNull().size().isEqualTo(0);

        invoicesForAccounts = financialService.convertAccItemsToInvoices(new ArrayList<>());
        assertThat(invoicesForAccounts).isNotNull().size().isEqualTo(0);

    }

    @DisplayName("Test Invoice is grouped correctly from accountItems")
    @Test
    public void testGetInvoicesForAccounts() {

        List<Invoice> invoicesForAccounts = financialService.convertAccItemsToInvoices(accItems);

        assertThat(invoicesForAccounts.size()).isEqualTo(2);
        Invoice invoice = invoicesForAccounts.stream().filter(i -> i.getInvoiceNo().equalsIgnoreCase(invoiceNo1_valid)).findFirst().orElse(null);
        assertThat(invoice).isNotNull();
        assertThat(invoice.getInvoiceItems().size()).isEqualTo(2);
    }

    @DisplayName("Invoice only contains valid InvoiceNumbers")
    @Test
    public void testGetInvoicesWithoutInvalidInvoiceNoForAccounts() {
        List<Invoice> invoicesForAccounts = financialService.convertAccItemsToInvoices(accItems);
        assertThat(invoicesForAccounts)
                .extracting(Invoice::getInvoiceNo)
                .contains(invoiceNo1_valid, invoiceNo2_valid)
                .doesNotContain(invoiceNo3_invalid);
    }

    @DisplayName("Should only return invoices which has item overdue and original amount over zero.")
    @Test
    public void testFilterOverDueInvoices() {
        List<Invoice> invoices = InvoiceTestData.invoices();

        invoices.get(0).getInvoiceItems().stream().forEach(i -> i.setOriginalAmount(new BigDecimal("0")));
        invoices.get(0).getInvoiceItems().stream().forEach(i -> i.setOriginalAmount(new BigDecimal("-10")));
        invoices.get(1).getInvoiceItems().stream().forEach(i -> i.setOriginalAmount(new BigDecimal("10")));
        invoices.get(2).getInvoiceItems().stream().forEach(i -> i.setOriginalAmount(new BigDecimal("10")));

        invoices = financialService.getInvoicesWithOverDueAndThatHasAmount(invoices);
        assertThat(invoices).size().isEqualTo(3);
        assertThat(invoices).extractingResultOf("hasOverdueItem").contains(true);
        assertThat(invoices).extractingResultOf("getOriginalAmount").doesNotContain(BigDecimal.ZERO);
    }

    @Test
    public void overdueInvoices() {
        List<AccountEntity> list = new ArrayList<>();
        AccountEntity premiumAccount1 = new AccountEntity()
                .setAccountNo(AccountItemEntityTest.accountNo)
                .setAccountType("1");
        list.addAll(Arrays.asList(premiumAccount1));

        List<Invoice> overdueInvoicesFor = financialService.getOverdueInvoicesFor(list);

        assertThat(overdueInvoicesFor.size()).isEqualTo(1);
    }

    @DisplayName("Accounts without creditcontrollers should default to CreditControllerEmail User")
    @Test
    public void accountsWithNullCreditControllers() {
        StatCodeAccountEntity statCodeEntity = new StatCodeAccountEntity().setCreditController("");
        List<StatCodeAccountEntity> statCodeAccountEntities = new ArrayList<>(Arrays.asList(statCodeEntity));

        List<LDAPUser> list = financialService.getCreditControllersForStatCode(statCodeAccountEntities);
        assertThat(list.size()).isEqualTo(1);
        LDAPUser ldapUser = list.get(0);
        assertThat(ldapUser.getEmail()).isEqualToIgnoringCase(creditControlEmail);

    }

    @Nested
    class instalments {

        List<InstallmentsEntity> instalments;

        @BeforeEach
        public void setup() {
            InstallmentsEntity row1 = buildInstallmentsEntity("1").setBalance(new BigDecimal("100")).setDepositPaid(new BigDecimal("100"));
            InstallmentsEntity row2 = buildInstallmentsEntity("2").setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("100"));
            InstallmentsEntity row3 = buildInstallmentsEntity("3").setBalance(new BigDecimal("100")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row4 = buildInstallmentsEntity("4").setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row5 = buildInstallmentsEntity("5").setBalance(new BigDecimal("-10000")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row6 = buildInstallmentsEntity("6").setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("-20000"));

            instalments = new ArrayList<>(Arrays.asList(row1, row2, row3, row4, row5, row6));
            when(mockFinancialRepository.getInstalments(anyString())).thenReturn(instalments);
        }

        @DisplayName("Should only return rows with value in either balance and/or deposit paid")
        @Test
        public void should_not_return_rows_with_zero_balance_and_depositPaid() {
            List<DepositInstalment> depositInstalmentOverview = financialService.getDepositInstalmentOverview("");
            assertThat(depositInstalmentOverview).size().isEqualTo(5);
        }

        @DisplayName("Should only return other instalments with balance")
        @Test
        public void should_not_return_other_instalments_with_zero_balance() {
            when(mockFinancialRepository.getInstalmentOthers(anyString())).thenReturn(instalments);

            List<InstallmentsEntity> list = financialService.getOtherInstalments("");
            assertThat(list).size().isEqualTo(3);
        }

        @Test
        public void should_return_three_instalment() {
            InstallmentsEntity row1 = buildInstallmentsEntity("1").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("100")).setDepositPaid(new BigDecimal("100"));
            InstallmentsEntity row2 = buildInstallmentsEntity("2").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("100"));
            InstallmentsEntity row3 = buildInstallmentsEntity("3").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("100")).setDepositPaid(new BigDecimal("0"));

            List<InstallmentsEntity> instalments = new ArrayList<>(Arrays.asList(row1, row2, row3));

            List<DepositInstalment> list = financialService.mapInstalmentsToDTO(instalments);

            assertThat(list.size()).isEqualTo(3);
        }

        @Test
        public void should_return_one_other_instalment() {
            InstallmentsEntity row1 = buildInstallmentsEntity("1").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("100")).setDepositPaid(new BigDecimal("100"));
            InstallmentsEntity row2 = buildInstallmentsEntity("2").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("100"));
            InstallmentsEntity row3 = buildInstallmentsEntity("3").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("100")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row4 = buildInstallmentsEntity("4").setCurrencyAmount(new BigDecimal("100")).setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row5 = buildInstallmentsEntity("").setCurrencyAmount(new BigDecimal("-100")).setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row6 = buildInstallmentsEntity("").setCurrencyAmount(new BigDecimal("-100")).setBalance(new BigDecimal("-100")).setDepositPaid(new BigDecimal("0"));

            List<InstallmentsEntity> instalments = new ArrayList<>(Arrays.asList(row1, row2, row3, row4, row5, row6));

            List<DepositInstalment> depositInstalments = financialService.mapInstalmentsToDTO(instalments);

            assertThat(depositInstalments).filteredOn("instalmentType", InstalmentType.other).size().isEqualTo(1);
        }

        @Test
        public void should_return_two_instalment_items_for_balance_not_equal_to_zero() {
            InstallmentsEntity row5 = buildInstallmentsEntity("").setCurrencyAmount(new BigDecimal("-100")).setBalance(new BigDecimal("0")).setDepositPaid(new BigDecimal("0"));
            InstallmentsEntity row6 = buildInstallmentsEntity("").setCurrencyAmount(new BigDecimal("-100")).setBalance(new BigDecimal("-100")).setDepositPaid(new BigDecimal("0"));

            List<InstallmentsEntity> instalments = new ArrayList<>(Arrays.asList(row5, row6));

            List<DepositInstalment> depositInstalments = financialService.mapInstalmentsToDTO(instalments);

            assertThat(depositInstalments).filteredOn("instalmentType", InstalmentType.other).size().isEqualTo(1);
            DepositInstalment depositInstalment = depositInstalments.stream().filter(x -> x.isOther()).findFirst().orElse(null);
            assertThat(depositInstalment.getItems().size()).isEqualTo(2);

        }

        private InstallmentsEntity buildInstallmentsEntity(String invoiceNo) {
            InstallmentsEntity entity = new InstallmentsEntity();
            entity.setInvoiceNo(invoiceNo).setAccountNo("10").setCurrencyCode("USD").setInvoiceDate(LocalDate.now()).setInvoiceDate(LocalDate.now());
            return entity;
        }

    }

    @Test
    public void other_instalment_will_always_have_zero_deposit_invoiced(){
        DepositInstalment instalment = new DepositInstalment();
        List<DepositInstalmentItem> items = new ArrayList<>();
        BigDecimal invoicedAmount = new BigDecimal("100");
        items.add(new DepositInstalmentItem().setInvoiced(invoicedAmount));
        instalment.setItems(items);

        instalment.setInstalmentType(InstalmentType.other);
        assertThat(instalment.getInvoicedSum()).isEqualByComparingTo(BigDecimal.ZERO);

        instalment.setInstalmentType(InstalmentType.normal);
        assertThat(instalment.getInvoicedSum()).isEqualByComparingTo(invoicedAmount);

    }
}
