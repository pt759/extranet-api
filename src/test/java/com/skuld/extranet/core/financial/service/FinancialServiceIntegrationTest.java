package com.skuld.extranet.core.financial.service;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.ExtranetApplication;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.BankDetail;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalment;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.CharterDeliveryReportEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import com.skuld.extranet.core.financial.domain.invoice.Invoice;
import com.skuld.extranet.core.financial.domain.invoice.InvoiceItem;
import com.skuld.extranet.core.financial.excel.StatementOfAccountExcel;
import com.skuld.extranet.ldap.domain.LDAPUser;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.common.domain.YesNo.No;
import static com.skuld.common.domain.YesNo.Yes;
import static com.skuld.extranet.GlobalForTest.creditControlEmail;
import static com.skuld.extranet.core.financial.domain.deposit.InstalmentType.other;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType.Unpaid;
import static org.assertj.core.api.Java6Assertions.assertThat;

@Disabled //Tests fail on TC agent
@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ExtranetApplication.class)
class FinancialServiceIntegrationTest {

    @Autowired
    private FinancialService financialService;

    String statCodeForAfricaExpress = "6554.50";
    String accountNumberAfricaExpress = "535775";
    String policyHolder = "2001115564";
    String itemType = "U";
    String saoReportUrl = "soa_report%26destype=cache&p_account=#ACCOUNT_NO#&p_agent_no=&p_handler=&p_center_code=&p_from_date=&p_to_date=&request_id=&p_item_types=#ITEMTYPE";
    String instalmentReportUrl = "instalment_report%26destype=cache&desname=&p_account_no=#ACCOUNT_NO&P_FROM_DATE=#FROM_DATE&P_SHOW_COMM=N&P_SHOW_QUOTE=N";
    String oracleReportsUrl = "http://pt-sxd9e-001.i04.local:9202/reports/rwservlet?";

    @Disabled //No unpaid items
    @Test
    public void bank_details_for_account_584265_is_one(){
        List<BankDetail> bankDetails = financialService.getBankDetailFor("584265", Unpaid, No,Yes);
        assertThat(bankDetails.size()).isEqualTo(1);
    }

    @Test
    public void testGetCreditControllersForStatCode() {
        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodePremiumAccountsFromRepo(statCodeForAfricaExpress);
        List<LDAPUser> list = financialService.getCreditControllersForStatCode(statCodeAccounts);
        assertThat(list.size()).isGreaterThan(0);
        assertThat(list.size()).isEqualTo(1);
    }


    @Test
    public void testGetCreditControllersForStatCode2() {
        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodePremiumAccountsFromRepo("23703.00");
        List<LDAPUser> list = financialService.getCreditControllersForStatCode(statCodeAccounts);
        assertThat(list.size()).isGreaterThan(0);
        assertThat(list.size()).isEqualTo(1);
    }

    @Test
    public void creditControllerForStatCodeWithoutCreditController() {
        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodePremiumAccountsFromRepo("34226.00");
        List<LDAPUser> list = financialService.getCreditControllersForStatCode(statCodeAccounts);
        assertThat(list.size()).isEqualTo(1);
        LDAPUser ldapUser = list.get(0);
        assertThat(ldapUser.getEmail()).isEqualToIgnoringCase(creditControlEmail);
    }

    @Disabled //No open items
    @Test
    public void testGetAccountSummary() {
        List<AccountSum> accountSum = financialService.getAccountSumSortedByCurrency("535775");
        assertThat(accountSum).isNotNull();
        assertThat(accountSum.get(0)).isNotNull();
        Assertions.assertThat(accountSum.get(0).getBalance()).isNotNull();

    }

    @Test
    public void testSOAaccountNumberGetPdf() {
        Base64Container container = financialService.statementOfAccountAsPDF(statCodeForAfricaExpress, accountNumberAfricaExpress, itemType, "SOA.pdf");
        assertThat(container.getData()).isNotEmpty();
    }

    @Disabled //No open items
    @Test
    public void testSOAPolicyHolderGetPdf() {
        Base64Container container = financialService.statementOfAccountAsPDF(statCodeForAfricaExpress, policyHolder, itemType, "SOA.pdf");
        assertThat(container.getData()).isNotEmpty();
    }

    @Test
    public void testgetStatementOfAccountUrl() {
        String statementOfAccountUrl = financialService.getStatementOfAccountUrl(statCodeForAfricaExpress, accountNumberAfricaExpress, itemType);
        assertThat(statementOfAccountUrl).isNotNull();
    }

    @Disabled //TODO RESTAPI-686
    @DisplayName("Instalment Report url")
    @Test
    public void testgetInstalmentReportUrl() {
        String instalmentReportUrl = financialService.getInstalmentReportUrl(statCodeForAfricaExpress, accountNumberAfricaExpress);
        assertThat(instalmentReportUrl).isNotNull();
    }

    @Disabled //TODO RESTAPI-686
    @DisplayName("Test Instalment Report Base64")
    @Test
    public void testInstallmentReportGetPdf() {
        Base64Container container = financialService.instalmentReportAsPDF(statCodeForAfricaExpress, accountNumberAfricaExpress, "Instalment Report.pdf");
        assertThat(container.getData()).isNotEmpty();
    }

    @Test
    public void fred_olsen_do_have_access_to_event() {
        boolean access = financialService.doStatCodeHaveAccessToEvent("0331.01", "40130688");
        assertThat(access).isTrue();
    }

    @Test
    public void fred_olsen_2_do_not_have_access_to_event() {
        boolean access = financialService.doStatCodeHaveAccessToEvent("0331.09", "40130688");
        assertThat(access).isFalse();
    }

    @DisplayName("Service Supplier for Stena Bulk")
    @Test
    public void testGetServiceSupplierAccountForStenaBulk() {
        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodeAccountsCached("28262.00");
        assertThat(statCodeAccounts).extracting("accountNo").contains("566935");
    }

    @DisplayName("Africa Express Line has more than 0 accounts")
    @Test
    public void africaExpressHasSixAccounts() {
        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodeAccountsCached("6554.50");
        assertThat(statCodeAccounts.size()).isGreaterThan(0);
    }

    @DisplayName("Rampion Offshore Wind Limited should display Deposit Accounts")
    @Test
    public void retrieveDepositAccountsWhichHaveOldTransactions() {
        List<StatCodeAccountEntity> statCodeAccounts = financialService.getStatCodeAccountsCached("32462.10");
        assertThat(statCodeAccounts).extracting("accountNo").containsOnly("577343", "590236");
    }

    @Disabled //No open items
    @DisplayName("Get Account Items for Account")
    @Test
    public void testExcelMissingItems() {
        List<AccountItemEntity> accItems = financialService.getAccItems("535775", AccountItemEntity.ItemType.Unpaid, No);
        assertThat(accItems).size().isGreaterThan(0);
        assertThat(accItems).extracting("vesselName").contains("LADY ROSE");
    }

    @Test
    public void testExcelMissingItems2() {
        StatementOfAccountExcel excel = (StatementOfAccountExcel) financialService.getStatementOfAccountExcel("x", "535775");
        assertThat(excel).isNotNull();
    }

    @DisplayName("Open Invoices for Bulk Trading")
    @Test
    public void testOpenInvoicesForBulkTrading() {
        List<Invoice> invoices = financialService.getInvoicesFor("7534.0");
        assertThat(invoices).size().isGreaterThanOrEqualTo(1);
    }

    @Disabled
    @DisplayName("Example on how to check a due date has changed")
    @Test
    public void invoice_due_date_has_changed() {
        List<Invoice> list = financialService.getInvoicesForAccount("509907");
        assertThat(list.size()).isGreaterThan(0);


        Invoice invoice = list.stream().filter(i -> i.getInvoiceNo().equals("9901666736")).findFirst().orElse(null);
        assertThat(invoice).isNotNull();
        assertThat(invoice.getDueDate()).isEqualTo(LocalDate.parse("2018-05-07"));
        assertThat(invoice.isDueDateHasChanged()).isTrue();

        List<InvoiceItem> collect = invoice.getInvoiceItems().stream().filter(item -> item.getDueDate().isEqual(LocalDate.parse("2018-12-01"))).collect(Collectors.toList());

        assertThat(collect.size()).isEqualTo(2);
    }


    @Disabled //No open invoices
    @DisplayName("Invoices for Africa Express - FinancialServiceTest ")
    @Test
    public void testGetInvoicesForStatcodeAfricaExpress() {
        List<Invoice> invoices = financialService.getInvoicesFor("6554.50");
        assertThat(invoices.size()).isGreaterThan(0);
    }

    //TODO:RESTAPI-757
//    @Test
//    @DisplayName("Get invoice from archive")
//    public void get_invoice_document_from_archive(){
//        String invoiceNo = "9901678518";
//        ArchiveEntity document = service.archiveService.getInvoiceBy(invoiceNo);
//        assertThat(document.getLength()).isGreaterThan(13000);
//    }

    @Disabled //Missing account_no in mv_charter_delivery_report
    @DisplayName("Retrieve all Declarations without filtering.")
    @Test
    public void charterDeclarations() {
        List<CharterDeliveryReportEntity> charterDeclarations = financialService.getAllCharterDeclarations("586950");
        assertThat(charterDeclarations).extracting("currentShipName").contains("SAVANNAH");
        assertThat(charterDeclarations)
                .filteredOn(x -> x.getCurrentShipName().equalsIgnoreCase("SAVANNAH"))
                .size().isGreaterThanOrEqualTo(4);
    }

    @Disabled //TODO RESTAPI-686
    @DisplayName("Find fist start date for active policies in statcode")
    @Test
    public void statCodeFirstPolicyStartDate(){
        String firstStartDate = financialService.getPolicyFistStartDate(statCodeForAfricaExpress, accountNumberAfricaExpress);
        assertThat(firstStartDate).isNotNull();
    }

    @Disabled //AfricaExpress has no open items
    @Nested
    class overdue{

        @Disabled //No overdue invoices
        @DisplayName("Overdue Invoices for AfricaExpress")
        @Test
        public void overdueInvoicesForAfrica() {
            List<Invoice> overdueInvoicesFor = financialService.getOverdueInvoicesFor(statCodeForAfricaExpress);
            assertThat(overdueInvoicesFor).size().isGreaterThan(0);
            List<Invoice> invoices = overdueInvoicesFor.stream()
                    .filter(x -> x.getAccountNo().equalsIgnoreCase(accountNumberAfricaExpress))
                    .collect(Collectors.toList());
            assertThat(invoices).size().isEqualTo(2);
        }

        @Test
        public void noOverdueInvoicesForFinbetaSpaWhichArePositive() {
            List<Invoice> overdueInvoicesFor = financialService.getOverdueInvoicesFor("1728.0");
            assertThat(overdueInvoicesFor).size().isGreaterThan(0);
            List<Invoice> invoices = overdueInvoicesFor.stream().filter(x -> x.getAccountNo().equalsIgnoreCase("575540")).collect(Collectors.toList());
            assertThat(invoices).size().isEqualTo(0);
        }

        @Disabled //No overdue invoices
        @DisplayName("Overdue Invoices for AfricaExpress")
        @Test
        public void overdue_invoices_should_have_policy_holder() {
            List<Invoice> overdueInvoicesFor = financialService.getOverdueInvoicesFor("6554.50");
            assertThat(overdueInvoicesFor).size().isGreaterThan(0);

            assertThat(overdueInvoicesFor).extracting("policyHolder").containsOnly("Africa Express Line Ltd.");
        }

    }


    @Nested
    class instalments{

        @DisplayName("Deposit Instalment Overview for Finbeta SPA")
        @Test
        public void testDepositInstalmentOverviewForFinBetaSpa() {
            List<DepositInstalment> result = financialService.getDepositInstalmentOverview("586078");
            assertThat(result.size()).isEqualTo(5);

        }

        @DisplayName("Deposit Instalment Overview")
        @Test
        public void testDepositInstalmentOverview() {
            List<DepositInstalment> result = financialService.getDepositInstalmentOverview("586266");
            assertThat(result.size()).isGreaterThan(0);
            assertThat(result)
                    .filteredOn("invoiceNo", "9901664385")
                    .size().isEqualTo(1);

            assertThat(result)
                    .filteredOn("invoiceNo", "9901664385")
                    .flatExtracting("items").hasSize(1);
        }




        @Disabled
        @DisplayName("Deposit Instalment Overview for XO Shipping with Other Instalments - Impossible to find Other ")
        @Test
        public void xoShippingHasOtherInstalments() {
            List<DepositInstalment> result = financialService.getDepositInstalmentOverview("586444");
            assertThat(result.size()).isGreaterThan(0);
            assertThat(result)
                    .filteredOn("instalmentType", other)
                    .size().isEqualTo(1);

            DepositInstalment first = result.stream().filter(x -> x.getInstalmentType().equals(other)).findFirst().orElse(null);
            assertThat(first.getItems().size()).isEqualTo(1);
        }


        /*Can be used to find specific information about an account and run test, but useless as a generic test*/
        @Disabled
        @DisplayName("Rampion Offshore")
        @Test
        public void rampionOffshoreAmountsInGBP() {
            List<DepositInstalment> result = financialService.getDepositInstalmentOverview("587531");
            assertThat(result ).size().isEqualTo(2).withFailMessage("Instalment");

            DepositInstalment declaration = result.stream()
                    .filter(x->x.getInvoiceNo().equals("9901670347"))
                    .findFirst()
                    .orElse(null);

            Assertions.assertThat(declaration)
                    .isNotNull();

            Assertions.assertThat(declaration.getInvoicedSum()).isEqualByComparingTo("60000");
            Assertions.assertThat(declaration.getPaidSum()).isEqualByComparingTo("-60000");
            Assertions.assertThat(declaration.getBalanceSum()).isEqualByComparingTo("0");
            Assertions.assertThat(declaration.getCurrencyCode()).isEqualToIgnoringCase("GBP");

            declaration = result.stream()
                    .filter(x->x.getInvoiceNo().equals("9901670348"))
                    .findFirst()
                    .orElse(null);

            Assertions.assertThat(declaration)
                    .isNotNull();

            Assertions.assertThat(declaration.getInvoicedSum()).isEqualByComparingTo("60000");
            Assertions.assertThat(declaration.getPaidSum()).isEqualByComparingTo("0");
            Assertions.assertThat(declaration.getBalanceSum()).isEqualByComparingTo("60000");
            Assertions.assertThat(declaration.getCurrencyCode()).isEqualToIgnoringCase("GBP");
        }
    }
}
