package com.skuld.extranet.core.financial.domain.deposit;

import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalment;
import com.skuld.extranet.core.financial.domain.deposit.dto.DepositInstalmentItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DepositInstalmentOverviewTest {

    DepositInstalment overview = new DepositInstalment();

    LocalDate today = LocalDate.now();
    LocalDate tomorrow = today.plusDays(2);
    LocalDate yesterday = today.minusDays(2);
    List<DepositInstalmentItem> depositInstalmentItems;

    @BeforeEach
    public void setup() {
        overview.setInvoiceDueDate(today);
        depositInstalmentItems = new ArrayList<>();
        overview.setItems(depositInstalmentItems);
    }

    @DisplayName("If due date hasn't changed, show invoice due date and due date has not changed")
    @Test
    public void dueDateForInvoice() {

        DepositInstalmentItem item1 = new DepositInstalmentItem().setItemDueDate(today);
        DepositInstalmentItem item2 = new DepositInstalmentItem().setItemDueDate(today);
        depositInstalmentItems.addAll((Arrays.asList(item1, item2)));

        assertThat(overview.isDueDateHasChanged()).isFalse();
        assertThat(overview.getInvoiceDueDate()).isEqualTo(today);

    }

    @DisplayName("Only show different day if all items have same due date")
    @Test
    public void invoiceDueDateShouldBeEqualToItem() {

        DepositInstalmentItem item1 = new DepositInstalmentItem().setItemDueDate(tomorrow);
        DepositInstalmentItem item2 = new DepositInstalmentItem().setItemDueDate(today);
        depositInstalmentItems.addAll((Arrays.asList(item1, item2)));

        assertThat(overview.isDueDateHasChanged()).isTrue();
        assertThat(overview.getInvoiceDueDate()).isEqualTo(today);
        assertThat(overview.getOriginalInvoiceDueDate()).isEqualTo(today);

    }

    @DisplayName("Invoice Due Date is same as all Items Due Date")
    @Test
    public void invoiceDueDateShouldBeEqualToItem2() {
        LocalDate theSameDate = this.tomorrow;
        DepositInstalmentItem item1 = new DepositInstalmentItem().setItemDueDate(theSameDate);
        DepositInstalmentItem item2 = new DepositInstalmentItem().setItemDueDate(theSameDate);
        depositInstalmentItems.addAll((Arrays.asList(item1, item2)));

        assertThat(overview.isDueDateHasChanged()).isTrue();
        assertThat(overview.getInvoiceDueDate()).isEqualTo(theSameDate);
        assertThat(overview.getOriginalInvoiceDueDate()).isEqualTo(today);
    }

    @DisplayName("Items have different value, then display Invoice Due Date")
    @Test
    public void all_item_different_values() {
        DepositInstalmentItem item1 = new DepositInstalmentItem().setItemDueDate(yesterday);
        DepositInstalmentItem item2 = new DepositInstalmentItem().setItemDueDate(tomorrow);
        depositInstalmentItems.addAll((Arrays.asList(item1, item2)));

        assertThat(overview.isDueDateHasChanged()).isTrue();
        assertThat(overview.getInvoiceDueDate()).isEqualTo(today);
        assertThat(overview.getOriginalInvoiceDueDate()).isEqualTo(today);
    }


    @Test
    public void testLocalDate(){
        System.out.println(LocalDate.now());
    }

}