package com.skuld.extranet.core.financial.domain.entity;

import com.skuld.extranet.core.financial.domain.AccountGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AccountEntityTest {

    AccountEntity entity;

    @BeforeEach
    public void before_each() {
        entity = new AccountEntity();
    }

    @Test
    public void account_group_1_is_premium_account() {

        entity.setAccountType("1").setPaymentMethod("SOMETHING");

        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.PREMIUM);
    }

    @Test
    public void account_group_1_with_payment_method_deposit_is_premium_deposit_account() {

        entity.setAccountType("1").setPaymentMethod("DEPOSIT");

        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.PREMIUM_DEPOSIT);
    }

    @Test
    public void account_group_3_and_4_and_5__is_claim_account() {

        entity.setAccountType("3");
        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.CLAIM);

        entity.setAccountType("5");
        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.CLAIM);

        entity.setAccountType("6");
        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.CLAIM);

    }

    @Test
    public void account_group_2_and_7_is_other_account() {
        entity.setAccountType("7");
        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.OTHER);

        entity.setAccountType("2");
        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.OTHER);

        entity.setAccountType("");
        assertThat(entity.getAccountGroup()).isEqualTo(AccountGroup.OTHER);

    }
}