package com.skuld.extranet.core.financial.domain.deposit;

import com.skuld.extranet.core.financial.domain.deposit.dto.DepositStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class DepositStatusTest {

    DepositStatus status ;

    @BeforeEach
    public void setup(){
        status = new DepositStatus();
    }

    @DisplayName("When Invoiced is null and Paid is null, return null")
    @Test
    public void testGetBalanceWhenNullValues(){
        status.setDepositInvoiced(null);
        status.setDepositPaid(null);
        BigDecimal depositBalance = status.getDepositBalance();
        assertThat(depositBalance).isNull();
    }

    @DisplayName("When Invoiced is null, Balance is same as Paid value")
    @Test
    public void testGetBalanceWhenInvoicedIsNull(){
        status.setDepositInvoiced(null);
        status.setDepositPaid(new BigDecimal("-100"));
        BigDecimal depositBalance = status.getDepositBalance();
        assertThat(depositBalance).isEqualByComparingTo("-100");
    }


    @DisplayName("Invoiced plus Paid")
    @Test
    public void testGetBalance(){
        status.setDepositInvoiced(new BigDecimal("100"));
        status.setDepositPaid(new BigDecimal("50"));
        BigDecimal depositBalance = status.getDepositBalance();
        assertThat(depositBalance).isEqualByComparingTo("150");
    }

    @DisplayName("Invoiced plus Paid")
    @Test
    public void testGetBalanceDeclaredBiggerThanInvoiced(){
        status.setDepositInvoiced(new BigDecimal("25"));
        status.setDepositPaid(new BigDecimal("-250"));
        BigDecimal depositBalance = status.getDepositBalance();
        assertThat(depositBalance).isEqualByComparingTo("-225");
    }

    @DisplayName("Invoiced plus Paid")
    @Test
    public void testGetBalancedEqual(){
        status.setDepositInvoiced(new BigDecimal("25"));
        status.setDepositPaid(new BigDecimal("-25"));
        BigDecimal depositBalance = status.getDepositBalance();
        assertThat(depositBalance).isEqualByComparingTo("0");
    }


    @DisplayName("Balance, round 2 decimals")
    @Test
    public void testGetBalancedRoundTwoDecimals(){
        status.setDepositInvoiced(new BigDecimal("25.0003"));
        status.setDepositPaid(new BigDecimal("-10"));
        BigDecimal depositBalance = status.getDepositBalance();
        assertThat(depositBalance).isEqualByComparingTo("15");
    }

}