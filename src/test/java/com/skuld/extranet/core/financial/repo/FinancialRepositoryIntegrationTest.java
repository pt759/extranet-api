package com.skuld.extranet.core.financial.repo;

import com.skuld.extranet.ExtranetApplication;
import com.skuld.extranet.core.financial.domain.AccountSum;
import com.skuld.extranet.core.financial.domain.deposit.entity.DepositStatusEntity;
import com.skuld.extranet.core.financial.domain.deposit.entity.InstallmentsEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountEntity;
import com.skuld.extranet.core.financial.domain.entity.AccountItemEntity;
import com.skuld.extranet.core.financial.domain.entity.StatCodeAccountEntity;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.skuld.common.domain.YesNo.No;
import static com.skuld.common.domain.YesNo.Yes;
import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static com.skuld.extranet.core.financial.domain.entity.AccountItemEntity.ItemType.All;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ExtranetApplication.class)
class FinancialRepositoryIntegrationTest {

    @Autowired
    private FinancialRepository financialRepository;

    @Test
    public void testAccItems() {
        List<AccountItemEntity> rows = financialRepository.accountItems("586570", All, No, No);
        assertThat(rows.size()).isGreaterThan(0);
    }

    @Test
    public void testAccount() {
        AccountEntity account = financialRepository.getAccount("571039");
        assertThat(account.getName()).isEqualTo("A.P. Moller Maersk (Offshore)");
//        assertThat(account.getPolicyHolder()).isEqualTo("A.P. Møller-Maersk A/S");
    }

    @Test
    public void premiumAccountsForAfricaExpressLine() {
        List<StatCodeAccountEntity> statCodeAccounts = financialRepository.getStatCodePremiumAccounts(africaExpressStatCode);
        assertThat(statCodeAccounts.size()).isEqualTo(2);

        assertThat(statCodeAccounts)
                .extracting("accountNo", "statCode", "policyHolderId")
                .containsOnly(tuple("584265", africaExpressStatCode, "2000437258")
                        , tuple("535775", africaExpressStatCode, "2000437258"));
    }

    @DisplayName("Finding Accounts with no balance, but still active - Case WWL")
    @Test
    public void getStatCodeAccountsForWWL() {
        List<StatCodeAccountEntity> statCodeAccounts = financialRepository.getStatCodePremiumAccounts("37048.40");
        assertThat(statCodeAccounts.size()).isGreaterThan(0);

        assertThat(statCodeAccounts)
                .extracting("accountNo", "statCode", "policyHolderId")
                .contains(tuple("583579", "37048.40", "2002380938"));
    }

    @Test
    public void testGetStatCodeClaimAccounts() {
        List<StatCodeAccountEntity> statCodeAccounts = financialRepository.getStatCodeClaimAccounts(africaExpressStatCode);
        assertThat(statCodeAccounts.size()).isGreaterThan(0);

        assertThat(statCodeAccounts)
                .extracting("accountNo", "statCode", "policyHolderId")
                .contains(tuple("540682", africaExpressStatCode, "2000437258"));

    }

    @Test
    public void depositStatusFromAccount() {
        DepositStatusEntity result = financialRepository.getDepositAccountStatus("582099");
        assertThat(result).isNotNull();
        assertThat(result.getCurrency()).isEqualTo("USD");
        assertThat(result.getInvoicedAmt()).isEqualByComparingTo("300000");
        assertThat(result.getPaidAmt()).isEqualByComparingTo("-867243.109999999986030161380767822265625");
    }

    @Test
    public void testGetInstalment() {
        List<InstallmentsEntity> instalments = financialRepository.getInstalments("586247");
        assertThat(instalments.size()).isGreaterThan(0);
    }

    @Test
    public void getInstalmentsWithOthers() {
        List<InstallmentsEntity> instalments = financialRepository.getInstalmentOthers("585211");
        assertThat(instalments.size()).isGreaterThan(0);
    }

    @Disabled //No open items
    @Test
    public void should_return_item_rows_for_account() {
        List<AccountSum> accountSum = financialRepository.getAccountSum("535775", All, Yes, Yes);
        AccountSum usd = accountSum.stream().filter(x -> x.getCurrency().equals("USD")).findAny().orElse(null);

        assertThat(usd).isNotNull();
        assertThat(usd.getItemRows()).isEqualTo(15);
    }


    @Test
    void test_premium_account_performance() {
        StopWatch stopWatch = StopWatch.createStarted();
        financialRepository.getStatCodePremiumAccounts("14944.00");
        stopWatch.stop();

        log.debug("Get getStatCodePremiumAccounts: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 1000);
    }

}