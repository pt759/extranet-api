package com.skuld.extranet.core.financial.domain.statementofaccount;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Disabled
class StatementOfAccountRowsTest {


    @Test
    public void testDoSum() {

        List<StatementOfAccountInvoice> invoices = new ArrayList<>();
        invoices.add(createInvoiceDetail());

        StatementOfAccountVessel row = new StatementOfAccountVessel();
        row.setInvoices(invoices);

        row.calculateInvoiceTotals();

//        assertThat(row.getBalance()).isEqualByComparingTo("100.0");
//        assertThat(row.getOriginalAmount()).isEqualByComparingTo("100.0");
//        assertThat(row.getDueBalance()).isEqualByComparingTo("100.0");


    }

    private StatementOfAccountInvoice createInvoiceDetail() {
        StatementOfAccountInvoice invoice = new StatementOfAccountInvoice();

        List<StatementOfAccountInvoiceItem> details = new ArrayList<>();
        StatementOfAccountInvoiceItem detail = new StatementOfAccountInvoiceItem();
        detail.setDueBalance(new BigDecimal("100.0"));
        detail.setBalance(new BigDecimal("100.0"));
        detail.setOriginalAmount(new BigDecimal("100.0"));
        details.add(detail);
        invoice.setDetails(details);
        return invoice;
    }


}