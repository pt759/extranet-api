package com.skuld.extranet.core.admin.service;

import com.skuld.extranet.core.admin.domain.UserAdminDetail;
import com.skuld.extranet.core.admin.domain.UserAdminStatCodeGroupLogin;
import com.skuld.extranet.core.user.domain.BusinessUnit;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserAdminServiceTest {

    @Autowired
    private UserAdminService service;

    @DisplayName("Get list of BU's")
    @Test
    public void testGetBusinessGroups() {
        List<BusinessUnit> businessUnits = service.getBusinessUnits();
        assertThat(businessUnits.size()).isEqualTo(13); //12 + broker
    }
    
    @DisplayName("Get stat code groups for oslo1")
    @Test
    public void testGetStatCodeGroups() {
    	List<StatCodeGroup> list = service.getBusinessUnitStataCodeGroups("8");
    	assertThat(list.size()).isGreaterThan(50);
    }
    
    @DisplayName("Get stat code groups for Brokers")
    @Test
    public void testGetStatCodeGroupsForBrokers() {
    	List<StatCodeGroup> list = service.getBrokerStataCodeGroups();
    	assertThat(list.size()).isGreaterThan(50);
    }
    
    @Test
    public void testGetUserAdminStatCodeGroupLogin() {
        List<UserAdminStatCodeGroupLogin> list = service.getUserAdminStatCodeGroupLogin("oslosyn1");
        assertThat(list.size()).isGreaterThan(0);
        UserAdminStatCodeGroupLogin userAdminStatCodeGroupLogin = list.get(0);

        assertThat(userAdminStatCodeGroupLogin.getAccountManager()).isNotNull();
        assertThat(userAdminStatCodeGroupLogin.getStatCodeName()).isNotNull();
    }

    @Test
    public void testUserDetail() {
        UserAdminDetail detail = service.getUserAdminDetail("kristiang");
        assertThat(detail).isNotNull();
        assertThat(detail.getUsers().size()).isGreaterThan(0);
        assertThat(detail.getAzureGroup().getName()).isEqualTo("kristiang");
    }

    @Test
    public void testUserDetailWithBrokerWith99Members() {
        UserAdminDetail detail = service.getUserAdminDetail("oslosyn1");
        assertThat(detail).isNotNull();
        assertThat(detail.getUsers().size()).isGreaterThan(0);

        assertThat(detail.getUsers().size()).isGreaterThan(90);
        assertThat(detail.getAzureGroup().getName()).isEqualTo("oslosyn1");
    }

    @Test
    public void testUserDetailWithBroker2() {
        UserAdminDetail detail = service.getUserAdminDetail("cpbhodin");
        assertThat(detail).isNotNull();
        assertThat(detail.getUsers().size()).isGreaterThan(0);
        assertThat(detail.getAzureGroup().getName()).isEqualTo("cpbhodin");
        assertThat(detail.getAzureGroup().isBroker()).isTrue();
        assertThat(detail.getStatCodes().size()).isGreaterThan(0);

    }
    
    @Test
    public void testUserDetailWithBroker3() {
        UserAdminDetail detail = service.getUserAdminDetail("farosmar");
        assertThat(detail).isNotNull();
        assertThat(detail.getUsers().size()).isGreaterThan(0);
        assertThat(detail.getAzureGroup().getName()).isEqualTo("farosmar");
        assertThat(detail.getAzureGroup().isBroker()).isTrue();
        assertThat(detail.getStatCodes().size()).isGreaterThan(0);
    }
    
}
