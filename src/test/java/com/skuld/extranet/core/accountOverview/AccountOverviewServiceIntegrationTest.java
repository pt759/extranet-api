package com.skuld.extranet.core.accountOverview;

import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.core.accountOverview.dto.SummaryHeaderDTO;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static com.skuld.common.global.GlobalTestValues.cpbHodingsAzureDisplayName;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("qa")
@Log4j2
@SpringBootTest
class AccountOverviewServiceIntegrationTest {

    @Autowired
    private AccountOverviewService accountOverviewService;

    @Test
    public void testConvertToAccountOverview() {
        List<StatCodeDTO> statCodes = new ArrayList<>();
        String statCodeForCompany = "7148.0";
        StatCodeDTO statCode = StatCodeDTO.builder().statCode(statCodeForCompany).build();
        statCode.setStatCode(statCodeForCompany);
        statCodes.add(statCode);
        List<AccountOverview> accountOverviews = accountOverviewService.convertToAccountOverview(statCodes);
        assertThat(accountOverviews.size()).isEqualTo(1);
        AccountOverview accountOverview = accountOverviews.get(0);
        assertThat(accountOverview.getStatCode()).isEqualTo(statCodeForCompany);
        assertThat(accountOverview.getProducts()).isNotNull();
        assertThat(accountOverview.getProducts().size()).isGreaterThan(0);
        assertThat(accountOverview.getProductsAsString().length()).isGreaterThan(0);
    }

    @Test
    @DisplayName("Find Members for a statCodeGroupLogin")
    public void getAllClientsForBrokerAndCheckNotNullForStatCode() {
        List<AccountOverview> list = accountOverviewService.getAllClientsForBroker("aonrisksoluti");
        assertStatCodeIsNotNullForList(list);
        assertThat(list.size()).isGreaterThan(1);
        assertThat(list).extracting("statCode").isNotNull();
        assertStatCodeIsNotNullForList(list);
    }

    private void assertStatCodeIsNotNullForList(List<AccountOverview> list) {
        for (AccountOverview accountOverview : list) {
            assertThat(accountOverview.getStatCode()).isNotNull();
        }
    }

    @Test
    @DisplayName("Find Stat Code For a statCodeGroupLogin")
    public void findStatCodesForLoginTest2() {
        List<AccountOverview> clients = accountOverviewService.getAllClientsForBroker("aonrisksoluti");
        assertThat(clients.size()).isGreaterThan(0);

        AccountOverview client = clients.get(0);
        assertThat(client.getStatCode()).isNotNull();
        assertThat(client.getClientName()).isNotNull();

    }

    @DisplayName("Find Recent Claims for statCodeGroupLogin using DB")
    @Test
    public void findRecentClaimsForLoginUsingDB() {
        List<Claim> list = accountOverviewService.recentOpenClaims("cpbhodin", 3);
        assertThat(list.size()).isEqualTo(3);

        Claim c0 = list.get(0);
        Claim c1 = list.get(1);
        Claim c2 = list.get(2);

        assertThat(c0.getIncidentDate().isAfter(c1.getIncidentDate()));
        assertThat(c1.getIncidentDate().isAfter(c2.getIncidentDate()));

    }

    @DisplayName("Get StatCodes from DB")
    @Test
    public void getStatCodesFromDBTest() {
        String authToken = "";
        List<String> statCodes = accountOverviewService.getStatCodesFromRepo("cpbhodin");
        assertThat(statCodes.size()).isGreaterThan(0);
    }


    @DisplayName("should get a blank AccountOverview")
    @Test
    public void getAccountOverviewForNonExistingStatCode() {
        StatCodeDTO statCode = StatCodeDTO.builder().statCode("-1").build();
        AccountOverview accountOverview = accountOverviewService.getAccountOverview(statCode.getStatCode());
        assertThat(accountOverview).isNotNull();
        assertThat(accountOverview.getCurrency()).isNullOrEmpty();
    }

    @Test
    void get_vessel_and_claims_summary_for_broker() {
        SummaryHeaderDTO brokerSummary = accountOverviewService.getBrokerSummaryHeader("oslosyn1");
        assertNotNull(brokerSummary.getVesselsHeader().getMainCovers());
        assertNotNull(brokerSummary.getVesselsHeader().getTotalNetPremium());
        assertTrue(brokerSummary.getVesselsHeader().getTotalVessels() > 0);
        assertTrue(brokerSummary.getClaimsHeader().getOpenClaims() > 0);
        assertTrue(brokerSummary.getClaimsHeader().getVesselsWithClaim() > 0);
    }

    @Test
    void broker_summary_performance() {
        StopWatch stopWatch = StopWatch.createStarted();
        accountOverviewService.getBrokerSummaryHeader(cpbHodingsAzureDisplayName);
        stopWatch.stop();
        log.debug("Get broker summary: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 5000);
    }

    @Test
    void count_vessels_for_broker() {
        Set<String> statCodes = getTestStatCodes();
        StopWatch stopWatch = StopWatch.createStarted();
        long brokersTotalVessels = accountOverviewService.countNumberOfVessels(statCodes);
        stopWatch.stop();
        log.debug("Vessels for " + getTestStatCodes().size() + " stat codes: " + stopWatch.getTime());

        assertTrue(stopWatch.getTime() < 1000);
        assertTrue(brokersTotalVessels > 1);
    }

    @Test
    void bu_total_claims_with_vessels() {
        List<AccountOverview> allStatCodes = accountOverviewService.getAllClientsForBroker("oslosyn1");
        Set<String> statCodes = allStatCodes.stream().map(AccountOverview::getStatCode).collect(Collectors.toSet());
        StopWatch stopWatch = StopWatch.createStarted();
        ClientClaimsSummaryDTO brokerClaimsWithVessels = accountOverviewService.getClaimsWithVessels(statCodes);
        stopWatch.stop();

        log.debug("Get total claims: " + stopWatch.getTime());

        assertTrue(stopWatch.getTime() < 20000); //TODO RESTAPI-1104 slow
        assertTrue(brokerClaimsWithVessels.getOpenClaims() > 1);
        assertTrue(brokerClaimsWithVessels.getVesselsWithClaim() > 1);
    }


    @Test
    void brokers_total_claims_with_vessels() {
        StopWatch stopWatch = StopWatch.createStarted();
        Set<String> statCodes = getTestStatCodes();
        ClientClaimsSummaryDTO brokerClaimsWithVessels = accountOverviewService.getClaimsWithVessels(statCodes);
        stopWatch.stop();

        log.debug("Get total claims: " + stopWatch.getTime());

        assertTrue(stopWatch.getTime() < 1000);
        assertTrue(brokerClaimsWithVessels.getOpenClaims() > 1);
        assertTrue(brokerClaimsWithVessels.getVesselsWithClaim() > 1);
    }

    private Set<String> getTestStatCodes() {
        Set<String> accountOverviewList = Set.of(africaExpressStatCode, "14222.00");
        return accountOverviewList;
    }

}
