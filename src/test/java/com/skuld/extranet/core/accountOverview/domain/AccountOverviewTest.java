package com.skuld.extranet.core.accountOverview.domain;

import com.skuld.extranet.spa.pages.domain.Product;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

class AccountOverviewTest {

    AccountOverview overview = new AccountOverview();

    @Test
    public void testProducts(){

        List<Product> list = new ArrayList<>();
        list.add(Product.charter);
        list.add(Product.owner);


        overview.setProducts(list);

        String productsAsString = overview.getProductsAsString();
        assertThat(productsAsString).isEqualToIgnoringCase("Charterer, Owner");
    }

}