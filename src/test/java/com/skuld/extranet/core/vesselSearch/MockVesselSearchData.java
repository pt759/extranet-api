package com.skuld.extranet.core.vesselSearch;

import com.skuld.extranet.core.vesselSearch.domain.VesselCertificate;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearchBuilder;

import java.util.ArrayList;
import java.util.List;

public class MockVesselSearchData {

    private static List<VesselCertificate> certificates = null;
    private static List<VesselSearch> vessels;

    private List<VesselSearch> mockVessel() {
        List<VesselSearch> vessels = new ArrayList<>();
        vessels.add(createVessel("1", "Ladyhawk", "9000000"));
        vessels.add(createVessel("2", "Silenthawk", "9000001"));
        vessels.add(createVessel("3", "Seatrader", "9000002"));
        vessels.add(createVessel("4", "Watereagle", "9000003"));
        vessels.add(createVessel("5", "Fridgemagnet 1 ", "9000004"));
        vessels.add(createVessel("6", "Fridgemagnet 2 ", "9000005"));
        vessels.add(createVessel("7", "Fridgemagnet 3", "9000006"));
        vessels.add(createVessel("8", "Fridgemagnet 4", "9000007"));
        vessels.add(createVessel("8", "Fridgemagnet 4", "9000007"));//This can happen

        return vessels;

    }

    private List<VesselCertificate> mockCertificates() {
        List<VesselCertificate> list = new ArrayList<>();
        list.add(createCertBC("1", "Cert A"));
        list.add(createCertBC("1", "Cert B"));
        list.add(createCertBC("2", "Cert A"));
        list.add(createCertBC("2", "Cert B"));
        list.add(createCertBC("2", "Cert B"));
        list.add(createCertBC("2", "Cert B"));
        list.add(createCertMLC("1", "Cert A<BR>Cert B"));
        list.add(createCertMLC("2", "Cert A<BR>Cert B"));

        return list;
    }


    public static List<VesselSearch> getOneVesselId(String vesselId) {
        List<VesselSearch> vessels = new ArrayList<>();
        for (VesselSearch vessel : MockVesselSearchData.getVessels()) {
            if (vessel.getVesselID().equalsIgnoreCase(vesselId)){
                vessels.add(vessel);
            }

        }
        return vessels;
    }


    public static List<VesselCertificate> getCertificatesFor(VesselSearch vessel) {
        List<VesselCertificate> cert = new ArrayList<>();
        for (VesselCertificate certificate : certificates) {
            if (certificate.getVesselID().equals(vessel.getVesselID())){
                cert.add(new VesselCertificate(certificate));
            }
        }
        return cert;
    }


    public static List<VesselCertificate> getCertificates() {

        if (certificates == null) {
            MockVesselSearchData data = new MockVesselSearchData();
            certificates = data.mockCertificates();
        }
        return certificates;
    }

    public static List<VesselSearch> getVessels() {
        if (vessels == null) {
            MockVesselSearchData data = new MockVesselSearchData();
            vessels = data.mockVessel();
        }
        return vessels;
    }

    private VesselCertificate createCertMLC(String vesselID, String cert_a) {
        return new VesselCertificate(vesselID, cert_a, "MLC");
    }

    private VesselCertificate createCertBC(String vesselID, String cert_a) {
        return new VesselCertificate(vesselID, cert_a, "BC");
    }

    public VesselSearch createVessel(String vesselID, String vesselName, String imo) {
        return new VesselSearchBuilder().setVesselID(vesselID).setVesselName(vesselName).setImo(imo).createVessel();
    }


}
