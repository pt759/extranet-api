package com.skuld.extranet.core.vesselSearch;

import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.extranet.core.vesselSearch.FetchType.*;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class VesselSearchServiceIntegrationTest {

    @Autowired
    VesselSearchService service;

    @Test
    public void testFilterVesselsById() {
        List<VesselSearch> list = service.filterVesselsBy("171113", VESSELID);
        VesselSearch vessel = list.get(0);
        assertThat(vessel.getVesselName()).isEqualTo("VISITOR");
        assertThat(list.size()).isEqualTo(1);
    }

    @Test
    public void testVesselFilterByNameOnLowerCase() {
        List<VesselSearch> list = service.filterVesselsBy("seatrad", NAME);
        assertThat(list.size()).isGreaterThan(3);
    }

    @Test
    public void testVesselFilterByNameWithNull() {
        List<VesselSearch> list = service.filterVesselsBy("", NAME);
        assertThat(list.size()).isEqualTo(0);
    }

    @Test
    public void testVesselFilterByNameContains() {
        List<VesselSearch> list = service.filterVesselsBy("tiger", NAME);
        assertThat(list.size()).isGreaterThan(0);
    }

    @Test
    public void testVesselFilterByIMO() {
        List<VesselSearch> listOfVesselInformation = service.filterVesselsBy("9780823", IMO);
        assertThat(listOfVesselInformation.size()).isGreaterThan(0);
    }

    @Test
    public void testMemberNameIsFullLength() {
        List<VesselSearch> listOfVesselInformation = service.filterVesselsBy("9780823", IMO);
        VesselSearch vessel = listOfVesselInformation.get(0);

        assertThat(vessel.getMember()).contains("Ruico (Guangdong) Shipping & Lo");
    }


    @Test
    public void get_registred_owner_name() {
        List<VesselSearch> vessels = service.filterVesselsBy("9780823", IMO);
        VesselSearch vessel = vessels.get(0);

        assertThat(vessel.getCurrentRegOwner()).contains("Foshan City Haiboshun");
    }

}
