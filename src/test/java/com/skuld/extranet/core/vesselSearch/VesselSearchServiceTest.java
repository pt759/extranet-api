package com.skuld.extranet.core.vesselSearch;

import com.skuld.extranet.core.vesselSearch.domain.VesselCertificate;
import com.skuld.extranet.core.vesselSearch.domain.VesselSearch;
import com.skuld.extranet.core.vesselSearch.repo.VesselSearchRepository;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static com.skuld.extranet.core.vesselSearch.FetchType.IMO;
import static com.skuld.extranet.core.vesselSearch.MockVesselSearchData.getCertificates;
import static com.skuld.extranet.core.vesselSearch.MockVesselSearchData.getCertificatesFor;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class VesselSearchServiceTest {

    @Mock
    Jdbi dbi;

    VesselSearchService service;

    MockVesselSearchData mockVesselData = new MockVesselSearchData();
    VesselSearchRepository mockVesselRepo;

    @BeforeEach
    public void setup() {
        service = new VesselSearchService();
        //service.setJdbi(DatabaseConfigForTest.DBIforTest());

        mockVesselRepo = mock(VesselSearchRepository.class);
        when(mockVesselRepo.getVesselByImoFromRepo("1")).thenReturn(MockVesselSearchData.getOneVesselId("1"));
        when(mockVesselRepo.getVesselByImoFromRepo("2")).thenReturn(MockVesselSearchData.getOneVesselId("2"));
        when(mockVesselRepo.getVesselByImoFromRepo("3")).thenReturn(MockVesselSearchData.getOneVesselId("3"));//No certificates

        when(mockVesselRepo.getVesselsByNameFromRepo(anyString())).thenReturn(MockVesselSearchData.getVessels());

        when(mockVesselRepo.getCertificatesFromRepo()).thenReturn(getCertificates());

        service.setVesselRepo(mockVesselRepo);

    }

    @Test
    public void testFindCertificatesForVessel() {

        VesselSearch vessel = MockVesselSearchData.getOneVesselId("1").get(0);
        when(mockVesselRepo.getCertificatesFromRepoFor(vessel)).thenReturn(getCertificatesFor(vessel));

        List<VesselSearch> vessels = service.filterVesselsBy("1", IMO);
        vessel = vessels.get(0);

        List<VesselCertificate> certificates = vessel.getCertificates();

        assertThat(certificates).isNotNull();
        assertThat(certificates.size()).isEqualTo(4);

        assertThat(certificates.get(0).getVesselID()).isEqualTo("1");
        assertThat(certificates.get(1).getVesselID()).isEqualTo("1");
        assertThat(certificates.get(2).getVesselID()).isEqualTo("1");
        assertThat(certificates.get(3).getVesselID()).isEqualTo("1");

    }


    @Test
    public void testFilterOnIMONoRepoInvoke() {
        List<VesselSearch> vessels1 = service.filterVesselsBy("1", IMO);
        verify(mockVesselRepo, times(0)).getCertificatesFromRepo();
    }

    @Test
    public void testLoadAllCertificatesOnlyRunOnce() {
        List<VesselSearch> vessels1 = service.filterVesselsBy("1", IMO);
        List<VesselSearch> vessels2 = service.filterVesselsBy("1", IMO);
        verify(mockVesselRepo, times(0)).getCertificatesFromRepo();
    }

    @Test
    public void testCertificatesLoadedCorrectlyWhenLoadingMultipleTimes() {
        List<VesselSearch> vessels = MockVesselSearchData.getOneVesselId("1");
        VesselSearch vessel = vessels.get(0);
        when(mockVesselRepo.getCertificatesFromRepoFor(vessel)).thenReturn(getCertificatesFor(vessel));

        List<VesselSearch> vessels1 = service.filterVesselsBy(vessel.getVesselID(), IMO);
        List<VesselSearch> vessels2 = service.filterVesselsBy(vessel.getVesselID(), IMO);
        assertCertificatesSizeIs(4, vessels1.get(0));
        assertCertificatesSizeIs(4, vessels2.get(0));
    }

    @Test
    public void testFindZeroCertificatesForVessel() {
        List<VesselSearch> listOfVesselInformation = service.filterVesselsBy("3", IMO);
        assertThat(listOfVesselInformation.size()).isEqualTo(1);
        VesselSearch myVessel = listOfVesselInformation.get(0);
        List<VesselCertificate> certificates = myVessel.getCertificates();
        assertThat(certificates.size()).isEqualTo(0);
    }

    @Test
    public void testVesselSplitMLCCertificates() {
        String mlc = "A<BR>B";

        VesselCertificate cert = new VesselCertificate();
        cert.setVesselID("100");
        cert.setCertificateType("MLC");
        cert.setCertificate(mlc);
        List<VesselCertificate> list = new ArrayList<>();
        list.add(cert);


        service.splitMLCCertificates(list);
        VesselCertificate actualA = list.get(0);
        VesselCertificate actualB = list.get(1);

        assertThat(list.size()).isEqualTo(2);
        assertThat(actualA.getCertificate()).isEqualTo("A");
        assertThat(actualB.getCertificate()).isEqualTo("B");
    }

    private void assertCertificatesSizeIs(int certificatesNumber, VesselSearch vessel) {
        List<VesselCertificate> certificates = vessel.getCertificates();
        assertThat(certificates).isNotNull();
        assertThat(certificates.size()).isEqualTo(certificatesNumber);

    }

    @DisplayName("When query is identical to vesselname, this vessel should appear first in list")
    @Test
    public void testVesselSearchHitOnTop(){
        MockVesselSearchData mock = new MockVesselSearchData();
        List<VesselSearch> vessels = new ArrayList<>();
        vessels.add(mock.createVessel("1", "Ladyhawk", "9000000"));
        vessels.add(mock.createVessel("2", "Silenthawk", "9000001"));
        vessels.add(mock.createVessel("3", "Seatrader", "9000002"));

        List<VesselSearch> seatrader = service.equalToQueryFirstInList("SEATRADER", vessels);
        VesselSearch vesselSearch = seatrader.get(0);
        assertThat(vesselSearch.getVesselName()).isEqualToIgnoringCase("SEATRADER");
    }

    @DisplayName("When query is NOT identical to vesselname, should appear in order from database.")
    @Test
    public void testVesselSearchHitNotOnTopWhenNot(){
        MockVesselSearchData mock = new MockVesselSearchData();
        List<VesselSearch> vessels = new ArrayList<>();
        vessels.add(mock.createVessel("1", "Silenthawk", "9000001"));
        vessels.add(mock.createVessel("2", "Ladyhawk", "9000000"));
        vessels.add(mock.createVessel("3", "Seatrader", "9000002"));

        List<VesselSearch> seatrader = service.equalToQueryFirstInList("TIGER", vessels);
        VesselSearch vesselSearch = seatrader.get(0);
        assertThat(vesselSearch.getVesselName()).isEqualToIgnoringCase("SILENTHAWK");
    }



}