package com.skuld.extranet.core.api.vessel.service;

import com.skuld.extranet.core.api.vessel.client.VesselClient;
import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VesselClientServiceTest {
    VesselClientService vesselClientService;

    VesselClient vesselClient = mock(VesselClient.class);

    String statCode = "1234";
    List<VesselListDTO> vesselList = List.of(
            VesselListDTO.builder().build(),
            VesselListDTO.builder().build()
    );

    @BeforeEach
    void setUp() {
        vesselClientService = new VesselClientService(vesselClient);
    }

    @Test
    public void should_get_vessels() {
        when(vesselClient.vesselsFor(statCode)).thenReturn(vesselList);
        List<VesselListDTO> vessels = vesselClientService.vesselsFor(statCode);
        assertThat(vessels).hasSize(2);
    }

    @Test
    public void should_search_for_vessel_list() {
        when(vesselClient.searchForVessel(any())).thenReturn(vesselList);
        List<VesselListDTO> vessels = vesselClientService.searchForVesselList(new VesselFilter());
        assertThat(vessels).hasSize(2);
    }


}
