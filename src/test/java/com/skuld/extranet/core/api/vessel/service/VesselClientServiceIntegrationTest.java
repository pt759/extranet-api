package com.skuld.extranet.core.api.vessel.service;

import com.skuld.extranet.core.api.vessel.dto.VesselListDTO;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class VesselClientServiceIntegrationTest {

    @Autowired
    VesselClientService vesselClientService;

    @Test
    public void should_find_vessels() {
        List<VesselListDTO> vessels = vesselClientService.vesselsFor(africaExpressStatCode);
        assertThat(vessels).hasSizeGreaterThan(0);
    }

    @Test
    public void should_search_for_vessels() {
        VesselFilter vesselFilter = new VesselFilter(africaExpressStatCode, "9150810");
        List<Vessel> vessels = vesselClientService.searchForVessel(vesselFilter);
        assertThat(vessels).hasSize(1);
    }

}
