package com.skuld.extranet.core.user.repo;

import com.skuld.extranet.core.user.domain.StatCode;
import com.skuld.extranet.core.user.domain.StatCodeGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class StatCodeRepositoryIntegrationTest {

    @Autowired
    StatCodeRepository statCodeRepository;

    @Test
    public void testGetStatCode() {
        StatCode statCode = statCodeRepository.getStatCode(africaExpressStatCode);
        assertThat(statCode).isNotNull();
    }

    @Test
    public void testGetStatCodeGroups() {
        List<StatCodeGroup> list = statCodeRepository.getBuStatCodeGroups("12");
        assertThat(list.size()).isGreaterThan(0);
    }


    @Test
    public void testGetBrokersStatCodeGroups() {
        List<StatCodeGroup> list = statCodeRepository.getBrokersStatCodeGroups();
        assertThat(list.size()).isGreaterThan(0);
    }

    @Test
    public void testGetStatCodes() {
        List<StatCode> list = statCodeRepository.getStatCodeGroupStatCodes("amaranteshi");
        assertThat(list.size()).isGreaterThan(0);
    }

}