package com.skuld.extranet.core.user;

import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.service.AzureAdGroupService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.cpbHodingsAzureDisplayName;
import static com.skuld.common.global.GlobalTestValues.cpbHodingsAzureGroupId;
import static com.skuld.extranet.GlobalForTest.azureGroupIdSkuldOslo2;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AzureAdGroupIntegrationTest {

    @Autowired
    private AzureAdGroupService service;

    @Test
    @DisplayName("Check if a group exists by ID")
    public void testGroupExists() {
        AzureAdGroup group = service.getGroupbyId(cpbHodingsAzureGroupId);
        assertThat(group.getGroupId()).isNotNull();
    }

    @Test
    @DisplayName("Check if a group exists by Name")
    public void testGroupNameExists() {
        AzureAdGroup group = service.getGroupByName(cpbHodingsAzureDisplayName);
        assertThat(group.getGroupId()).isNotNull();
    }

    @Test
    @DisplayName("List members of cpbhodin")
    public void testGetGroupMembers() {
        List<AzureAdUser> members = service.getGroupMemberWithSkuldsById(cpbHodingsAzureGroupId);
        assertThat(members.size()).isGreaterThan(0);
    }

    @Test
    @DisplayName("Filter @skuld.com users from group members")
    public void testGroupMemberExcudeSkuld() {
        List<AzureAdUser> members = service.getGroupMembersById(azureGroupIdSkuldOslo2);
        assertThat(members.size()).isEqualTo(0);
    }
}
