package com.skuld.extranet.core.user.service;

import com.skuld.common.domain.YesNo;
import com.skuld.common.dto.InitializationGroupDTO;
import com.skuld.common.dto.InitializationStatCodeDTO;
import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.user.domain.AzureAdUser;
import com.skuld.extranet.core.user.domain.CreateUserEntity;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import com.skuld.extranet.core.user.repo.AzureAdRepo;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.skuld.common.domain.YesNo.No;
import static com.skuld.common.domain.YesNo.Yes;
import static com.skuld.common.global.GlobalTestValues.*;
import static com.skuld.common.stream.StreamSingletonCollector.toSingleton;
import static com.skuld.extranet.GlobalForTest.*;
import static com.skuld.extranet.global.Global.AZURE_USER_ADMIN_GROUP_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@Log4j2
@SpringBootTest
class AzureAdUserServiceTest {

    @Autowired
    private AzureAdUserService azureAdUserService;

    @Autowired
    private AzureAdRepo azureRepo;

    @Test
    @DisplayName("Check if user exist by id")
    public void testGetUserByhId() {
        AzureAdUser user = azureAdUserService.getUserById(azureUserTestId);
        assertThat(user.getUserId()).isNotNull();
    }

    @Test
    public void testGetStatGroupForLogin() {
        List<StatCodeDTO> list = azureAdUserService.getStatCodesForGroup("cpbhodin");
        for (StatCodeDTO statCode : list) {
            assertThat(statCode.getStatCode()).isNotNull();
        }
    }

    @Disabled
    @Test
    public void testSendMail() {
        azureRepo.setMemberMailUrl("http://localhost:8111/tia/membermail/?memberemail=");
        ResponseEntity<String> status = azureRepo.sendMailToMember("oddbjorn.lona@skuld.com");
        assertThat(status).isNotNull();
    }

    @Test
    public void testGetStatCodesForGroup() {
        List<StatCodeDTO> list = azureAdUserService.getStatCodesForGroup("oslosyn1");
        assertTrue(list.size() > 0);
    }


    @DisplayName("Initialize user performance")
    @Test
    @WithMockCustomUser(token = authorizationApp_Dev, azureUserId = appDevAzureUserID)
    public void testInitializeUserPerformance() {
        StopWatch stopWatch = StopWatch.createStarted();
        azureAdUserService.initializeUser();
        stopWatch.stop();
        log.debug("Initialize user: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 2000);
    }

    @WithMockCustomUser(applicationRole = testClaimsDefaultRole)
    @Test
    void new_claims_available_for_claims_default_role() {
        List<InitializationGroupDTO> initializationGroupDTOS = azureAdUserService.initializeUser();
        initializationGroupDTOS.get(0).getStatCodes().stream().allMatch(x -> x.getClaimsOnline().equals(Yes));

        assertNotNull(initializationGroupDTOS);
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    public void include_azure_only_groups_in_token() {
        List<InitializationGroupDTO> userGroups = azureAdUserService.initializeUser();
        assertThat(userGroups.stream().anyMatch(group -> group.getGroupId().equalsIgnoreCase(AZURE_USER_ADMIN_GROUP_ID))).isTrue();
    }


    @WithMockCustomUser(azureGroupId = "africaex")
    @Test
    void africa_express_line_has_access_to_declarations() {
        List<InitializationGroupDTO> azureAdGroups = azureAdUserService.initializeUser();
        InitializationGroupDTO cpbHodinGroup = azureAdGroups.stream().filter(group -> cpbHodingsAzureDisplayName.equalsIgnoreCase(group.getDisplayName())).collect(toSingleton());
        InitializationStatCodeDTO africaStatCode = cpbHodinGroup.getStatCodes().stream().filter(statCode -> africaExpressStatCode.equalsIgnoreCase(statCode.getStatCode())).collect(toSingleton());

        assertTrue(YesNo.toBoolean(africaStatCode.getDeclarations()));
    }

    @Disabled //Crewing manager does not have any stat codes yet
    @Test
    @WithMockCustomUser(token = tokenWithCrewingManager)
    void user_with_crewing_manager_role_does_not_have_access_to_vessels_and_statistics() {
        List<InitializationGroupDTO> azureAdGroups = azureAdUserService.initializeUser();
        List<InitializationStatCodeDTO> statCodeDTOS = azureAdGroups.stream()
                .map(InitializationGroupDTO::getStatCodes).collect(Collectors.toList())
                .stream().flatMap(List::stream).distinct().collect(Collectors.toList());
        assertTrue(statCodeDTOS.stream().allMatch(x -> No == x.getVessels()));
        assertTrue(statCodeDTOS.stream().allMatch(x -> No == x.getDeclarations()));
        assertTrue(statCodeDTOS.stream().allMatch(x -> No == x.getFinancials()));
        assertTrue(statCodeDTOS.stream().anyMatch(x -> Yes == x.getClaimsOnline()));
    }

    @Disabled
    @DisplayName("Test of sending mail when user is being created (Mocking Azure Create User)")
    @Test
    public void testSendMailWhenUserCreated() {
        azureRepo = mock(AzureAdRepo.class);
        when(azureRepo.createAzureAdUser(any(), any(), any())).thenReturn(new AzureAdUser());
        when(azureRepo.addUserToGroup(any(), any())).thenReturn("");

        azureRepo.setMailCreateUserUrl("http://localhost:8111/extranet/mailcreateuser/?useremail=#usermail#&recipient=#recipient#");
        CreateUserEntity user = new CreateUserEntity();
        user.setConfirmationMailToUser(false);
        user.setEmail("oddbjorn_lona@hotmail.com");
        user.setRecipients(Arrays.asList("oddbjorn.lona@skuld.com"));

        AzureAdUser userWithMail = azureAdUserService.createUserWithMailNotification(user);
        assertThat(userWithMail).isNotNull();
    }
}

