package com.skuld.extranet.core.user.service;

import com.skuld.extranet.core.user.domain.AzureAdUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class AzureAdGroupServiceTest {

    @Autowired
    private AzureAdGroupService service;

    @Test
    public void testGetGroupMembersHideSkuld() {
        List<AzureAdUser> list = service.getGroupMembersById("7a2bcd33-c2a9-46e2-8478-5094e808a252");//Oslo1, only @skuld.com users
        assertThat(list.size()).isEqualTo(0);
    }

    @Test
    public void testGetGroupMembersShowSkuld() {
        List<AzureAdUser> list = service.getGroupMemberWithSkuldsById("7a2bcd33-c2a9-46e2-8478-5094e808a252");//Oslo1, only @skuld.com users
        assertThat(list.size()).isGreaterThan(0);
    }

}