package com.skuld.extranet.core.user;

import com.skuld.extranet.core.user.domain.TiaUser;
import com.skuld.extranet.core.user.service.TiaUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;

@SpringBootTest
class UserServiceIntegrationTest {

    @Autowired
    private TiaUserService service;
    List<TiaUser> tiaUsers = null;


    @Test
    public void testListOfUsers() {
        tiaUsers = service.tiaUsers();
        assertThat(tiaUsers, is(not(empty())));
    }

    @Test
    public void testFindHandler() {
        TiaUser tiaUser = service.getUser("ODL");
        assertUserHasValuesForOddbjorn(tiaUser);
    }

    @Test
    public void testFindPtUser() {
        TiaUser tiaUser = service.getUser("pt184");
        assertUserHasValuesForOddbjorn(tiaUser);
    }

    @Test
    public void testFindPtUserWithMail() {
        TiaUser tiaUser = service.getUser("pt184@skuld.com");
        assertUserHasValuesForOddbjorn(tiaUser);
    }

    private void assertUserHasValuesForOddbjorn(TiaUser tiaUser) {

        assertThat(tiaUser, is(notNullValue()));
        assertThat(tiaUser.getPtUserId(), equalToIgnoringCase("PT184"));
        assertThat(tiaUser.getTiaUserName(), equalToIgnoringCase("ODL"));
        assertThat(tiaUser.getEmailWithUserId(), equalToIgnoringCase("pt184@skuld.com"));
        assertThat(tiaUser.getEmail(), equalToIgnoringCase("oddbjorn.lona@skuld.com"));
    }


}
