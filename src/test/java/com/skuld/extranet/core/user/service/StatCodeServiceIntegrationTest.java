package com.skuld.extranet.core.user.service;

import com.skuld.extranet.core.financial.service.FinancialService;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.cpbHodingsAzureDisplayName;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class StatCodeServiceIntegrationTest {

    private final StatCodeService statCodeService;

    @Autowired
    public StatCodeServiceIntegrationTest(AzureAdGroupService azureAdGroupService,
                                          StatCodeService statCodeService,
                                          FinancialService financialService) {
        this.statCodeService = statCodeService;
    }


    @Test
    void get_stat_codes_for_group(){
        List<StatCodeDTO> mersrivie = statCodeService.getStatCodesForGroupCached(cpbHodingsAzureDisplayName);
        assertThat(mersrivie.size()).isGreaterThan(0);
    }

    @Test
    @DisplayName("Is group broker or Bu")
    public void testIsBrokerOrBusinessUnit() {
        boolean skuldBu = statCodeService.isSkuldBusinessUnit("oslosyn1");
        assertThat(skuldBu).isTrue();

        boolean broker = statCodeService.isBroker("24vision");
        assertThat(broker).isTrue();

    }

}
