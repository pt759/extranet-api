package com.skuld.extranet.core.creator;

import com.skuld.extranet.core.common.repo.DBDwhRepo;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.service.AzureAdGroupService;
import com.skuld.extranet.core.user.service.AzureAdUserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@SpringBootTest
class AzureServiceCreator extends DBDwhRepo {

    @Autowired
    private AzureAdGroupService azureAdGroupService;

    @Autowired
    private AzureAdUserService azureAdUserService;

    List<MemberMail> memberMails;

    public static void main(String[] args) {
        AzureServiceCreator creator = new AzureServiceCreator();
        creator.run();
    }

    private void run() {
        getEmailsFromDB();
        createGroupsWithEmails();

    }

    private void createGroupsWithEmails() {
        List<StatCodeGroupLogin> statCodeGroupLogins = getJdbi().withExtension(AzureServiceDao.class, AzureServiceDao::getStatCodeGroupLogins);

        for (StatCodeGroupLogin dbGroup : statCodeGroupLogins) {

            AzureAdGroup group = azureAdGroupService.getGroupByName(dbGroup.getStatCodeGroupLogin());

            if (group == null) {
                log.info("Create group" + dbGroup.getStatCodeGroupLogin());
                //group = aadGroupService.createGroup(items.getStatCodeGroupLogin(), items.getLogin_desc());
            } else {
                log.info("Group already exist" + group.getName());
            }

            if (group != null && group.getGroupId() != null) {
                List<MemberMail> memberEmailsFor = findMemberEmailsFor(dbGroup.getStatCodeGroupLogin());
                for (MemberMail memberMail : memberEmailsFor) {
                    log.info("Adding email:" + memberMail.getEmail());
                    //aadUserService.createUser(memberMail.getName_first(), memberMail.getName_family(), memberMail.getEmail(), group.getId());
                }
            }
        }
    }


    private List<MemberMail> findMemberEmailsFor(String statCodeGroupLogin) {
        return memberMails.stream().filter(line -> line.getLogin().equalsIgnoreCase(statCodeGroupLogin)).collect(Collectors.toList());
    }

    private void getEmailsFromDB() {
        memberMails = getJdbi().withExtension(AzureServiceDao.class, AzureServiceDao::getMembers);
        log.info("Member Mail size: " + memberMails.size());
    }


}