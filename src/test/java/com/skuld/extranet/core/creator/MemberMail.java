package com.skuld.extranet.core.creator;


import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MemberMail {
  private String line_id;
  private String login;
  private String name_first;
  private String name_family;
  private String email;
  private String excelcol;

  public MemberMail(ResultSet r) throws SQLException {
    this.line_id = r.getString("line_id");
    this.login = r.getString("login");
    this.name_first = r.getString("name_first");
    this.name_family = r.getString("name_family");
    this.email = r.getString("email");
    this.excelcol = r.getString("excelcol");
  }

  public static class Mapper implements RowMapper<MemberMail> {
    public MemberMail map(ResultSet r, StatementContext ctx) throws SQLException {
      return new MemberMail(r);
    }
  }

  public String getLine_id() {
    return line_id;
  }

  public void setLine_id(String line_id) {
    this.line_id = line_id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getName_first() {
    return name_first;
  }

  public void setName_first(String name_first) {
    this.name_first = name_first;
  }

  public String getName_family() {
    return name_family;
  }

  public void setName_family(String name_family) {
    this.name_family = name_family;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getExcelcol() {
    return excelcol;
  }

  public void setExcelcol(String excelcol) {
    this.excelcol = excelcol;
  }
}
