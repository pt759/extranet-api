package com.skuld.extranet.core.creator;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface AzureServiceDao {


    @SqlQuery("select * from list_email order by login, name_first")
    @RegisterRowMapper(MemberMail.Mapper.class)
    List<MemberMail> getMembers();


    @SqlQuery("select * from list_broker order by login")
    @RegisterRowMapper(StatCodeGroupLogin.Mapper.class)
    List<StatCodeGroupLogin> getStatCodeGroupLogins();

}
