package com.skuld.extranet.core.creator;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StatCodeGroupLogin {
    private String line_id;
    private String login_desc;
    private String login;

    public StatCodeGroupLogin(ResultSet r) throws SQLException {

        this.login_desc = r.getString("login_desc");
        this.login = r.getString("login");
    }

    public static class Mapper implements RowMapper<StatCodeGroupLogin> {
        public StatCodeGroupLogin map(ResultSet r, StatementContext ctx) throws SQLException {
            return new StatCodeGroupLogin(r);
        }
    }

    public String getLine_id() {
        return line_id;
    }

    public void setLine_id(String line_id) {
        this.line_id = line_id;
    }

    public String getLogin_desc() {
        return login_desc;
    }

    public void setLogin_desc(String login_desc) {
        this.login_desc = login_desc;
    }

    public String getStatCodeGroupLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
