package com.skuld.extranet.core.common;

import com.skuld.extranet.core.common.domain.CodeValue;

import java.util.ArrayList;
import java.util.List;

public class CodeValueTestData {


    public static List<CodeValue> claimTypeAndValue(){
        List<CodeValue> list = new ArrayList<>();

//        list.add(new CodeValue("totalClaims", "totalClaims","37"));
        list.add(new CodeValue("HUM", "18","Human accident/illness" ));
        list.add(new CodeValue("FDD", "10","FD&D " ));
        list.add(new CodeValue("SBI", "2","Smuggling/Breach of Immigration or Customs Law" ));
        list.add(new CodeValue("CAR", "2","Cargo" ));
        list.add(new CodeValue("COL", "2","Collision with other vessel" ));
        list.add(new CodeValue("EQP", "1","Machinery or equipment damage/failure" ));
        list.add(new CodeValue("CWD", "1","Contact with quays, docks, dolphins" ));
        list.add(new CodeValue("SOS", "1","Spill of other substances" ));
        return list;
    }
}
