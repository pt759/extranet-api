package com.skuld.extranet.core.common;

import com.google.common.collect.Iterables;
import com.skuld.extranet.core.common.domain.CodeValue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CodeValueTest {

    @DisplayName("Should organize the list in groups")
    @Test
    public void shouldOrganiseListInGroups() {

        List<CodeValue> list = CodeValueTestData.claimTypeAndValue();
        int limit = 4;

        list = CodeValue.groupListWithLimitOf(limit, list);
        assertThat(list.size()).isEqualTo(4);

        CodeValue limitCodeValue = Iterables.getLast(list);
        assertThat(limitCodeValue.getValue()).isEqualTo("7");
        assertThat(limitCodeValue.getCode()).isEqualTo("OTHERS");

    }

    @DisplayName("Should organize the list in groups,without going into nullpointer")
    @Test
    public void shouldOrganiseListInGroupsWithoutGoingintoError() {

        List<CodeValue> beforelist = CodeValueTestData.claimTypeAndValue();

        int limit = 40;
        List<CodeValue> afterlist = CodeValue.groupListWithLimitOf(limit, beforelist);

        assertThat(Iterables.getLast(afterlist)).isEqualTo(Iterables.getLast(beforelist));

    }


    @DisplayName("Should organize the list in groups")
    @Test
    public void shouldOrganiseListInGroupsWithLimit6() {

        List<CodeValue> list = CodeValueTestData.claimTypeAndValue();
        int limitValue = 6;
        list = CodeValue.groupListWithLimitOf(limitValue, list);
        assertThat(list.size()).isEqualTo(6);

        assertThat(list.get(0).getValue()).isEqualTo("18");

        CodeValue limitCodeValue = Iterables.getLast(list);
        assertThat(limitCodeValue.getValue()).isEqualTo("3");
        assertThat(limitCodeValue.getCode()).isEqualTo("OTHERS");
    }


}
