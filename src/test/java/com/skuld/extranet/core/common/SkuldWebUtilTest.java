package com.skuld.extranet.core.common;

import com.skuld.extranet.ExtranetApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ExtranetApplication.class)
class SkuldWebUtilTest {

    @Test
    public void testDecodeSpaces() {
        String s = SkuldWebUtil.urlDecode("A%20B");
        assertThat(s).isEqualTo("A B");
        s = SkuldWebUtil.urlDecode("A+B");
        assertThat(s).isEqualTo("A B");
    }

    @Test
    public void testEncodeSpacesWith_20() {
        String s = SkuldWebUtil.urlEncode("A B");
        assertThat(s).isEqualTo("A+B");

    }


}