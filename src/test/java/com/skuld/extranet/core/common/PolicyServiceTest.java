package com.skuld.extranet.core.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PolicyServiceTest {


    PolicyService service;

    @BeforeEach
    public void setup() {
        service = new PolicyService();
    }

    @Test
    public void testCurrentpolicyYearAsString() {
        String s = service.currentPolicyYearAsString();
        assertThat(s).isEqualTo(String.valueOf(LocalDate.now().getYear()));
    }

    @Test
    public void testpolicyYearAsStringFor2016() {

        String policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2016-01-01"));
        assertThat(policyYearAsString2016).isNotEqualTo("2016").isEqualTo("2015");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2016-01-16"));
        assertThat(policyYearAsString2016).isNotEqualTo("2016").isEqualTo("2015");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2016-02-20"));
        assertThat(policyYearAsString2016).isNotEqualTo("2016").isEqualTo("2015");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2016-02-21"));
        assertThat(policyYearAsString2016).isEqualTo("2016");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2016-06-20"));
        assertThat(policyYearAsString2016).isEqualTo("2016");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2017-01-01"));
        assertThat(policyYearAsString2016).isEqualTo("2016");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2017-02-20"));
        assertThat(policyYearAsString2016).isEqualTo("2016");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2017-02-21"));
        assertThat(policyYearAsString2016).isNotEqualTo("2016").isEqualTo("2017");

        policyYearAsString2016 = service.policyYearAsString(LocalDate.parse("2017-02-21"));
        assertThat(policyYearAsString2016).isNotEqualTo("2016").isEqualTo("2017");

    }


    @Test
    public void testValidFiveYear() {

        service.setCurrentPolicyYear(2017);
        List<String> validFiveYears = Arrays.asList("2012", "2013", "2014", "2015", "2016");
        List<String> notValidFiveYears = Arrays.asList("2010", "2011", "2018");


        for (String year : validFiveYears) {
            boolean valid = service.isValidFiveYearPolicyYear(year);
            assertThat(valid).isTrue();
        }

        for (String year: notValidFiveYears) {
            boolean notValid = service.isValidFiveYearPolicyYear(year);
            assertThat(notValid).as(year).isFalse();
        }
    }

}