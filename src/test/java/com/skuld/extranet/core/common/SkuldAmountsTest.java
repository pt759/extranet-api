package com.skuld.extranet.core.common;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

class SkuldAmountsTest {

    @Test
    public void testRound(){
        double round = SkuldAmounts.round(new Double(100.2999), 2);
        assertThat(round).isEqualTo(100.30);

        round = SkuldAmounts.round(new Double(100.00), 2);
        System.out.println(round);
        assertThat(round).isEqualTo(100.00);

    }

}