package com.skuld.extranet.core.common;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class SkuldFormatterTest {

    @Test
    public void formatWithThousandSeparatorNoDecimals(){
        BigDecimal thisNumber = new BigDecimal("12000.00");

        String txtNumber = SkuldFormatter.formatBigDecimalWithThousandsSeparator(thisNumber);
        assertThat(txtNumber).isEqualToIgnoringCase("12,000");

        thisNumber = new BigDecimal("12000");
        txtNumber = SkuldFormatter.formatBigDecimalWithThousandsSeparator(thisNumber);
        assertThat(txtNumber).isEqualToIgnoringCase("12,000");

        thisNumber = new BigDecimal("1234");
        txtNumber = SkuldFormatter.formatBigDecimalWithThousandsSeparator(thisNumber);
        assertThat(txtNumber).isEqualToIgnoringCase("1,234");

        thisNumber = new BigDecimal("42");
        txtNumber = SkuldFormatter.formatBigDecimalWithThousandsSeparator(thisNumber);
        assertThat(txtNumber).isEqualToIgnoringCase("42");

    }

    @Test
    public void should_format_localDateTime_remove_time_aspect() {
        LocalDateTime timestamp = LocalDateTime.parse("2018-12-31T15:45:00");
        String s = SkuldFormatter.formatTimeStamp(timestamp);
        assertThat(s).isEqualToIgnoringCase("31.12.2018");
    }

    @Test
    public void should_format_localDate_with_correct_year() {
        LocalDate date = LocalDate.parse("2018-12-31");
        String s = SkuldFormatter.formatLocaldateFor(date);
        assertThat(s).isEqualToIgnoringCase("31.12.2018");
    }

    @Test
    public void should_format_localDate() {
        LocalDate localDate = LocalDate.parse("2020-05-02");
        String s = SkuldFormatter.formatLocaldateFor(localDate);
        assertThat(s).isEqualToIgnoringCase("02.05.2020");
    }

}