package com.skuld.extranet.core.claim.service;

import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Log4j2
@SpringBootTest
class ClaimsClientServiceIntegrationTest {

    @Autowired
    ClaimsClientService claimsClientService;

    @Test
    void get_number_of_openClaims_with_vessel() {
        ClientClaimsSummaryDTO openClaims = claimsClientService.getClaimOverviewSummary(africaExpressStatCode);
        assertTrue(openClaims.getVesselsWithClaim() > 1);
        assertTrue(openClaims.getOpenClaims() > 1);
    }

    @Test
    void call_claim_api_in_parallel() {
        List<String> statCodes = List.of(africaExpressStatCode, "14222.00", "22641.00", "14944.00", "7249.0");
        StopWatch stopWatch = StopWatch.createStarted();
        List<ClientClaimsSummaryDTO> overviewSummaryDTOS = new ArrayList<>();
        statCodes.parallelStream().forEach(
                statCode -> overviewSummaryDTOS.add(claimsClientService.getClaimOverviewSummary(statCode)));
        stopWatch.stop();

        log.debug("Getting claims for three stat codes: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 2000);

    }

}