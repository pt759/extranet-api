package com.skuld.extranet.core.claim.service;

import com.google.common.collect.Iterables;
import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.core.claim.domain.ClaimListFilter;
import com.skuld.extranet.core.claim.domain.EventStatus;
import com.skuld.extranet.core.claim.repo.ClaimDetailRepository;
import com.skuld.extranet.core.common.CodeValueTestData;
import com.skuld.extranet.core.common.domain.CodeValue;
import com.skuld.extranet.core.user.service.TiaUserService;
import com.skuld.extranet.ldap.LDAPService;
import com.skuld.extranet.ldap.domain.LDAPUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.skuld.common.global.GlobalTestValues.employeeToken;
import static com.skuld.extranet.core.claim.domain.ClaimDetailVBuildTestData.claim;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class ClaimServiceTest {

    @Autowired
    ClaimService claimService;

    @Autowired
    ClaimDetailRepository repoMock;

    @Autowired
    LDAPService ldapService;

    @Autowired
    TiaUserService tiaUserService;

    String statCode = "1234";

    @BeforeEach
    public void setup() {
        repoMock = mock(ClaimDetailRepository.class);
        claimService = new ClaimService(repoMock);
        ldapService = mock(LDAPService.class);

        when(ldapService.matchUserOnSaMAccountName(anyString())).thenReturn(new LDAPUser());
        claimService.setupLdapService(ldapService);
        claimService.setupTiaUserService(tiaUserService);
    }

    @Test
    @DisplayName("Should group and count Claims, with Others and Total")
    public void groupAndCountClaimsForCurrentYearWithManyCategories() {
        List<CodeValue> claimTypeAndValue = CodeValueTestData.claimTypeAndValue();
        when(repoMock.claimCountForCurrentYear(anyString())).thenReturn(claimTypeAndValue);

        int limit = 4;
        List<CodeValue> list = claimService.claimCountForCurrentYearWithGroupOthers("0781.3", limit);

        assertThat(list.size()).isEqualTo(limit + 1);
        assertThat(Integer.valueOf(list.get(0).getValue())).isEqualTo(18);
        assertThat(Integer.valueOf(list.get(1).getValue())).isEqualTo(10);
        assertThat(Integer.valueOf(list.get(2).getValue())).isEqualTo(2);
        assertThat(Integer.valueOf(list.get(3).getValue())).isEqualTo(7);// Others

        assertThat(Integer.valueOf(Iterables.getLast(list).getValue())).isEqualTo(37);// SUM
    }


    @Test
    @DisplayName("Should group and count Claims, with limit of 3")
    public void groupAndCountClaimsForCurrentYear() {
        List<CodeValue> claimTypeAndValue = CodeValueTestData.claimTypeAndValue();
        when(repoMock.claimCountForCurrentYear(anyString())).thenReturn(claimTypeAndValue);

        int limit = 3;
        List<CodeValue> list = claimService.claimCountForCurrentYearWithGroupOthers("0331.01", limit);

        assertThat(list.size()).isEqualTo(limit + 1);
        assertThat(Integer.valueOf(list.get(0).getValue())).isEqualTo(18);
        assertThat(Integer.valueOf(list.get(1).getValue())).isEqualTo(10);
        assertThat(Integer.valueOf(list.get(2).getValue())).isEqualTo(9);
        assertThat(Integer.valueOf(Iterables.getLast(list).getValue())).isEqualTo(37);
    }


    @Test
    @DisplayName("Filter Claim list")
    @WithMockCustomUser(token = employeeToken)
    public void shouldFilterListOnCaseTypeAndStatus() {

        List<Claim> list = new ArrayList<>();
        list.add(claim("7498.0", "2015", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2015", "SUR", "Open", "P & I Mutual", "MSE1"));

        list.add(claim("7498.0", "2016", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "SUR", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "FDD", "Closed", "P & I Mutual", "MSE1"));

        list.add(claim("7498.0", "2017", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "HUM", "Closed", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "FDD", "Closed", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "SUR", "Open", "P & I Mutual", "MSE1"));

        when(repoMock.listOfClaims(anyString(), any(), any())).thenReturn(list);

        ClaimListFilter filter = new ClaimListFilter();

        filter.statCode("7498.0")
                .years(Arrays.asList("2016", "2017"))
                .caseType(Arrays.asList("HUM", "SUR"))
                .status(Arrays.asList(EventStatus.open.toString()))
                .setProduct(Arrays.asList("P & I Mutual"))
                .setPageSize(5)        ;

        List<Claim> beforeFilter = claimService.claimListAll(filter.getStatCode());
        int listSize = beforeFilter.size();

        List<Claim> afterFilter = claimService.filterClaims(beforeFilter, filter);

        assertThat(afterFilter.size()).isLessThan(listSize).isEqualTo(5);

        assertThat(beforeFilter)
                .extracting("status")
                .contains("Open")
                .contains("Closed");

        assertThat(afterFilter)
                .extracting("status")
                .contains("Open")
                .doesNotContain("Closed");

        assertThat(beforeFilter)
                .extracting("claimsType")
                .contains("FDD");

        assertThat(afterFilter)
                .extracting("claimsType")
                .contains("HUM")
                .contains("SUR")
                .doesNotContain("FDD");

    }


    @DisplayName("Claim Estimates calculation")
    @Test
    public void shouldCalculateClaimEstimates() {

        ClaimEstimates estimates1 = new ClaimEstimates().setItemGroup("A").setPayments(1500L).setReserves(100L).setTotal(200L);
        ClaimEstimates estimates2 = new ClaimEstimates().setItemGroup("B").setPayments(-100L).setReserves(100L).setTotal(200L);
        ClaimEstimates estimates3 = new ClaimEstimates().setItemGroup("C").setPayments(-100L).setReserves(100L).setTotal(200L);

        List<ClaimEstimates> estimates = new ArrayList<>(Arrays.asList(estimates1, estimates2, estimates3));

        when(repoMock.estimates(anyString(), anyString())).thenReturn(estimates);

        final List<ClaimEstimates> estimateWithTotal = claimService.estimates(statCode, "20102482");
        assertThat(estimateWithTotal.size()).isEqualTo(4);

        ClaimEstimates last = Iterables.getLast(estimateWithTotal);
        assertThat(last.getItemTypeCode()).isEqualTo("TOTAL");
        assertThat(last.getPayments()).isEqualTo(1300L);
        assertThat(last.getReserves()).isEqualTo(300L);
        assertThat(last.getTotal()).isEqualTo(600L);

    }

    @DisplayName("This test is not really any good -  there is no logic testet!")
    @Test
    public void shouldOnlyGetOpenAndNoEventHandler() {
        List<Claim> list = new ArrayList<>();
        list.add(claim("7498.0", "2015", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2015", "SUR", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "SUR", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2016", "FDD", "Closed", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "HUM", "Open", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "HUM", "Closed", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "FDD", "Closed", "P & I Mutual", "MSE1"));
        list.add(claim("7498.0", "2017", "SUR", "Open", "P & I Mutual", "MSE1"));

        when(repoMock.listOfClaims(anyString(), any(), any())).thenReturn(list);

        List<Claim> claims = claimService.claimListOpenWithoutEventHandler("something");

        Claim claim = claims.get(0);
        assertThat(claim.getEventHandlerUser()).isNull();
        assertThat(claim.getEventHandlerLdapUser()).isNull();

    }

    @Test
    public void should_get_distinct_estimates() {
        String statCode = "1";
        String claEventNo = "1000";

        final List<ClaimEstimates> estimatesFromRepo = new ArrayList<>();
        estimatesFromRepo.add(ClaimEstimates.builder().claEventNo(claEventNo).itemTypeCode("C").itemType("A").itemGroup("G").payments(10L).reserves(10L).total(20L).build());
        estimatesFromRepo.add(ClaimEstimates.builder().claEventNo(claEventNo).itemTypeCode("C").itemType("A").itemGroup("G").payments(10L).reserves(10L).total(20L).build());
        estimatesFromRepo.add(ClaimEstimates.builder().claEventNo(claEventNo).itemTypeCode("C").itemType("B").itemGroup("G").payments(10L).reserves(10L).total(20L).build());

        when(repoMock.estimates(statCode, claEventNo)).thenReturn(estimatesFromRepo);
        final List<ClaimEstimates> estimates = claimService.estimates(statCode, claEventNo);
        assertThat(estimates).hasSize(3);
    }
}
