package com.skuld.extranet.core.claim.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

class ClaimTest {

    @Test
    public void testDateformatterYearMonthDay1() {
        LocalDate localDate = LocalDate.parse("2017-09-23");
        Claim claim = Claim.builder().incidentDate(localDate).build();
        assertThat(claim.getIncidentDateAsString(), is(equalTo("23.09.2017")));
    }

    @Test
    public void testDateformatterYearMonthDay2() {
        LocalDate localDate = LocalDate.parse("2017-09-23");
        Claim claim = Claim.builder().incidentDate(localDate).build();
        assertThat(claim.getIncidentDateAsString(), is(equalTo("23.09.2017")));
    }

}