package com.skuld.extranet.core.claim.domain;

public class ClaimDetailVBuildTestData {


    public static Claim claim(String statCode, String statYear, String caseType, String status, String product, String  handler) {

        Claim claim = Claim.builder().statCode(statCode)
                .statYear(statYear)
                .claimsType(caseType)
                .status(status)
                .product(product)
                .eventHandler(handler)
                .build();
        return claim;
    }


}
