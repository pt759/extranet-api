package com.skuld.extranet.core.claim.service;

import com.skuld.extranet.core.claim.client.ClaimsClient;
import com.skuld.extranet.core.claim.dto.ClientClaimsSummaryDTO;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Log4j2
class ClaimsClientServiceTest {

    private ClaimsClientService claimsClientService;
    private ClaimsClient claimsClient;

    @BeforeEach
    void setUp() {
        claimsClient = mock(ClaimsClient.class);
        claimsClientService = new ClaimsClientService(claimsClient);
    }

    @Test
    void requests_run_in_parallel() {
        List<String> statCodes = List.of(africaExpressStatCode, "14222.00", "22641.00", "14944.00", "7249.0");
        when(claimsClient.getClaimOverviewSummary(any())).thenReturn(ClientClaimsSummaryDTO.builder().build());
        StopWatch stopWatch = StopWatch.createStarted();

        statCodes.forEach(statCode -> {
            log.debug(statCode);
            claimsClientService.getClaimOverviewSummary(statCode);
        });

        stopWatch.stop();
        log.debug("Execution time: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 100);


    }

}