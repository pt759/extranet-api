package com.skuld.extranet.core.claim.domain;

import com.skuld.extranet.spa.pages.domain.Product;

import java.util.ArrayList;
import java.util.List;

import static com.skuld.extranet.core.claim.domain.ClaimDetailVBuildTestData.claim;

public class ClaimDetailTestData {

    public static List<Claim> getlist() {
        List<Claim> list = new ArrayList<>();
        list.add(claim("7498.0", "2015", "HUM", "Open", Product.owner.toString(), "MSE1"));
        list.add(claim("7498.0", "2015", "SUR", "Open", Product.owner.toString(), "MSE1"));
        list.add(claim("7498.0", "2016", "HUM", "Open", Product.charter.toString(), "MSE1"));
        list.add(claim("7498.0", "2016", "HUM", "Open", Product.charter.toString(), "MSE1"));
        list.add(claim("7498.0", "2016", "SUR", "Open", Product.charter.toString(), "MSE1"));
        list.add(claim("7498.0", "2016", "FDD", "Closed", Product.charter.toString(), "MSE1"));
        list.add(claim("7498.0", "2017", "HUM", "Open", Product.fixedOwner.toString(), "MSE1"));
        list.add(claim("7498.0", "2017", "HUM", "Closed", Product.fixedOwner.toString(), "MSE1"));
        list.add(claim("7498.0", "2017", "FDD", "Closed", Product.fixedOwner.toString(), "MSE1"));
        list.add(claim("7498.0", "2017", "SUR", "Open", Product.fixedOwner.toString(), "MSE1"));
        return list;
    }
}
