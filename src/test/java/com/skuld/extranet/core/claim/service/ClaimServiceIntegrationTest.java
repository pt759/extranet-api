package com.skuld.extranet.core.claim.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.claim.domain.Claim;
import com.skuld.extranet.core.claim.domain.ClaimEstimates;
import com.skuld.extranet.core.claim.domain.ClaimListFilter;
import com.skuld.extranet.core.claim.domain.Views;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static com.skuld.common.global.GlobalTestValues.employeeToken;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class ClaimServiceIntegrationTest {

    @Autowired
    private ClaimService service;
    private final String statCode = "1692.1";

    @DisplayName("Should set Case Handler")
    @Test
    public void shouldGetCaseHandlerForCase() {

        Claim item = service.claimEvent(statCode, "40134329");
        assertThat(item.getEventHandlerMail()).isNotNull();
    }

    @DisplayName("Simple test to see if get something from estimates")
    @Test
    public void testClaimEstimates() {
        List<ClaimEstimates> estimates = service.estimates(statCode, "40134034");
        assertThat(estimates.size()).isEqualTo(1);
    }

    @Disabled
    @DisplayName("Should only get 6+1 rows")
    @Test
    public void test_for_restapi_859() {
        List<ClaimEstimates> estimates = service.estimates("7505.0", "40165328");
        assertThat(estimates.size()).isEqualTo(7);
    }

    @DisplayName("Claim Estimates calculation")
    @Test
    public void shouldCalculateClaimEstimates() {
        List<ClaimEstimates> estimates = service.estimates(statCode, "20102482");
        assertThat(estimates.size()).isGreaterThan(0);
    }


    @DisplayName("Simple test to see if get something from an event and event has Estimates and Vessel information")
    @Test
    public void testClaimEvent() {

        Claim claimEvent = service.claimEvent(africaExpressStatCode, "40134034");
        assertThat(claimEvent.getVesselName()).isEqualTo("LADY ROSEMARY");
    }


    @Disabled
    @DisplayName("Simple test to see if get something from an event and event has Estimates and Vessel information")
    @Test
    public void testClaimEventMany() {
        Claim claimEvent = service.claimEvent(statCode, "19505245");
        assertThat(claimEvent.getVesselName()).isEqualTo("LADY ROSEMARY");
    }


    @Disabled
    @DisplayName("Should find vessel for Hebei Spirit Claim")
    @Test
    public void testClaimEventHebeiSpirit() {
        Claim claim = service.claimEvent(statCode, "40041155");
        assertThat(claim.getVesselName()).isEqualTo("HEBEI SPIRIT");
        assertThat(claim.getImoNumber()).isEqualTo("9034640");
    }

    @DisplayName("Should not return exception")
    @Test
    public void testClaimEvent40097341() {
        List<Claim> claim = service.claimListAll("-1");
        assertThat(claim).isNotNull();
        assertThat(claim.size()).isEqualTo(0);
    }

    @Test
    @DisplayName("Should Filter on VesselName Only")
    @WithMockCustomUser(token = employeeToken)
    public void shouldFilterOnlyVessel() {
        ClaimListFilter filter = new ClaimListFilter();

        filter.statCode("10926.0")
                .setVesselName("OSI")
        ;
        List<Claim> beforeFilter = service.claimListAll(filter.getStatCode());

        List<Claim> afterFilter = service.filterClaims(beforeFilter, filter);

        assertThat(beforeFilter)
                .extracting("vesselName")
                .contains("OSI")
                .contains("DMS VENTURE");

        assertThat(afterFilter)
                .extracting("vesselName")
                .contains("OSI")
                .doesNotContain("DMS VENTURE");

    }


    @Test
    @DisplayName("Should Filter on CaseNumber only")
    @WithMockCustomUser(token = employeeToken)
    public void shouldFilterOnlyCaseNumber() {
        ClaimListFilter filter = new ClaimListFilter();

        String caseNumber = "40135215";
        filter.statCode("0331.01")
                .setCaseNumber(caseNumber)
        ;

        List<Claim> beforeFilter = service.claimListAll(filter.getStatCode());
        List<Claim> afterFilter = service.filterClaims(beforeFilter, filter);

        assertThat(afterFilter.size()).isEqualTo(1);
        assertThat(afterFilter.get(0).getClaEventNo()).isEqualTo(caseNumber);

    }

    @Test
    @DisplayName("Filter Claim list with * ")
    @WithMockCustomUser(token = employeeToken)
    public void shouldFilterListOnStatusWithStarForOtherOptions() {
        ClaimListFilter filter = new ClaimListFilter();

        filter.statCode("7498.0")
                .years(Arrays.asList("*"))
                .caseType(Arrays.asList("*"))
                .status(Arrays.asList("Open"))
                .setProduct(Arrays.asList("*"))
                .setPage(0)
                .setPageSize(5);

        List<Claim> beforeFilter = service.claimListAll(filter.getStatCode());

        List<Claim> afterFilter = service.filterClaims(beforeFilter, filter);

        assertThat(afterFilter.size()).isEqualTo(5);

        assertThat(afterFilter)
                .extracting("status")
                .containsOnly("Open");
    }

    @DisplayName("Get only the 10 <limit parameter> latest claims, in sorted order")
    @Test
    public void testClaimLatest() {
        int limit = 10;
        List<Claim> list = service.recentClaimsForStatCode("0331.01", "Open", limit);
        assertThat(list.size()).isEqualTo(limit);
        assertThat(list.get(0).getIncidentDate()).isAfterOrEqualTo(list.get(1).getIncidentDate());
        assertThat(list.get(1).getIncidentDate()).isAfterOrEqualTo(list.get(2).getIncidentDate());
    }

    @DisplayName("Verify fields for Latest View")
    @Test
    public void testClaimLatestView() throws JsonProcessingException {
        List<Claim> list = service.recentClaimsForStatCode("0331.01", "Open", 1);

        Claim claim = list.get(0);

        ObjectMapper mapper = new ObjectMapper();
        String result = mapper
                .writerWithView(Views.ClaimLatest.class)
                .writeValueAsString(claim);

        //Fields included
        claimLatestViewIncludeTheseFields(result);
        // Does not include
        assertThat(result).doesNotContain("description");
        assertThat(result).doesNotContain("product");
        //TODO: should not be here...        assertThat(result, not(containsString("jurisdiction")));
        //        assertThat(result, not(containsString("vesselType")));

    }

    private void claimLatestViewIncludeTheseFields(String result) {
//        assertThat(result, containsString("caseNumber"));
//        assertThat(result, containsString("vesselName"));
//        assertThat(result, containsString("caseType"));
//        assertThat(result, containsString("incidentDate"));

        // TODO: ClientName
//        assertThat(result, containsString("clientName"));
//        assertThat(result, (containsString(SkCommon.todo)));
    }

    private void assertClaimListViewFields(String result) {
//        claimLatestViewIncludeTheseFields(result);
//        assertThat(result, containsString("description"));
        // Does not include

    }

    @DisplayName("Verify fields for Claim List View")
    @Test
    @WithMockCustomUser(token = employeeToken)
    public void testClaimListView() throws JsonProcessingException {

        List<Claim> list = service.claimListAll("0331.01");

        Claim claim = list.get(0);

        ObjectMapper mapper = new ObjectMapper();
        String result = mapper
                .writerWithView(Views.ClaimList.class)
                .writeValueAsString(claim);

        //Fields included
        assertClaimListViewFields(result);
    }


    @DisplayName("Verify Pagination for Claim List View")
    @WithMockCustomUser(token = employeeToken)
    @Test
    public void testClaimListViewWithPage() throws JsonProcessingException {
        List<Claim> list = service.claimListAllWithPage("0331.01", 0, 10);
        assertThat(list.size()).isEqualTo(10);
        Claim item1 = Iterables.getLast(list);

        list = service.claimListAllWithPage("0331.01", 9, 1);
        assertThat(list.size()).isEqualTo(1);
        Claim item2 = list.get(0);

        assertThat(item1.getImoNumber()).isEqualTo(item2.getImoNumber());
    }


    @DisplayName("Get a list of Claim.")
    @Test
    @WithMockCustomUser(token = employeeToken)
    public void testClaimList() {
        StopWatch watch = new StopWatch();
        watch.start();

        List<Claim> list = service.claimListAll("0331.01");
        watch.stop();
        System.out.println("Total time: " + watch.getTotalTimeMillis());
        assertThat(list.size()).isGreaterThan(5);
    }





}
