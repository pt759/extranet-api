package com.skuld.extranet.core.vessel.domain;


public class VesselTestData {

    public static Vessel vessel(String statcode, String products, String status) {
        return new Vessel().setStatCode(statcode)
                .setProducts(products)
                .setStatus(status);
    }
}
