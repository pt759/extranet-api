package com.skuld.extranet.core.vessel;

import com.skuld.common.domain.Base64Container;
import com.skuld.extranet.core.common.SkuldFileUtil;
import com.skuld.extranet.core.vessel.domain.GrossPremium;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselPolicyLine;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.africaExpressStatCode;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class VesselServiceIntegrationTest {

    @Autowired
    VesselService service;

    @Test
    @DisplayName("Return list of VesselSearch for Statcode")
    public void vesselListTest() {
        List<Vessel> vessels = service.vesselsFor("7669.0");
        assertThat(vessels.size()).isGreaterThan(1);
    }

    @Disabled //TODO RESTAPI-686
    @Test
    public void testFindVesselPolicyLine() {
        Vessel vessel = new Vessel();
        vessel.setVesselId("174177").setStatCode("35202.00");
        List<VesselPolicyLine> list = service.findVesselPolicyLine(vessel);
        assertThat(list.size()).isGreaterThan(0);

    }

    @Test
    public void testGrossPremium() {
        List<GrossPremium> deductibles = service.grossPremiumsFor("10445", "6287.0");
        assertThat(deductibles).isNotNull();
        assertThat(deductibles.size()).isGreaterThan(0);
    }

    @Test
    public void downloadLinkAsPdfTest() {
        Base64Container data = service.getVesselExcelConverterBase64(africaExpressStatCode);
        assertThat(data).isNotNull();
        String tmpFilename = "c:/temp/" + data.getFilename();
        SkuldFileUtil.writeBase64ToFile(data.getData(), tmpFilename);
        System.out.println(tmpFilename);
    }


}