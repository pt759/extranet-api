package com.skuld.extranet.core.vessel;

import com.skuld.extranet.core.api.vessel.service.VesselClientService;
import com.skuld.extranet.core.common.SkCommon;
import com.skuld.extranet.core.vessel.domain.Vessel;
import com.skuld.extranet.core.vessel.domain.VesselFilter;
import com.skuld.extranet.core.vessel.domain.VesselTestData;
import com.skuld.extranet.core.vessel.repo.VesselRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
@Disabled
@Deprecated
/*
Move to VesselService, as this is only calling filter method
 */
class VesselServiceTest {

    VesselService service;
    VesselRepository vesselRepository = mock(VesselRepository.class);
    VesselClientService vesselClientService = mock(VesselClientService.class);

    @BeforeEach
    public void setup() {
        service = new VesselService(vesselRepository,vesselClientService);
    }

    @Test
    @DisplayName("Should Filter on Product Only")
    public void shouldFilterOnProduct() {

        List<Vessel> mockList = new ArrayList<>();

        mockList.add(VesselTestData.vessel("7669.0", "P&I", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "P&I", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "P&I", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "P&I", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "FD&D", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "FD&D", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "FD&D", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "X", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "X", SkCommon.todo));
        mockList.add(VesselTestData.vessel("7669.0", "Y", SkCommon.todo));
        when(vesselRepository.vesselsFor(anyString())).thenReturn(mockList);

        VesselFilter filter = new VesselFilter();

        filter.setStatCode("7669.0")
                .setProduct(Arrays.asList("P&I"))
                .setStatus(Arrays.asList(VesselFilter.all));

        List<Vessel> list = service.searchForVessel(filter);

        assertThat(list.size()).isEqualTo(4);

        assertThat(list)
                .extracting("products")
                .containsSequence(Arrays.asList("P&I"))
                .doesNotContain("FD&D")
        ;
    }

    @Test
    @DisplayName("Should Filter on VesselName Only")
    public void shouldFilterOnlyVessel() {
        String statCode = "7669.0";

        List<Vessel> vessels = new ArrayList<>();
        Vessel vessel1 = Vessel.builder().statCode(statCode).vesselName("BLUE YACHT").imo_number("123").build();
        Vessel vessel2 = Vessel.builder().statCode(statCode).vesselName("RED YACHT").imo_number("123").build();
        Vessel vessel3 = Vessel.builder().statCode(statCode).vesselName("GREEN YACHT").imo_number("123").build();
        vessels.addAll(Arrays.asList(vessel1, vessel2, vessel3));

        when(vesselRepository.vesselsFor(any())).thenReturn(vessels);

        VesselFilter filter = new VesselFilter();
        filter.setStatCode(statCode)
                .setVessel("GREEN YACHT");

        List<Vessel> list = service.searchForVessel(filter);

        assertThat(list.size()).isEqualTo(1);

        assertThat(list)
                .extracting("vesselName")
                .containsOnly("GREEN YACHT")
        ;
    }

    @Test
    @DisplayName("Should Filter on IMO number")
    public void filter_on_imo_number() {
        String statCode = "7669.0";

        List<Vessel> vessels = new ArrayList<>();
        Vessel vessel1 = Vessel.builder().statCode(statCode).vesselName("BLUE YACHT").imo_number("3").build();
        Vessel vessel2 = Vessel.builder().statCode(statCode).vesselName("RED YACHT").imo_number("1").build();
        Vessel vessel3 = Vessel.builder().statCode(statCode).vesselName("GREEN YACHT").imo_number("2").build();
        vessels.addAll(Arrays.asList(vessel1, vessel2, vessel3));

        when(vesselRepository.vesselsFor(any())).thenReturn(vessels);

        VesselFilter filter = new VesselFilter();

        filter.setStatCode(statCode).setVessel("3");
        List<Vessel> list = service.searchForVessel(filter);

        assertThat(list.size()).isEqualTo(1);

        assertThat(list)
                .extracting("vesselName")
                .containsOnly("BLUE YACHT")
        ;
    }


}
