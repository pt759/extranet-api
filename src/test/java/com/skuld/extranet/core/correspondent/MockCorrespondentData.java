package com.skuld.extranet.core.correspondent;

import com.skuld.extranet.core.correspondent.domain.Correspondent;

import java.util.ArrayList;
import java.util.List;

public class MockCorrespondentData {


    private static List<Correspondent> correspondent;
    private List<Correspondent> correspondentPort;

    public List<Correspondent> getCorrespondentFromCountry() {
        List<Correspondent> list = new ArrayList<>();

        list.add(createCorrespondent("1", "1001", "Jon Snow", "London", "LON", null, null, "Y", "N", "Malaysia"));
        list.add(createCorrespondent("2", "1002", "Stumpie", "Dubai", "DUB", null, null, "Y", "N", "Malaysia"));
        list.add(createCorrespondent("3", "1003", "Darth Vader", "Kuala Lumpur", "KUL", null, null, "Y", "N", "Malaysia"));
        list.add(createCorrespondent("4", "1004", "Luke Skywalker", "Kuala Lumpur", "KUL", null, null, "Y", "N", "Malaysia"));
        list.add(createCorrespondent("5", "1005", "Han Solo", "Melacca", "MEL", null, null, "Y", "N", "Malaysia"));
        list.add(createCorrespondent("6", "1006", "Lando", "Melacca", "MEL", null, null, "N", "Y", "Malaysia"));
        return list;
    }

    public List<Correspondent> getCorrespondentsFromSamePort() {
        List<Correspondent> list = new ArrayList<>();
        list.add(createCorrespondent("8", "1008", "Jon Snow2", "Sydney", "SYDC", null, null, "Y", "N", "Canada"));
        list.add(createCorrespondent("7", "1007", "Jon Snow1", "Sydney", "SYDX", null, null, "Y", "N", "Australia"));
        return list;
    }

    public List<Correspondent> getCorrespondentsWithSameTownNameInDifferentStates() {
        List<Correspondent> list = new ArrayList<>();
        list.add(createCorrespondent("8", "1008", "Jon Snow2", "Sydney", "SYDC", null, null, "Y", "N", "USA").setSubArea("Missisippi"));
        list.add(createCorrespondent("7", "1007", "Jon Snow1", "Sydney", "SYDX", null, null, "Y", "N", "USA").setSubArea("Texas"));
        return list;
    }

    public List<Correspondent> getCorrespondent() {
        List<Correspondent> list = new ArrayList<>();
        list.add(createCorrespondent("1", "1001", "Jon Snow", "London", "LON", null, null, "Y", "N", "United Kingdom"));
        list.add(createCorrespondent("2", "1002", "Stumpie", "Dubai", "DUB", null, null, "Y", "N", "United Emirates"));
        list.add(createCorrespondent("3", "1003", "Darth Vader", "Kuala Lumpur", "KUL", null, null, "Y", "N", "Malaysia"));
        list.add(createCorrespondent("4", "1004", "Luke Skywalker", "Kuala Lumpur", "KUL", null, null, "Y", "N", "Malaysia"));
        return list;
    }

    private Correspondent createCorrespondent(String seq_no, String party_id, String name, String town, String townId, String townRefId, String townRef, String pi, String hull, String country) {
        Correspondent corr = new Correspondent();
        corr.setName(name);
        corr.setPartyId(party_id);
        corr.setSeqNo(seq_no);
        corr.setTown(town);
        corr.setTownId(townId);
        corr.setTownRefId(townRefId);
        corr.setTownRef(townRef);
        corr.setCountry(country);
        corr.setPi(pi);
        corr.setHull(hull);
        return corr;
    }

    public List<Correspondent> getCorrespondentPort() {
        List<Correspondent> list = getCorrespondentFromCountry();
        for (Correspondent correspondent1 : list) {
            correspondent1.setTown("MyTown");
        }
        return list;
    }
}
