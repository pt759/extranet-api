package com.skuld.extranet.core.correspondent;

import com.skuld.extranet.core.correspondent.domain.Correspondent;
import com.skuld.extranet.core.correspondent.repo.CorrespondentRepository;
import com.skuld.extranet.core.correspondent.service.CorrespondentService;
import com.skuld.extranet.core.correspondent.sortorder.GroupCorrespondent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class CorrespondentServiceTest {

    CorrespondentService service;
    CorrespondentRepository mockVesselRepo = mock(CorrespondentRepository.class);
    MockCorrespondentData mockData = new MockCorrespondentData();

    @BeforeEach
    public void setup() {

        when(mockVesselRepo.getCorrespondentListFromRepo()).thenReturn(mockData.getCorrespondent());
        when(mockVesselRepo.correspondentById(any())).thenReturn(mockData.getCorrespondent());
        when(mockVesselRepo.filterBy(any())).thenReturn(mockData.getCorrespondent());
        when(mockVesselRepo.filterPortBy(any())).thenReturn(mockData.getCorrespondentPort());


        service = new CorrespondentService();
        service.setRepo(mockVesselRepo);
    }

    @Test
    public void testSomething() {
        List<Correspondent> correspondents = service.correspondentList();
        assertThat(correspondents.size(), is(equalTo(4)));
    }


    @Test
    public void testFilterAndGroup() {
        when(mockVesselRepo.filterCountryBy(any())).thenReturn(mockData.getCorrespondentFromCountry());

        List<GroupCorrespondent> list = service.filterAndGroup("", "Malaysia");
        assertThat(list.size(), is(equalTo(4)));
        assertThat(list.get(0).getGroupName(), is(equalTo("Dubai, Malaysia")));
        assertThat(list.get(1).getCorrespondents().size(), is(equalTo(2)));
        assertThat(list.get(1).getCorrespondents().get(0).getTown(), is(equalTo("Kuala Lumpur")));
    }

    @Test
    public void testgroupForSameTown() {
        when(mockVesselRepo.filterCountryBy(any())).thenReturn(mockData.getCorrespondentsFromSamePort());

        List<GroupCorrespondent> list = service.filterAndGroup("", "MyTown");
        assertThat(list.size(), is(equalTo(2)));
        assertThat(list.get(0).getGroupName(), is(equalTo("Sydney, Australia")));
        assertThat(list.get(1).getGroupName(), is(equalTo("Sydney, Canada")));
    }

    @Test
    public void testgroupForSameTownInSameCountry() {
        when(mockVesselRepo.filterPortBy(any())).thenReturn(mockData.getCorrespondentsWithSameTownNameInDifferentStates());

        List<GroupCorrespondent> list = service.filterAndGroup("Sydney", "");
        assertThat(list.size(), is(equalTo(2)));
        assertThat(list.get(0).getGroupName(), is(equalTo("Sydney, Missisippi, USA")));
        assertThat(list.get(1).getGroupName(), is(equalTo("Sydney, Texas, USA")));
    }




}