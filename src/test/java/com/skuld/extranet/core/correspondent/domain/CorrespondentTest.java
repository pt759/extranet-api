package com.skuld.extranet.core.correspondent.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CorrespondentTest {


    @Test
    public void testUSAShoulShowState() {
        Correspondent corr = new Correspondent().setCountry("USA").setSubArea("Missisippi").setTown("Boringtown");
        assertThat(corr.getShowState()).isTrue();
    }

    @Test
    public void testNotUSAShoulNotShowState() {
        Correspondent corr = new Correspondent().setCountry("NORWAY").setSubArea("Missisippi").setTown("Boringtown");
        assertThat(corr.getShowState()).isFalse();
    }

}