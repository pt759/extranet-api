package com.skuld.extranet.core.correspondent;

import com.skuld.extranet.core.correspondent.contact.Contact;
import com.skuld.extranet.core.correspondent.domain.Correspondent;
import com.skuld.extranet.core.correspondent.domain.CorrespondentPort;
import com.skuld.extranet.core.correspondent.number.CorrespondentNumber;
import com.skuld.extranet.core.correspondent.repo.CorrespondentRepository;
import com.skuld.extranet.core.correspondent.service.CorrespondentService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class CorrespondentServiceIntegrationTest {


    @Autowired
    private CorrespondentService service;

    @Autowired
    private CorrespondentRepository repo;

    @Test
    @DisplayName("Find correspondent by PartyID for PI")
    public void findCorrespondentForPIById() {
        List<Correspondent> correspondent = service.correspondentByIdForPI("2000001972");
        assertThat(correspondent.size()).isEqualTo(1);
    }

    @Test
    @DisplayName("Find correspondent by PartyID for H%M")
    public void findCorrespondentForHullMachineryById() {
        List<Correspondent> correspondent = service.correspondentByIdForHM("2000001972");
        assertThat(correspondent.size()).isGreaterThanOrEqualTo(1);
    }

    @Disabled
    @Test
    @DisplayName("Find Skuld correspondent by ID")
    public void findCorrespondentByIdSkuld() {
        List<Correspondent> correspondent = service.correspondentByIdForPI("2000247424");
        assertThat(correspondent.get(0).getName()).contains("Appleby");
        assertThat(correspondent.size()).isEqualTo(1);
    }

    @Test
    @DisplayName("Find Skuld correspondent by ID")
    public void findCorrespondentByIdFor529() {

        String partyId = "529";
        List<Contact> allContacts = repo.getAllContacts();
        List<CorrespondentNumber> allNumbers = repo.getAllNumbers();
        List<Correspondent> correspondentsMaster = repo.correspondentByIdForPI(partyId);

        CorrespondentRepository mockRepo = mock(CorrespondentRepository.class);
        when(mockRepo.getAllContacts()).thenReturn(allContacts);
        when(mockRepo.getAllNumbers()).thenReturn(allNumbers);
        when(mockRepo.correspondentByIdForPI(partyId)).thenReturn(correspondentsMaster);
        service.setRepo(mockRepo);

        List<Correspondent> correspondents = service.correspondentById(partyId, "Y", "N");
        assertThat(correspondents.get(0).getName()).contains("Agence Maritime de Fare Ute");
        assertThat(correspondents.size()).isEqualTo(1);

        Correspondent correspondent = correspondents.get(0);
        assertThat(correspondent.getContacts().size()).isEqualTo(1);
        assertThat(correspondent.getCorrespondentContactNumbers().size()).isEqualTo(5);

    }

    @Test
    @DisplayName("Find Skuld correspondent by Port")
    public void findCorrespondentForPort() {
        List<Correspondent> correspondent = service.filterBy("Calais", null);
        assertThat(correspondent.size()).isEqualTo(2);
    }


    @Test
    @DisplayName("Find with Details should contain details")
    public void findCorrespondentForPortWithDetails() {
        List<Correspondent> list = service.filterByWithDetails("London", null);
        assertThat(list.size()).isEqualTo(3);
        Correspondent corr = list.get(0);
        assertThat(corr.getContacts().size()).isGreaterThan(0);

        Contact contact = corr.getContacts().get(0);
        assertThat(contact.getContactNumbers()).isNotNull();

    }

    @Test
    @DisplayName("Find Skuld correspondent by Port which has both hull and pi")
    public void findCorrespondentForPortSaoVicente() {
        List<Correspondent> correspondents = service.filterBy("Sao Vicente", null);
        assertThat(correspondents.size()).isEqualTo(1);
        Correspondent c = correspondents.get(0);
        assertThat(c.getHull()).isEqualTo("Y");
        assertThat(c.getPi()).isEqualTo("Y");
    }

    @Test
    @DisplayName("Find Skuld correspondent for OSLO (uppercase)")
    public void findCorrespondentForPortOslo() {
        List<Correspondent> correspondents = service.filterBy("OSLO", null);
        assertThat(correspondents.size()).isEqualTo(1);
        Correspondent c = correspondents.get(0);
        assertThat(c.getHull()).isEqualTo("Y");
        assertThat(c.getPi()).isEqualTo("Y");
    }

    @Test
    @DisplayName("Find the same count of correspondent for NORWAY (UPPERCASE) and norway lowercase ")
    public void findCorrespondentForNORWAY() {
        int resultSearchByUppercase;
        int resultSearchByLowercase;

        List<Correspondent> correspondents = service.filterBy(null, "NORWAY");
        assertThat(correspondents.size()).isGreaterThan(0);
        resultSearchByUppercase = correspondents.size();

        correspondents = service.filterBy(null, "norway");
        assertThat(correspondents.size()).isGreaterThan(0);
        resultSearchByLowercase = correspondents.size();
        assertThat(resultSearchByUppercase).isEqualTo(resultSearchByLowercase);
    }

    @Disabled("Fix this in UI")
    @Test
    @DisplayName("Should Sort P&I/H&M, P&I and then H&M")
    public void shouldSortBasedOnPIandHM() {
        List<Correspondent> correspondents = service.filterBy("ALEX", null);
        assertThat(correspondents.size()).isEqualTo(3);
        Correspondent c = correspondents.get(0);
        assertThat(c.getHull()).isEqualTo("Y");
        assertThat(c.getPi()).isEqualTo("Y");

        c = correspondents.get(1);
        assertThat(c.getHull()).isEqualTo("Y");
        assertThat(c.getPi()).isEqualTo("N");

        c = correspondents.get(2);
        assertThat(c.getHull()).isEqualTo("N");
        assertThat(c.getPi()).isEqualTo("Y");
    }

    @Disabled("Fix this in UI")
    @Test
    @DisplayName("Should Sort P&I and then H&M")
    public void shouldSortBasedOnPIandHMForLondon() {
        List<Correspondent> correspondents = service.filterBy("LON", null);
        assertThat(correspondents.size()).isEqualTo(3);
        Correspondent c = correspondents.get(0);
        assertThat(c.getHull()).isEqualTo(("Y"));
        assertThat(c.getPi()).isEqualTo("N");

        c = correspondents.get(1);
        assertThat(c.getHull()).isEqualTo("N");
        assertThat(c.getPi()).isEqualTo("Y");

    }

    @Disabled
    @Test
    @DisplayName("Should not display Skuld/Copenhagen first when search Denmark.")
    public void shouldNotReturnCopenhagenFirstWhenSelectDenmark() {
        List<Correspondent> correspondents = service.filterBy("", "Denmark");
        assertThat(correspondents.size()).isGreaterThan(1);

        Correspondent c = correspondents.get(0);
        assertThat(c.getPartyId()).doesNotContain("2000083596");
    }


    @Disabled("Fix this in UI")
    @Test
    @DisplayName("Should display Skuld/Copenhagen first when search Copenhagen.")
    public void shouldReturnCopenhagenFirstWhenSelectDenmark() {
        List<Correspondent> correspondents = service.filterBy("CPH", "");
        assertThat(correspondents.size()).isGreaterThan(1);

        Correspondent c = correspondents.get(0);
        assertThat(c.getPartyId()).isEqualTo("2000083596");
        assertThat(c.getName()).contains("Assuranceforeningen Skuld (Gjensidig) filial København");
    }

    @Disabled("Fix this in UI")
    @Test
    @DisplayName("Should display Skuld/New York when search New York.")
    public void shouldReturnNewYorkFirstWhenSelectUSA() {
        List<Correspondent> correspondents = service.filterBy("NYKT", "");
        assertThat(correspondents.size()).isGreaterThan(1);

        Correspondent c = correspondents.get(0);
        assertThat(c.getPartyId()).isEqualTo("759");
        assertThat(c.getName()).contains("Skuld North America Inc.");
    }


    @Test
    public void slowSearchForUSA() {
        List<Correspondent> correspondents = service.filterBy("", "USA");
        assertThat(correspondents.size()).isGreaterThan(50);
    }

    @Test
    @DisplayName("Port should contain country")
    public void testPortHaveCountry() {
        List<CorrespondentPort> ports = service.ports();
        CorrespondentPort london = ports.stream().filter(item -> item.getPort().equalsIgnoreCase("Oslo")).findFirst().orElse(null);
        assertThat(london.getCountry()).isEqualTo("Norway");
    }

}