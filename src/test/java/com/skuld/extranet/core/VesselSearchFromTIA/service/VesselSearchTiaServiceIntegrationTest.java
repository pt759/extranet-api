package com.skuld.extranet.core.VesselSearchFromTIA.service;

import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.VesselSearchFromTIA.dto.VesselDTO;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.tokenWithDefaultRoles;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class VesselSearchTiaServiceIntegrationTest {

    @Autowired
    VesselSearchTiaService vesselSearchTiaService;

    @Test
    public void should_get_empty_array_when_vessel_not_found() {

        List<VesselDTO> firstList = vesselSearchTiaService.filterVesselBy("ffonteee");
        List<VesselDTO> secondList = vesselSearchTiaService.filterVesselBy("222222222");
        List<VesselDTO> thirdList = vesselSearchTiaService.filterVesselBy("null");

        assertThat(firstList.size() == 0);
        assertThat(secondList.isEmpty());
        assertThat(thirdList.isEmpty());

    }

    @Test
    @WithMockCustomUser(token = tokenWithDefaultRoles)
    public void should_get_result_with_vessel_data_when_param_match_value_in_tia_2() {
        assertThat(vesselSearchTiaService.filterVesselBy("yada").size()).isGreaterThan(0);
    }

    @Disabled
    @Test
    public void should_get_result_with_vessel_data_when_param_match_value_in_tia() {
        assertThat(vesselSearchTiaService.filterVesselBy("9219484")).hasSize(1);
    }

    @Disabled
    @Test
    public void should_search_for_tiger_7() {
        assertThat(vesselSearchTiaService.filterVesselBy("tiger 7")).hasSize(1);
    }

    @Test
    public void should_get_result() {
        assertThat(vesselSearchTiaService.getVesselOnCoverFromTIA("tiger")).isNotNull();
    }
}
