package com.skuld.extranet.core.VesselSearchFromTIA.service;

import com.skuld.extranet.core.VesselSearchFromTIA.client.TiaVesselClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

class VesselSearchTiaServiceTest {

    VesselSearchTiaService vesselSearchTiaService;

    TiaVesselClient tiaVesselClient = mock(TiaVesselClient.class);

    @BeforeEach
    void setUp() {
        vesselSearchTiaService = new VesselSearchTiaService(tiaVesselClient);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "% ", "*", "%TIGER"})
    public void should_not_call_client(String query) {
        assertThat(vesselSearchTiaService.filterVesselBy(query)).hasSize(0);
        verify(tiaVesselClient, never()).getVesselOnCover(any());
    }

    @Test
    public void should_not_call_client_when_null() {
        assertThat(vesselSearchTiaService.filterVesselBy(null)).hasSize(0);
        verify(tiaVesselClient, never()).getVesselOnCover(any());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Tiger", "Tiger 7", " Tiger"})
    public void should_call_client_when_valid_query_but_still_zero_result(String query) {
        assertThat(vesselSearchTiaService.filterVesselBy(query)).hasSize(0);
        verify(tiaVesselClient).getVesselOnCover(any());
    }

}
