package com.skuld.extranet.core.VesselSearchFromTIA.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class VesselCertificateDTOTest {


    @Test
    public void should_split_on_line_break() {
        List<VesselCertificateDTO> certificates = new ArrayList<>();
        certificates.add(VesselCertificateDTO.builder().certificateType("MLC").certificate("MLC Certificate - Regulation 2.5.2 Standard A2.5.2, 20 Feb 2021 to 20 Feb 2022\nMLC Certificate - Regulation 4.2 Standard A4.2.1 Paragraph 1 (b), 20 Feb 2021 to 20 Feb 2022").build());
        certificates.add(VesselCertificateDTO.builder().certificateType("BC").certificate("20 Feb 2021 to 20 Feb 2022").build());
        certificates.add(VesselCertificateDTO.builder().certificateType("BC").certificate("20 Feb 2021 to 20 Feb 2022").build());

        List<VesselCertificateDTO> vesselCertificateDTOS = VesselCertificateDTO.fixForLineBreaks(certificates);

        assertThat(vesselCertificateDTOS).hasSize(4);
        assertThat(vesselCertificateDTOS.get(0).getCertificate()).isEqualTo("MLC Certificate - Regulation 2.5.2 Standard A2.5.2, 20 Feb 2021 to 20 Feb 2022");
        assertThat(vesselCertificateDTOS.get(0).getCertificateType()).isEqualTo("MLC");

        assertThat(vesselCertificateDTOS.get(1).getCertificate()).isEqualTo("MLC Certificate - Regulation 4.2 Standard A4.2.1 Paragraph 1 (b), 20 Feb 2021 to 20 Feb 2022");
        assertThat(vesselCertificateDTOS.get(1).getCertificateType()).isEqualTo("MLC");
    }


}
