package com.skuld.extranet.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class VesselCertificatesControllerTestFromTiaDTO {
    final static String baseUrl = "/poc-rest-service/skapi/";

    @BeforeAll
    @DisplayName("Before Class")
    public static void init() {

    }

    @BeforeEach
    @DisplayName("Before Method")
    public void setUp() throws Exception {
    }

    @Test
    @DisplayName("VesselSearch One Test")
    public void testVesselOne() {
        assertTrue(true);

    }

    @Test
    @DisplayName("Dummy Test")
    public void dummy() throws Exception {
        assertTrue(true);
    }
}
