package com.skuld.extranet.security.service;

import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.user.domain.AzureAdGroup;
import com.skuld.extranet.core.user.dto.StatCodeDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.skuld.common.global.GlobalTestValues.cpbHodingsAzureGroupId;
import static com.skuld.common.global.GlobalTestValues.nonSkuldUserToken;
import static com.skuld.extranet.global.RESTendPoints.claims;
import static com.skuld.extranet.security.service.JwtSecConstant.SecurityConstants.HEADER_EVENT_NO;
import static com.skuld.extranet.security.service.JwtSecConstant.SecurityConstants.HEADER_STATCODE;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserDataAccessTest {

    @Autowired
    UserAccessService userAccessService;

    @MockBean
    UserGroupAccessService mockUserGroupAccessService;

    List<AzureAdGroup> groups;

    String authToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjJyRUpkX1pJXzRway12c3RIZDZ1bmhhSUhNWG5aZlliVU5BN0ktUGdzQTQifQ.eyJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vMzI2NTM3NTgtNWEzYy00YWI5LWE0NzYtOTViMjk5YzhhMTU3L3YyLjAvIiwiZXhwIjoxNTc1MDMxNjgyLCJuYmYiOjE1NzUwMjgwODIsImF1ZCI6IjExZjllOGQwLTFlODUtNDdkNS1iMDQxLTgyN2Y5NTk4NDgyZSIsIm9pZCI6ImJiMzBiYjUyLWU4MDYtNDdlMy05NGNlLTkyZjJmYzg2ZDFmNiIsInN1YiI6ImJiMzBiYjUyLWU4MDYtNDdlMy05NGNlLTkyZjJmYzg2ZDFmNiIsImdyb3VwcyI6IlwiYWZyaWNhZXgsY3BiaG9kaW4sYWNvcnNzbGVmLHpvcmFiaW5zLFVzZXJBZG1pbixZYWNodENyZXdMaWFiaWxpdHlcIiIsImVtYWlsIjoiYXBwX2RldkBza3VsZC5jb20iLCJub25jZSI6IjYzNzEwNjI0NzY2NjU1NzM0Ni5aR1k0Tmpka1lqTXRPVE5qTmkwME5qVTFMV0pqTmpJdE1URTBNek5qWmpJeVl6SXlNekUxTnpFMlptRXROelUzT1MwME56VTFMV0psWVdJdE9XTXpabVExWXprMllqY3giLCJzY3AiOiJkZW1vLnJlYWQiLCJhenAiOiI3ZWQ5NjJkYS05NTZkLTQwNjktYTE0Yy1hNDczMmVjMmJhYTciLCJ2ZXIiOiIxLjAiLCJpYXQiOjE1NzUwMjgwODJ9.Hv3_FqJUTsA83aoXskyPxPbc016ZIfmxIOOhWWXh5PPEJdUy3GHwx8Fz3wIvxT2UikNlIEkDlgOelEBZSANimdPE_gR42mKns_pK1txFC9flC8KOF6JgJQofJ72huDNxXiQlWqiZ5A5jOJ9JLWHTl0VuO483VLJyX-0uXp9LHGrszgpRP3053xNRXasu4B9-K2j6aNjBkM-9VH-0xxke-jJicqRRQ_uYiex0oVpxdGmqabeY43lYKXMd9v5ssQ_S_V21OiEXM_axbNFqRHho95VKimKQPCxnXBvSKSdd566ms1yiIJbmXMmoIBEyXvhEKSAeA_R4cuiwJKu36AoQYA";

    @BeforeEach
    private void setup() {

        AzureAdGroup group1 = AzureAdGroup.builder().name("TEAM1").statCodes(Arrays.asList(StatCodeDTO.builder().statCode("100.0").build())).build();
        AzureAdGroup group2 = AzureAdGroup.builder().name("TEAM2").statCodes(Arrays.asList(StatCodeDTO.builder().statCode("200.0").build())).build();
        AzureAdGroup group3 = AzureAdGroup.builder().name("TEAM3").statCodes(Arrays.asList(StatCodeDTO.builder().statCode("300.0").build())).build();
        AzureAdGroup group4 = AzureAdGroup.builder().name("oslosyn2").statCodes(Arrays.asList(StatCodeDTO.builder().statCode("17270.04").build())).build();
        AzureAdGroup group5 = AzureAdGroup.builder().name("cpbhodin").groupId(cpbHodingsAzureGroupId).build();

        groups = new ArrayList<>(Arrays.asList(group1, group2, group3, group4, group5));

        when(mockUserGroupAccessService.getUsersGroupsFromToken(any())).thenReturn(groups);
    }

    @Test
    @DisplayName("Match the stat code user wants to access to one in the Token (No Mock)")
    public void testAccessToStatCode() {
        String statCodeWithAccess = "100.0";
        String statCodeWithoutToAccess = "26502.00";

        boolean result = userAccessService.accessToStatCode(authToken, statCodeWithAccess);
        assertThat(result).isTrue();

        result = userAccessService.accessToStatCode(authToken, statCodeWithoutToAccess);
        assertThat(result).isFalse();

    }

    @Test
    public void testIt() {
        boolean match = userAccessService.accessToStatCode("TOKEN", "100.0");
        assertThat(match).isTrue();
    }

    @Test
    public void testItNotAccess() {
        boolean match = userAccessService.accessToStatCode("TOKEN", "900.0");
        assertThat(match).isFalse();
    }

    @DisplayName("Should NOT find any Groups that has StatCodeGroupLogin")
    @Test
    public void testGroup() {
        boolean result = userAccessService.accessToGroup(authToken, "TEAM_100");
        assertThat(result).isFalse();
    }

    @DisplayName("Should find the Group that has StatCodeGroupLogin no mock")
    @Test
    public void findGroupWithStatCodeGroupLoginTest2() {
        boolean result = userAccessService.accessToGroup(authToken, "cpbhodin");
        assertThat(result).isTrue();
    }

    @DisplayName("Test genearl access to data")
    @Test
    @WithMockCustomUser(token = nonSkuldUserToken)
    public void testDataAccess() {
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put(HEADER_STATCODE, "100.0");

        HttpServletRequest httpRequest = mock(HttpServletRequest.class);
        when(httpRequest.getAttribute(any())).thenReturn(pathVariables);
        when(httpRequest.getServletPath()).thenReturn(claims);
        when(httpRequest.getHeader(any())).thenReturn(authToken);

        boolean flag = userAccessService.hasAccess(httpRequest);
        assertThat(flag).isTrue();
    }

    @DisplayName("Test token based access stat code")
    @Test
    @WithMockCustomUser(token = nonSkuldUserToken)
    public void testTokenBasedDataAccessToStatCode() {
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put(HEADER_STATCODE, "17270.04");

        HttpServletRequest httpRequest = mock(HttpServletRequest.class);
        when(httpRequest.getAttribute(any())).thenReturn(pathVariables);
        when(httpRequest.getHeader(any())).thenReturn(authToken);
        when(httpRequest.getServletPath()).thenReturn(claims);

        boolean flag = userAccessService.hasAccess(httpRequest);
        assertThat(flag).isTrue();
    }

    @DisplayName("Test @skuld.com users access conflict event")
    @Test
    @WithMockCustomUser(token = nonSkuldUserToken)
    public void testTokenDataAccessToEvent() {
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put(HEADER_EVENT_NO, "40143716");
        pathVariables.put(HEADER_STATCODE, "17270.04");

        HttpServletRequest httpRequest = mock(HttpServletRequest.class);
        when(httpRequest.getAttribute(any())).thenReturn(pathVariables);
        when(httpRequest.getHeader(any())).thenReturn(authToken);
        when(httpRequest.getServletPath()).thenReturn(claims);

        boolean access = userAccessService.hasAccess(httpRequest);
        assertThat(access).isFalse();
    }
}