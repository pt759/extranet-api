package com.skuld.extranet.security.service;

import com.skuld.common.security.authentication.WithMockCustomUser;
import com.skuld.extranet.core.accountOverview.AccountOverviewService;
import com.skuld.extranet.core.accountOverview.domain.AccountOverview;
import com.skuld.extranet.spa.SearchService;
import com.skuld.extranet.spa.pages.ClientOverview.ClientOverviewPage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.skuld.common.global.GlobalTestValues.*;
import static com.skuld.extranet.GlobalForTest.statCodeGroupFastnetm;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ContentFilterServiceTest {

    private final AccountOverviewService accountOverviewService;
    private final SearchService searchService;
    private final ContentFilterService contentFilterService;

    @Autowired
    public ContentFilterServiceTest(AccountOverviewService accountOverviewService,
                                    ContentFilterService contentFilterService,
                                    SearchService searchService) {
        this.accountOverviewService = accountOverviewService;
        this.contentFilterService = contentFilterService;
        this.searchService = searchService;
    }

    @Test
    @WithMockCustomUser(token = tokenWithCrewingManager)
    void filter_entered_vessels_on_client_overview_for_crewing_manager() {
        ClientOverviewPage page = searchService.clientOverviewPage(africaExpressStatCode);
        ClientOverviewPage filteredClientOverviewPage = contentFilterService.filterClientOverviewPage(page);

        assertNull(filteredClientOverviewPage.getVessels());
        assertNull(filteredClientOverviewPage.getAccountManager());
        assertNull(filteredClientOverviewPage.getVessels());
        assertNull(filteredClientOverviewPage.getVessels());
    }

    @Test
    @WithMockCustomUser(token = employeeToken)
    void role_other_than_crewing_manager_has_access_to_data() {
        ClientOverviewPage page = searchService.clientOverviewPage(africaExpressStatCode);
        ClientOverviewPage filteredClientOverviewPage = contentFilterService.filterClientOverviewPage(page);

        assertNotNull(filteredClientOverviewPage.getVessels());
        assertNotNull(filteredClientOverviewPage.getAccountManager());
        assertNotNull(filteredClientOverviewPage.getVessels());
        assertNotNull(filteredClientOverviewPage.getVessels());
    }

    @Test
    @WithMockCustomUser(token = tokenWithCrewingManager)
    void crewing_manager_does_not_see_premium_on_overview() {
        List<AccountOverview> membersForLogin = accountOverviewService.getAllClientsForBroker(statCodeGroupFastnetm);
        List<AccountOverview> accountOverviews = contentFilterService.filterAccountOverview(membersForLogin);

        assertFalse(accountOverviews.stream().anyMatch(accountOverview -> accountOverview.getClientName() == null));
        assertFalse(accountOverviews.stream().anyMatch(accountOverview -> accountOverview.getProductsAsString() == null));
        assertTrue(accountOverviews.stream().anyMatch(accountOverview -> accountOverview.getCurrency() == null));
        assertTrue(accountOverviews.stream().anyMatch(accountOverview -> accountOverview.getGrossPremium() == null));
    }


    @Test
    @WithMockCustomUser(token = employeeToken)
    void role_other_than_crewing_manager_has_access_to_client_overview() {
        List<AccountOverview> membersForLogin = accountOverviewService.getAllClientsForBroker(statCodeGroupFastnetm);
        List<AccountOverview> accountOverviews = contentFilterService.filterAccountOverview(membersForLogin);

        assertFalse(accountOverviews.stream().allMatch(accountOverview -> accountOverview.getClientName() == null));
        assertFalse(accountOverviews.stream().allMatch(accountOverview -> accountOverview.getProductsAsString() == null));
        assertTrue(accountOverviews.stream().anyMatch(accountOverview -> accountOverview.getCurrency() != null));
        assertTrue(accountOverviews.stream().anyMatch(accountOverview -> accountOverview.getGrossPremium() != null));
    }

}
