package com.skuld.extranet.security.service;

import com.skuld.common.security.authentication.WithMockCustomUser;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static com.skuld.common.global.GlobalTestValues.employeeToken;
import static com.skuld.common.global.GlobalTestValues.tokenWithCrewingManager;
import static com.skuld.extranet.global.RESTendPoints.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Log4j2
@SpringBootTest
public class UserAccessServiceIntegrationTest {

    @Autowired
    UserAccessService userAccessService;

    @Test
    @WithMockCustomUser(token = employeeToken)
    void test_page_access_performance() {
        StopWatch stopWatch = StopWatch.createStarted();
        userAccessService.pageAccess(financialPage);
        stopWatch.stop();
        log.debug("Page access for employee: " + stopWatch.toString());
        assertTrue(stopWatch.getTime() < 2000);

        stopWatch.reset();
        stopWatch.start();
        userAccessService.pageAccess(financialPage);
        stopWatch.stop();
        log.debug("Page access for single group user: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 3000); //TODO slow
    }

    @Test
    void test_data_access_performance() {
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("statCode", "37951.00");
        pathVariables.put("accountNo", "586969");

        StopWatch stopWatch = StopWatch.createStarted();
        userAccessService.dataAccess(pathVariables, "employeeToken");  //statcode = 4390 //account
        stopWatch.stop();
        log.debug("Employee dataAccess time: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 30000);
    }

    @Test
    @DisplayName("Check access to account")
    @WithMockCustomUser(token = employeeToken)
    public void testCheckAccessToAccount() {
        String account = "535775";
        userAccessService.accessToAccount(account);
        StopWatch stopWatch = StopWatch.createStarted();
        userAccessService.accessToAccount(account);
        stopWatch.stop();
        log.debug("Employee dataAccess time: " + stopWatch.getTime());
        assertTrue(stopWatch.getTime() < 20000); //TODO slow before cached
    }

    @Test
    @WithMockCustomUser(token = tokenWithCrewingManager)
    void crewing_manager_role_has_not_access_to_vessel_or_statistics() {
        assertFalse(userAccessService.pageAccess(vesselPage));
        assertFalse(userAccessService.pageAccess(statisticsPage));
    }
}
