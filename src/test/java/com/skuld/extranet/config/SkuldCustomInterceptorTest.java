package com.skuld.extranet.config;

import com.skuld.common.security.service.JwtService;
import com.skuld.extranet.global.GlobalText;
import com.skuld.extranet.security.service.UserAccessService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.skuld.extranet.global.RESTendPoints.*;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;


class SkuldCustomInterceptorTest {

    private SkuldCustomInterceptor interceptor;
    private final UserAccessService userAccessService = mock(UserAccessService.class);
    private final JwtService jwtService = mock(JwtService.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final HttpServletResponse response = mock(HttpServletResponse.class);

    @BeforeEach
    private void setup() {
        interceptor = new SkuldCustomInterceptor(userAccessService, jwtService);
        userAccessService.setWhiteListedIp("127.0.0.1");
    }

    @Disabled
    @Test
    public void testClosedConnection() {
        assertThatPathIsClosed("somepath", "testHost");
        assertThatPathIsClosed("vessels/list", "testHost");
    }

    @DisplayName("Specifically states these needs to be open.")
    @Test
    public void testOpenConnections() {
        assertThatPathIsOpen(correspondentSearch);
        assertThatPathIsOpen(vesselSearch);
        assertThatPathIsOpen(users);
        assertThatPathIsOpen(tiaUsers);
        assertThatPathIsOpen(tiaCreateUSer);
    }

    @DisplayName("If JWT is not valid should give 401 status code.")
    @Test
    public void testExpiredTokenGives401() throws IOException {

        when(request.getServletPath()).thenReturn("anything");
        when(request.getHeader(anyString())).thenReturn("anything");
        when(jwtService.verifyTokenFromRequest(any())).thenReturn(false);

        boolean hasAccess = interceptor.hasTokenAccess(request, response);
        assertThat(hasAccess).isFalse();
        verify(response, times(1)).sendError(401, GlobalText.NO_ENDPOINT_ACCESS);
    }

    @DisplayName("If JWT is valid but NO data acecss should give 403 status code.")
    @Test
    public void testValidTokenGivesWithNoDataAccess403() throws IOException {

        when(request.getServletPath()).thenReturn("anything");
        when(request.getHeader(anyString())).thenReturn("anything");

        when(jwtService.verifyTokenFromRequest(any())).thenReturn(true);
        when(userAccessService.hasAccess(any())).thenReturn(false);

        boolean hasAccess = interceptor.hasTokenAccess(request, response);
        assertThat(hasAccess).isFalse();
        verify(response, times(1)).sendError(403, GlobalText.NO_DATA_ACCESS);
    }


    @DisplayName("If both JWT is valid and has Data Access, should let through.")
    @Test
    public void testValidTokenAndAccessGivesTrue() throws IOException {

        when(request.getServletPath()).thenReturn("anything");
        when(request.getHeader(anyString())).thenReturn("anything");

        when(jwtService.verifyTokenFromRequest(any())).thenReturn(true);
        when(userAccessService.hasAccess(any())).thenReturn(true);

        boolean hasAccess = interceptor.hasTokenAccess(request, response);

        verify(response, times(0)).sendError(401, GlobalText.NO_ENDPOINT_ACCESS);
        verify(response, times(0)).sendError(403, GlobalText.NO_DATA_ACCESS);

        assertThat(hasAccess).isTrue();
    }

    private void assertThatPathIsOpen(String path) {
        HttpServletRequest mock = mock(HttpServletRequest.class);
        when(mock.getServletPath()).thenReturn(path);
        when(mock.getRemoteAddr()).thenReturn("127.0.0.1");
        when(userAccessService.isEndpointOpen(any())).thenReturn(true);
        assertThat(interceptor.jwtByPass(mock)).isTrue();
    }

    private void assertThatPathIsClosed(String path, String header) {
        HttpServletRequest mock = mock(HttpServletRequest.class);
        when(mock.getServletPath()).thenReturn(path);
        when(mock.getHeader(anyString())).thenReturn(header);
        assertThat(interceptor.jwtByPass(mock)).isFalse();
    }
}