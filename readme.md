# Skuld Rest API for  TIA
Based on Spring Boot REST API

### Documentation of API 
http://localhost:8080/swagger-ui.html#/

### JIRA 
[JIRA](https://jira.skuld.com/projects/RESTAPI)

# Java JDK
Set JAVA_HOME to your JDK11

# Docker
See *Dockerfile* for details.    
  
##Build and run  
`mvn clean package spring-boot:repackage -DskipTests`  
`docker build -f Dockerfile -t extranet-api .`    
`docker run --rm -e "spring_profiles_active=dev" -e TZ=CET -p  8100:8100  --dns=188.95.243.210 --name extranet-api extranet-api`

#Manual tag and push to Open Shift
`docker images --filter=reference=extranet-api --format "{{.ID}}`
`docker tag [image id] docker.apps.openshift.intility.com/images/extranet-api:test`

##Log in to OpenShifts registry  and push image (Windows terminal works well)
`oc login`  
`docker login -u %username% -p $(oc whoami -t) docker.apps.openshift.intility.com`  
`docker push docker.apps.openshift.intility.com/images/extranet-api:test`
 
    


To remove image:  
`docker rm skuld-extranet-rest-api`
 

### TIPS & TODOs 
Check out [devtools](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-devtools.html)
Check out [markdown](https://bitbucket.org/tutorials/markdowndemo)

####Running with accecss contrl locally
Create a pring profile dev_access_control and add/rename application-dev.properties to 
application-dev_access_control.properties
 
###Swagger
https://skuld-dev-extranet-api.int.skuld.com/swagger-ui/index.html?url=/v3/api-docs&validatorUrl=#/  
