FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.12_7_openj9-0.27.0 as builder
WORKDIR application
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.12_7_openj9-0.27.0
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./

EXPOSE 8100
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
ENV TZ=CET
